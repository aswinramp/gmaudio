package com.gm.audiocluster

import com.gm.audiocluster.interfaces.IClusterManager
import com.gm.audiocluster.mock.SimulationClusterService
import com.gm.audiocluster.sdk.SdkClusterService

object ClusterController {

    private lateinit var manager: IClusterManager

    /**
     * Get [IManager] w.r.t to the working platform
     * @return which type of Manager functionality has to perform like Simulation or SDK  or android
     */
    private fun getSourceManager(): IClusterManager {
        manager = when {
            isSDKAvailable() -> {
                SdkClusterService()
            }
            else ->
            {
                SimulationClusterService()
            }
        }
        return manager
    }

    /**
     * Check if this application is running on GM's OS build
     */
    fun isSDKAvailable(): Boolean =
            android.os.Build.MANUFACTURER == BuildConfig.VENDOR

}