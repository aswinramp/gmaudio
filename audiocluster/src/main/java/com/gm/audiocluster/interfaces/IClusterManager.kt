package com.gm.audiocluster.interfaces

import android.app.Service

interface IClusterManager {

    fun getService(): Service?
}