package com.gm.audiocluster.mock

import android.app.Service
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.gm.audiocluster.interfaces.IClusterManager
import gm.cluster.IClusterHmi

class SimulationClusterService(context:Context) : IClusterManager {

    private var service: IClusterHmi? = null

    private var mContext:Context = context;

    init {
        initConnection()
    }

    override fun getService(): Service? {
       return service
    }

    private fun initConnection() {
        if (service == null) {
            val intent = Intent(IClusterHmi::class.java.name)

            /*this is service name*/
            intent.action = "service.cluster"

            /*From 5.0 annonymous intent calls are suspended so replacing with server app's package name*/
            intent.setPackage("com.gm.cluster")

            // binding to remote service
            mContext.bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        }
    }

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d("SimulationCluster", "Service Connected")
            service = IClusterHmi.Stub.asInterface(iBinder)
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d("SimulationCluster", "Service Disconnected")
            service = null
        }
    }
}