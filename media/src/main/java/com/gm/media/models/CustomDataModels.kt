package com.gm.media.models

import android.databinding.ObservableArrayList
import android.support.v7.widget.RecyclerView
import com.gm.media.apiintegration.apiinterfaces.MediaCustomInterface
import com.gm.media.database.GMDatabase
import gm.calibrations.LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM
import gm.entertainment.ActiveBrowseItem
import java.util.*

/* Custom Data Models */


data class EventObject
(
        var eventName: String,
        var obj: Any?
)

/**
 * Non UI Variables
 */
class NONUIVariables {

    /** To set the Previous Source Type */
    var previousSourceType: NOWPLAYING_SOURCE_TYPE = NOWPLAYING_SOURCE_TYPE.AM
    /** To set the list of the favoriteList */
    var favoriteList = ArrayList<String>()
    /** Used to Disabled audio Control */
    var audioControlDisabled: Boolean = false
        set(value) {
            DataPoolDataHandler.audioControlDisabled.set(value)
            field = value
        }
    /** To set the BROWSE_SOURCE_TYPE */
    var BROWSE_SOURCE_TYPE: String = ""
        set(value) {
            DataPoolDataHandler.BROWSE_SOURCE_TYPE.set(value)
            field = value
        }
    /**
     * To set the FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME
     */
    var FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME: String = ""
    /**
     * To set the FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT
     */
    var FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT: String = ""

    var aMFMStationInfo_t: AMFMStationInfo_t? = null
        set(value) {
            DataPoolDataHandler.aMFMStationInfo_t.set(value)
            field = value
        }
    var updateProgress: Int = 0
        set(value) {
            DataPoolDataHandler.updateProgress.set(value)
            field = value
        }
    var showUpdateProgress: Boolean = false
        set(value) {
            DataPoolDataHandler.showUpdateProgress.set(value)
            field = value
        }

    var AUDIOMANAGER_CHANGE_AUDIOSOURCE = NOWPLAYING_SOURCE_TYPE.AM
        set(value) {
            DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.set(value)
            field = value
        }
    var FMTUNER_TPSTATUS: Int = 0
        set(value) {
            DataPoolDataHandler.FMTUNER_TPSTATUS.set(value)
            field = value
        }
    var audioControlDisabledforFM: Boolean = false
        set(value) {
            DataPoolDataHandler.audioControlDisabledforFM.set(value)
            field = value
        }
    var gmDatabase: GMDatabase? = null
    var playingUsbIndex: Int = 0
    var mState = MEDIA_PLAYER_STATE.STOP.name
    var playTimeDuration = 0
    var isFastBackEnabled = false
    var isStartTouch = false
    var DRIVINGMODE: Int? = -1

    var HIDEMEDIAMETADATA: Boolean = false
        set(value) {
            DataPoolDataHandler.HIDEMEDIAMETADATA.set(value)
            field = value
        }
    var ISVEHICLEPARKED: Boolean = true
    var ISVIDEOMODE: Boolean = false

    /**
     * To set Dialog box main text
     */
    var dialogMainText: String = ""
    set(value) {
        DataPoolDataHandler.CUSTOM_ALERT_DIALOG_MAIN_TEXT.set(value)
        field=value
    }
    /**
     * To set Dialog button text
     */
    var dialogButtonOneText: String = ""
        set(value) {
            DataPoolDataHandler.CUSTOM_ALERT_DIALOG_BUTTON_TEXT_YES.set(value)
            field=value
        }
    /**
     * To set Dialog button text
     */
    var dialogButtonTwoText: String = ""
        set(value) {
            DataPoolDataHandler.CUSTOM_ALERT_DIALOG_BUTTON_TEXT_NO.set(value)
            field=value
        }
    /**
     * To set event name, which type of dialog text is to be set
     */
    var dialogDisplayEvent: String = ""
    var mActiveBrowseAddresses = Stack<ActiveBrowseItem>()
    var SHOW_LETTER_KEYPAD: Boolean = false
        set(value) {
            DataPoolDataHandler.SHOW_LETTER_KEYPAD.set(value)
            field = value
        }
    var IS_MEDIA_BROWSE_LISTVIEW_VISIBLE: Boolean = false
        set(value) {
            DataPoolDataHandler.IS_MEDIA_BROWSE_LISTVIEW_VISIBLE.set(value)
            field = value
        }
    var ISFROMUSBNOWPLAYING: Boolean = false
    var MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME: Int = 0
        set(value) {
            DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME.set(value)
            field = value
        }
    var ACTIVE_MEDIA_ADDRESS: ActiveBrowseItem? = null
    var MEDIA_NOWPLAYING_MEDIAOBJ_REMAININGTIME: Int = 0
        set(value) {
            DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_REMAININGTIME.set(value)
            field = value
        }
    var mMediaCallBack: MediaCustomInterface? = null

    var MEDIA_PLAYING_SONG_LIST = ArrayList<MediaObject_t>()
        set(value) {
            if (value.size == 0)
                DataPoolDataHandler.MEDIA_PLAYING_SONG_LIST.clear()
            else
                DataPoolDataHandler.MEDIA_PLAYING_SONG_LIST.addAll(value)
            field = value
        }
    var USB_BROWSE_PAGENAME = USB_BROWSE_PAGE.CATEGORY
        set(value) {
            DataPoolDataHandler.USB_BROWSE_PAGENAME.set(value)
            DataPoolDataHandler.USB_BROWSE_PAGENAME.notifyChange()
            field = value
        }
    var MEDIA_USB_BROWSE_LIST = ArrayList<MediaObject_t>()
        set(value) {
            if (value.size == 0)
                DataPoolDataHandler.MEDIA_USB_BROWSE_LIST.clear()
            else
                DataPoolDataHandler.MEDIA_USB_BROWSE_LIST.addAll(value)
            field = value
        }
    var allSongs_st = "All Songs"
        set(value) {
            DataPoolDataHandler.allSongs_st = value
            field = value
        }
    var MEDIA_BROWSE_LEVEL = MEDIA_BROWSE_LEVELS.LEVEL_ONE
        set(value) {
            DataPoolDataHandler.MEDIA_BROWSE_LEVEL.set(value)
            field = value
        }
    var IS_MEDIA_BROWSE_SONGS: Boolean = false
        set(value) {
            DataPoolDataHandler.IS_MEDIA_BROWSE_SONGS.set(value)
            field = value
        }

    var MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING: Boolean = false
        set(value) {
            DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING.set(value)
            field = value
        }
    var USB_ORIGINAL_MEDIA_LIST = ArrayList<MediaObject_t>()
    var USB_MEDIA_OBJECT_INFO: MediaObject_t? = null
        set(value) {
            DataPoolDataHandler.USB_MEDIA_OBJECT_INFO.set(value)
            DataPoolDataHandler.USB_MEDIA_OBJECT_INFO.notifyChange()
            field = value
        }
    var MEDIA_PLAYBACKMODE = eMediaPBMode.MEDIA_PBM_NORMAL.ordinal
        set(value) {
            DataPoolDataHandler.MEDIA_PLAYBACKMODE.set(value)
            field = value
        }
    var IS_MEDIA_INDEXING_VISISBLE: Boolean = false
        set(value) {
            DataPoolDataHandler.IS_MEDIA_INDEXING_VISISBLE.set(value)
            field = value
        }

    /** To set the list of the AMFMStationInfo */
    var browseList = ArrayList<AMFMStationInfo_t>()
        set(value) {
            if (value.size == 0)
                DataPoolDataHandler.browseList.clear()
            else
                DataPoolDataHandler.browseList.addAll(value)

            field = value
        }
    var AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = ArrayList<AMFMStationInfo_t>()
        set(value) {
            if (value.size == 0)
                DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.clear()
            else
                DataPoolDataHandler.AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.addAll(value)
            field = value
        }

    var MEDIA_USB_INDEXING_PROGRESS: Int = 0
        set(value) {
            DataPoolDataHandler.MEDIA_USB_INDEXING_PROGRESS.set(value)
            DataPoolDataHandler.MEDIA_USB_INDEXING_PROGRESS.notifyChange()
            field = value
        }

    companion object {
        var INSTANCE: NONUIVariables? = null
        fun getInstance(): NONUIVariables {
            if (INSTANCE == null)
                INSTANCE = NONUIVariables()
            return INSTANCE!!
        }
    }
}


/**
 * This class has information about DeviceData.
 * @property type the type of DeviceData class.
 * @property name the name of DeviceData class.
 * @property id the id of DeviceData class.
 * @property isReadable the isReadable of DeviceData class.
 * @property isBrowsable the isBrowsable of DeviceData class.
 */
data class DeviceData(var type: String = "", var name: String = "", var id: String = "", var isReadable: String = "", var isBrowsable: String = "")

/**
 * This class has information about ManualTuneData.
 * @property frequency the frequency of ManualTuneData class.
 * @property isValidChannel the isValidChannel of ManualTuneData class.
 * @property buttonValues True/False for enabling/disabling the button respectively.
 */
data class ManualTuneData(var frequency: String = "", var isValidChannel: Boolean = false, var buttonValues: Array<Boolean> = arrayOf(true, true, true, true, true, true, true, true, true, true))

//this builder class name should be changed based on features
data class Builder
(
        var frequency: Float = 0f,//TODO need to convert to int
        var stationName: String? = "",
        var rdsStatus: eRdsStatus = eRdsStatus.AMFM_RDS_NOT_AVAILABLE,
        var objectId: Int = 0,
        var rdsStationInfo: String? = "",
        var ptyCategory: Int = 0,
        var tpStationStatus: eTPStationStatus = eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP,
        var isFavorite: Boolean = false,
        var categoryData: MutableMap<String, ArrayList<AMFMStationInfo_t>>? = null
) {
    fun frequency(frequency: Float) = apply { this.frequency = frequency }
    fun stationName(stationName: String?) = apply {
        if (stationName == null)
            this.stationName = ""
        else
            this.stationName = stationName
    }

    fun rdsStatus(rdsStatus: eRdsStatus) = apply {
        this.rdsStatus = rdsStatus
    }

    fun objectId(objectId: Int) = apply { this.objectId = objectId }

    fun rdsStationInfo(rdsStationInfo: String?) = apply {
        if (rdsStationInfo == null)
            this.rdsStationInfo = ""
        else
            this.rdsStationInfo = rdsStationInfo
    }

    fun ptyCategory(ptyCategory: Int) = apply { this.ptyCategory = ptyCategory }
    fun tpStationStatus(tpStationStatus: eTPStationStatus) = apply { this.tpStationStatus = tpStationStatus }
    fun isFavorite(isFavorite: Boolean) = apply { this.isFavorite = isFavorite }
    fun categoryData(categoryData: MutableMap<String, ArrayList<AMFMStationInfo_t>>) = apply { this.categoryData = categoryData }


    fun build() = AMFMStationInfo_t(frequency, stationName, rdsStatus, objectId, rdsStationInfo, ptyCategory,
            tpStationStatus, isFavorite, categoryData)
}