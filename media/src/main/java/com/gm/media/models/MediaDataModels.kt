package com.gm.media.models

import android.graphics.Bitmap

/**
 * Created by Dhorababu on 7/10/2018.
 */
enum class USB_BROWSE_PAGE(val value: Number) {
    CATEGORY(0) {
        override fun toString(): String {
            return "Category"
        }
    },
    ARTIST(1) {
        override fun toString(): String {
            return "Artists"
        }
    },
    ARTIST_ALBUM(2) {
        override fun toString(): String {
            return "Artists_Albums"
        }
    },
    ARTIST_ALBUM_SONGS(3) {
        override fun toString(): String {
            return "Artists_Albums_Songs"
        }
    },
    ALBUM(4) {
        override fun toString(): String {
            return "Albums"
        }
    },
    ALBUM_SONGS(5) {
        override fun toString(): String {
            return "Albums_Songs"
        }
    },
    ARTIST_SONGS(5) {
        override fun toString(): String {
            return "Artist_Songs"
        }
    },
    SONGS(6) {
        override fun toString(): String {
            return "Songs"
        }
    }
    ,
    GENRES(6) {
        override fun toString(): String {
            return "Genres"
        }
    }
    ,
    COMPOSERS(7) {
        override fun toString(): String {
            return "Composers"
        }
    },
    GENRE_ARTIST(8) {
        override fun toString(): String {
            return "Genre_Artist"
        }
    },
    GENRE_ARTIST_ALBUM(9) {
        override fun toString(): String {
            return "Genre_Artist_Album"
        }
    },
    GENRE_ARTIST_ALBUM_SONG(10) {
        override fun toString(): String {
            return "Genre_Artist_Album_Songs"
        }
    },
    GENRE_ALBUM_SONG(11) {
        override fun toString(): String {
            return "Genre_Album_Songs"
        }
    },GENRE_ARTIST_SONG(12) {
        override fun toString(): String {
            return "Genre_Artist_Songs"
        }
    },
    COMPILATIONS(13) {
        override fun toString(): String {
            return "Compilations"
        }
    },
    COMPILATION_ALBUMS(14) {
        override fun toString(): String {
            return "Compilation_Album"
        }
    },
    COMPILATION_ALBUM_SONGS(15) {
        override fun toString(): String {
            return "Compilation_Album_Songs"
        }
    },
    COMPILATION_SONGS(16) {
        override fun toString(): String {
            return "Compilation_Songs"
        }
    },COMPOSER_ALBUM(13) {
        override fun toString(): String {
            return "Composer_Album"
        }
    },COMPOSER_ALBUM_SONG(14) {
        override fun toString(): String {
            return "Composer_Album_Songs"
        }
    },COMPOSER_SONG(15) {
        override fun toString(): String {
            return "Composer_Songs"
        }
    },FOLDER(16) {
        override fun toString(): String {
            return "Folder"
        }
    },FOLDER_SONGS(17) {
        override fun toString(): String {
            return "Folder_Songs"
        }
    }
}

enum class eMediaDeviceType(val value: Number) {
    MEDIA_DTY_UNKNOWN(0),
    MEDIA_DTY_USB(1),
    MEDIA_DTY_IPOD(2),
    MEDIA_DTY_SD(3),
    MEDIA_DTY_BLUETOOTH10(4),
    MEDIA_DTY_BLUETOOTH13(5),
    MEDIA_DTY_BLUETOOTH14(6),
    MEDIA_DTY_CARPLAY(7),
    MEDIA_DTY_ANDROIDAUTO(8),
    MEDIA_DTY_MIRRORLINK(9),
    MEDIA_DTY_AGGREGATION(10),
    MEDIA_DTY_AUX(11)
}

enum class eMediaDevIndexedState(val value: Number) {
    MEDIA_DEV_IDS_NOT_SUPPORTED(0),
    MEDIA_DEV_IDS_NOT_STARTED(1),
    MEDIA_DEV_IDS_PARTIAL(2),
    MEDIA_DEV_IDS_COMPLETED(3)
}

//## type eMediaListType
enum class eMediaListType(val value: Number) {
    MEDIA_LTY_SONG(0),
    MEDIA_LTY_GENRE(1),
    MEDIA_LTY_ARTIST(2),
    MEDIA_LTY_ALBUM(3),
    MEDIA_LTY_GENRE_ARTIST(4),
    MEDIA_LTY_GENRE_ARTIST_ALBUM(5),
    MEDIA_LTY_GENRE_ARTIST_ALBUM_SONG(6),
    MEDIA_LTY_GENRE_ARTIST_SONG(7),
    MEDIA_LTY_GENRE_ALBUM(8),
    MEDIA_LTY_GENRE_ALBUM_SONG(9),
    MEDIA_LTY_GENRE_SONG(10),
    MEDIA_LTY_ARTIST_ALBUM(11),
    MEDIA_LTY_ARTIST_ALBUM_SONG(12),
    MEDIA_LTY_ARTIST_SONG(13),
    MEDIA_LTY_ALBUM_SONG(14),
    MEDIA_LTY_PODCAST(15),
    MEDIA_LTY_PODCAST_EPISODE(16),
    MEDIA_LTY_AUDIOBOOK(17),
    MEDIA_LTY_AUDIOBOOK_CHAPTER(18),
    MEDIA_LTY_AUTHOR(19),
    MEDIA_LTY_AUTHOR_BOOKTITLE(20),
    MEDIA_LTY_AUTHOR_BOOKTITLE_CHAPTER(21),
    MEDIA_LTY_COMPOSER(22),
    MEDIA_LTY_COMPOSER_ALBUM(23),
    MEDIA_LTY_COMPOSER_ALBUM_SONG(24),
    MEDIA_LTY_COMPOSER_SONG(25),
    MEDIA_LTY_VIDEO(26),
    MEDIA_LTY_VIDEO_EPISODE(27),
    MEDIA_LTY_PLAYLIST(28),
    MEDIA_LTY_PLAYLIST_SONG(29),
    MEDIA_LTY_ITUNESRADIO(30),
    MEDIA_LTY_ITUNESRADIO_PLAYLISTS(31),
    MEDIA_LTY_ITUNESRADIO_PLAYLIST_SONGS(32),
    MEDIA_LTY_COMPILATION(33),
    MEDIA_LTY_COMPILATION_ALBUMS(34),
    MEDIA_LTY_COMPILATION_ALBUM_SONGS(35),
    MEDIA_LTY_COMPILATION_SONGS(36),
    MEDIA_LTY_FOLDER(37),
    MEDIA_LTY_FOLDER_SONG(38),
    MEDIA_LTY_CURRENT_SELECTION(39),
    MEDIA_LTY_NONE(40),
    MEDIA_LTY_ALBUM_ALL_SONGS(41),
    MEDIA_LTY_COMPILATION_ALL_SONGS(42),
    MEDIA_LTY_BTGENRE_SONG(43),
    MEDIA_LTY_BTGENRE_ARTIST_SONG(44),
    MEDIA_LTY_BTARTIST_SONG(45)
}

//## type eMediaPBMode
enum class eMediaPBMode(val value: Number) {
    MEDIA_PBM_NORMAL(0),
    MEDIA_PBM_RANDOM(1),
    MEDIA_PBM_REPEAT(2),
    MEDIA_PBM_REPEATALL(3)
}

//## type eMediaPBAction
enum class eMediaPBAction(val value: Number) {
    MEDIA_PBA_PAUSE(0),
    MEDIA_PBA_PLAY(1),
    MEDIA_PBA_STOP(2),
    MEDIA_PBA_PREV(3),
    MEDIA_PBA_NEXT(4),
    MEDIA_PBA_FAST_PREV(5),
    MEDIA_PBA_FAST_PREV_STOP(6),
    MEDIA_PBA_FAST_NEXT(7),
    MEDIA_PBA_FAST_NEXT_STOP(8),
    MEDIA_PBA_FASTFORWARD(9),
    MEDIA_PBA_FASTBACKWARD(10),
    MEDIA_PBA_SEEK(11)
}

//## type MediaPlayerIndexedList_t
data class MediaPlayerIndexedList_t(
        var deviceId: Int,
        var listHandle: Int,
        var listSize: Int
)

//## type MediaDevice_t
data class MediaDevice_t(
        var id: Int,
        var type: eMediaDeviceType
)

//## type MediaDeviceInfo_t
data class MediaDeviceInfo_t(var deviceId: Int,
                             var deviceType: eMediaDeviceType,
                             var deviceConnected: Boolean,
                             var deviceIndexedState: eMediaDevIndexedState,
                             var deviceActiveSource: Int,
                             var deviceName: String,
                             var deviceSerialNumber: String,
                             var deviceReadable: Boolean,
                             var folderBrowseSupported: Boolean)

//## type MediaPlayerIndexedListReq_t
data class MediaPlayerIndexedListReq_t(
        var deviceId: Int,
        var listType: eMediaListType,
        var filterTag1: String,
        var filterTag2: String,
        var filterTag3: String,
        var playIndex: Int
)

//## type MediaAlbumArtInfo_t
data class MediaAlbumArtInfo_t(var MIMEimageSubType: String,
                               var albumArt: Bitmap,
                               var photoSize: Int)

//## type ActiveDevMediaIndexingState_t

data class ActiveDevMediaIndexingState_t(var deviceId: Int,
                                         var mediaDevIndexedState: eMediaDevIndexedState,
                                         var indexingPercentComplete: Int,
                                         var playableContentPresent: Boolean)

//## type MediaPlayerDeviceConnections_t
data class MediaPlayerDeviceConnections_t(var numDevices: Int = 0,
                                          var deviceInfo: MediaDeviceInfo_t)

//## type MediaPlayerListChangeRes_t
data class MediaPlayerListChangeRes_t(
        var listHandle: Int,
        var listSize: Int
)

//## type MediaPlayItemFromListByTagReq_t
data class MediaPlayItemFromListByTagReq_t(
        var listHandle: Int,
        var listIndex: Int
)

//## type MediaPlayBackAction_t
data class MediaPlayBackAction_t(
        var mediaPBAction: eMediaPBAction,
        var nextPrevSkipCnt: Int
)

//## type MPRequestListFilter_t
data class MPRequestListFilter_t(
        var listType: eMediaListType,
        var filterTag1: String,
        var filterTag2: String,
        var filterTag3: String,
        var filterText: String
)

//## type MediaPlayTime_t
data class MediaPlayTime_t(
        var remainingTime: Int,
        var elapsedTime: Int
)

//## type eMediaCategoryType
enum class eMediaCategoryType(val value: Number) {
    MEDIA_CTY_NONE(0),
    MEDIA_CTY_GENRE(1),
    MEDIA_CTY_ARTIST(2),
    MEDIA_CTY_ALBUM(3),
    MEDIA_CTY_SONG(4),
    MEDIA_CTY_COMPOSER(5),
    MEDIA_CTY_COMPILATION(6),
    MEDIA_CTY_AUDIOBOOK(7),
    MEDIA_CTY_CHAPTER(8),
    MEDIA_CTY_VIDEO(9),
    MEDIA_CTY_EPISODE(10),
    MEDIA_CTY_PLAYLIST(11),
    MEDIA_CTY_PODCAST(12),
    MEDIA_CTY_FOLDER(13)
}

//## Metadata of media files
data class MediaObject_t(var isPlaying: Boolean = false,//## attribute isPlaying
                         var albumArt: String = "",//## attribute albumArt
                         var fileName: String = "",//## attribute fileName
                         var objectId: Int = 0,//## attribute objectId
                         var playTime: MediaPlayTime_t, //= MediaPlayTime_t(0, 0),//## attribute playTime
                         var metaDataGenre: String = "",//## attribute metaDataGenre
                         var metaDataArtist: String = "",//## attribute metaDataArtist
                         var metaDataAlbum: String = "",//## attribute metaDataAlbum
                         var metaDataSong: String = "",//## attribute metaDataSong
                         var metaDataComposers: String = "",//## attribute metaDataComposer
                         var categoryType: eMediaCategoryType = eMediaCategoryType.MEDIA_CTY_NONE, //## attribute categoryType
                         var isFavorite: Boolean = false)

data class MediaPlayerListInfo_t(
        var objectId: Int,
        var contentName: String
)

data class MediaPlayerListRes_t(var listHandle: Int,
                                var listType: eMediaListType,
                                var totalCount: Int,
                                var focusIndex: Int,
                                var playIndex: Int,
                                var currentCount: Int,
                                var currentContent: MediaPlayerListInfo_t)

data class MediaSearchKeyboardList_t(var searchKeyboardLetter: String,
                                     var letterAvailable: Int,
                                     var letterStartIndex: Int,
                                     var letterEndIndex: Int)

data class MediaPlayerListReq_t
(
        var deviceId: Int,
        var listType: eMediaListType,
        var filterTag1: String,
        var filterTag2: String,
        var filterTag3: String,
        var startIndexId: Int,
        var endIndexId: Int
)

data class MediaMetadata_t(

        var metaDataGenre: String,
        var metaDataComposer: String,
        var metaDataAudioBook: String,
        var metaDataPodCast: String
)

data class MediaScreenTransition_t
(
        var listType: eMediaListType,
        var totalCount: Int
)
enum class eMediaListReadAck(val value: Number) {
    MEDIA_READSTATUS_UNKNOWN (0),
    MEDIA_READSTATUS_COMPLETE (1),
    MEDIA_READSTATUS_STOPPED (2),
    MEDIA_READSTATUS_LOADCOMPLETE  (3)
}
/**

 * This class has MEDIA_BROWSE_LEVELS values.
 * It represents the Browse level for media sources
 * @property value the value of MEDIA_BROWSE_LEVELS class.

 */
enum class MEDIA_BROWSE_LEVELS(val value: Number) {
    LEVEL_NONE(0),
    LEVEL_ONE(1),
    LEVEL_TWO(2),
    LEVEL_THREE(3),
    LEVEL_FOUR(4)

}


// indicates the state our Media Player Service:
enum class MEDIA_PLAYER_STATE(val value: Number) {
    INIT(0) {
        override fun toString(): String {
            return "INIT"
        }
    },
    STOP(1) {
        override fun toString(): String {
            return "STOP"
        }
    },
    PREPARING(2) {
        override fun toString(): String {
            return "PREPARING"
        }
    },
    PLAY(3) {
        override fun toString(): String {
            return "PLAY"
        }
    },
    PLAYPAUSE(4) {
        override fun toString(): String {
            return "togglePlayPause"
        }
    },
    SEEKTO(5) {
        override fun toString(): String {
            return "SEEKTO"
        }
    },
    UPDATE_PROGRESS(6) {
        override fun toString(): String {
            return "SET_PROGRESS"
        }
    },
    PAUSE(7) {
        override fun toString(): String {
            return "togglePlayPause"
        }
    },
    REMOVECALLBACKS(8) {
        override fun toString(): String {
            return "REMOVECALLBACKS"
        }
    },
    DESTROY_SERVICE(9) {
        override fun toString(): String {
            return "DESTROY_SERVICE"
        }
    },
}


//page header object--custom
data class HeaderObject(var isTwoLineHeader: Boolean = false, var firstLineHeaderSt: String? = "", var secondLineHeaderSt: String = "")
//data class AlbumRowLine(var isLineOneVisible: Boolean = false,var isLineTwoVisible: Boolean = false,var isLineThreeVisible: Boolean = false)
//data class SongsRowLine(var isLineOneVisible: Boolean = false,var isLineTwoVisible: Boolean = false,var isLineThreeVisible: Boolean = false,var isLineFourVisible: Boolean = false)