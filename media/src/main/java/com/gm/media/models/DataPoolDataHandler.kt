package com.gm.media.models

import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.graphics.Bitmap

/**
 * Container of observable fields used for UI update. Technically called each variable as Data pool ID's.
 * @author GM on 3/7/2018.
 */
object DataPoolDataHandler {

    /** To set the list of the Medias */
    var mDrawerOptions = ObservableArrayList<String>()

    /** To set the list of the AMFMStationInfo */
    var browseList = ObservableArrayList<AMFMStationInfo_t>()

    /** To set the list of the AMStationInfo */
    //var browseListAM = ObservableArrayList<AMFMStationInfo_t>()

    /** To set the list of the FMStationInfo */
   // var browseListFM = ObservableArrayList<AMFMStationInfo_t>()

    /** Current tune station info is set in this variable */
    var aMFMStationInfo_t = ObservableField<AMFMStationInfo_t>()

    /** To set the themeType */
    var themeType = ObservableField<Boolean>()

    var LHD_RHD_RTL = ObservableField<eLHD_RHD_RTL>()

    /** To set the list of the amfmCategoryList */
    var amfmCategoryList = ObservableArrayList<AMFMStationInfo_t>()

    /** Used to update Progress bar */
    var updateProgress = ObservableField(0)

    /** Used to show Progress bar */
    var showUpdateProgress = ObservableField<Boolean>(false)

    /** Used to Disabled audio Control */
    var audioControlDisabled = ObservableField<Boolean>(false)

    /** Used to Disabled audio Control FM */
    var audioControlDisabledforFM = ObservableField<Boolean>(false)

     /** To set the manualTuneData */
    var manualTuneData = ObservableField<ManualTuneData>()

    //DataPoolDataHandler from Low Radio
    /**
     * To set the AUDIOMANAGER_CHANGE_AUDIOSOURCE
     * It can have any constant from NOWPLAYING_SOURCE_TYPE
     * It represents current selected source for audio
     */
    var AUDIOMANAGER_CHANGE_AUDIOSOURCE = ObservableField<NOWPLAYING_SOURCE_TYPE>(NOWPLAYING_SOURCE_TYPE.AM)
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_OBJECTID
     * It represents current selected channel id
     */
    var AMTUNER_CURRENTSTATIONINFO_OBJECTID = ObservableField<Int>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_FREQUENCY
     * It represents current selected channel frequency
     */
    var AMTUNER_CURRENTSTATIONINFO_FREQUENCY = ObservableField<String>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_STATIONNAME
     * It represents current selected channel name
     */
    var AMTUNER_CURRENTSTATIONINFO_STATIONNAME = ObservableField<String>("")
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_RDSSTATUS
     * It represents current selected channel rds status
     */
    var AMTUNER_CURRENTSTATIONINFO_RDSSTATUS = ObservableField<Float>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO
     * It represents current selected channel rds info
     */
    var AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO = ObservableField<String>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY
     * It represents current selected channel type
     */
    var AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY = ObservableField<Int>()
    /**
     * To set the AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS
     * It represents current selected channel tp station status
     */
    var AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_OBJECTID
     * It represents current selected channel id
     */
    var FMTUNER_CURRENTSTATIONINFO_OBJECTID = ObservableField<Int>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY
     * It represents current selected channel type
     */
    var FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY = ObservableField<Int>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS
     * It represents current selected channel tp station status
     */
    var FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_RDSSTATUS
     * It represents current selected channel rds status
     */
    var FMTUNER_CURRENTSTATIONINFO_RDSSTATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO
     * It represents current selected channel rds info
     */
    var FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO = ObservableField<String>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_FREQUENCY
     * It represents current selected channel frequency
     */
    var FMTUNER_CURRENTSTATIONINFO_FREQUENCY = ObservableField<String>()
    /**
     * To set the FMTUNER_CURRENTSTATIONINFO_STATIONNAME
     * It represents current selected channel name
     */
    var FMTUNER_CURRENTSTATIONINFO_STATIONNAME = ObservableField<String>("")
    /**
     * To set the FMTUNER_TPSTATUS_NOWPLAYING
     * It represents current selected channel playing status
     */
    var FMTUNER_TPSTATUS_NOWPLAYING = ObservableField<Float>()
    /**
     * To set the FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING
     */
    var FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING = ObservableField<Float>()
    /**
     * To set the FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE
     */
    var FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE = ObservableField<String>()
    /**
     * To set the FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT
     */
    var FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT = ObservableField<Int>()
    /**
     * To set the AMTUNER_STRONGSTATIONS_LIST_LISTID
     */
    var AMTUNER_STRONGSTATIONS_LIST_LISTID = ObservableField<Int>()
    /**
     * To set the AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS
     */
    var AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS = ObservableField<Float>()
    /**
     * To set the FMTUNER_STRONGSTATIONS_LIST_LISTID
     */
    var FMTUNER_STRONGSTATIONS_LIST_LISTID = ObservableField<Int>()
    /**
     * To set the FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS
     */
    var FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS = ObservableField<Float>()
    /**
     * To set the AMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS
     */
    var AMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS = ObservableField<Int>()
    /**
     * To set the FMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS
     */
    var FMTUNER_STRONGSTATIONS_LIST_UPDATE_PROGRESS = ObservableField<Int>()
    /**
     * To set the AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY
     */
    var AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY = ObservableField<String>("")
    /**
     * To set the AMTUNER_DIRECTTUNE_AVAILABLE_KEYS
     */
    var AMTUNER_DIRECTTUNE_AVAILABLE_KEYS = ObservableField<String>()
    /**
     * To set the FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY
     */
    var FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY = ObservableField<String>("")
    /**
     * To set the FMTUNER_DIRECTTUNE_AVAILABLE_KEYS
     */
    var FMTUNER_DIRECTTUNE_AVAILABLE_KEYS = ObservableField<String>()
    /**
     * To set the FMTUNER_RDSSWITCH
     */
    var FMTUNER_RDSSWITCH = ObservableField<Int>()
    /**
     * To set the FMTUNER_REGIONSETTING
     */
    var FMTUNER_REGIONSETTING = ObservableField<Int>()
    /**
     * To set the FMTUNER_TPSTATUS
     */
    var FMTUNER_TPSTATUS = ObservableField(0)
    /**
     * To set the FMTUNER_TPSTATUS_SEARCHSTATE
     */
    var FMTUNER_TPSTATUS_SEARCHSTATE = ObservableField<Float>()
    /**
     * To set the FMTUNER_TRAFFICALERT_NAME
     */
    var FMTUNER_TRAFFICALERT_NAME = ObservableField<String>()
    /**
     * To set the FMTUNER_TRAFFICALERTACTIVECALL_NAME
     */
    var FMTUNER_TRAFFICALERTACTIVECALL_NAME = ObservableField<String>()
    /**
     * To set the FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER
     */
    var FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER = ObservableField<Int>()
    /**
     * To set the FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME
     */
   // var FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME = ObservableField<String>()
    /**
     * To set the FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT
     */
    //var FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT = ObservableField<String>()
    /**
     * To set the FMTUNER_STATIONAVAILABILITY_METADATA
     */
    var FMTUNER_STATIONAVAILABILITY_METADATA = ObservableField<String>()
    /**
     * To set the FMTUNER_STATIONAVAILABILITY_STATUS
     */
    var FMTUNER_STATIONAVAILABILITY_STATUS = ObservableField<Float>()
    /**
     * To set the AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST
     */
    var AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = ObservableArrayList<AMFMStationInfo_t>()
    /**
     * To set the FMTUNER_TPSEARCH_UPDATE
     */
    //var FMTUNER_TPSEARCH_UPDATE = ObservableField<String>()
    /**
     * To set the AMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE
     */
    var AMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE = ObservableField<String>()
    /**
     * To set the AMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT
     */
    var AMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT = ObservableField<Int>()
    /*
    * To set the screen layout size is Large or not*/
    var isLargeScreenLayout = ObservableField<Boolean>(false)

    /* To set the screen layout mode enabled or disabled*/
    var isLargeScreenLayoutEnabled = ObservableField<Boolean>(true)

    /** To Enable and Disable Skew */
    var isSkewEnabled = ObservableField<Boolean>(false)

    /** To set Skew slope angle to 8*/
    var SKEW_ANGLE_8 = ObservableField<Float>()

    /** To set Skew slope angle to 9*/
    var SKEW_ANGLE_9 = ObservableField<Float>()

    /** To set Skew slope angle to */
    var SKEW_ANGLE_15 = ObservableField<Float>()

    /** To set the PopupMenu Visibility */
    //var isPopupMenuVisibile = ObservableField<Boolean>(false)

    /** To set the BROWSE_SOURCE_TYPE */
    var BROWSE_SOURCE_TYPE = ObservableField<String>()

    /** To set the Curved View */
    var isInfoCurvedView = ObservableField<Boolean>(false)

    /** To set and update the select item in browse preset list */
    var nowPlayingBrowseListPosition  = ObservableField(0)

 /**
     * start of usb integration
     */

   // var USB_BROWSE_LIST = ObservableArrayList<String>()
    var MEDIA_PLAYING_SONG_LIST = ObservableArrayList<MediaObject_t>()
    var MEDIA_USB_BROWSE_LIST = ObservableArrayList<MediaObject_t>()
    //var MEDIA_USB_BROWSE_LIST_DATA = ObservableArrayList<String>()

    var color = ObservableField<Int>()
    var USB_MEDIA_OBJECT_INFO = ObservableField<MediaObject_t>()

    var USB_BROWSE_PAGENAME = ObservableField<USB_BROWSE_PAGE>(USB_BROWSE_PAGE.CATEGORY)

    //var USB_BROWSE_FAV_UPDATE= ObservableBoolean(false)
    // var artistMediaList = ObservableArrayList<MediaObject_t>()
    //var filteredSongsList = ObservableArrayList<MediaObject_t>()
    var allSongs_st = "All Songs"
    var MEDIA_HEADER_OBJECT = ObservableField<HeaderObject>(HeaderObject(isTwoLineHeader = false, firstLineHeaderSt = "Browse"))
   /* var MEDIA_ALBUM_ROW_LINE = ObservableField<AlbumRowLine>()
    var MEDIA_SONGS_ROW_LINE = ObservableField<SongsRowLine>()*/
    //var MEDIA_FAVOURITES_ALBUM_LIST = ObservableArrayList<String>()
    var MEDIA_FAVOURITES_SONGS_LIST = ObservableArrayList<String>()
    //var MEDIA_FAVOURITES_ARTIST_LIST = ObservableArrayList<String>()

    var IS_MEDIA_BROWSE_ROOT_PAGE= ObservableBoolean(true)
    var MEDIA_USB_INDEXING_PROGRESS = ObservableInt(0)
    var IS_MEDIA_INDEXING_VISISBLE = ObservableBoolean(false)
    //var MEDIA_USB_BROWSE_TYPE = ObservableField<String>("")
    var IS_MEDIA_BROWSE_LISTVIEW_VISIBLE= ObservableBoolean(false)
    var IS_MEDIA_BROWSE_NO_CONTENT_VISIBLE= ObservableBoolean(false)
    var IS_SDK_ACTIVE= ObservableBoolean(false)


    var IS_MEDIA_BROWSE_SONGS= ObservableBoolean(false)
    var MEDIA_BROWSE_LEVEL = ObservableField<MEDIA_BROWSE_LEVELS>(MEDIA_BROWSE_LEVELS.LEVEL_ONE)
    var SHOW_LETTER_KEYPAD= ObservableBoolean(false)
    var HIDE_AZ_BTN= ObservableBoolean(false)

    var MEDIA_NOWPLAYING_MEDIAOBJ_FILENAME = ObservableField<String>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_ALBUM = ObservableField<String>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_ARTIST = ObservableField<String>()
    var MEDIA_RES_BROWSEBUTTONSUPPORT = ObservableField<Boolean>(false)
    var MEDIA_NOWPLAYING_PLAYLISTID = ObservableField<Int>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICEID = ObservableField<Int>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICETYPE = ObservableField<eMediaDeviceType>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICECONNECTED = ObservableField<Boolean>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICEINDEXEDSTATE = ObservableField<eMediaDevIndexedState>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICEACTIVESOURCE = ObservableField<Int>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICENAME = ObservableField<String>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICESERIALNUMBER = ObservableField<String>()
    var MEDIA_ACTIVEMEDIADEVICE_DEVICEREADABLE = ObservableField<Boolean>()
    var MEDIA_ACTIVEMEDIADEVICE_FOLDERBROWSESUPPORTED = ObservableField<Boolean>()
    var AUDIO_PREV_MEDIAACTIVE_DEVICEID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_USB_NAME = ObservableField<String>()
    var MEDIA_INDEXINGSTATE_DEVICEID = ObservableField<Int>()
    var MEDIA_INDEXINGSTATE_STATE = ObservableField<eMediaDevIndexedState>(eMediaDevIndexedState.MEDIA_DEV_IDS_NOT_SUPPORTED)
    var MEDIA_INDEXINGSTATE_PERCENTCOMPLETE = ObservableField<Int>()
    var MEDIA_DEV_PLAYABLECONTENT_PRESENT = ObservableField<Boolean>()
    var MEDIA_ACTIVEMEDIADEVICE_INDEXINGSTATE_PERCENTCOMPLETE = ObservableField<Int>()
    var MEDIA_ACTIVEMEDIADEVICE_PLAYABLECONTENT_PRESENT = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_TOTALCOUNT = ObservableField<Int>()
    var MEDIA_MEDIAOBJECTALBUMART = ObservableField<String>()
    var MEDIA_MEDIAPLAYERDEVICECON_COUNT = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_LISTHANDLE = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_LISTTYPE = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_CURRENTCOUNT = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_FOCUSINDEX = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_PLAYINDEX = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_STARTINDEX = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERINDEXEDLIST_ADDCOUNT = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERLISTCHANGE_LISTHANDLE = ObservableField<Int>()
    var MEDIA_MEDIAPLAYERLISTCHANGE_LISTSIZE = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING = ObservableField<Boolean>(false)
    var MEDIA_NOWPLAYING_MEDIAOBJ_ALBUMART = ObservableField<String>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_GENRE = ObservableField<String>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_SONG = ObservableField<String>("")
    var MEDIA_NOWPLAYING_ALBUM_ART_IMAGE = ObservableField<Bitmap>()
    var MEDIA_NOWPLAYING_EMPTY_BITMAP_IMAGE = ObservableField<Bitmap>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_REMAININGTIME = ObservableField<Int>(0)
    var MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME = ObservableField<Int>(0)
    var MEDIA_NOWPLAYING_MEDIAOBJ_CATEGORYTYPE = ObservableField<eMediaCategoryType>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_TRACKTIME = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_PLAYLIST = ObservableField<String>()
    var MEDIA_PLAYBACKMODE = ObservableField<Int>(eMediaPBMode.MEDIA_PBM_NORMAL.ordinal)
    var MEDIA_PLAYBACKMODE_EU = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_INT_ELAPSEDTIME = ObservableField<Int>()
    var MEDIA_NOWPLAYING_PROGRESS_PERCENTAGE = ObservableField<Int>()
    var ONSTAR_NOWPLAYING_MEDIAOBJ_ISPLAYING = ObservableField<Int>()
    var MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_SEARCHKEYBOARDLETTER = ObservableField<String>()
    var MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_LETTERAVAILABLE = ObservableField<Int>()
    var MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_LETTERSTARTINDEX = ObservableField<Int>()
    var MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_LETTERENDINDEX = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_PLAYINDEX = ObservableField<Int>()
    var MEDIA_NOWPLAYING_LISTFILTER = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_ALBUM_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_ARTIST_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_ARTISTALBUM_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_GENRE_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_GENREARTIST_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_GENREARTISTALBUM_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_COMPOSER_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_COMPOSERALBUM_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_PLAYLIST_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_FOLDER_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_COMPILATION_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_COMPILATIONSONG_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_ABOOK_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_ITUNES_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_PODCAST_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_PODCASTEPI_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_COMPILATION_ALBUM_OBJECTID = ObservableField<Int>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_COMPOSER = ObservableField<String>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_AUDIOBOOK = ObservableField<String>()
    var MEDIA_NOWPLAYING_MEDIAOBJ_PODCAST = ObservableField<String>()
    var MEDIA_DEVICE_MOUNTED_STATUS = ObservableField<Boolean>(false)
    var MEDIA_SEEK_PENDING = ObservableField<Int>()
    var MEDIA_MEDIAOBJECTALBUMARTINFO_PHOTOSIZE = ObservableField<Int>()
    var MEDIA_MEDIAOBJECTALBUMARTINFO_MIMETYPE = ObservableField<String>()
    // var HMI_MEDIA_LISTTYPE_SCREENTRANSITION = ObservableField<HMI_MEDIA_LISTTYPE_SCREENTRANSITION, &(indexedList->listType,Int>()
    var MEDIA_IS_LISTSCREEN_LOADING = ObservableField<Int>()
    var MEDIA_DURATION = ObservableField<Int>()

    var HIDEMEDIAMETADATA= ObservableField<Boolean>(false)
   // var ISBORWSELOCKED = ObservableField<Boolean>(false)
    var MEDIA_IS_PROGRESS_BAR_VISIBLE = ObservableField<Boolean>(true)

    var CUSTOM_ALERT_DIALOG_MAIN_TEXT=ObservableField<String>()
    var CUSTOM_ALERT_DIALOG_BUTTON_TEXT_YES=ObservableField<String>()
    var CUSTOM_ALERT_DIALOG_BUTTON_TEXT_NO=ObservableField<String>()

}
