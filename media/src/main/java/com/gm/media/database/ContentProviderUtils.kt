package com.gm.media.database

import android.content.ContentProvider
import android.content.ContentUris
import android.content.ContentValues
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import com.gm.audioapp.database.Favorites
import com.gm.media.models.NONUIVariables

class ContentProviderUtils : ContentProvider() {
    private val mTAG = ContentProviderUtils::class.java!!.getSimpleName()

    private val FAVORITES = 100
    private val FAVORITES_ID = 200
    private val FAVORITES_FREQUENCY = 300
    private val FAVORITES_INDEX = 400
    private val MATCHER = buildUriMatcher()

    override fun onCreate(): Boolean {
        return true
    }

    override fun insert(uri: Uri?, values: ContentValues?): Uri {
        when (MATCHER.match(uri)) {
            FAVORITES -> {
                val id = NONUIVariables.getInstance().gmDatabase!!.getFavouritesDao().insert(fromContentValues(values!!))
                context.contentResolver.notifyChange(uri, null)
                return ContentUris.withAppendedId(uri, id)
            }
            FAVORITES_ID -> throw IllegalArgumentException("Invalid URI, cannot insert with ID: $uri")
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun update(uri: Uri?, values: ContentValues?, p2: String?, p3: Array<out String>?): Int {
        when (MATCHER.match(uri)) {
            FAVORITES -> throw IllegalArgumentException("Invalid URI, cannot update without ID$uri")

            FAVORITES_ID -> {
                val context = context ?: return 0
                val mediaFavourites = fromContentValues(values!!)
                mediaFavourites._id = ContentUris.parseId(uri)
                var count = 0
                count = if (mediaFavourites.favorite_labeltext !== "") {
                    NONUIVariables.getInstance().gmDatabase!!.getFavouritesDao().updateLabelDes(mediaFavourites.favorite_labeltext, mediaFavourites._id)

                } else {
                    NONUIVariables.getInstance().gmDatabase!!.getFavouritesDao().updateOrder(mediaFavourites.favorite_order, mediaFavourites._id)

                }
                context.contentResolver.notifyChange(uri, null)
                return count
            }

            FAVORITES_INDEX -> {
                val context = context ?: return 0
                val mediaFavourites = fromContentValues(values!!)
                mediaFavourites.favorite_order = ContentUris.parseId(uri)
//                var cursor = gmDatabase?.getMediaFavouritesDao()!!.selectById(mediaFavourites._id)
//                if(null!=cursor&&cursor.moveToFirst()){
//                    mediaFavourites.favorite_labeltext = cursor.getString(cursor.getColumnIndex("favorite_labeltext"))
//                }
                val count =NONUIVariables.getInstance().gmDatabase!!.getFavouritesDao().updateOrder(mediaFavourites.favorite_order, mediaFavourites._id)
//                val count = gmDatabase?.getMediaFavouritesDao()!!.update(mediaFavourites)
                context.contentResolver.notifyChange(uri, null)
                return count
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun delete(uri: Uri?, p1: String?, p2: Array<out String>?): Int {
        when (MATCHER.match(uri)) {
            FAVORITES -> throw IllegalArgumentException("Invalid URI, cannot update without ID$uri")
            FAVORITES_ID -> {
                val context = context ?: return 0
                val count = NONUIVariables.getInstance().gmDatabase!!.getFavouritesDao().deleteById(ContentUris.parseId(uri))
                context.contentResolver.notifyChange(uri, null)
                return count
            }
            else -> throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun query(uri: Uri?, p1: Array<out String>?, p2: String?, p3: Array<out String>?, p4: String?): Cursor {
        val code = MATCHER.match(uri)
        val mediaFavourites = NONUIVariables.getInstance().gmDatabase!!.getFavouritesDao()
        val cursor: Cursor
        if (code == FAVORITES || code == FAVORITES_ID || code == FAVORITES_INDEX) {
            if (code == FAVORITES) {
                if (p3 != null) {
                    cursor = mediaFavourites.selectByProperty(p3[0])
                } else {
                    cursor = mediaFavourites.selectAll()
                }
            } else {
                cursor = mediaFavourites.selectById(ContentUris.parseId(uri))
            }
            cursor.setNotificationUri(context.contentResolver, uri)
            return cursor
        } else if (code == FAVORITES_FREQUENCY) {
            cursor = mediaFavourites.selectByProperty(p3!![0])
            return cursor
        } else {
            throw IllegalArgumentException("Unknown URI: $uri")
        }
    }

    override fun getType(uri: Uri?): String {
        val match = MATCHER.match(uri)
        when (match) {
            FAVORITES -> return FavoritesContract.Favorites.CONTENT_TYPE
            FAVORITES_ID -> return FavoritesContract.Favorites.CONTENT_ITEM_TYPE
            FAVORITES_FREQUENCY -> return FavoritesContract.Favorites.CONTENT_ITEM_TYPE
            FAVORITES_INDEX -> return FavoritesContract.Favorites.CONTENT_ITEM_TYPE
            else -> throw IllegalArgumentException("Unknown URI : $uri")
        }
    }


    private fun buildUriMatcher(): UriMatcher {
        // Initalize
        val matcher = UriMatcher(UriMatcher.NO_MATCH)
        val authority = FavoritesContract.CONTENT_AUTHORITY
        // See if matches country records
        matcher.addURI(authority, "favorites", FAVORITES)
        // See if matches country item
        matcher.addURI(authority, "favorites/#", FAVORITES_ID)
        matcher.addURI(authority, "favorites/*", FAVORITES_FREQUENCY)
        matcher.addURI(authority, "favorites/" + FavoritesContract.Cols.FAVORDER, FAVORITES_INDEX)




        return matcher
    }


    private fun fromContentValues(values: ContentValues): Favorites {
        val mediaFavourites = Favorites()
        try {
            if (null != values) {
                if (values.containsKey("_id")) {
                    mediaFavourites._id = values.getAsLong("_id")!!
                }
                if (values.containsKey("favorite_type")) {
                    mediaFavourites.favorite_type = values.getAsString("favorite_type")
                }
                if (values.containsKey("favorite_labeltext")) {
                    mediaFavourites.favorite_labeltext = values.getAsString("favorite_labeltext")
                }
                if (values.containsKey("favorite_desctext")) {
                    mediaFavourites.favorite_desctext = values.getAsString("favorite_desctext")
                }
                if (values.containsKey("favorite_package")) {
                    mediaFavourites.favorite_package = values.getAsString("favorite_package")
                }
                if (values.containsKey("favorite_class")) {
                    mediaFavourites.favorite_class = values.getAsString("favorite_class")
                }
                if (values.containsKey("favorite_action")) {
                    mediaFavourites.favorite_action = values.getAsString("favorite_action")
                }
                if (values.containsKey("favorite_extradata")) {
                    mediaFavourites.favorite_extradata = values.getAsString("favorite_extradata")
                }
                if (values.containsKey("favorite_order")) {
                    mediaFavourites.favorite_order = values.getAsLong("favorite_order")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


        return mediaFavourites
    }

}