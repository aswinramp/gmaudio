package com.gm.media.database

import android.content.UriMatcher
import android.net.Uri

class FavoritesContract {

    interface Cols {
        companion object {
            val ID = "_id"
            val FAVTYPE = "favorite_type"
            val FAVCATEGORY = "favorite_category"
            val FAVLABELTEXT = "favorite_labeltext"
            val FAVDESCTEXT = "favorite_desctext"
            val FAVPACKAGE = "favorite_package"
            val FAVCLASS = "favorite_class"
            val FAVACTION = "favorite_action"
            val FAVEXTRADATA = "favorite_extradata"
            val FAVORDER = "favorite_order"
        }
    }

    companion object {
        val MAX_AUDIO_FAVORITES = 40
        val MAX_PHONE_FAVORITES = 10
        val MAX_NAVIGATION_FAVORITES = 20
        val URI_MATCHER: UriMatcher? = null
        // Used to access the content
        val CONTENT_AUTHORITY = "com.gm.audioapp.favoritesprovider.provider"
        val BASE_CONTENT_URI = Uri.parse("content://$CONTENT_AUTHORITY")

        val PATH_FOR_FAVORITES = "favorites"

        val URI_TABLE = Uri.parse(BASE_CONTENT_URI.toString() + "/" + PATH_FOR_FAVORITES)

        val TOP_LEVEL_PATHS = arrayOf(PATH_FOR_FAVORITES)
    }

    // Table for Favorite
    class Favorites : Cols {
        companion object {
            val CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendEncodedPath(PATH_FOR_FAVORITES).build()
            // Accessing content directory and item
            val CONTENT_TYPE = "vnd.android.cursor.dir/vnd.$CONTENT_AUTHORITY.favorites"
            val CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.$CONTENT_AUTHORITY.favorites"

            fun buildFavoriteUri(countryId: String): Uri {
                return CONTENT_URI.buildUpon().appendEncodedPath(countryId).build()
            }

            fun getFavoriteId(uri: Uri): String {
                return uri.pathSegments[1]
            }
        }

    }

    class CAT_TYPE {
        init {
            throw RuntimeException("stub")
        }
        companion object {
            val CAT_UKNOWN = 0
            val CAT_AUDIO = 1
            val CAT_PHONE = 2
            val CAT_NAVIGATION = 3
        }
    }

    class FAV_TYPE {
        init {
            throw RuntimeException("stub")
        }

        companion object {
            val FT_UKNOWN = 0
            val FT_AM_FREQUENCY = 1
            val FT_FM_FREQUENCY = 2
            val FT_DAB_STATION = 3
            val FT_SDARS_CHANNEL = 4
            val FT_APP_STATION = 5
            val FT_PANDORA_STATION = 6
            val FT_MEDIA_ARTIST = 7
            val FT_MEDIA_SONG = 8
            val FT_MEDIA_ALBUM = 9
            val FT_MEDIA_PLAYLIST = 10
            val FT_MEDIA_PODCAST = 11
            val FT_MEDIA_GENRE = 12
            val FT_MEDIA_AUDIO_BOOK = 13
            val FT_MEDIA_VIDEO = 14
            val FT_MEDIA_COMPILATION = 15
            val FT_MEDIA_COMPOSER = 16
            val FT_MEDIA_FOLDER = 17
            val FT_PHONE_NUMBER = 18
            val FT_CONTACT_NAME = 19
            val FT_DESTINATION = 20
            val FT_SEARCH_TERM_POI = 21
            val FT_POI_CATEGORY = 22
            val FT_POI_CHAIN = 23
            val FT_SEARCH_TERM_ADDRESS = 24
            val FT_SAVED_TRIP = 25
            val FT_DESTINATION_HOME = 26
            val FT_DESTINATION_KEY_1 = 27
            val FT_DESTINATION_KEY_2 = 28
        }
    }
}