package com.gm.media.database

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import android.database.Cursor


@Dao
interface MediaDataDao {

    /**
     * To get insert data into database
     */
    @Insert
    fun insert(mediaData: MediaData): Long

    /**
     * To update Media Meta data in Database
     */
    @Update
    fun update(mediaData: MediaData): Int

    @Query("SELECT * FROM MediaData")
    fun selectAll(): Cursor

    /**
     * To get compilation media data from database
     */
    @Query("SELECT filename from MediaData")
    fun getAllFileName(): List<String>

    /**
     * To get artist List media data from database
     */
    @Query("SELECT artist from MediaData")
    fun getAllArtists(): List<String>

    /**
     * To get albums List media data from database
     */
    @Query("SELECT album from MediaData")
    fun getAllAlbums(): List<String>

    /**
     * To get all types genre media data from database
     */
    @Query("SELECT genre from MediaData")
    fun getAllGenre(): List<String>

    /**
     * To get all songs media data from database
     */
    @Query("SELECT * from MediaData")
    fun getAllSongs(): List<MediaData>

    /**
     * To get all composers media data from database
     */
    @Query("SELECT composers from MediaData")
    fun getAllComposer(): List<String>

    /**
     * To get compilation media data from database
     */
    @Query("SELECT *  FROM MediaData ")
    fun getAllCompilations(): List<MediaData>

    /**
     * To get entire media data from database
     */
    @Query("SELECT * from MediaData")
    fun getAll(): List<MediaData>

    /**
     * To get entire media data from database
     */
    @Query("SELECT * FROM MediaData")
    fun getAllSongsData(): List<MediaData>

    /**
     * To get entire media data from database
     */
    @Query("SELECT *  FROM MediaData WHERE artist = :artistName")
        fun getArtistAlbumData(artistName: String): List<MediaData>

    @Query("SELECT * FROM MediaData WHERE composers IS :composerName")
    fun getAlbumComposerData(composerName: String): List<MediaData>

    @Query("SELECT * from MediaData WHERE artist = :artistName AND album = :albumName ")
    fun getArtistAlbumSongs(artistName: String, albumName: String): List<MediaData>

    @Query("SELECT * from MediaData WHERE artist = :artistName ")
    fun getArtistAllSongs(artistName: String): List<MediaData>

    @Query("SELECT filename FROM MediaData WHERE artist = :artistName AND album = :albumName")
    fun getSongsFromAlbums(artistName: String, albumName: String): List<String>
    //for albums
    @Query("SELECT * FROM MediaData WHERE album = :albumName ")
    fun getAlbumSongs(albumName: String): List<MediaData>


    //for genre
    @Query("SELECT artist FROM MediaData WHERE genre = :genreName ")
    fun getGenreArtist(genreName: String): List<String>

    @Query("SELECT * FROM MediaData WHERE artist = :artistName AND genre = :genreName")
    fun getGenreArtistAlbums(artistName: String, genreName: String): List<MediaData>

    @Query("SELECT * FROM MediaData WHERE album = :albumName AND artist = :artistName AND genre = :genreName")
    fun getGenreArtistAlbumsSongs(albumName: String, artistName: String, genreName: String): List<MediaData>

    @Query("SELECT * FROM MediaData WHERE artist = :artistName AND genre Is :genreName ")
    fun getGenreArtistAlbumAllSongs(artistName: String,genreName: String): List<MediaData>

    @Query("SELECT * FROM MediaData WHERE genre Is :genreName ")
    fun getGenreAllSongs(genreName: String): List<MediaData>

    @Query("SELECT * FROM MediaData WHERE compilations  NOT  LIKE '%Unknown Compilation%' ")
    fun getCompilationAllSongs(): List<MediaData>



    //for composers
    @Query("SELECT album FROM MediaData WHERE composers = :composerName ")
    fun getAlbumFromComposer(composerName: String): List<String>
    @Query("SELECT * FROM MediaData WHERE  album IS :albumName AND composers IS:composerName ")
    fun getSongsFromAlbumsFromComposer(composerName:String,albumName: String): List<MediaData>

    @Query("SELECT * FROM MediaData WHERE composers = :composerName ")
    fun getAllSongsFromComposer(composerName: String): List<MediaData>

    /*   @Query("SELECT filename from MediaData WHERE artist = :albums AND album = :albums ")
       fun getAlbumsFromData(albums:String): List<String>*/

}