package com.gm.media.database

import android.arch.persistence.room.ColumnInfo

 class AlbumArtistSongData {
    @ColumnInfo(name = "filename")
    var filename: String = ""
    @ColumnInfo(name = "album")
    var album: String = ""
    @ColumnInfo(name = "artist")
    var artist: String = ""
    @ColumnInfo(name = "compilations")
    var compilations: String = ""
     @ColumnInfo(name = "filepath")
     var filepath: String = ""
 }