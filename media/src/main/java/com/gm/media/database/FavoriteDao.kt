package com.gm.media.database
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import android.database.Cursor
import com.gm.audioapp.database.Favorites

@Dao
interface FavoriteDao {
    @Insert
    fun insert(mediaFavourites: Favorites): Long

    @Update
    fun update(mediaFavourites: Favorites): Int

    @Query("SELECT * FROM Favorites ORDER BY favorite_order asc")
    fun selectAll(): Cursor

    @Query("SELECT * FROM  Favorites  WHERE _id = :id")
    fun selectById(id: Long): Cursor

    @Query("DELETE FROM  Favorites  WHERE _id = :id")
    fun deleteById(id: Long): Int

    @Query("Update Favorites  Set favorite_labeltext= :favDes  WHERE _id = :id")
    fun updateLabelDes(favDes:String,id: Long):Int

    @Query("Update Favorites  Set favorite_order= :favOrd  WHERE _id = :id")
    fun updateOrder(favOrd:Long,id: Long):Int

    @Query("SELECT * FROM  Favorites  WHERE favorite_desctext = :columnValue")
    fun selectByProperty(columnValue: String?): Cursor

    @Query("SELECT favorite_desctext FROM  Favorites  WHERE _id = :id")
    fun selectLabelTextById(id: Long): Cursor


}