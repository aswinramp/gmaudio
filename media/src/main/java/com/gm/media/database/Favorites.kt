package com.gm.audioapp.database

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
class Favorites {
    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(index = true, name = "_id")
    var _id : Long = 0
    @ColumnInfo(name = "favorite_type")
    var favorite_type:String = ""

    @ColumnInfo(name = "favorite_labeltext")
    var favorite_labeltext : String = ""

    @ColumnInfo(name = "favorite_desctext")
    var favorite_desctext : String = ""

    @ColumnInfo(name = "favorite_package")
    var favorite_package : String = ""

    @ColumnInfo(name = "favorite_class")
    var favorite_class :String = ""

    @ColumnInfo(name = "favorite_action")
    var favorite_action :String = ""

    @ColumnInfo(name = "favorite_extradata")
    var favorite_extradata:String = ""

    @ColumnInfo(name = "favorite_order")
    var favorite_order :Long =  0
}