package com.gm.media.database


import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey


@Entity
class MediaData {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "objectid")
    var objectId: Int = 0
    @ColumnInfo(name = "albumArt")
    var albumArt: String = ""
    @ColumnInfo(name = "filename")
    var filename: String = ""
    @ColumnInfo(name = "genre")
    var genre: String = ""
    @ColumnInfo(name = "artist")
    var artist: String = ""
    @ColumnInfo(name = "album")
    var album: String = ""
    @ColumnInfo(name = "filepath")
    var filepath: String = ""
    @ColumnInfo(name = "isfavorite")
    var isFavorite: Boolean = false
    @ColumnInfo(name = "composers")
    var composers: String = ""
    @ColumnInfo(name = "compilations")
    var compilations: String = ""
    @ColumnInfo(name = "folders")
    var folders: String = ""

}