package com.gm.media.database
import android.arch.persistence.db.SupportSQLiteOpenHelper
import android.arch.persistence.room.*
import android.content.Context
import com.gm.audioapp.database.Favorites


@Database(entities = [MediaData::class, Favorites::class], version = 1, exportSchema = false)
abstract class GMDatabase: RoomDatabase() {

    abstract fun mediaDataDao(): MediaDataDao
    abstract fun getFavouritesDao() : FavoriteDao

    companion object {
        @Volatile
        private var INSTANCE: GMDatabase? = null
        const val DATABASE_NAME="audio_db"

        fun getInstance(context: Context): GMDatabase? {
            if(INSTANCE == null){
                INSTANCE = Room.databaseBuilder(context.applicationContext,
                        GMDatabase::class.java,
                        DATABASE_NAME)
                        .allowMainThreadQueries()
                        .build()
            }
            return  INSTANCE
        }

    }
    override fun createOpenHelper(config: DatabaseConfiguration?): SupportSQLiteOpenHelper {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createInvalidationTracker(): InvalidationTracker {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun clearAllTables() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}