package com.gm.audioapp.ui.customclasses

import com.gm.media.R
import com.gm.media.apiintegration.AudioService
import gm.media.enums.EnumBrowseChildType
import gm.media.utilities.CConstants
import gm.provider.GMFavoritesContract



object MediaStringMatcher{
    val MEDIA_INDEXING_PERCENTAGE_TOTAL = 10000
    fun getMediaNameByConstant(constant: String): String {
        var lBrowseChildName: String = constant
        when (constant) {
            CConstants.Browse_Albums_Token -> lBrowseChildName = getStringBYId(R.string.audio_albums_small)
            CConstants.Browse_Artists_Token -> lBrowseChildName = getStringBYId(R.string.audio_artists_small)
            CConstants.Browse_Songs_Token -> lBrowseChildName = getStringBYId(R.string.audio_songs_small)
            CConstants.Browse_Genres_Token -> lBrowseChildName = getStringBYId(R.string.audio_genre_small)
            CConstants.Browse_Playlists_Token -> lBrowseChildName = getStringBYId(R.string.audio_playlist_small)
            CConstants.Browse_Compilations_Token -> lBrowseChildName = getStringBYId(R.string.audio_compilation_small)
            CConstants.Browse_Composers_Token -> lBrowseChildName = getStringBYId(R.string.audio_composer_small)
            CConstants.Browse_Folder_Token -> lBrowseChildName = getStringBYId(R.string.audio_folder_small)
            CConstants.Browse_Podcasts_Token -> lBrowseChildName = getStringBYId(R.string.audio_podcast_small)
            CConstants.Browse_Audiobooks_Token -> lBrowseChildName = getStringBYId(R.string.audio_audiobooks_small)
            CConstants.Browse_All_Songs_Token -> lBrowseChildName = getStringBYId(R.string.audio_all_songs_small)
            CConstants.Browse_Videos_Token -> lBrowseChildName = getStringBYId(R.string.audio_videos_small)
            else -> {
            }
        }
        return lBrowseChildName
    }
    fun getMediaNameByType(enumBrowseChildType: EnumBrowseChildType): String {
        var lBrowseChildName: String = ""
        when (enumBrowseChildType) {
            EnumBrowseChildType.Albums -> lBrowseChildName = getStringBYId(R.string.audio_albums_small)
            EnumBrowseChildType.Artists -> lBrowseChildName = getStringBYId(R.string.audio_artists_small)
            EnumBrowseChildType.Songs -> lBrowseChildName = getStringBYId(R.string.audio_songs_small)
            EnumBrowseChildType.Roots -> lBrowseChildName = getStringBYId(R.string.browse_st)
            else -> {
            }
        }
        return lBrowseChildName
    }


    /**
     * This method returns the Favorite type based on Browse child type.
     * @param browseChildType
     * is of type EnumBrowseChildType.
     * @return lFavoriteType as integer.
     */
    fun getFavTypeFromBrowseChildType(browseChildType: EnumBrowseChildType): Int {
        var lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_UKNOWN
        when (browseChildType) {
            EnumBrowseChildType.Songs, EnumBrowseChildType.iTunesRadio -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG
            EnumBrowseChildType.Albums -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_ALBUM
            EnumBrowseChildType.Artists -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_ARTIST
            EnumBrowseChildType.Genres -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_GENRE
            EnumBrowseChildType.Composers -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_COMPOSER
            EnumBrowseChildType.Playlists -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_PLAYLIST
            EnumBrowseChildType.Podcasts -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_PODCAST
            EnumBrowseChildType.Episodes -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG
            EnumBrowseChildType.FolderInfo -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER
            EnumBrowseChildType.Audiobooks, EnumBrowseChildType.Chapters -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK
            EnumBrowseChildType.Videos -> lFavoriteType = GMFavoritesContract.FAV_TYPE.FT_MEDIA_VIDEO
            else -> {
            }
        }
        return lFavoriteType
    }
    fun getStringBYId(id: Int): String {
        return AudioService.instance!!.resources.getString(id)
    }

    fun getKeyByValue(map: HashMap<Int, String>, value: String): Int {
        for (entry in map.entries) {
            if (value == entry.value) {
                return entry.key
            }
        }
        return -1
    }
}