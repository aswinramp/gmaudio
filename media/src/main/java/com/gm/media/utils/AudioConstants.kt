package com.gm.media.utils

class AudioConstants {
    companion object {
        const val Folder = "Folder"
        const val mp3 = ".mp3"
        const val MP3 = ".MP3"
        const val Remove_Browse = "removeBrowse"
        const val OnKeypadLetterClick = "onKeypadLetterClick"
        const val OnUsbNowPlaying = "onUsbNowPlaying"
        const val OnUsbBrowse = "onUsbBrowse"
        const val BROWSE_CLOSE_KEY = "BROWSE_CLOSE_KEY"
        const val BROWSE_ROOT_SONGS = 5L
        const val ShowMetaData = "showMetaData"
        const val HideVideo = "hideVideo"
        const val ShowVideo = "showVideo"
        const val EnableDisableBrowseVideo = "enableDisableBrowseVideo"
        const val NormalScreenVideo = "normalScreenVideo"
        const val USBBrowseItemClick= "USBBrowseItemClick"
        const val MEDIA_REQ_BACKTRAVERSE = "MEDIA_REQ_BACKTRAVERSE"
        const val BROWSE_CLOSE_KEY_EVENT = "eUsbBrowseClose"
        const val CONTENT_FRAGMENT_TAG = "CONTENT_FRAGMENT_TAG"
        const val USB_STORAGE_KEY = "Usb_Storage_Key"
        const val USB_STORAGE_PATH = "Usb_Storage_Path"
    }
}