package com.gm.audioapp.ui.customclasses

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import com.gm.media.R

/**
 * Created by Dhorababu on 6/12/2018.
 */
class PermissionsUtils (var activity: Activity?){
     private val permission:String = Manifest.permission.READ_EXTERNAL_STORAGE
    val recordPermissionRequestCode = 101

    fun requestPermission():Boolean {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity!!, permission)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission)) {

                AlertDialog.Builder(activity!!, R.style.DialogTheme)
                        .setTitle("Storage Permission")
                        .setMessage("This is mandatory permission to access media files")
                        .setCancelable(false)
                        .setNeutralButton("Ok") { it, _ ->
                            run {
                                it.dismiss()
                                ActivityCompat.requestPermissions(activity!!, arrayOf(permission), recordPermissionRequestCode)
                            }
                        }.create().show()
            } else {
                ActivityCompat.requestPermissions(activity
                !!, arrayOf(permission), recordPermissionRequestCode)
            }
        }else{
            return true
        }
        return false
    }
}

