package com.gm.media.utils


import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.net.Uri
import android.os.HandlerThread
import android.preference.PreferenceManager
import android.support.v4.content.CursorLoader
import android.support.v4.content.Loader
import android.util.Log
import com.gm.media.database.FavoritesContract
import com.gm.media.database.FavoritesContract.Cols.Companion.FAVLABELTEXT
import com.gm.media.database.FavoritesContract.Favorites.Companion.CONTENT_URI
import com.gm.audioapp.ui.customclasses.MediaStringMatcher
import com.gm.media.R
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.database.FavoritesContract.Cols.Companion.FAVDESCTEXT
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler.IS_SDK_ACTIVE
import com.gm.media.models.DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST
import gm.entertainment.AudioSources
import gm.media.CMediaPlayer
import gm.media.enums.EnumBrowseChildType
import gm.media.enums.EnumFavoriteType
import gm.media.exceptions.ExceptionInvalidStringVersion
import gm.media.exceptions.ExceptionPlayFunctionNotEnabled
import gm.media.interfaces.IBrowseItem
import gm.media.interfaces.IContext
import gm.provider.GMFavoritesContract

/**
 * This Object contains favorite related functions like update favorite data, load favorites
 */
object FavoriteDataProcessor : Loader.OnLoadCompleteListener<Cursor> {

    private val mTAG = FavoriteDataProcessor::class.simpleName

    private const val HD_STATION = "HD"
    private const val NO_INDEX_FOR_FAVORITE = -1
    private const val EXTRA_SPLITTER = "|"
    private const val FAVORITE_BY_STAR_ICON: Long = -1
    private var isInit:Boolean=false

    init {
        lazyInit()
    }

    /**
     *This method is used to start the thread  for loading favorite data.
     */
    fun lazyInit() {
        if(!isInit) {
            isInit = true
            val mCursorLoaderThread = object : HandlerThread("FavCursorLoaderThread") {
                override fun onLooperPrepared() {

                    initLoaderForFavoritesDataUpdation()
                }
            }
            mCursorLoaderThread.start()
        }
    }

    /**
     * This method is used to intialise the loader and listen to the favorites
     * content provider for the updates while insertion, deletion of the
     * favorites.
     */
    private fun initLoaderForFavoritesDataUpdation() {
        try {
            if (AudioService.isSDKAvailable()) {
                val selection = (GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_ALBUM + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_ARTIST + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_GENRE + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_COMPILATION + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_COMPOSER + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_PLAYLIST + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_PODCAST + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_MEDIA_VIDEO + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_FM_FREQUENCY + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_AM_FREQUENCY + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_DAB_STATION + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_SDARS_CHANNEL + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_PANDORA_STATION + " OR "
                        + GMFavoritesContract.Favorite.Cols.FAVTYPE + "="
                        + GMFavoritesContract.FAV_TYPE.FT_APP_STATION)

                val mCursorLoader = CursorLoader(AudioService.instance!!, GMFavoritesContract.Favorite.CONTENT_URI, null,
                        selection, null, GMFavoritesContract.Favorite.Cols.FAVORDER + " ASC ")

                mCursorLoader.registerListener(1, this)
                mCursorLoader.startLoading()
            } else {
                val selection = (FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_ALBUM + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_ARTIST + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_GENRE + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_SONG + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_COMPILATION + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_COMPOSER + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_PLAYLIST + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_PODCAST + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_MEDIA_VIDEO + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_FM_FREQUENCY + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_AM_FREQUENCY + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_DAB_STATION + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_SDARS_CHANNEL + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_PANDORA_STATION + " OR "
                        + FavoritesContract.Cols.FAVTYPE + "="
                        + FavoritesContract.FAV_TYPE.FT_APP_STATION)

                val mCursorLoader = CursorLoader(AudioService.instance!!, FavoritesContract.Favorites.CONTENT_URI, null,
                        selection, null, FavoritesContract.Cols.FAVORDER + " ASC ")

                mCursorLoader.registerListener(1, this)
                mCursorLoader.startLoading()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        isInit = false
    }

    override fun onLoadComplete(loader: Loader<Cursor>, cursor: Cursor?) {
        fetchFavoriteListAMFM(cursor)
        loader.unregisterListener(this)
    }

    private fun fetchFavoriteListAMFM(aCurrentFavorite: Cursor?) {
        try{
            if (null != aCurrentFavorite) {
                NONUIVariables.getInstance().favoriteList.clear()
                if (aCurrentFavorite.count > 0) {
                    aCurrentFavorite.moveToFirst()
                    do{
                        val favDescText = aCurrentFavorite.getString(
                                aCurrentFavorite.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVDESCTEXT))
                        if (favDescText != null) {
                            NONUIVariables.getInstance().favoriteList.add(favDescText)
                        }
                    }while(aCurrentFavorite.moveToNext())
                }
            }
        }
        catch (e:Exception){
            e.printStackTrace()
        }
    }

    /**
     *This method is used to add/remove favData from favoriteList.
     * @param any the AMFMStationInfo_t passing station information.
     */
    fun addOrRemoveFavorite(any: Any) {
        if (any is AMFMStationInfo_t) {
            if (any.isFavorite)
                removeFavorite(any)
            else
                addFavorite(any)
            //for simulation
        } else if (any is MediaObject_t && any.fileName.isNotEmpty()) {
            if (IS_SDK_ACTIVE.get()) {
                if (DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST.contains(any.objectId.toString())) {
                    EnumBrowseChildType.Songs
                    if (removeMediaFavorite(NO_INDEX_FOR_FAVORITE, GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG, EnumBrowseChildType.Songs, any.metaDataSong, any.objectId.toLong())) MEDIA_FAVOURITES_SONGS_LIST.remove(any.objectId.toString())
                } else
                    if (setFavoriteForCurrentSong(any.metaDataSong)) MEDIA_FAVOURITES_SONGS_LIST.add(any.objectId.toString())

            } else {
                if (DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST.contains(any.fileName))
                    removeMediaFavorite(NO_INDEX_FOR_FAVORITE, FavoritesContract.FAV_TYPE.FT_MEDIA_SONG,
                            EnumBrowseChildType.Songs, any.fileName, any.objectId.toLong())
                else
                    setFavoriteForCurrentSong(any.fileName)
            }
        } else if (any is IBrowseItem) {
            //for sdk browse
            processMediaFavorite(any)
        }
    }

    /**
     *This method is used to add favData from favoriteList.
     * @param stationInfo the AMFMStationInfo_t passing station information.
     */
    private fun addFavorite(stationInfo: AMFMStationInfo_t?) {

        if (IS_SDK_ACTIVE.get()) {
            var values: ContentValues? = null
            if (null != stationInfo) {
                val source = stationInfo.rdsStationInfo
                values = ContentValues()
                if (source != null) {
                    val favType = FavoriteDataProcessor.getFavTypeFromAudioSource(source)
                    values = FavoriteDataProcessor.getContentValueFromFavoriteStation(favType, FavoriteDataProcessor.FAVORITE_BY_STAR_ICON,
                            source, stationInfo)
                }
            }
            try {
                var uri: Uri? = null
                var lMessage: String? = null
                var favLabelText = ""
                if (null != values && values.size() > 0) {
                    uri = AudioService.instance!!.contentResolver!!.insert(GMFavoritesContract.Favorite.CONTENT_URI, values)
                    favLabelText = FavoritesContract.Cols.FAVLABELTEXT
                }
                if (null != uri) {
                    lMessage = AudioService.instance!!.resources.getString(R.string.audio_saved_as_a_favorite,
                            values!!.get(favLabelText))
                }


                if (null != lMessage)
                    SystemListener.showQuickNotice(lMessage)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            var values: ContentValues? = null
            if (null != stationInfo) {
                val source = stationInfo.rdsStationInfo
                values = ContentValues()
                if (source != null) {
                    val favType = getFavTypeFromAudioSource(source)
                    updateIndexByOne(AudioService.instance!!)
                    values = getContentValueFromFavoriteStation(favType, getIndex(AudioService.instance!!),
                            source, stationInfo)
                }
            }
            try {
                if (null != values && values.size() > 0)
                    AudioService.instance!!.contentResolver!!.insert(FavoritesContract.Favorites.CONTENT_URI, values)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    /**
     *This method is used to get getFavType from station list.
     * @param audioSources passing station information.
     * @return lFavType

     */
    private fun getFavTypeFromAudioSource(audioSources: String): Int {
        var lFavType = GMFavoritesContract.FAV_TYPE.FT_UKNOWN
        when (audioSources) {
            NOWPLAYING_SOURCE_TYPE.AM.name -> lFavType = GMFavoritesContract.FAV_TYPE.FT_AM_FREQUENCY
            NOWPLAYING_SOURCE_TYPE.FM.name -> lFavType = GMFavoritesContract.FAV_TYPE.FT_FM_FREQUENCY
            else -> {
            }
        }
        return lFavType
    }

    /**
     *This method is used to get getContentValue from favorite station list.
     * @param favType passing favType.
     * @param position passing position.
     * @param source passing source.
     * @param stationInfo passing station information.
     * @return lContentValues.

     */
    private fun getContentValueFromFavoriteStation(favType: Int, position: Long, source: String, stationInfo: AMFMStationInfo_t?): ContentValues {
        val lContentValues = ContentValues()
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVTYPE, favType)
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVCLASS, "")
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVPACKAGE, source)
        if (position >= 0) {
            lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVORDER, position)
        }
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVLABELTEXT,
                stationInfo!!.frequency.toString())
        lContentValues.put(GMFavoritesContract.Favorite.Cols.FAVDESCTEXT,
                stationInfo.frequency.toString())
        return lContentValues
    }

    /**
     *This method is used to remove favData from favoriteList.
     * @param stationInfo the AMFMStationInfo_t passing station information.

     */
    private fun removeFavorite(stationInfo: AMFMStationInfo_t?) {
        if (IS_SDK_ACTIVE.get()) {
            val position = FavoriteDataProcessor.FAVORITE_BY_STAR_ICON
            if (null == stationInfo) {
                Log.d(ContentValues.TAG, "removeFavorite stationInfo $stationInfo")
                return
            }

            val source = stationInfo.rdsStationInfo
            val selection: String?
            var selectionArgs = arrayOf(source, source + FavoriteDataProcessor.HD_STATION, "", "")
            var lastIndex = 3

            when {
                position > FavoriteDataProcessor.NO_INDEX_FOR_FAVORITE -> {
                    selection = ("((" + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?" + " OR "
                            + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?)" + " AND "
                            + GMFavoritesContract.Favorite.Cols.FAVORDER + "= ?" + " AND "
                            + GMFavoritesContract.Favorite.Cols.FAVTYPE + "= ?)")
                    selectionArgs[2] = position.toString()
                }
                source == NOWPLAYING_SOURCE_TYPE.FM.name -> {
                    // This is to fix Elvis 1985389: Error when trying to delete a
                    // previous saved FM
                    // When tuner antenna is disconnected & FM favorite is saved from
                    // Now playing screen, FreqDesc is saved with '0' picode (ex., 91.1|0 ).
                    // so Additional SelectionArgs column is added here,
                    // to check for both if freqdesc contains 0 picode (ex: 91.1|0 ) or valid
                    // picode (91.1|picode_number)
                    selectionArgs = arrayOf(source, source + FavoriteDataProcessor.HD_STATION, "", "", "")
                    selection = ("((" + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?" + " OR "
                            + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?)" + " AND ("
                            + GMFavoritesContract.Favorite.Cols.FAVDESCTEXT + " LIKE ?" + " OR "
                            + GMFavoritesContract.Favorite.Cols.FAVDESCTEXT + " LIKE ?" + ") AND "
                            + GMFavoritesContract.Favorite.Cols.FAVTYPE + "= ?)")
                    selectionArgs[2] = stationInfo.frequency.toString() + "%"
                    selectionArgs[3] = stationInfo.frequency.toString() + FavoriteDataProcessor.EXTRA_SPLITTER + "0" + "%"
                    lastIndex = 4
                }
                else -> {
                    selection = ("((" + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?" + " OR "
                            + GMFavoritesContract.Favorite.Cols.FAVPACKAGE + "= ?)" + " AND "
                            + GMFavoritesContract.Favorite.Cols.FAVDESCTEXT + " LIKE ?" + " AND "
                            + GMFavoritesContract.Favorite.Cols.FAVTYPE + "= ?)")

                    when (source) {
                        NOWPLAYING_SOURCE_TYPE.AM.name -> selectionArgs[2] = stationInfo.frequency.toString() + "%"
                        NOWPLAYING_SOURCE_TYPE.FM.name -> selectionArgs[2] = stationInfo.frequency.toString() + "%"
                    }
                }
            }

            when (source) {
                NOWPLAYING_SOURCE_TYPE.AM.name -> selectionArgs[lastIndex] = GMFavoritesContract.FAV_TYPE.FT_AM_FREQUENCY.toString()
                NOWPLAYING_SOURCE_TYPE.FM.name -> selectionArgs[lastIndex] = GMFavoritesContract.FAV_TYPE.FT_FM_FREQUENCY.toString()
                //NOWPLAYING_SOURCE_TYPE.DAB -> SelectionArgs[lastIndex] = GMFavoritesContract.FAV_TYPE.FT_DAB_STATION.toString()
            }

            var count = 0

            if (null != selection) {
                count = AudioService.instance!!.contentResolver!!.delete(GMFavoritesContract.Favorite.CONTENT_URI,
                        selection, selectionArgs)
            }
            Log.d(ContentValues.TAG, "removeFavorite count = $count")

        } else {

            val columns = arrayOf(FavoritesContract.Cols.FAVLABELTEXT)

            val cursor1 = AudioService.instance!!.contentResolver!!.query(
                    FavoritesContract.Favorites.CONTENT_URI, columns, null, null, null)
            DatabaseUtils.dumpCursorToString(cursor1)
            val cursor = AudioService.instance!!.contentResolver!!.query(
                    FavoritesContract.Favorites.CONTENT_URI, columns, FavoritesContract.Cols.FAVLABELTEXT, arrayOf(stationInfo!!.frequency.toString()), null)
            DatabaseUtils.dumpCursorToString(cursor)
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    AudioService.instance!!.contentResolver!!.delete(ContentUris.withAppendedId(FavoritesContract.Favorites.CONTENT_URI, cursor.getString(0).toLong()),
                            null, null)
                }
            }
        }
    }

    fun loadFavoritesForSimulation() {
        try {
            MEDIA_FAVOURITES_SONGS_LIST.clear()
            val columns = arrayOf(FAVDESCTEXT)
            val cursor = AudioService.instance!!.contentResolver!!.query(
                    CONTENT_URI, columns, null, null, null)
            DatabaseUtils.dumpCursorToString(cursor)
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    val cursor1 = NONUIVariables.getInstance().gmDatabase?.getFavouritesDao()!!.selectLabelTextById(cursor.getLong(0))
                    if (cursor1.moveToFirst())
                        MEDIA_FAVOURITES_SONGS_LIST.add(cursor1.getString(0))

                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    // usb favourite-------------------------------------------------------------------------USB MEDIA-------------------

    private const val FAVORITE_EXTRAS_DELIMITER = ";"
    var mCMediaPlayer: CMediaPlayer? = null
    private const val AND = " AND "
    private const val EQUAL = " = "
    val mediaBrowseFavLoad = MediaBrowseFavLoad()

    private fun processMediaFavorite(any: IBrowseItem) {
        var context: IContext = mCMediaPlayer!!.getBrowseContext(any.objectID)
        context = mCMediaPlayer!!.prepareContextForFavorite(context, null)
        val mCurrentBrowseType = SystemListener.getMediaBrowseType()
        val favType = MediaStringMatcher.getFavTypeFromBrowseChildType(mCurrentBrowseType)
        if (MEDIA_FAVOURITES_SONGS_LIST.isNotEmpty()) {
            if (MEDIA_FAVOURITES_SONGS_LIST.contains(any.objectID.toString())) {
                if (removeMediaFavorite(NO_INDEX_FOR_FAVORITE, favType, mCurrentBrowseType, any.displayText, any.objectID)) MEDIA_FAVOURITES_SONGS_LIST.remove(any.objectID.toString())
            } else {
                if (setCurrentMediaFavorite(FAVORITE_BY_STAR_ICON, favType, getContentValues(favType, any.displayText, context))) MEDIA_FAVOURITES_SONGS_LIST.add(any.objectID.toString())
            }
        } else
            if (setCurrentMediaFavorite(FAVORITE_BY_STAR_ICON, favType, getContentValues(favType, any.displayText, context))) MEDIA_FAVOURITES_SONGS_LIST.add(any.objectID.toString())

        if (NONUIVariables.getInstance().mMediaCallBack != null)
            NONUIVariables.getInstance().mMediaCallBack!!.updateAdapter()
    }


    private fun getContentValues(favType: Int, favDisplayText: String, context: IContext): ContentValues? {
        return if ((favType == GMFavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK) || (favType == FavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK))
            buildContentValueForAudioBookFavorite(favDisplayText, context)
        else
            buildContentValueForFavorite(favDisplayText, context)
    }


    private fun getContentValuesForSimulation(favtype: Int, favDisplayText: String): ContentValues? {
        return if ((favtype == GMFavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK) || (favtype == FavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK))
            buildContentValueForAudioBookFavoriteSimulation(favDisplayText)
        else
            buildContentValueForFavoriteSimulation(favDisplayText)
    }


    /*
     * This function is for saving media favorites
     */
    private fun setCurrentMediaFavorite(position: Long, favType: Int, cvTemp: ContentValues?): Boolean {
        var isSuccess = false
        //val favType = favType
        //var lMessage: String? = null
        val source = AudioSources.USBMSD
        if (cvTemp != null) {

            if (IS_SDK_ACTIVE.get()) {
                cvTemp.put(GMFavoritesContract.Favorite.Cols.FAVTYPE, favType)
                if (position >= 0) {
                    cvTemp.put(GMFavoritesContract.Favorite.Cols.FAVORDER, position)
                }
                cvTemp.put(GMFavoritesContract.Favorite.Cols.FAVPACKAGE, source.name)
                cvTemp.put(GMFavoritesContract.Favorite.Cols.FAVCLASS,
                        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEID.get())
            } else {
                cvTemp.put(FavoritesContract.Cols.FAVTYPE, favType)
                if (position >= 0) {
                    cvTemp.put(FavoritesContract.Cols.FAVORDER, position)
                }
                cvTemp.put(FavoritesContract.Cols.FAVPACKAGE, NOWPLAYING_SOURCE_TYPE.USBMSD.name)
                cvTemp.put(FavoritesContract.Cols.FAVCLASS,
                        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEID.get())
            }

            var lMessage: String? = null
            val uri: Uri?
            val favLabelText: String
            try {
                if (IS_SDK_ACTIVE.get()) {
                    uri = AudioService.instance!!.contentResolver!!.insert(GMFavoritesContract.Favorite.CONTENT_URI, cvTemp)
                    favLabelText = GMFavoritesContract.Favorite.Cols.FAVLABELTEXT

                } else {
                    uri = AudioService.instance!!.contentResolver!!.insert(FavoritesContract.Favorites.CONTENT_URI, cvTemp)
                    favLabelText = FavoritesContract.Cols.FAVLABELTEXT
                }
                if (null != uri) {
                    lMessage = AudioService.instance!!.resources.getString(R.string.audio_saved_as_a_favorite,
                            cvTemp.get(favLabelText))
                    isSuccess = true
                }
            } catch (e: Exception) {
                lMessage = AudioService.instance!!.resources.getString(R.string.audio_favorite_remove_error_content)
                isSuccess = false
            }

            if (null != lMessage)
                SystemListener.showQuickNotice(lMessage)

        }
        return isSuccess
    }


    /**
     * Build the ContentValue for saving the favorite item in the database.
     *
     * @param labelText
     * @param iContext
     * @return ContentValues
     */

    private fun buildContentValueForFavorite(labelText: String, iContext: IContext?): ContentValues? {
        var values: ContentValues? = null
        if (iContext != null) {
            values = ContentValues()
            values.put(GMFavoritesContract.Favorite.Cols.FAVLABELTEXT, labelText)
            values.put(GMFavoritesContract.Favorite.Cols.FAVDESCTEXT, labelText)
            val contextJason = StringBuffer(iContext.toJSONString())
            contextJason.append(FAVORITE_EXTRAS_DELIMITER + true)
            values.put(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA, contextJason.toString())
        }
        return values
    }


    private fun buildContentValueForAudioBookFavorite(labeltext: String, iContext: IContext): ContentValues? {
        val values = buildContentValueForFavorite(labeltext, iContext)

        if (values != null) {
            val contextJason = StringBuffer(values.getAsString(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA))
            // add the audiobook flag as part of the extra
            contextJason.append(FAVORITE_EXTRAS_DELIMITER + "audiobook=true")
            values.put(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA, contextJason.toString())
        }

        return values
    }

    /**
     * Build the ContentValue for saving the favorite item in the database.     *
     * @param labelText     *
     * @return ContentValues
     */

    private fun buildContentValueForFavoriteSimulation(labelText: String): ContentValues? {
        val values: ContentValues?
        values = ContentValues()
        values.put(FavoritesContract.Cols.FAVLABELTEXT, labelText)
        values.put(FavoritesContract.Cols.FAVDESCTEXT, labelText)
        values.put(FavoritesContract.Cols.FAVEXTRADATA, "")
        return values
    }

    private fun buildContentValueForAudioBookFavoriteSimulation(labelText: String): ContentValues? {
        val values = buildContentValueForFavoriteSimulation(labelText)
        values?.put(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA, "")
        return values
    }

    private fun removeMediaFavorite(position: Int, favType: Int,
                                    browseType: EnumBrowseChildType?, favLabel: String, lObjectID: Long): Boolean {//, browseItem: IBrowseItem?
        var isSuccess = false
        if (IS_SDK_ACTIVE.get()) {
            if (position == NO_INDEX_FOR_FAVORITE) {
                if (favLabel.isNotEmpty()) {
                    var favoritesItemsRemovedCount = 0
                    val favoriteRemovedMessage: String?
                    //val favLabel = browseItem.displayText
                    val source = AudioSources.USBMSD
                    val columns = arrayOf(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA)
                    val selection = StringBuilder()
                            .append(GMFavoritesContract.Favorite.Cols.FAVPACKAGE).append(EQUAL)
                            .append(DatabaseUtils.sqlEscapeString(source.name)).append(AND)
                            .append(GMFavoritesContract.Favorite.Cols.FAVCLASS).append(EQUAL)
                            .append(DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEID.get().toString()).append(AND)
                            .append(GMFavoritesContract.Favorite.Cols.FAVTYPE).append(EQUAL)
                            .append(favType.toString()).append(AND)
                            .append(GMFavoritesContract.Favorite.Cols.FAVDESCTEXT).append(EQUAL)
                            .append(DatabaseUtils.sqlEscapeString(favLabel))
                    val cursor = AudioService.instance!!.contentResolver!!.query(
                            GMFavoritesContract.Favorite.CONTENT_URI, columns, selection.toString(), null, null)
                    if (cursor != null) {
                        while (cursor.moveToNext()) {
                            val favExtraData = cursor.getString(
                                    cursor.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA))
                            val iContext = mCMediaPlayer!!.createContextFromJSONString(favExtraData)
                            var lStoredID: Long = -1
                            if (iContext != null) {
                                lStoredID = iContext.storedID
                            }
                            //   val lObjectID = browseItem.objectID
                            if (browseType == EnumBrowseChildType.iTunesRadio || (lStoredID.toInt() != -1 && lStoredID == lObjectID)) {
                                val delSelection = StringBuilder()
                                        .append(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA).append(EQUAL)
                                        .append(DatabaseUtils.sqlEscapeString(favExtraData))
                                favoritesItemsRemovedCount = AudioService.instance!!.contentResolver!!.delete(GMFavoritesContract.Favorite.CONTENT_URI,
                                        delSelection.toString(), null)
                                break
                            }
                        }
                        cursor.close()
                    }
                    if (favoritesItemsRemovedCount > 0) {
                        favoriteRemovedMessage = AudioService.instance!!.resources.getString(R.string.audio_deleted_as_a_favorite,
                                favLabel)
                        isSuccess = true
                    } else {
                        favoriteRemovedMessage = AudioService.instance!!.resources
                                .getString(R.string.audio_favorite_remove_error_content)
                        isSuccess = false
                    }
                    SystemListener.showQuickNotice(favoriteRemovedMessage)
                }
            }
        } else {
            val columns = arrayOf(FAVLABELTEXT)
            val cursor = AudioService.instance!!.contentResolver.query(
                    CONTENT_URI, columns, "$FAVLABELTEXT=?", arrayOf(favLabel), null)
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    val count = AudioService.instance!!.contentResolver!!.delete(ContentUris.withAppendedId(CONTENT_URI, cursor.getString(0).toLong()),
                            null, null)
                    if (count > 0) {
                        val favoriteRemovedMessage = AudioService.instance!!.resources.getString(R.string.audio_deleted_as_a_favorite,
                                favLabel)
                        isSuccess = true
                        SystemListener.showQuickNotice(favoriteRemovedMessage)
                    }
                }
            }
        }
        return isSuccess
    }

    private fun setFavoriteForCurrentSong(displayText: String): Boolean {
        var isSuccess = false
        try {
            if (IS_SDK_ACTIVE.get()) {
                var context: IContext = mCMediaPlayer!!.playContext
                context = mCMediaPlayer!!.prepareContextForFavorite(context, EnumFavoriteType.Song)
                isSuccess = setCurrentMediaFavorite(FAVORITE_BY_STAR_ICON, GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG, getContentValues(GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG, displayText, context))
            } else
                isSuccess = setCurrentMediaFavorite(FAVORITE_BY_STAR_ICON, FavoritesContract.FAV_TYPE.FT_MEDIA_SONG, getContentValuesForSimulation(FavoritesContract.FAV_TYPE.FT_MEDIA_SONG, displayText))

        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.v(mTAG!!, "ExceptionPlayFunctionNotEnabled during Media play context")
        } catch (e: Exception) {
            Log.v(mTAG!!, "ExceptionPlayFunctionNotEnabled during Media play context")
        }
        return isSuccess
    }

    class MediaBrowseFavLoad : Loader.OnLoadCompleteListener<Cursor> {
        override fun onLoadComplete(loader: Loader<Cursor>, data: Cursor?) {
            loader.unregisterListener(this)
            fetchFavoriteList(data)
            if (NONUIVariables.getInstance().mMediaCallBack != null)
                NONUIVariables.getInstance().mMediaCallBack!!.updateAdapter()
        }

        fun loadFavourite() {
            val mCursorLoaderThread = object : HandlerThread("BrowseFavCursorLoaderThread") {
                override fun onLooperPrepared() {
                    queryFavoriteDB()
                }
            }
            mCursorLoaderThread.start()
        }


        fun queryFavoriteDB() {
            val mCurrentBrowseType = SystemListener.getMediaBrowseType()
            val lSelection = StringBuilder(GMFavoritesContract.Favorite.Cols.FAVPACKAGE)
            lSelection.append("= ? AND ")
            lSelection.append(GMFavoritesContract.Favorite.Cols.FAVCLASS)
            lSelection.append("= ? AND ")
            lSelection.append(GMFavoritesContract.Favorite.Cols.FAVTYPE)
            lSelection.append("= ?")
            val lSelectionArgs = arrayOf(AudioSources.USBMSD.name, DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEID.get().toString(), "")

            val lProjection = arrayOf(GMFavoritesContract.Favorite.Cols.FAVTYPE, GMFavoritesContract.Favorite.Cols.FAVDESCTEXT, GMFavoritesContract.Favorite.Cols.FAVEXTRADATA, GMFavoritesContract.Favorite.Cols.FAVORDER)
            when (mCurrentBrowseType) {
                EnumBrowseChildType.Songs, EnumBrowseChildType.iTunesRadio -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG.toString()
                EnumBrowseChildType.Albums -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_ALBUM.toString()
                EnumBrowseChildType.Artists -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_ARTIST.toString()
                EnumBrowseChildType.Genres -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_GENRE.toString()
                EnumBrowseChildType.Audiobooks, EnumBrowseChildType.Chapters -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_AUDIO_BOOK.toString()
                EnumBrowseChildType.Composers -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_COMPOSER.toString()
                EnumBrowseChildType.Episodes -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_SONG.toString()
                EnumBrowseChildType.Playlists -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_PLAYLIST.toString()
                EnumBrowseChildType.FolderInfo -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_FOLDER.toString()
                EnumBrowseChildType.Podcasts -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_PODCAST.toString()
                EnumBrowseChildType.Videos -> lSelectionArgs[2] = GMFavoritesContract.FAV_TYPE.FT_MEDIA_VIDEO.toString()
                else -> {
                }
            }
            val mCursorLoader = CursorLoader(AudioService.instance!!, GMFavoritesContract.Favorite.CONTENT_URI, lProjection,
                    lSelection.toString(), lSelectionArgs, null)
            mCursorLoader.registerListener(1, this)
            mCursorLoader.startLoading()
        }

        private fun fetchFavoriteList(aCurrentFavorite: Cursor?) {
            val mCurrentBrowseType = SystemListener.getMediaBrowseType()
            if (null != aCurrentFavorite) {
                MEDIA_FAVOURITES_SONGS_LIST.clear()
                if (aCurrentFavorite.count > 0) {
                    aCurrentFavorite.moveToFirst()
                    while (!aCurrentFavorite.isAfterLast) {
                        //Changed the logic to Compare Context JsonString to handle the favorite case
                        val context = aCurrentFavorite.getString(aCurrentFavorite.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVEXTRADATA))
                        val favOrder = Integer.parseInt(aCurrentFavorite.getString(aCurrentFavorite.getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVORDER)))
                        val favDescText = aCurrentFavorite.getString(aCurrentFavorite
                                .getColumnIndex(GMFavoritesContract.Favorite.Cols.FAVDESCTEXT))
                        Log.v(mTAG!!, "context :" + context + "   favOrder=" + favOrder
                                + " favDescText : " + favDescText)
                        if (favDescText != null && mCurrentBrowseType === EnumBrowseChildType.iTunesRadio) {
                            Log.v(mTAG, "Current child is iTunesRadio : favOrder : " + favOrder
                                    + " favDescText : " + favDescText)
                            MEDIA_FAVOURITES_SONGS_LIST.add(favDescText)
                        } else {
                            if (null != context) {
                                try {
                                    val iContext = mCMediaPlayer!!.createContextFromJSONString(context)
                                    MEDIA_FAVOURITES_SONGS_LIST.add(iContext.storedID.toString())
                                } catch (e: ExceptionInvalidStringVersion) {
                                    Log.d(mTAG,
                                            "ExceptionInvalidStringVersion : Media create Context from JSONString ",
                                            e)
                                }
                            }
                        }
                        aCurrentFavorite.moveToNext()
                    }
                }
            }
        }
    }

    private fun updateIndexByOne(c: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(c)
        var inxex = getIndex(c)
        inxex += 1
        prefs.edit().putLong("inxeddddddd", inxex).apply()
    }

   private fun getIndex(c: Context): Long {
        val prefs = PreferenceManager.getDefaultSharedPreferences(c)
        return prefs.getLong("inxeddddddd", 0)
    }
}
