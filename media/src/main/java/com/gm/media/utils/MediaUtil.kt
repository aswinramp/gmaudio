package com.gm.media.utils

import android.content.Intent
import android.net.Uri
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.mock.MediaPlayerService
import com.gm.media.models.EventObject
import com.gm.media.models.MEDIA_PLAYER_STATE

/**
 * Common class used in all places
 */
object MediaUtil {



    /**
     * process the event corresponding to given eventName
     * @param eventObject passing which name of event has to perform.
     *
     */
    fun processMedia(eventObject: EventObject?) {
        val intent = Intent(AudioService.instance!!, MediaPlayerService::class.java)
        when (eventObject!!.eventName) {
            MEDIA_PLAYER_STATE.INIT.name,
            MEDIA_PLAYER_STATE.PLAY.name -> {
                intent.data = eventObject.obj as Uri
                intent.putExtra("Action", eventObject.eventName)
            }
            MEDIA_PLAYER_STATE.PAUSE.name,
            MEDIA_PLAYER_STATE.PLAYPAUSE.name,
            MEDIA_PLAYER_STATE.REMOVECALLBACKS.name,
            MEDIA_PLAYER_STATE.UPDATE_PROGRESS.name,
            MEDIA_PLAYER_STATE.STOP.name,
            MEDIA_PLAYER_STATE.DESTROY_SERVICE.name -> {
                intent.putExtra("Action", eventObject.eventName)
            }
            MEDIA_PLAYER_STATE.SEEKTO.name -> {
                intent.putExtra("Action", eventObject.eventName)
                intent.putExtra("SEEKTO", eventObject.obj as Int)
            }
        }
        AudioService.instance!!.startService(intent)
    }


}