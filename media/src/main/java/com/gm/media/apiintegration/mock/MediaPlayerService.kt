package com.gm.media.apiintegration.mock

import android.app.Service
import android.content.Intent
import android.media.AudioManager
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Environment
import android.os.PowerManager
import android.text.TextUtils
import com.gm.media.R
import com.gm.media.apiintegration.SystemListener
import com.gm.media.database.MediaData
import com.gm.media.models.*
import com.gm.media.utils.Log
import java.io.File
import java.io.IOException
import java.util.*


class MediaPlayerService : Service(), MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {

    private val TAG = MediaPlayerService::class.java.simpleName

    private val SEEK_FAST_FORWARD_INTERVAL = 5
    private val SEEK_FAST_BACKWARD_INTERVAL = -5
    private val rand = Random()

    private var seekEnable = -1
    private var canPlayNext = true
    private var mPlayer: MediaPlayer? = null
    private var mediaTypeTag: Any? = null
    private var mHandler = android.os.Handler()
    private var shuffleIndex: Int = 0
    private var MEDIA_SHUFFLING_LIST = ArrayList<Int>()


    override fun onBind(intent: Intent) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, " onStartCommand ... $intent")
        try {
            if (intent != null && intent.hasExtra("Action")) { //if open brace
                when (intent.getStringExtra("Action")) {
                    MEDIA_PLAYER_STATE.INIT.name -> {
                        setMediaTag(NOWPLAYING_SOURCE_TYPE.USBMSD.ordinal)
                        initMediaPlayerIfNeeded()
                        if (null != intent.data && !TextUtils.isEmpty(intent.data!!.toString()))
                            startPlayer(intent.data)
                    }
                    "loadData" -> {
                        getMediaFilesList()
                    }
                    MEDIA_PLAYER_STATE.PLAYPAUSE.name -> {
                        togglePlayPause()
                    }
                    MEDIA_PLAYER_STATE.STOP.name -> {
                        stopPlayer()
                    }
                    MEDIA_PLAYER_STATE.UPDATE_PROGRESS.name -> {
                        progressUpdate()
                    }
                    MEDIA_PLAYER_STATE.SEEKTO.name -> {
                        seekTo(intent.extras!!.getInt("SEEKTO"))
                    }
                    MEDIA_PLAYER_STATE.PLAY.name -> {
                        setPlayerState(MEDIA_PLAYER_STATE.PLAY.name)
                        initMediaPlayerIfNeeded()
                        if (null != intent.data && !TextUtils.isEmpty(intent.data!!.toString()))
                            startPlayer(intent.data)
                    }
                    MEDIA_PLAYER_STATE.PAUSE.name -> {
                        pausePlayer()
                    }
                    MEDIA_PLAYER_STATE.REMOVECALLBACKS.name ->
                    {
                        removeCallbacks()
                    }
                    MEDIA_PLAYER_STATE.DESTROY_SERVICE.name -> {
                        destroyPlayer()
                    }
                    "onPlayPrevious" -> {
                        onPlayPrevious()
                    }
                    "onPlayNext" -> {
                        onPlayNext()
                    }
                    "fastForwardFastBackward" -> {
                        val forwardSeek = intent.getBooleanExtra("forwardSeek", false)
                        if (forwardSeek)
                            fastForwardFastBackward(SEEK_FAST_FORWARD_INTERVAL, forwardSeek)
                        else
                            fastForwardFastBackward(SEEK_FAST_BACKWARD_INTERVAL, forwardSeek)
                    }
                }
            } // if close brace

        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        return START_NOT_STICKY
    }

    private fun getMediaFilesList() {
        Log.d(TAG, " getMediaFilesList ... ")
        val externalStorageRoot = Environment.getExternalStorageDirectory()
        NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST.clear()
        displayDirectoryContents(externalStorageRoot)
        NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST.sortWith(Comparator { media1, media2 ->
            media1.fileName.compareTo(media2.fileName)
        })
        if (NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST.size != 0) {
            SystemListener.onMEDIA_RES_DEVICE_MOUNTED_STATUS(true)
            SystemListener.onMEDIA_RES_BROWSEBUTTONSUPPORT(true)
        }

        NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST = NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST

        if (NONUIVariables.getInstance().playingUsbIndex <= 0) {
            val station = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex]
            SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, station.albumArt, station.fileName, station.objectId, station.playTime, station.metaDataGenre, station.metaDataArtist, station.metaDataAlbum, station.metaDataSong, station.metaDataComposers, station.categoryType))
            playSelectedSong(Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong)))
        }
    }

    /**
     * To get data mp3 files from External storage from specific directory
     * @param dir File or folder path
     */
    private fun displayDirectoryContents(dir: File) {
        Log.d(TAG, " getMediaFilesList ... ")
        var fileName: String?
        var metaDataAlbum: String?
        var metaDataArtist: String?
        var metaDataGenre: String?
        var remainingTime: String?
        var mediaPlayTime: MediaPlayTime_t?
        var composers: String?
        var compilation: String?

        try {
            val files = dir.listFiles()

            for (file in files!!) {
                if (file.isDirectory) {
                    println("Directory Name==>:" + file.canonicalPath)
                    displayDirectoryContents(file)
                } else {

                    if (file.name.endsWith(".mp3") || file.name.endsWith(".MP3")) {

                        val metaRetriver = MediaMetadataRetriever()
                        metaRetriver.setDataSource(file.absolutePath)
                        try {
                            val mediaData = MediaData()
                            val mediaObject = MediaObject_t(false, "", "", objectId = 0, playTime = MediaPlayTime_t(remainingTime = 0, elapsedTime = 0), metaDataGenre = "", metaDataAlbum = "", metaDataArtist = "", metaDataSong = "", categoryType = eMediaCategoryType.MEDIA_CTY_ALBUM)
                            fileName = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
                            metaDataAlbum = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM)
                            metaDataArtist = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)
                            metaDataGenre = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE)
                            remainingTime = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
                            composers = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_COMPOSER)
                            compilation = metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_COMPILATION)

                            Log.i(TAG, "duration::::", fileName + "::::::" + remainingTime.toString())

                            if (fileName == null || fileName.length < 0) fileName = resources.getString(R.string.unknown_song)
                            if (metaDataAlbum == null || metaDataAlbum.length < 0) metaDataAlbum = resources.getString(R.string.unknown_album)
                            if (metaDataArtist == null || metaDataArtist.length < 0) metaDataArtist = resources.getString(R.string.unknown_artist)
                            if (metaDataGenre == null || metaDataGenre.length < 0) metaDataGenre = resources.getString(R.string.unknown_genre)
                            compilation = if (compilation == null || compilation.length < 0) resources.getString(R.string.unknown_comilation) else metaDataAlbum
                            if (composers == null || composers.length < 0) composers = resources.getString(R.string.unknown_composer)
                            if (remainingTime == null || remainingTime.length < 0) remainingTime = "0"

                            mediaPlayTime = MediaPlayTime_t(remainingTime.toInt() / 1000, elapsedTime = 0)
                            mediaObject.playTime = mediaPlayTime
                            mediaObject.metaDataSong = file.absolutePath
                            mediaObject.metaDataAlbum = metaDataAlbum
                            mediaObject.metaDataArtist = metaDataArtist
                            mediaObject.metaDataGenre = metaDataGenre
                            mediaObject.fileName = fileName

                            mediaData.albumArt = fileName
                            mediaData.filename = fileName
                            mediaData.genre = metaDataGenre
                            mediaData.artist = metaDataArtist
                            mediaData.album = metaDataAlbum
                            mediaData.filepath = file.absolutePath
                            mediaData.compilations = compilation!!
                            mediaData.composers = composers

                            NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST.add(mediaObject)

                            val dbData = NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAll()
                            var isDataAvailable: Boolean? = false
                            if (dbData != null && dbData.isNotEmpty()) {
                                for (data in dbData) {
                                    if (data.filename == mediaData.filename)
                                        isDataAvailable = true

                                }
                                if (isDataAvailable == true)
                                else
                                    NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.insert(mediaData)

                            } else {
                                NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.insert(mediaData)
                            }

                        } catch (e: java.lang.Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun startPlayer(mediaUri: Uri?) {
        Log.d(TAG, " startPlayer ... $mediaUri")
        try {
            if (mediaUri != null) {
                mPlayer!!.reset()
                mPlayer!!.setDataSource(this, mediaUri)
                setPlayerState(MEDIA_PLAYER_STATE.PREPARING.name)
                mPlayer!!.prepare()
                mPlayer!!.start()
                canPlayNext = true
                NONUIVariables.getInstance().playTimeDuration = getDuration() / 1000
                progressUpdate()
            }
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    private fun togglePlayPause() {
        Log.d(TAG, " togglePlayPause ... ")
        try {
            if (isPlaying()) {
                setPlayerState(MEDIA_PLAYER_STATE.PAUSE.name)
                mPlayer!!.pause()
                removeCallbacks()
            } else {
                setPlayerState(MEDIA_PLAYER_STATE.PLAY.name)
                mPlayer!!.start()
                progressUpdate()
            }
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onPrepared(p0: MediaPlayer?) {
        Log.d(TAG, " onPrepared ... $p0")
        try {
            setPlayerState(MEDIA_PLAYER_STATE.PLAY.name)
            if (seekEnable != -1) {
                seekTo(seekEnable)
                seekEnable = -1
            }
            mPlayer!!.start()
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onError(p0: MediaPlayer?, p1: Int, p2: Int): Boolean {
        Log.d(TAG, " onError ... $p0")
        stopPlayer()
        return true
    }

    override fun onCompletion(p0: MediaPlayer?) {
        Log.d(TAG, " onCompletion ... $p0")
        try {
            if (mPlayer!!.currentPosition > 0) {
                mPlayer!!.reset()
                if (!NONUIVariables.getInstance().isFastBackEnabled) {
                    if (canPlayNext)
                        startPlayer(getNextUri(mediaTypeTag))
                    else
                        stopPlayer()
                } else
                    setPlayerState(MEDIA_PLAYER_STATE.STOP.name)
            }

        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    private fun initMediaPlayerIfNeeded() {
        Log.d(TAG, " initMediaPlayerIfNeeded ... ")
        if (mPlayer != null)
            return
        mPlayer = MediaPlayer().apply {
            setWakeMode(applicationContext, PowerManager.PARTIAL_WAKE_LOCK)
            setAudioStreamType(AudioManager.STREAM_MUSIC)
            setOnPreparedListener(this@MediaPlayerService)
            setOnCompletionListener(this@MediaPlayerService)
            setOnErrorListener(this@MediaPlayerService)
        }
    }

    private fun setPlayerState(state: String) {
        Log.d(TAG, " setPlayerState ... $state")
        NONUIVariables.getInstance().mState = state
    }

    private fun setMediaTag(mediaTypeTag: Int) {
        Log.d(TAG, " setMediaTag ... $mediaTypeTag")
        this.mediaTypeTag = mediaTypeTag
    }

    /**
     * To skip 10sec of playtime when fastforward  action occured
     * @param time 10sec of time to skip
     */
    private fun seekTo(time: Int) {
        Log.d(TAG, " seekTo ... $time")
        try {
            if (NONUIVariables.getInstance().mState == MEDIA_PLAYER_STATE.PLAY.name)
                mPlayer!!.seekTo(time * 1000)
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * To update progressbar while playing song
     */
    private fun progressUpdate() {
        Log.d(TAG, " progressUpdate ... ")
        try {
            mHandler.postDelayed(mUpdateTimeTask, 0)
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * Playing next item in the List when [eMediaPBAction.MEDIA_PBA_NEXT] event occur.
     */
    private fun onPlayNext() {
        Log.d(TAG, " onPlayNext... ")
        playNextUsbSong()
        val station = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex]
        SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, station.albumArt, station.fileName, station.objectId, station.playTime, station.metaDataGenre, station.metaDataArtist, station.metaDataAlbum, station.metaDataSong, station.metaDataComposers, station.categoryType))
    }

    /**
     * playing next item from the list.
     */
    private fun playNextUsbSong() {
        try {
            if (NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size == 0) return
            if (NONUIVariables.getInstance().MEDIA_PLAYBACKMODE != eMediaPBMode.MEDIA_PBM_RANDOM.ordinal) {
                NONUIVariables.getInstance().playingUsbIndex++
                if (NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size <= NONUIVariables.getInstance().playingUsbIndex) {
                    NONUIVariables.getInstance().playingUsbIndex = 0
                }
            } else {
                NONUIVariables.getInstance().playingUsbIndex = shufflePlayList()
            }
            playSelectedSong(Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong)))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Playing next item in the List when [eMediaPBAction.MEDIA_PBA_PREV] event occur.
     */
    private fun onPlayPrevious() {
        Log.d(TAG, " onPlayPrevious... ")
        try {
            val currentPlayerPosition = (getCurrentPosition().toFloat() / (1000))
            if (currentPlayerPosition < 5)
                playPreviousUsbSong(true)
            else if (currentPlayerPosition > 5)
                playPreviousUsbSong(false)

            val station = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex]
            SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, station.albumArt, station.fileName, station.objectId, station.playTime, station.metaDataGenre, station.metaDataArtist, station.metaDataAlbum, station.metaDataSong, station.metaDataComposers, station.categoryType))

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * playing previous item when item played duration is <5sec.
     * If item played duration is more than 5sec then it plays from beginning of the item
     * @param playNext if true, next song will play else song will play from beginning
     */
    private fun playPreviousUsbSong(playNext: Boolean) {
        if (NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size == 0) return
        try {
            if (playNext) {
                if (NONUIVariables.getInstance().MEDIA_PLAYBACKMODE != eMediaPBMode.MEDIA_PBM_RANDOM.ordinal) {
                    NONUIVariables.getInstance().playingUsbIndex--
                    if (0 > NONUIVariables.getInstance().playingUsbIndex) {
                        NONUIVariables.getInstance().playingUsbIndex = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size - 1
                    }
                } else {
                    if (MEDIA_SHUFFLING_LIST.size > 0) {
                        if (shuffleIndex != 0) {
                            NONUIVariables.getInstance().playingUsbIndex = MEDIA_SHUFFLING_LIST[--shuffleIndex]
                            Log.d("shuffle previous::::", shuffleIndex.toString() + ":::::" + NONUIVariables.getInstance().playingUsbIndex)
                        } else {
                            shuffleIndex = MEDIA_SHUFFLING_LIST.size
                            playPreviousUsbSong(true)
                        }
                    }
                }
            }
            playSelectedSong(Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong)))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     *  common method for play the songs
     */
    private fun playSelectedSong(uri: Uri) {
        setPlayerState(MEDIA_PLAYER_STATE.PLAY.name)
        initMediaPlayerIfNeeded()
        startPlayer(uri)
    }

    /**
     * To get Current positions of Media player
     * @return returns current Media player position
     */
    private fun getCurrentPosition(): Int {
        Log.d(TAG, " getCurrentPosition ... ")
        return mPlayer!!.currentPosition
    }

    /**
     * To get Current positions of Mediaplayer
     * @return returns current Mediaplayer position
     */
    private fun getDuration(): Int {
        Log.d(TAG, " getDuration ... ")
        return mPlayer!!.duration
    }

    /**
     * Background Runnable thread to update progressbar .
     * for every second this thread will called
     */
    private val mUpdateTimeTask = object : Runnable {
        override fun run() {
            try {
                Log.d(TAG, " mUpdateTimeTask ... ")
                if (NONUIVariables.getInstance().mState == MEDIA_PLAYER_STATE.PLAY.name) {
                    SystemListener.onMEDIA_RES_PLAYTIME(MediaPlayTime_t(
                            (getDuration() - getCurrentPosition()) / 1000,
                            getCurrentPosition() / 1000))
                    mHandler.postDelayed(this, 0)
                }
            } catch (e: Exception) {
                Log.e(TAG, e.printStackTrace().toString())
            }
        }
    }

    private fun pausePlayer() {
        try {
            Log.d(TAG, " pausePlayer ... ")
            if (NONUIVariables.getInstance().mState == MEDIA_PLAYER_STATE.PLAY.name) {
                setPlayerState(MEDIA_PLAYER_STATE.PAUSE.name)
                mPlayer!!.pause()
                removeCallbacks()
            }
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * This method is used to stop the player
     */
    private fun stopPlayer() {
        try {
            Log.d(TAG, " stopPlayer ... ")
            if (null != mPlayer) {
                setPlayerState(MEDIA_PLAYER_STATE.STOP.name)
                mPlayer!!.stop()
                removeCallbacks()
            }
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * removing callbacks when fastforward or fastbackward events occured
     */
    private fun removeCallbacks() {
        Log.d(TAG, " removeCallbacks ... ")
        try {
            mHandler.removeCallbacksAndMessages(null)
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * This method is used to destroy the player
     */
    private fun destroyPlayer() {
        Log.d(TAG, " destroyPlayer ... ")
        try {
            if (null != mPlayer) {
                mPlayer?.stop()
                removeCallbacks()
                mPlayer?.release()
                mPlayer = null
            }
            stopForeground(true)
            stopSelf()
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onDestroy() {
        Log.d(TAG, " onDestroy ... ")
        super.onDestroy()
        destroyPlayer()
    }

    private fun isPlaying() = mPlayer?.isPlaying == true

    private fun shufflePlayList(): Int {
        Log.d(TAG, " shufflePlayList ... ")
        val temp = rand.nextInt(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size)
        MEDIA_SHUFFLING_LIST.add(temp)
        shuffleIndex = MEDIA_SHUFFLING_LIST.size
        return temp
    }

    private fun getNextUri(mediaTypeTag: Any?): Uri? {
        Log.d(TAG, " getNextUri ... $mediaTypeTag")
//        if (mediaTypeTag is Int && NOWPLAYING_SOURCE_TYPE.USBMSD.ordinal == mediaTypeTag) {
        if (NONUIVariables.getInstance().MEDIA_PLAYBACKMODE == eMediaPBMode.MEDIA_PBM_RANDOM.ordinal) {
            NONUIVariables.getInstance().playingUsbIndex = shufflePlayList()
        } else {
            NONUIVariables.getInstance().playingUsbIndex++
        }
        if (NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size <= NONUIVariables.getInstance().playingUsbIndex) {
            NONUIVariables.getInstance().playingUsbIndex = 0
        }
        val station = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex]
        SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, station.albumArt, station.fileName, station.objectId, station.playTime, station.metaDataGenre, station.metaDataArtist, station.metaDataAlbum, station.metaDataSong, station.metaDataComposers, station.categoryType))
        SystemListener.onMEDIA_RES_PLAYTIME(station.playTime)
        return Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong))
        //  }
//        return null
    }


    /**
     * Method to play item in Fast forward mode/Reverse mode
     * @param seekTime play song with stepSize of seekTime
     * @param forwardSeek checking whether seek is forward or backward
     */
    private fun fastForwardFastBackward(seekTime: Int, forwardSeek: Boolean) {
        Log.d(TAG, " fastForwardFastBackward...  $seekTime,$forwardSeek")
        try {
            val mediaObject: MediaObject_t = NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!
            var index = -1
            for (c in 0 until NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size) { //for loop open
                if (NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[c].metaDataSong == NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!.metaDataSong) {
                    index = c
                    break
                }
            }
            if (forwardSeek) {
                if (index <= NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size - 1 && NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME < NONUIVariables.getInstance().playTimeDuration) {
                    canPlayNext = index != NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size - 1
                    removeCallbacks()
                    mediaObject.playTime.elapsedTime = NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME + seekTime
                    if (mediaObject.playTime.elapsedTime > NONUIVariables.getInstance().playTimeDuration)
                        mediaObject.playTime.elapsedTime = NONUIVariables.getInstance().playTimeDuration
                    mediaObject.playTime.remainingTime = NONUIVariables.getInstance().playTimeDuration - mediaObject.playTime.elapsedTime
                    SystemListener.onMEDIA_RES_NOWPLAYING(mediaObject)
                    SystemListener.onMEDIA_RES_PLAYTIME(mediaObject.playTime)
                    seekTo(mediaObject.playTime.elapsedTime)
                } else if (index == NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size - 1) {//&&  mediaObject_t.playTime.remainingTime==0
                    pausePlayer()
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PAUSE)
                    mediaObject.isPlaying = false
                }
            } else {
                if (index == 0 && NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME == 0) {//if playing item number is 0 and elapsedtime is zero, then current playing state is pause
                    pausePlayer()
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PAUSE)
                    mediaObject.isPlaying = false
                } else if (NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME > 0) {//performing reverse playback for 10 sec of current song
                    removeCallbacks()
                    mediaObject.playTime.elapsedTime = NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME + seekTime
                    if (mediaObject.playTime.elapsedTime < 0)
                        mediaObject.playTime.elapsedTime = 0
                    mediaObject.playTime.remainingTime = NONUIVariables.getInstance().playTimeDuration - mediaObject.playTime.elapsedTime
                    SystemListener.onMEDIA_RES_NOWPLAYING(mediaObject)
                    SystemListener.onMEDIA_RES_PLAYTIME(mediaObject.playTime)
                    if (NONUIVariables.getInstance().mState != MEDIA_PLAYER_STATE.STOP.name)
                        seekTo(mediaObject.playTime.elapsedTime)
                    else {
                        seekEnable = mediaObject.playTime.elapsedTime
                        playSelectedSong(Uri.fromFile(File(mediaObject.metaDataSong)))
                    }
                } else {
                    //navigating to previous song
                    playSelectedSong(getPreviousUri(NOWPLAYING_SOURCE_TYPE.USBMSD.ordinal)!!)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //get previous uri
    private fun getPreviousUri(mediaTypeTag: Any?): Uri? {
        Log.d(TAG, " getPreviousUri... $mediaTypeTag")
        try {
            if (mediaTypeTag is Int && NOWPLAYING_SOURCE_TYPE.USBMSD.ordinal == mediaTypeTag) {
                NONUIVariables.getInstance().playingUsbIndex--
                val station = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex]
                val mediaPlayTime = MediaPlayTime_t(0, station.playTime.remainingTime)
                SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, station.albumArt, station.fileName, station.objectId, mediaPlayTime, station.metaDataGenre, station.metaDataArtist, station.metaDataAlbum, station.metaDataSong, station.metaDataComposers, station.categoryType))
                SystemListener.onMEDIA_RES_PLAYTIME(mediaPlayTime)
                seekEnable = station.playTime.remainingTime
                return Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

}