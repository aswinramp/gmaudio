package com.gm.media.apiintegration.apiinterfaces

import com.gm.media.models.MPRequestListFilter_t
import com.gm.media.models.MediaPlayerListReq_t
import com.gm.media.models.eMediaListReadAck


interface IMediaManagerReq {
    fun onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST(any: Any?)
    fun onMEDIA_REQ_MEDIAPLAYERLISTFILTER(any: Any?)
    /**
     *play selected item from the list of following filters:
     *Playlists, Artists, Songs, Albums, Genres, iTunes Radio, Compilations,
     *Composers, Folder, Podcasts, Audiobooks and Videos
     * @param any index of item
     */
    fun onMEDIA_REQ_PLAYITEMFROMLISTBYTAG(any: Any?)
    /**
     * To perform Play and Pause and play next , play previous, fast forward,fast backward actions
     * @param any [eMediaPBAction]
     */
    fun onMEDIA_REQ_REQUESTPLAYBACKACTION(any: Any?)
    fun onMEDIA_REQ_SEEKTO(any: Any?)
    fun onMEDIA_REQ_SETPLAYBACKMODE(any: Any?)
    fun onMEDIA_REQ_BACKTRAVERSE()
    fun onMEDIA_REQ_EXITBROWSE()
    fun onMEDIA_REQ_RESETMUSICINDEX()
    fun onMEDIA_REQ_FOLDERBACK(indexedListReq: MediaPlayerListReq_t)
    fun onMEDIA_REQ_READACK(readAck: eMediaListReadAck)
    fun onMEDIA_REQ_LISTRELOAD()
}