package com.gm.media.apiintegration

import android.app.Service
import android.content.Intent
import com.gm.media.BuildConfig
import com.gm.media.utils.Log


class AudioService : Service() {

    private val TAG = AudioService::class.java.simpleName


    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun onBind(intent: Intent) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d(TAG, " onStartCommand ... $intent")
        try {
            if (intent != null && intent.hasExtra("eventName")) {
                SystemController.execute(intent.getStringExtra("eventName"), anyData[intent.extras.getLong("eventTime")])
                anyData.remove(intent.extras.getLong("eventTime"))
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        return super.onStartCommand(intent, flags, startId)
    }

    companion object {
        var instance: AudioService? = null
        var anyData = HashMap<Long, Any?>()
        /**
         * Check if this application is running on GM's OS build
         */
        fun isSDKAvailable(): Boolean =
                android.os.Build.MANUFACTURER == BuildConfig.VENDOR
    }

}