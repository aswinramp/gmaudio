package com.gm.media.apiintegration

import com.gm.media.apiintegration.apiinterfaces.IAMFMManagerRes
import com.gm.media.apiintegration.apiinterfaces.IManager
import com.gm.media.apiintegration.mock.SimulationManager
import com.gm.media.apiintegration.sdk.SDKManager
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.utils.Log
import java.lang.reflect.InvocationTargetException

/**
 * A controller decides which platform api call must be invoked.
 *
 * @author GM on 3/9/2018.
 */
object SystemController {

    private var manager: IManager

    /**
     * Get [IManager] w.r.t to the working platform
     * @return which type of Manager functionality has to perform like Simulation or SDK  or android
     */
    private fun getSourceManager(): IManager {
        manager = when {
            AudioService.isSDKAvailable() -> {
                DataPoolDataHandler.IS_SDK_ACTIVE.set(true)
                SDKManager.getInstance()
            }
            else ->
            {
                DataPoolDataHandler.IS_SDK_ACTIVE.set(false)
                SimulationManager()
            }
        }
        return manager
    }

    /**
     * Trigger the produced event using obtained [IManager] object
     * @param event Function name based on tags from events table
     * @param any data to pass function as a parameter
     */

    fun execute(event: String, any: Any?) {
        val cls = IManager::class.java
        try {

           if (any != null) {
                val method = cls.getDeclaredMethod(event, Any::class.java)
                method.invoke(manager, any)
            }
            else {
                val method = cls.getDeclaredMethod(event)
                method.invoke(manager)
            }

        } catch (ex: InvocationTargetException) {
            Log.e(SystemController::class.java.simpleName,"An InvocationTargetException was caught!" + ex.cause!!)
        }
    }

    init {
        manager = getSourceManager()
    }
}

