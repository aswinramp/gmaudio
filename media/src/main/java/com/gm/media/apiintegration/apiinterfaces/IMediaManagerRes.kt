package com.gm.media.apiintegration.apiinterfaces

import com.gm.media.models.*

interface IMediaManagerRes {
    fun onMEDIA_RES_ACTIVEMEDIADEVICE(mediadeviceinfo: MediaDeviceInfo_t)
    fun onMEDIA_RES_INDEXINGSTATE(activedevmediaindexingstate: ActiveDevMediaIndexingState_t)
    fun onMEDIA_RES_MEDIAPLAYERINDEXEDLIST(mediaplayerlistres: MediaPlayerListRes_t)
    fun onMEDIA_RES_NOWPLAYING(mediaobject: MediaObject_t)
    fun onMEDIA_RES_PLAYBACKMODE(eMediaPBMode: eMediaPBMode)
    fun onMEDIA_RES_PLAYTIME(mediaplaytime: MediaPlayTime_t)
    fun onMEDIA_RES_PLAYBACKACTION(emediapbaction: eMediaPBAction)
    fun onMEDIA_RES_MEDIAOBJECTUNAVAILABLE()
    fun onMEDIA_RES_INDEXINGLIMITREACHED(int: Int)
    fun onMEDIA_RES_MEDIAOBJECTCONNECTIONUNAVAILABLE(int: Int)
    fun onMEDIA_RES_LISTFILTERINFO(mediaplayerlistreq: MediaPlayerListReq_t)
    fun onMEDIA_RES_SEARCHKEYBOARDMEDIALIST(mediaSearchKeyboardList_t: MediaSearchKeyboardList_t)
    fun onMEDIA_RES_MEDIAACTIONUNAVAILABLE(int: Int)
    fun onMEDIA_RES_NOWPLAYING_METADATA(mediametadata: MediaMetadata_t)
    fun onMEDIA_RES_DEVICE_MOUNTED_STATUS(enable: Boolean)
    fun onMEDIA_RES_MEDIAACTIONUNAVAILABLE_CHECK_CONNECTION(int: Int)
    fun onMEDIA_RES_SEEKTO(int: Int)
    fun onMEDIA_RES_MEDIAPLAYERSCREENTRANSITION(mediascreentransition: MediaScreenTransition_t)
    fun onMEDIA_RES_MEDIAOBJECTALBUMARTINFO(mediaalbumartinfo: MediaAlbumArtInfo_t)
    fun onMEDIA_RES_LISTBACKTRAVERSE(mediaplayerlistreq: MediaPlayerListReq_t)
    fun onMEDIA_RES_MEDIASOURCECHANGEDTOUSB()
    fun onMEDIA_RES_MEDIASOURCECHANGEDTOBT()

}