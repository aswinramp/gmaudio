package com.gm.media.apiintegration.mock

import com.gm.media.models.*

/**
 * This class is a data provider for AM/FM channel/station
 *
 * @author Aswin on 3/21/2018.
 */

object DataSourceProvider {

    var sourceData: IDataSourceProvider = JSONParser()

    /**
     *  Uses IDataSourceProvider for fetching station list , sorts on frequency in ascending order
     *
     *  @return list of AM/FM channel/station
     */
    fun nowPlayingDataList(source_TYPE: NOWPLAYING_SOURCE_TYPE?): List<AMFMStationInfo_t> {
        if (source_TYPE == NOWPLAYING_SOURCE_TYPE.AM || source_TYPE == NOWPLAYING_SOURCE_TYPE.FM)
            return sourceData.nowPlayingDataList().filter { it.rdsStationInfo == source_TYPE.name }.sortedBy {it.frequency.toDouble()}
        else
            return sourceData.nowPlayingDataList().filter { it.rdsStationInfo == source_TYPE!!.name }
    }
}