package com.gm.media.apiintegration.apiinterfaces

import gm.media.CMediaPlayer
import gm.media.interfaces.IBrowseItem
import gm.media.interfaces.IDevice

interface MediaCustomInterface {
    fun checkFragmentFunction(functionType:String)
    fun closeEvent(functionType:String)
    fun getCMediaPlayer(functionType:String,cmMediaPlayer: CMediaPlayer,iDevice: IDevice,any:Any)
    fun updateMediaData(functionType:String,iBrowseItem: IBrowseItem?,cmMediaPlayer: CMediaPlayer,iDevice: IDevice)
    fun updateAdapter()
    fun showCustomAlertDialog()
    fun dismissCustomAlertDialog()
    //fun updateMediaData(outArray: LongArray,cmMediaPlayer: CMediaPlayer,iDevice: IDevice)
    //fun checkFragmentTriggerAction(functionType:String,any:Any)
}