package com.gm.media.apiintegration.mock;

import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

import com.gm.media.apiintegration.AudioService;

import gm.media.exceptions.ExceptionDispatcherAlreadyTerminated;
import gm.media.utilities.CinemoEglHolder;

public class CinemoVideoSurfaceListener implements CinemoEglHolder.SurfaceListener {

    private static class SurfaceHolderCallback implements SurfaceHolder.Callback {

        protected CinemoEglHolder mCinemoEglHolder;
        private static final String tag = SurfaceHolderCallback.class.getSimpleName();

        public SurfaceHolderCallback(CinemoEglHolder cinemoEglHolder) {
            if(cinemoEglHolder == null) {
                throw new IllegalArgumentException("Invalid parameter");
            }
            mCinemoEglHolder = cinemoEglHolder;
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d(tag, "surfaceCreated");
            mCinemoEglHolder.onSurfaceAvailable(holder, -1, -1);
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d(tag, "surfaceDestroyed");
            try {
                mCinemoEglHolder.disableVideoPlayback();
                mCinemoEglHolder.onSurfaceDestroyed();
            } catch (ExceptionDispatcherAlreadyTerminated e) {
                e.printStackTrace();
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Log.d(tag, "surfaceChanged");
            mCinemoEglHolder.onSurfacSizeChanged(width, height);
        }

    }

    private CinemoEglHolder mEglHolder;
    private SurfaceHolderCallback _mCallback;
    private static final String TAG = CinemoVideoSurfaceListener.class.getSimpleName();
    private SurfaceView mVideoView;
    private GestureDetectorCompat mGestureDetector;
    private IGestureListener mGestureListener;

    public CinemoVideoSurfaceListener(SurfaceView videoView) {
        if (videoView == null) {
            throw new IllegalArgumentException("Video View is not valid");
        }
        mVideoView = videoView;
        initVideoView();
    }

    public void initVideoView() {
        mEglHolder = new CinemoEglHolder();
        _mCallback = new SurfaceHolderCallback(mEglHolder);
        SurfaceHolder holder = mVideoView.getHolder();
        holder.addCallback(_mCallback);
        mEglHolder.addListener(this);
        mGestureDetector = new GestureDetectorCompat(AudioService.Companion.getInstance(),
                VideoViewOnGestureListener);

        mVideoView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View arg0, MotionEvent e) {
                return mGestureDetector.onTouchEvent(e);
            }
        });
    }

    /**
     * GestureListener to handle the swipe and tap control on Video Playback
     * Fullscreen and Viewpager Sweep control.
     */
    private SimpleOnGestureListener VideoViewOnGestureListener = new SimpleOnGestureListener() {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
                Log.v(TAG, "onSingleTapConfirmed");
            mGestureListener.onTapDetected();
            return super.onSingleTapConfirmed(e);
        }

    };

    public void setGestureListener(IGestureListener listener) {
        mGestureListener = listener;
    }

    @Override
    public void onFinishRequested() {
        // TODO Auto-generated method stub
    }

    @Override
    public void onSurfaceAvailable(CinemoEglHolder display) {
        // TODO
    }

    @Override
    public void onSurfaceDestroyed(CinemoEglHolder display) {
        // TODO
    }

    @Override
    public void onSurfaceSizeChanged(CinemoEglHolder arg0, int arg1, int arg2) {
        // TODO Auto-generated method stub
    }

    private void onCinemoEglHolderSurfaceDestroyed(CinemoEglHolder display) {
            try {
                removeVideoCallback();
            } catch (ExceptionDispatcherAlreadyTerminated e) {
                    Log.d(TAG,
                            "onSurfaceDestroyed : stop() ExceptionDispatcherAlreadyTerminated");
            }
    }

    private void removeVideoCallback() {
        if (mVideoView.getHolder() != null && _mCallback != null) {
            mVideoView.getHolder().removeCallback(_mCallback);
        }
        if (mEglHolder != null) {
            mEglHolder.removeListener(this);
        }
    }

    /**
     * This method is to remove the reference from CinemoEglHolder when fragment
     * is destroyed
     */
    public void onDestroy() {
            Log.v(TAG, "onDestroy");
        setGestureListener(null);
        removeVideoCallback();
    }

    public interface IGestureListener {
        void onTapDetected();
    }
}
