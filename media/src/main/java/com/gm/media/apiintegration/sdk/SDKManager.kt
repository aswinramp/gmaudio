package com.gm.media.apiintegration.sdk

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.os.Handler
import android.text.TextUtils
import com.gm.media.R
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.apiintegration.apiinterfaces.IManager
import com.gm.media.apiintegration.apiinterfaces.MediaCustomInterface
import com.gm.media.models.*
import com.gm.media.utils.*
import com.gm.media.utils.ManualTunerController.amLowerValue
import com.gm.media.utils.ManualTunerController.amStepValue
import com.gm.media.utils.ManualTunerController.amUpperValue
import com.gm.media.utils.ManualTunerController.fmLowerValue
import com.gm.media.utils.ManualTunerController.fmStepValue
import com.gm.media.utils.ManualTunerController.fmUpperValue
import com.gm.media.utils.ManualTunerController.getEnteredFrequency
import com.gm.media.utils.ManualTunerController.getFreqTxtForDisplay
import com.gm.media.utils.ManualTunerController.getFrequency
import com.gm.media.utils.ManualTunerController.getPossibleValues
import com.gm.media.utils.ManualTunerController.isValidEntry
import com.gm.media.utils.ManualTunerController.mCurrentModeChart
import com.gm.media.utils.ManualTunerController.prepareChart
import com.gm.media.utils.ManualTunerController.updateButtons
import gm.calibrations.GIS502_TUNERHMI
import gm.calibrations.GMCalibrationsManager
import gm.drivingmode.DrivingModeManager
import gm.media.CMediaPlayer
import gm.media.enums.*
import gm.media.exceptions.*
import gm.media.interfaces.*
import gm.media.utilities.CConstants
import gm.media.utilities.MediaRunnable
import gm.tuner.*
import gm.vehicle.OnVehicleDataChangedListener
import gm.vehicle.VehicleMovement


/**
 *  It should contain AMFM_REQ, MEDIA_REQ, AUDIOMGR_REQ, DAB_REQ, SDARS_REQ only func from CAMFMProxy.cpp, CAudioManagerProxy.cpp, CMediaProxy.cpp, CSDARSProxy.cpp & CDABProxy.cpp.
 *  This file will also contain SDK calls from GM SDK.
 *  only some requests that are being used contain "AMFM_REQ" in function definition, only these should be added
 *  these funcs usually start after proxy constructor func in CAMFMProxy.cpp
 *  This should exclude all the functions under RecvData func, as the func in RecvData only corresponds to RES functions
 */
class SDKManager : TunerListener, IDeviceListener, IPlaybackListener, IManager, OnVehicleDataChangedListener {

    private val TAG = SDKManager::class.simpleName

    private var IS_USB_BROWSE_ACTIVE: Boolean = false
    private var IS_BROWSE_CLOSE_EVENT = false
    private var mTunerManager: TunerManager? = null
    private var mTunerInitializationState: Int? = 0
    private var mIDevice: IDevice? = null
    private var mCMediaPlayer: CMediaPlayer? = null
    private var isTrafficAlertPlaying: Boolean = false
    private var regionCode: RegionCodeInfo? = null
    private var mCurrentCategoryType: String? = eRDSPtyNACategory.AMFM_RDS_PTY_NA_NO_PTY.name
    private var mVehicleMovement: VehicleMovement? = null

    //create object for IAMFMManagerRes and use context
    fun registerApiCallback(mediaCallBack: MediaCustomInterface) {
        Log.d(TAG, "registerTunerListener... $mediaCallBack")
        NONUIVariables.getInstance().mMediaCallBack = mediaCallBack
    }

    /**
     * Initialises SDK Manager
     */
    init {
        Log.d(TAG, "init Block... ")
        INIT_AMFM_REQUEST()
        registerDrivingMode()
    }

    companion object {
        var INSTANCE: SDKManager? = null
        fun getInstance(): SDKManager {
            if (INSTANCE == null)
                INSTANCE = SDKManager()
            return INSTANCE!!
        }

        /**
         * IConfig is used to Configure the CMediaPlayer
         */
        val mConfig = IConfig {
            when (it) {
                IConfig.enumConfigItems.AudioZone -> EnumAudioZone.MainCabin
                IConfig.enumConfigItems.BrowseReturnEmptyContainers -> true
                IConfig.enumConfigItems.BrowseReturnUnplayables -> true
                IConfig.enumConfigItems.BrowseSupport -> true
                IConfig.enumConfigItems.CinemoLogLevel -> android.util.Log.WARN
                IConfig.enumConfigItems.DeviceSupport -> true
                IConfig.enumConfigItems.EnableBrowseArt -> true
                IConfig.enumConfigItems.EnableNowPlayingArt -> false
                IConfig.enumConfigItems.EnableTrackListArt -> true
                IConfig.enumConfigItems.EnableVideoSupport -> false
                IConfig.enumConfigItems.MediaLogLevel -> android.util.Log.INFO
                IConfig.enumConfigItems.PlaybackSupport -> true
                IConfig.enumConfigItems.ResumeOnNewTrack -> false
                else -> null
            }
        }

    }

    /**
     * It initializes TunerManager. Sets waveband of TunerManager and updating to [SystemListener]
     *
     * @throws ExceptionInInitializerError, TunerException,Exception
     */
    @Throws(Exception::class)
    private fun registerTunerListener() {
        Log.d(TAG, "registerTunerListener...")
        try {
            // If tuner manager is null then initialise it
            if (mTunerManager == null) {
                mTunerManager = TunerManager()
                // Region code for Manual tune values
                regionCode = mTunerManager?.regionCodeInfo
            }
            mTunerInitializationState = mTunerManager?.tunerAudioStatus
            tunerListenerRegistration()
        } catch (e: ExceptionInInitializerError) {
            e.printStackTrace()
        } catch (e: TunerException) {
            e.printStackTrace()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }

    }

    /**
     * Adds this class as listener for Tuner event
     * @throws TunerServiceStatusException, TunerException , Exception
     */
    private fun tunerListenerRegistration() {
        Log.d(TAG, "tunerListenerRegistration... ")
        if (mTunerManager == null) {
            Log.d(TAG, "tunerListenerRegistration...$mTunerManager")
            return
        }
        try {
            mTunerManager?.addListener(this)
        } catch (e: TunerServiceStatusException) {
            Log.e(TAG, "Exception in TunerServiceStatusException..." + e.localizedMessage)
            e.printStackTrace()
        } catch (e: TunerException) {
            Log.e(TAG, "Tuner Exception while setting tuner listener" + e.localizedMessage)
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    /************************Tuner Listener Call Back Methods*******************************/

    override fun onCurrentStationChanged(tunerStation: TunerStation?) {
        Log.d(TAG, "onCurrentStationChanged... $tunerStation")
        try {
            if (tunerStation == null) {
                Log.d(TAG, "onCurrentStationChanged...$tunerStation")
                return
            }
            when (tunerStation.getWaveband()) {
                NOWPLAYING_SOURCE_TYPE.AM.name ->
                    SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(addAMFMHardWareStation(tunerStation, NONUIVariables.getInstance().favoriteList.contains(tunerStation.frequency.toFloat().toString()),
                            eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP, tunerStation.frequency.toFloat()))
                NOWPLAYING_SOURCE_TYPE.FM.name -> {
                    val freq = tunerStation.frequency.toDouble() / 1000
                    val eTPStationStatus = if (tunerStation.isTrafficProgramme)
                        eTPStationStatus.AMFM_STATION_SUPPORTS_TP
                    else
                        eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP

                    SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(addAMFMHardWareStation(tunerStation, NONUIVariables.getInstance().favoriteList.contains(freq.toFloat().toString()),
                            eTPStationStatus, freq.toFloat()))
                }
                else -> {
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onAvailableStationListChanged(stationList: MutableList<TunerStation>?) {
        Log.d(TAG, "onAvailableStationListChanged... $stationList")
        processStationList(stationList)
    }

    override fun onStaticStationListChanged(stationList: MutableList<TunerStation>?) {
        Log.d(TAG, "onStaticStationListChanged... $stationList")
        if (stationList == null)
            return
    }

    override fun onRadioTextChanged(p0: RadioText?) {
        Log.d(TAG, "onRadioTextChanged Not Implemented... $p0")
    }

    override fun onRadioTextPlusChanged(p0: MutableList<RadioTextPlus>?) {
        Log.d(TAG, "onRadioTextPlusChanged Not Implemented... $p0")
    }

    override fun onFavoritesChanged(p0: MutableList<TunerFavorite>?) {
        Log.d(TAG, "onFavoritesChanged Not Implemented... $p0")
    }

    override fun onAutoStoreStatusChanged(autoStoreStatus: AutoStoreStatus?) {
        Log.d(TAG, "onAutoStoreStatusChanged... $autoStoreStatus")
        try {
            when { //when open brace
                autoStoreStatus!!.status == AutoStoreStatus.Status.ERROR -> {
                    SystemListener.showQuickNotice(AudioService.instance!!.getString(R.string.update_failed))
                    NONUIVariables.getInstance().audioControlDisabled = false
                }
                autoStoreStatus.status == AutoStoreStatus.Status.IN_PROGRESS -> {
                    NONUIVariables.getInstance().audioControlDisabled = true
                    when (mTunerManager?.waveband.toString()) {
                        TunerManager.Waveband.AM.name ->
                            SystemListener.onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(autoStoreStatus.getProgress())
                        TunerManager.Waveband.FM.name ->
                            SystemListener.onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(autoStoreStatus.getProgress())
                        else -> {
                        }
                    }
                }
                autoStoreStatus.status == AutoStoreStatus.Status.ABORTED -> {
                    NONUIVariables.getInstance().audioControlDisabled = false
                    when (mTunerManager?.waveband.toString()) {
                        TunerManager.Waveband.AM.name ->
                            SystemListener.onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE("")
                        TunerManager.Waveband.FM.name ->
                            SystemListener.onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE("")
                        else -> {
                        }
                    }
                }
            } //when close brace
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onHdStatusInfoChanged(p0: String?) {
        Log.d(TAG, "onHdStatusInfoChanged Not Implemented... $p0")
    }


    override fun onRdsTimeChanged(p0: RdsTime?) {
        Log.d(TAG, "onRdsTimeChanged Not Implemented... $p0")
    }

    override fun onSeekStatusChanged(p0: String?) {
        Log.d(TAG, "onSeekStatusChanged... $p0")
        when {
            p0.equals("SEEK_UP") -> {
            }
            p0.equals("SEEK_DOWN") -> {
            }
            p0.equals("SEEK_STOP") -> {
            }
        }
    }

    /*SDK Response Listener Implementation*/
    override fun onRdsStateChanged(p0: Boolean) {
        Log.d(TAG, "onRdsStateChanged Not Implemented... $p0")
    }

    /************************Tuner Listener Call Back Methods*******************************/


    /************************Device Listener Call Back Methods*******************************/

    override fun onDeviceAvailableChange(available: Boolean, iDevice: IDevice?): Boolean {
        Log.d(TAG, "onDeviceAvailableChange... $available,$iDevice")
        try {
            if (iDevice == null) {
                Log.d(TAG, "onDeviceAvailableChange...$iDevice")
                setIDevice(null)
                return false
            }
            setIDeviceData(iDevice)
            Log.i(TAG, "device is available :: $available")
            if (iDevice.type == EnumDeviceType.USBMSD) {
                onDeviceChanged(iDevice, available)
                if (NONUIVariables.getInstance().ISFROMUSBNOWPLAYING && iDevice.isMounted) {
//                    onUsbNowPlayingList()
                    onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST(null)
                    onMediaRequest()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return true
    }

    override fun onDeviceAttributeChange(iDevice: IDevice?): Boolean {
        Log.d(TAG, "onDeviceAttributeChange... $iDevice")
        if (iDevice == null) {
            Log.d(TAG, "onDeviceAttributeChange...$iDevice")
            setIDevice(null)
            return false
        }
        setIDeviceData(iDevice)
        if (iDevice.type == EnumDeviceType.USBMSD)
            onDeviceChanged(iDevice, iDevice.isMounted)

        return true
    }

    private fun setIDeviceData(iDevice: IDevice) {
        setIDevice(iDevice)
        Log.i(TAG, "device.getObjectID() :: ${iDevice.objectID}")
        Log.i(TAG, "device.getType() :: ${iDevice.type}")
        Log.i(TAG, "device.canBrowse() :: ${iDevice.canBrowse()}")
        Log.i(TAG, "device.getVolumeName() :: ${iDevice.volumeName}")
    }

    //common method for browse support and mounted status
    private fun isDeviceMountedOrBrowseSupport(flag: Boolean) {
        SystemListener.onMEDIA_RES_DEVICE_MOUNTED_STATUS(flag)
        SystemListener.onMEDIA_RES_BROWSEBUTTONSUPPORT(flag)
    }

    //common method logic for on device changed
    private fun onDeviceChanged(iDevice: IDevice, available: Boolean) {
        val indexedState = if (iDevice.isDeviceIndexable) eMediaDevIndexedState.MEDIA_DEV_IDS_NOT_STARTED else eMediaDevIndexedState.MEDIA_DEV_IDS_NOT_SUPPORTED
        SystemListener.onMEDIA_RES_ACTIVEMEDIADEVICE(MediaDeviceInfo_t(iDevice.objectID.toInt(), eMediaDeviceType.MEDIA_DTY_USB, available, indexedState, 0, iDevice.volumeName, "", true, iDevice.canBrowse()))
        SystemListener.updateMediaIndexingStatus(iDevice)

        if (!iDevice.isMounted) {
            checkFragmentFunction(AudioConstants.ShowMetaData)
            isDeviceMountedOrBrowseSupport(false)
            NONUIVariables.getInstance().mActiveBrowseAddresses.clear()
            if (IS_USB_BROWSE_ACTIVE) {
                IS_BROWSE_CLOSE_EVENT = true
                onMEDIA_REQ_EXITBROWSE()
                SystemListener.showQuickNotice(AudioService.instance!!.resources.getString(R.string.audio_media_no_device_connected))
            }
        } else
            isDeviceMountedOrBrowseSupport(true)
    }

    override fun getListAdapter(): IDeviceListAdapter? {
        Log.d(TAG, "getListAdapter... ")
        return null
    }

    override fun onDeviceSyncError(iDevice: IDevice?, p1: String?): Boolean {
        Log.d(TAG, "onDeviceSyncError... $iDevice,$p1")
        if (CConstants.Error_Indexing_Limit_Reached == p1) {
            SystemListener.onMEDIA_RES_INDEXINGLIMITREACHED(0)
            onDialogDisplay()
        }
        return false
    }

    override fun onVolumeRemoved(p0: Long): Boolean {
        Log.d(TAG, "onVolumeRemoved... $p0")
        NONUIVariables.getInstance().mActiveBrowseAddresses.clear()
        onMEDIA_REQ_EXITBROWSE()
        return true
    }
    /************************Device Listener Call Back Methods*******************************/


    /************************PlayBack Listener Call Back Methods*******************************/

    override fun onAlbumArtChange(p0: Bitmap?): Boolean {
        Log.d(TAG, "onAlbumArtChange... $p0")
        return false
    }

    override fun onMetadataChange(iMetadata: IMetadata?): Boolean {
        Log.d(TAG, "onMetadataChange... $iMetadata")

        if (iMetadata == null) {
            Log.d(TAG, "onMetadataChange...$iMetadata")
            NONUIVariables.getInstance().ISVIDEOMODE = false
            return false
        }
        try {
            if (iMetadata is IVideoMeta) { //sub if open brace
                NONUIVariables.getInstance().ISVIDEOMODE = true
                var title = iMetadata.title
                if (TextUtils.isEmpty(title))
                    title = ""

                var episodeID = iMetadata.episodeID
                if (TextUtils.isEmpty(episodeID))
                    episodeID = ""

                SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, iMetadata.album,
                        iMetadata.filename, iMetadata.objectID.toInt(), MediaPlayTime_t(0, 0),
                        iMetadata.genre, "", episodeID, title, "", eMediaCategoryType.MEDIA_CTY_ALBUM))
                checkFragmentFunction(AudioConstants.ShowVideo)
            } //sub if close brace
            else { //else open brace
                checkFragmentFunction(AudioConstants.HideVideo)
                NONUIVariables.getInstance().HIDEMEDIAMETADATA = false
                SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, iMetadata.album,
                        iMetadata.filename, iMetadata.objectID.toInt(), MediaPlayTime_t(0, 0),
                        iMetadata.genre, iMetadata.artist, iMetadata.album, iMetadata.title, "", eMediaCategoryType.MEDIA_CTY_ALBUM))
            } //else close brace
            return true
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
        NONUIVariables.getInstance().ISVIDEOMODE = false
        return false
    }

    override fun onPlayStateChange(p0: EnumPlayState?): Boolean {
        Log.d(TAG, "onPlayStateChange... $p0")
        return false
    }

    override fun onRepeatStateChange(p0: EnumRepeatState?): Boolean {
        Log.d(TAG, "onRepeatStateChange... $p0")
        return false
    }

    override fun onShuffleStateChange(p0: EnumShuffleState?): Boolean {
        Log.d(TAG, "onShuffleStateChange... $p0")
        return false
    }

    override fun onTrackTimeChange(currentSeconds: Int, remainingSeconds: Int, percentElapsed: Float): Boolean {
        Log.d(TAG, "onTrackTimeChange... $currentSeconds,$remainingSeconds,$percentElapsed")
        SystemListener.onMEDIA_RES_PLAYTIME(MediaPlayTime_t(remainingSeconds - currentSeconds, currentSeconds))
        return true
    }

    override fun onTrackCountChange(p0: Int, p1: Int): Boolean {
        Log.d(TAG, "onTrackCountChange... $p0")
        return false
    }

    override fun onActiveDeviceChange(p0: Long) {
        Log.d(TAG, "onActiveDeviceChange Not Implemented... $p0")
    }

    /************************PlayBack Listener Call Back Methods*******************************/

    /************************IPC Request Methods*******************************/

    override fun onAMFM_REQ_AMSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMSEEKSTATION... $any")
        seekAmFMStation(any)
    }

    override fun onAMFM_REQ_FMSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMSEEKSTATION... $any")
        seekAmFMStation(any)
    }

    override fun onAMFM_REQ_AMTUNEBYFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNEBYFREQUENCY... $any")
        if (any == null) {
            Log.d(TAG, "onAMFM_REQ_AMTUNEBYFREQUENCY...$any")
            return
        }
        val frequency = any.toString().toDouble()
        tuneTo(frequency.toInt())
    }

    override fun onAMFM_REQ_FMTUNEBYFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNEBYFREQUENCY... $any")
        if (any == null) {
            Log.d(TAG, "onAMFM_REQ_FMTUNEBYFREQUENCY...$any")
            return
        }
        val frequency = (any.toString()).toDouble()
        tuneTo((frequency * 1000).toInt())
    }

    override fun onAMFM_REQ_AMTUNESTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNESTATION... $any")
        tuneStation(any)
    }

    override fun onAMFM_REQ_FMTUNESTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNESTATION... $any")
        tuneStation(any)
    }

    override fun onAMFM_REQ_AMSTATIONLIST() {
        Log.d(TAG, "onAMFM_REQ_AMSTATIONLIST...")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_AMSTATIONLIST...$mTunerManager")
            return
        }
        try {
            onSourceReset(NOWPLAYING_SOURCE_TYPE.AM)
            setWaveBand(TunerManager.Waveband.AM)
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(null)
            previousTunerStationList!!.clear()
            mCurrentCategoryType = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ALL.name
            CategoriesUtil.getPTYValues(mCurrentCategoryType)
        } catch (e: TunerException) {
            Log.e(TAG, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_FMSTATIONLIST() {
        Log.d(TAG, "onAMFM_REQ_FMSTATIONLIST...")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_FMSTATIONLIST...$mTunerManager")
            return
        }
        try {
            if (mTunerManager?.autoStoreStatus?.status == AutoStoreStatus.Status.IN_PROGRESS) {
                mTunerManager?.seekTuneRelease()
                mTunerManager?.autoStoreAbort()
                Thread.sleep(500)
            }
            onSourceReset(NOWPLAYING_SOURCE_TYPE.FM)
            setWaveBand(TunerManager.Waveband.FM)
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(null)
            previousTunerStationList!!.clear()
            mCurrentCategoryType = eRDSPtyNACategory.AMFM_RDS_PTY_NA_ALL.name
            CategoriesUtil.getPTYValues(mCurrentCategoryType)
        } catch (e: TunerException) {
            Log.e(TAG, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_AMHARDSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMHARDSEEKSTATION... $any")
        seekAmFMStation(any)
    }

    override fun onAMFM_REQ_FMHARDSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMHARDSEEKSTATION... $any")
        seekAmFMStation(any)
    }

    override fun onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST() {
        Log.d(TAG, "onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST... ")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST...$mTunerManager")
            return
        }
        try {
            if (mTunerManager?.autoStoreStatus?.status != AutoStoreStatus.Status.IN_PROGRESS) {
                mTunerManager?.seekTuneRelease()
                mTunerManager!!.autoStoreUpdate()
            }
        } catch (e: TunerException) {
            Log.e(TAG, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST() {
        Log.d(TAG, "onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST... ")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST...$mTunerManager")
            return
        }
        try {
            if (mTunerManager?.autoStoreStatus?.status != AutoStoreStatus.Status.IN_PROGRESS) {
                mTunerManager?.seekTuneRelease()
                mTunerManager!!.autoStoreUpdate()
            }
        } catch (e: TunerException) {
            Log.e(TAG, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_AMTUNEBYOBJECTID(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNEBYOBJECTID... $any")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_AMTUNEBYOBJECTID...$mTunerManager")
            return
        }
        try {
            mTunerManager?.setStationByListId(any as Int)
        } catch (e: TunerException) {
            Log.e(TAG, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    override fun onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY... $any")
        if (regionCode == null) {
            Log.d(TAG, "onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY...$regionCode")
            return
        }
        try {
            val frequencyKeys: String = getFrequency(any)!!
            mCurrentModeChart = prepareChart(amLowerValue.toLong(), amUpperValue.toLong(), amStepValue.toLong())
            val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
            val buttonValues = updateButtons(false, getPossibleValues(enteredFrequency))
            SystemListener.onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE() {
        Log.d(TAG, "onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE...")
        abortAutoStore()
    }

    override fun onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE() {
        Log.d(TAG, "onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE...")
        abortAutoStore()
    }

    override fun onAMFM_REQ_FMTUNEBYOBJECTID(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNEBYOBJECTID... $any")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_FMTUNEBYOBJECTID...$mTunerManager")
            return
        }
        try {
            mTunerManager?.setStationByListId(any as Int)
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY... $any")
        if (regionCode == null) {
            Log.d(TAG, "onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY...$regionCode")
            return
        }
        try {
            val frequencyKeys: String = getFrequency(any)!!
            mCurrentModeChart = prepareChart(fmLowerValue.toLong(), fmUpperValue.toLong(), fmStepValue.toLong())
            val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
            val buttonValues = updateButtons(false, getPossibleValues(enteredFrequency))
            if (isValidEntry(frequencyKeys)) {
                for (i in buttonValues.indices) {
                    buttonValues[i] = false
                }
            }
            SystemListener.onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onAMFM_REQ_AMCATEGORYSTATIONLIST(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMCATEGORYSTATIONLIST... $any")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_AMCATEGORYSTATIONLIST...$mTunerManager")
            return
        }
        mCurrentCategoryType = (any as AMFMStationInfo_t).stationName
        val filterList = CategoriesUtil.getPTYValues(mCurrentCategoryType)
        if (filterList != null && filterList.isNotEmpty())
            mTunerManager?.setPtyListFilter(filterList)
        else
            SystemListener.onAMFM_RES_AMCATEGORYSTATIONLIST(any)
    }

    override fun onAMFM_REQ_FMCATEGORYSTATIONLIST(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMCATEGORYSTATIONLIST... $any")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_FMCATEGORYSTATIONLIST...$mTunerManager")
            return
        }
        mCurrentCategoryType = (any as AMFMStationInfo_t).stationName
        val filterList = CategoriesUtil.getPTYValues(mCurrentCategoryType)
        if (filterList != null && filterList.isNotEmpty())
            mTunerManager?.setPtyListFilter(filterList)
        else
            SystemListener.onAMFM_RES_FMCATEGORYSTATIONLIST(any)

    }

    override fun onAMFM_REQ_FMRDSSWITCH(rdsswitch: Int) {
        Log.d(TAG, "onAMFM_REQ_FMRDSSWITCH... $rdsswitch")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_FMRDSSWITCH...$mTunerManager")
            return
        }
        try {
            when (rdsswitch) {
                1 -> mTunerManager?.isRdsEnabled = true
                else -> mTunerManager?.isRdsEnabled = false
            }
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_TPSTATUS(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_TPSTATUS Not Implemented... $any")
        onDialogNoButton()
    }

    override fun onAMFM_REQ_FMTUNETMCSTATION(programIdentifier: Int) {
        Log.d(TAG, "onAMFM_REQ_FMTUNETMCSTATION Not Implemented... $programIdentifier")
    }

    override fun onAMFM_REQ_TRAFFICALERT_DISMISS() {
        Log.d(TAG, "onAMFM_REQ_TRAFFICALERT_DISMISS Not Implemented...")
        onDialogYesButton()

    }

    override fun onAMFM_REQ_TRAFFICALERT_LISTEN() {
        Log.d(TAG, "onAMFM_REQ_TRAFFICALERT_LISTEN Not Implemented...")
    }

    override fun onAMFM_REQ_SETREGIONSETTING(state: Int) {
        Log.d(TAG, "onAMFM_REQ_SETREGIONSETTING... $state")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFM_REQ_SETREGIONSETTING...$mTunerManager")
            return
        }
        try {
            SystemListener.onAMFM_RES_REGIONSETTING(mTunerManager!!.regionalizationMode)
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }
    /************************IPC Request Methods*******************************/

    /************************IManager Methods*******************************/

    override fun INIT_AMFM_REQUEST() {
        Log.d(TAG, "INIT_AMFM_REQUEST...")
        registerTunerListener()
        INIT_MEDIA_REQUEST()
        try {
            if (mTunerManager?.waveband == TunerManager.Waveband.AM) {
                setWaveBand(TunerManager.Waveband.AM)
                SystemListener.onCurrentSourceChanged(NOWPLAYING_SOURCE_TYPE.AM)
            } else {
                setWaveBand(TunerManager.Waveband.FM)
                SystemListener.onCurrentSourceChanged(NOWPLAYING_SOURCE_TYPE.FM)
            }
            processStationList(mTunerManager?.availableStationList)
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
        FavoriteDataProcessor.lazyInit()
    }

    /************************IManager Methods*******************************/

    override fun INIT_MEDIA_REQUEST() {
        Log.d(TAG, "INIT_MEDIA_REQUEST...")
        registerMediaListener()

    }

    override fun onCategoryList() {
        Log.d(TAG, "onCategoryList...")
        SystemListener.updateCategoriesList(null)
    }

    override fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?) {
        Log.d(TAG, "onSourceReset... $sourceTYPE")
        when (NONUIVariables.getInstance().previousSourceType) {
            NOWPLAYING_SOURCE_TYPE.AM ->
                resetTuner()
            NOWPLAYING_SOURCE_TYPE.FM ->
                resetTuner()
            NOWPLAYING_SOURCE_TYPE.USBMSD, NOWPLAYING_SOURCE_TYPE.AUX ->
                resetMedia()
        }
        SystemListener.onCurrentSourceChanged(sourceTYPE!!)
    }

    override fun onAMFMTuneRequest(any: Any?) {
        Log.d(TAG, "onAMFMTuneRequest... $any")
        if (mTunerManager == null) {
            Log.d(TAG, "onAMFMTuneRequest...$mTunerManager")
            return
        }
        try {
            if (mTunerManager?.currentStation!!.waveband == TunerManager.Waveband.AM.name)
                onAMFM_REQ_AMTUNEBYFREQUENCY(any)
            else
                onAMFM_REQ_FMTUNEBYFREQUENCY(any)

        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onFavoriteRequest(any: Any?) {
        Log.d(TAG, "onFavoriteRequest... $any")
        if (NONUIVariables.getInstance().audioControlDisabled) {
            SystemListener.showQuickNotice(AudioService.instance!!.getString(R.string.favorite_unavailable))
            return
        }
        if (any != null) {
            FavoriteDataProcessor.addOrRemoveFavorite(any)
            SystemListener.updateFavorites(any)
        }
    }

    override fun onAMFM_REQ_GREYEDBUTTON_CLICK() {
        Log.d(TAG, "onAMFM_REQ_GREYEDBUTTON_CLICK...")
        SystemListener.showQuickNotice(AudioService.instance!!.getString(R.string.audio_action_not_supported))
    }

    /**
     * Initialises instance of CMediaPlayer
     *
     * @throws ExceptionPlayFunctionNotEnabled, ExceptionInitializationFailed
     */
    @Throws(Exception::class)
    private fun registerMediaListener() {
        Log.d(TAG, "registerMediaListener...")
        try {
            if (mCMediaPlayer == null) {
                mCMediaPlayer = CMediaPlayer(mConfig)
                setupMediaEngine()
                FavoriteDataProcessor.mCMediaPlayer = mCMediaPlayer
            }
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            e.printStackTrace()
        } catch (e: ExceptionInitializationFailed) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Sets this(SDKManager) class as listener for event on Playback and Device
     *
     * @throws ExceptionDeviceFunctionNotEnabled, ExceptionPlayFunctionNotEnabled
     */
    @Throws(Exception::class)
    private fun setupMediaEngine() {
        Log.d(TAG, "setupMediaEngine...")
        if (mCMediaPlayer == null) {
            Log.d(TAG, "setupMediaEngine...$mCMediaPlayer")
            return
        }
        try {
            mCMediaPlayer?.addListener(this as IPlaybackListener)
            mCMediaPlayer?.addListener(this as IDeviceListener)
            mCMediaPlayer?.requestDevices()
        } catch (e: ExceptionDeviceFunctionNotEnabled) {
            e.printStackTrace()
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * sets  waveband for Tuner Manager
     *
     * @param waveband any constant for TunerManager.Waveband
     * @throws TunerException
     */
    @Throws(TunerException::class)
    private fun setWaveBand(waveband: TunerManager.Waveband) {
        Log.d(TAG, "setWaveBand... $waveband")
        if (mTunerManager == null) {
            Log.d(TAG, "setWaveBand...$mTunerManager")
            return
        }
        mTunerManager?.waveband = waveband
    }

    /**
     * this method tunes the tuner manager to given frequency
     * @param frequency
     * @throws TunerException
     */
    @Throws(TunerException::class)
    private fun tuneTo(frequency: Int) {
        Log.d(TAG, "tuneTo... $frequency")
        if (mTunerManager == null) {
            Log.d(TAG, "tuneTo...$mTunerManager")
            return
        }
        mTunerManager?.tuneTo(frequency)
    }


    /*SDK Request Listener Implementation*/
    private var previousTunerStationList: MutableList<TunerStation>? = null

    private fun processStationList(stationList: MutableList<TunerStation>?) {
        Log.d(TAG, "processStationList... $stationList")
        if (mTunerManager == null) {
            Log.d(TAG, "processStationList...$mTunerManager")
            return
        }
        val stationListData = ArrayList<AMFMStationInfo_t>()
        val currentTunerStationList: MutableList<TunerStation> = if (stationList!!.size <= 0)
            mTunerManager!!.staticStationList
        else
            stationList

        if (previousTunerStationList != null)
            if (areListsEqual(previousTunerStationList!!, currentTunerStationList)) return

        try {
            when (mTunerManager?.waveband) {
                TunerManager.Waveband.AM -> { // am open brace
                    currentTunerStationList.forEach {
                        stationListData.add(addAMFMHardWareStation(it, NONUIVariables.getInstance().favoriteList.contains(it.frequency.toFloat().toString()),
                                eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP, it.frequency.toFloat()))
                    }
                    if (NONUIVariables.getInstance().BROWSE_SOURCE_TYPE == AudioService.instance!!.getString(R.string.categoryinfo))
                        SystemListener.onAMFM_RES_AMCATEGORYSTATIONLIST(addAMFMCategory(mCurrentCategoryType, getAMFMCategoryListByType(mCurrentCategoryType!!, stationListData)))
                    else
                        SystemListener.onAMFM_RES_AMSTRONGSTATIONSLIST(AMFMStationList_t(stationListData.size, stationListData))
                } // am close brace
                TunerManager.Waveband.FM -> { //fm open brace
                    currentTunerStationList.forEach {
                        val freq = it.frequency.toDouble() / 1000
                        val eTPStationStatus = if (it.isTrafficProgramme)
                            eTPStationStatus.AMFM_STATION_SUPPORTS_TP
                        else
                            eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP
                        stationListData.add(addAMFMHardWareStation(it, NONUIVariables.getInstance().favoriteList.contains(freq.toFloat().toString()),
                                eTPStationStatus, freq.toFloat()))
                    }
                    if (NONUIVariables.getInstance().BROWSE_SOURCE_TYPE == AudioService.instance!!.getString(R.string.categoryinfo))
                        SystemListener.onAMFM_RES_FMCATEGORYSTATIONLIST(addAMFMCategory(mCurrentCategoryType, getAMFMCategoryListByType(mCurrentCategoryType!!, stationListData)))
                    else
                        SystemListener.onAMFM_RES_FMSTRONGSTATIONSLIST(AMFMStationList_t(stationListData.size, stationListData))
                } //fm close brace
                else -> {
                    //todo
                }
            }
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
        previousTunerStationList = currentTunerStationList
    }

    /*IDeviceListener Implementation*/
    override fun onErrorImpossible(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorImpossible... $p0")
    }

    override fun onErrorOperationProhibited(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorOperationProhibited... $p0")
    }

    override fun onErrorConnectionFailed(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorConnectionFailed... $p0")
    }

    override fun onErrorConnectionRefused(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorConnectionRefused... $p0")
    }

    override fun onErrorFailed(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorFailed... $p0")
    }

    override fun onErrorTimeout(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorTimeout... $p0")
    }

    override fun onErrorOperationFailed(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorOperationFailed... $p0")
    }

    override fun onErrorUnexpected(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorUnexpected... $p0")
    }

    override fun onErrorNotConnected(p0: MediaRunnable?) {
        Log.d(TAG, "onErrorNotConnected... $p0")
    }

    //Override methods for USB
    override fun onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST(any: Any?) {
        Log.d(TAG, "onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST... $any")
        try {
            if (NONUIVariables.getInstance().ISFROMUSBNOWPLAYING) {
                Log.d(TAG, "onUsbNowPlayingList...")
                if (mCMediaPlayer == null || mIDevice == null) {
                    Log.d(TAG, "onUsbNowPlayingList... mCMediaPlayer...$mCMediaPlayer ... mIDevice...$mIDevice")
                    return
                }

                Handler().postDelayed({
                    SystemListener.isMediaProgressBarVisible(false)
                }, 2500)


                onSourceReset(NOWPLAYING_SOURCE_TYPE.USBMSD)
                IS_USB_BROWSE_ACTIVE = true
                if (mIDevice?.type == EnumDeviceType.USBMSD && mIDevice?.isMounted!!) {
                    getCMediaPlayer(AudioConstants.OnUsbNowPlaying, "")
                    return
                }
            } else {
                IS_USB_BROWSE_ACTIVE = true
                getCMediaPlayer(AudioConstants.OnUsbBrowse, "")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMEDIA_REQ_MEDIAPLAYERLISTFILTER(any: Any?) {
        Log.d(TAG, "onMEDIA_REQ_MEDIAPLAYERLISTFILTER... $any")
        try {
            if (null != any && any is String) {
                getCMediaPlayer(AudioConstants.OnKeypadLetterClick, any)
                SystemListener.onMEDIA_RES_SEARCHKEYBOARDMEDIALIST(MediaSearchKeyboardList_t(any, 1, 0, 0))
                onCloseLetterKeypad()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMEDIA_REQ_PLAYITEMFROMLISTBYTAG(any: Any?) {
        Log.d(TAG, "onMEDIA_REQ_PLAYITEMFROMLISTBYTAG... $any")
        try {
            if (any is Long) {
                mCMediaPlayer?.playItemInCurrentBrowse(any)
                mCMediaPlayer?.mediaIsActiveSource = true
            }
        } catch (e: ExceptionBrowseFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * This method is used for playback actions(Play,Pause,Next,Previous etc...)
     * @param any
     *
     **/
    override fun onMEDIA_REQ_REQUESTPLAYBACKACTION(any: Any?) {
        Log.d(TAG, "onMEDIA_REQ_REQUESTPLAYBACKACTION... $any")
        checkFragmentFunction(AudioConstants.NormalScreenVideo)
        try {
            when (any) {
                eMediaPBAction.MEDIA_PBA_PLAY -> {
                    pauseMedia()
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PAUSE)
                }
                eMediaPBAction.MEDIA_PBA_PAUSE -> {
                    playMedia()
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PLAY)
                }
                eMediaPBAction.MEDIA_PBA_NEXT -> {
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PLAY)
                    onPlayNext()
                }
                eMediaPBAction.MEDIA_PBA_PREV -> {
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PLAY)
                    onPlayPrevious()
                }
                eMediaPBAction.MEDIA_PBA_FASTFORWARD -> {
                    fastForwardFastBackward(true)
                }
                eMediaPBAction.MEDIA_PBA_FASTBACKWARD -> {
                    fastForwardFastBackward(false)
                }
                eMediaPBAction.MEDIA_PBA_FAST_PREV_STOP -> {
                    playMedia()
                }
            }
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * Seek to a position in the current song.
     *
     * @param any
     * Proportion of song completed (where 1000 is the end of the
     * song)
     */

    override fun onMEDIA_REQ_SEEKTO(any: Any?) {
        Log.d(TAG, "onMEDIA_REQ_SEEKTO... $any")
        if (mCMediaPlayer == null) {
            Log.d(TAG, "onMEDIA_REQ_SEEKTO...$mCMediaPlayer")
            return
        }
        SystemListener.onMEDIA_RES_SEEKTO(any as Int)

        try {
            val playTimeDuration = NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME + NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_REMAININGTIME
            val progressInTime = (any * playTimeDuration) / 100
            if (NONUIVariables.getInstance().isStartTouch)
                SystemListener.onMEDIA_RES_PLAYTIME(MediaPlayTime_t((playTimeDuration - progressInTime), progressInTime))
            else {
                mCMediaPlayer!!.seekToTime(progressInTime)
                playMedia()
            }
        } catch (e: ExceptionParameterOutOfBounds) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onMEDIA_REQ_SETPLAYBACKMODE(any: Any?) {
        Log.d(TAG, "onMEDIA_REQ_SETPLAYBACKMODE... $any")
        if (mCMediaPlayer == null) {
            Log.d(TAG, "onMEDIA_REQ_SETPLAYBACKMODE...$mCMediaPlayer")
            return
        }
        checkFragmentFunction("normalScreenVideo")
        try {
            if (any as eMediaPBMode == eMediaPBMode.MEDIA_PBM_NORMAL) {
                SystemListener.onMEDIA_RES_PLAYBACKMODE(eMediaPBMode.MEDIA_PBM_RANDOM)
                mCMediaPlayer!!.setShuffle(EnumShuffleState.On)
            } else {
                SystemListener.onMEDIA_RES_PLAYBACKMODE(eMediaPBMode.MEDIA_PBM_NORMAL)
                mCMediaPlayer!!.setShuffle(EnumShuffleState.Off)
            }
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onMEDIA_REQ_RESETMUSICINDEX() {
        Log.d(TAG, "onMEDIA_REQ_RESETMUSICINDEX...")

        NONUIVariables.getInstance().dialogDisplayEvent = AudioService.instance!!.resources.getString(R.string.dialog_display_event_reset_music_index)
        NONUIVariables.getInstance().dialogMainText = AudioService.instance!!.resources.getString(R.string.dialog_main_text_reset_music_index)
        NONUIVariables.getInstance().dialogButtonOneText = AudioService.instance!!.resources.getString(R.string.dialog_button_1_reset_music_index)
        NONUIVariables.getInstance().dialogButtonTwoText = AudioService.instance!!.resources.getString(R.string.dialog_button_2_reset_music_index)
        onDialogDisplay()
    }

    override fun onMEDIA_REQ_FOLDERBACK(indexedListReq: MediaPlayerListReq_t) {
        Log.d(TAG, "onMEDIA_REQ_FOLDERBACK... $indexedListReq")
    }

    override fun onMEDIA_REQ_READACK(readAck: eMediaListReadAck) {
        Log.d(TAG, "onMEDIA_REQ_READACK... $readAck")
    }

    override fun onMEDIA_REQ_LISTRELOAD() {
        Log.d(TAG, "onMEDIA_REQ_LISTRELOAD...")
    }

    override fun onUsbTuneRequest(any: Any?) {
        Log.d(TAG, "onUsbTuneRequest... $any")
    }

    override fun onUsbRequestSource() {
        Log.d(TAG, "onUsbRequestSource...")
    }

    override fun onMediaRequest() {
        Log.d(TAG, "onMediaRequest...")
        if (mIDevice == null || mCMediaPlayer == null) {
            Log.d(TAG, "onPlayNext... mIDevice...$mIDevice ... mCMediaPlayer...$mCMediaPlayer")
            return
        }
        onSourceReset(NOWPLAYING_SOURCE_TYPE.USBMSD)
        try {
            if (mIDevice?.isMounted!!)
                mCMediaPlayer?.playDevice(mIDevice)

        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: ExceptionDeviceNotConnected) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onAuxRequest() {
        Log.d(TAG, "onAuxRequest...")
    }

    /**
     * This method is used to play the next song
     *
     */
    override fun onPlayNext() {
        Log.d(TAG, "onPlayNext...")
        if (mIDevice == null || mCMediaPlayer == null) {
            Log.d(TAG, "onPlayNext... mIDevice...$mIDevice ... mCMediaPlayer...$mCMediaPlayer")
            return
        }
        try {
            if (mIDevice?.isMounted!!)
                mCMediaPlayer?.nextTrack(1)
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * This method is used to play the previous song
     *
     */
    override fun onPlayPrevious() {
        Log.d(TAG, "onPlayPrevious...")
        if (mIDevice == null || mCMediaPlayer == null) {
            Log.d(TAG, "onPlayPrevious... mIDevice...$mIDevice ... mCMediaPlayer...$mCMediaPlayer")
            return
        }
        try {
            if (mIDevice?.isMounted!!)
                mCMediaPlayer?.previousTrack(1, Integer.valueOf(5).toShort())
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onUSBBrowseItemClick(any: Any?) {
        Log.d(TAG, "onUSBBrowseItemClick... $any")
        if (any is IBrowseItem) {
            when {
                isCellularDataUnavailable(any) -> SystemListener.onMEDIA_RES_MEDIAACTIONUNAVAILABLE_CHECK_CONNECTION(0)
                any.playable -> {
                    onMEDIA_REQ_PLAYITEMFROMLISTBYTAG(any.objectID)
                    onMEDIA_REQ_EXITBROWSE()
                }
                else -> updateMediaData(AudioConstants.USBBrowseItemClick, any)
            }
        }
    }

    override fun onMEDIA_REQ_BACKTRAVERSE() {
        Log.d(TAG, "onMEDIA_REQ_BACKTRAVERSE...")
        if (NONUIVariables.getInstance().mActiveBrowseAddresses.size > 0) {
            updateMediaData(AudioConstants.MEDIA_REQ_BACKTRAVERSE, null)
        } else {
            onMEDIA_REQ_EXITBROWSE()
        }
    }

    override fun onMEDIA_REQ_EXITBROWSE() {
        Log.d(TAG, "onMEDIA_REQ_EXITBROWSE...")
        if (IS_BROWSE_CLOSE_EVENT) {
            IS_BROWSE_CLOSE_EVENT = false
            if (NONUIVariables.getInstance().mMediaCallBack == null) {
                Log.d(TAG, "closeEvent... mMediaCallBack..." + NONUIVariables.getInstance().mMediaCallBack)
                return
            }
            NONUIVariables.getInstance().mMediaCallBack!!.closeEvent(AudioConstants.BROWSE_CLOSE_KEY)
        } else {
            IS_USB_BROWSE_ACTIVE = false
            getCMediaPlayer(AudioConstants.Remove_Browse, "")
        }

    }

    override fun onUsbBrowse() {
        Log.d(TAG, "onUsbBrowse...")
        IS_USB_BROWSE_ACTIVE = true
        getCMediaPlayer(AudioConstants.OnUsbBrowse, "")
    }

    /**
     * This method is used to get songs list
     */
    override fun onUsbNowPlayingList() {
        Log.d(TAG, "onUsbNowPlayingList...")
        if (mCMediaPlayer == null || mIDevice == null) {
            Log.d(TAG, "onUsbNowPlayingList... mCMediaPlayer...$mCMediaPlayer ... mIDevice...$mIDevice")
            return
        }

        Handler().postDelayed({
            SystemListener.isMediaProgressBarVisible(false)
        }, 2500)


        onSourceReset(NOWPLAYING_SOURCE_TYPE.USBMSD)
        IS_USB_BROWSE_ACTIVE = true
        if (mIDevice?.type == EnumDeviceType.USBMSD && mIDevice?.isMounted!!) {
            getCMediaPlayer(AudioConstants.OnUsbNowPlaying, "")
            return
        }
    }

    //common method for CMediaPlayer
    private fun getCMediaPlayer(functionType: String, any: Any) {
        if (NONUIVariables.getInstance().mMediaCallBack == null) {
            Log.d(TAG, "getCMediaPlayer... mMediaCallBack..." + NONUIVariables.getInstance().mMediaCallBack)
            return
        }
        NONUIVariables.getInstance().mMediaCallBack!!.getCMediaPlayer(functionType, mCMediaPlayer!!, mIDevice!!, any)
    }

    //common method for checkFragmentFunction
    private fun checkFragmentFunction(functionType: String) {
        if (NONUIVariables.getInstance().mMediaCallBack == null) {
            Log.d(TAG, "checkFragmentFunction... mMediaCallBack..." + NONUIVariables.getInstance().mMediaCallBack)
            return
        }
        NONUIVariables.getInstance().mMediaCallBack!!.checkFragmentFunction(functionType)
    }

    //common method for updateMediaData
    private fun updateMediaData(functionType: String, iBrowseItem: IBrowseItem?) {
        if (NONUIVariables.getInstance().mMediaCallBack == null) {
            Log.d(TAG, "updateMediaData... mMediaCallBack..." + NONUIVariables.getInstance().mMediaCallBack)
            return
        }
        NONUIVariables.getInstance().mMediaCallBack!!.updateMediaData(functionType, iBrowseItem, mCMediaPlayer!!, mIDevice!!)
    }

    //common method for closeEVent
    //common method for checkFragmentFunction
    private fun closeEvent(functionType: String) {
        if (NONUIVariables.getInstance().mMediaCallBack == null) {
            Log.d(TAG, "closeEvent... mMediaCallBack..." + NONUIVariables.getInstance().mMediaCallBack)
            return
        }
        NONUIVariables.getInstance().mMediaCallBack!!.closeEvent(functionType)
    }


    override fun onUSBBrowseSongItemClick(any: Any?) {
        Log.d(TAG, "onUSBBrowseSongItemClick...$any")
        onUSBBrowseItemClick(any)
    }

    override fun onUsbNowPlayingItemClick(any: Any?) {
        Log.d(TAG, "onUsbNowPlayingItemClick...$any")
        if (any == null) {
            Log.d(TAG, "onUsbNowPlayingItemClick...any..$any")
            return
        }
        try {
            if (any is IBrowseItem && any.playable)
                mCMediaPlayer?.playItemInCurrentSession(any.objectID)
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

//    override fun onKeypadLetterClick(any: Any?) {
//        Log.d(TAG, "onKeypadLetterClick...$any")
//        if (any is String) {
////            onMEDIA_REQ_MEDIAPLAYERLISTFILTER(MPRequestListFilter_t(eMediaListType.MEDIA_LTY_SONG,"","","",any))
//            getCMediaPlayer(AudioConstants.OnKeypadLetterClick, any)
//            SystemListener.onMEDIA_RES_SEARCHKEYBOARDMEDIALIST(MediaSearchKeyboardList_t(any,1,0,0))
//            onCloseLetterKeypad()
//        }
//    }

    override fun onShowLetterKeypad(any: Any?) {
        Log.d(TAG, "onShowLetterKeypad...$any")
        NONUIVariables.getInstance().SHOW_LETTER_KEYPAD = true
        NONUIVariables.getInstance().IS_MEDIA_BROWSE_LISTVIEW_VISIBLE = false
    }

    override fun onCloseLetterKeypad() {
        Log.d(TAG, "onCloseLetterKeypad...")
        NONUIVariables.getInstance().SHOW_LETTER_KEYPAD = false
        NONUIVariables.getInstance().IS_MEDIA_BROWSE_LISTVIEW_VISIBLE = true
    }

    override fun onDialogDisplay() {
        Log.d(TAG, "onDialogDisplay...")
        NONUIVariables.getInstance().mMediaCallBack!!.showCustomAlertDialog()

    }

    override fun onDialogYesButton() {
        Log.d(TAG, "onDialogYesButton...")
        if (NONUIVariables.getInstance().dialogDisplayEvent == AudioService.instance!!.resources.getString(R.string.dialog_display_event_tps))
            onAMFM_REQ_TPSTATUS(NONUIVariables.getInstance().FMTUNER_TPSTATUS)
        else if (NONUIVariables.getInstance().dialogDisplayEvent == AudioService.instance!!.resources.getString(R.string.dialog_display_event_reset_music_index)) {
            try {
                if (mCMediaPlayer != null)
                    mCMediaPlayer!!.clearAllMediaData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            NONUIVariables.getInstance().mMediaCallBack!!.dismissCustomAlertDialog()
            SystemListener.showQuickNotice(AudioService.instance!!.resources.getString(R.string.quick_notice_reset_music_index))
        }

    }

    override fun onDialogNoButton() {
        Log.d(TAG, "onDialogNoButton...")
        if (NONUIVariables.getInstance().dialogDisplayEvent == AudioService.instance!!.resources.getString(R.string.dialog_display_event_tps))
            onAMFM_REQ_TRAFFICALERT_DISMISS()
        else
            NONUIVariables.getInstance().mMediaCallBack!!.dismissCustomAlertDialog()
    }

    override fun onVehicleDataChanged() {
        Log.d(TAG, "onVehicleDataChanged...")
        if (mVehicleMovement == null) {
            Log.d(TAG, "onVehicleDataChanged...mVehicleMovement..$mVehicleMovement")
            return
        }
        val value = mVehicleMovement?.vehicleMotionMovementProtectedVehicleMotionMovementStateAuthenticated
        val iSparked = VehicleMovement.VehicleMotionMovementProtectedVehicleMotionMovementStateAuthenticated.VMMP_VEHMTNMVMTSTATAUTH_PARKED == value
        NONUIVariables.getInstance().ISVEHICLEPARKED = iSparked
    }

    override fun onVehicleDataChanged(p0: Int) {
        Log.d(TAG, "onVehicleDataChanged... $p0")
    }

    private fun seekAmFMStation(any: Any?) {
        Log.d(TAG, "seekAmFMStation... $any")
        if (mTunerManager == null) {
            Log.d(TAG, "seekAmFMStation...mTunerManager..$mTunerManager")
            return
        }
        if (isTrafficAlertPlaying)
            onAMFM_REQ_TRAFFICALERT_DISMISS()
        try {
            mTunerManager?.seekTuneRelease()
            when (any as eAMFMSeekType) {
                eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP -> mTunerManager?.seekNextAvailable(TunerManager.Direction.UP)
                eAMFMSeekType.AMFM_SEEK_TYPE_SEEKDOWN -> mTunerManager?.seekNextAvailable(TunerManager.Direction.DOWN)
                eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKUP -> mTunerManager?.seekUp()
                eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKDOWN -> mTunerManager?.seekDown()
                else ->
                    mTunerManager?.seekNextAvailable(TunerManager.Direction.UP)
            }
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    private fun tuneStation(any: Any?) {
        Log.d(TAG, "tuneStation... $any")
        if (mTunerManager == null) {
            Log.d(TAG, "tuneStation...mTunerManager..$mTunerManager")
            return
        }
        try {
            mTunerManager?.seekTuneRelease()
            when (any as eAMFMTuneType) {
                eAMFMTuneType.AMFM_TUNE_TYPE_TUNEUP -> mTunerManager?.tuneOneStep(TunerManager.Direction.UP)
                eAMFMTuneType.AMFM_TUNE_TYPE_TUNEDOWN -> mTunerManager?.tuneOneStep(TunerManager.Direction.DOWN)
                eAMFMTuneType.AMFM_TUNE_TYPE_FASTTUNEUP -> mTunerManager?.tuneNext()
                eAMFMTuneType.AMFM_TUNE_TYPE_FASTTUNEDOWN -> mTunerManager?.tunePrevious()
                else ->
                    mTunerManager?.tuneNext()
            }
        } catch (e: TunerException) {
            Log.e(TAG, e.localizedMessage)
        } catch (e: Exception) {
            Log.e(TAG, e.localizedMessage)
        }
    }

    private fun abortAutoStore() {
        Log.d(TAG, "abortAutoStore...")
        if (mTunerManager == null) {
            Log.d(TAG, "abortAutoStore...mTunerManager..$mTunerManager")
            return
        }
        try {
            mTunerManager?.autoStoreAbort()
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    private fun registerDrivingMode() {
        Log.d(TAG, "registerDrivingMode...")
        val intentFilter = IntentFilter(DrivingModeManager.ACTION_DRIVING_MODE_CHANGED)
        intentFilter.addCategory(DrivingModeManager.CATEGORY_DRIVING_MODE)
        AudioService.instance!!.registerReceiver(IDrivingModeBroadcastReceiver(), intentFilter)
    }

    class IDrivingModeBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            Log.d("IDrivingModeBroadcastReceiver", "onReceive...")
            val action = p1?.action
            if (DrivingModeManager.ACTION_DRIVING_MODE_CHANGED == action) {
                val curMode = p1.getIntExtra(DrivingModeManager.EXTRA_CURRENT_DRIVING_MODE, -1)
                NONUIVariables.getInstance().DRIVINGMODE = curMode
                SDKManager.getInstance().checkFragmentFunction("enableDisableBrowse")
            }
        }
    }

    /**
     * called this method to release Tuner Manager
     */
    private fun resetTuner() {
        Log.d(TAG, "resetTuner...")
        if (mTunerManager == null) {
            Log.d(TAG, "resetTuner...mTunerManager..$mTunerManager")
            return
        }
        try {
            mTunerManager?.seekTuneRelease()
            Log.v(TAG, "resetTuner")
        } catch (e: TunerException) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    private fun resetMedia() {
        Log.d(TAG, "resetMedia...")
    }

    /**
     *  Sets the current connected device
     *  @param iDevice device
     */
    private fun setIDevice(iDevice: IDevice?) {
        Log.d(TAG, "setIDevice... $iDevice")
        mIDevice = iDevice
    }

    // Compares lists
    private fun areListsEqual(list1: List<Any>, list2: List<Any>): Boolean {
        Log.v(TAG, "areListsEqual... $list1,$list2")
        return if (list1.size == list2.size) {
            Log.v(TAG, "Both lists have same data ?? " + (list1 == list2))
            list1 == list2
        } else false
    }

    //add FMAM Hardware station data
    private fun addAMFMHardWareStation(stationInfo: TunerStation, isFavorite: Boolean, eTPStationStatus: eTPStationStatus, frequency: Float): AMFMStationInfo_t {
        Log.d(TAG, "addAMFMHardWareStation... $stationInfo,$isFavorite,$eTPStationStatus,$frequency")
        return Builder()
                .frequency(frequency)
                .stationName(stationInfo.hdStationNameLong)
                .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                .objectId(stationInfo.id.toInt())
                .rdsStationInfo(mTunerManager?.waveband.toString())
                .ptyCategory(stationInfo.ptyCode.toInt())
                .tpStationStatus(eTPStationStatus)
                .isFavorite(isFavorite)
                .build()
    }

    /**
     * Returns list of station based on category
     * @param categoryType any constant of String
     * @return ArrayList<AMFMStationInfo_t>
     */
    private fun getAMFMCategoryListByType(categoryType: String, stationListData: ArrayList<AMFMStationInfo_t>): MutableMap<String, ArrayList<AMFMStationInfo_t>> {
        Log.d(TAG, "getAMFMCategoryListByType... $categoryType,$stationListData")
        val stationList: ArrayList<AMFMStationInfo_t> = ArrayList()
        val map: MutableMap<String, ArrayList<AMFMStationInfo_t>> = hashMapOf()
        stationList.addAll(stationListData)
        map["" + categoryType] = stationList
        return map
    }

    //add category data
    private fun addAMFMCategory(categoryType: String?, map: MutableMap<String, ArrayList<AMFMStationInfo_t>>): AMFMStationInfo_t {
        Log.d(TAG, "addAMFMCategory... $categoryType,$map")
        return Builder()
                .stationName("" + categoryType)
                .categoryData(map).build()
    }

    /**
     * Method to play item in Fast forward mode/Reverse mode     *
     * @param forwardSeek checking whether seek is forward or backward
     */
    private fun fastForwardFastBackward(forwardSeek: Boolean) {
        Log.d(TAG, "fastForwardFastBackward... $forwardSeek")
        try {
            if (null == mCMediaPlayer || null == mIDevice) {
                Log.d(TAG, "fastForwardFastBackward...mCMediaPlayer..$mCMediaPlayer")
                Log.d(TAG, "fastForwardFastBackward...mIDevice..$mIDevice")
                return
            }

            val calManager = GMCalibrationsManager(AudioService.instance!!)
            if (mIDevice?.isMounted!!)
                if (forwardSeek)
                    mCMediaPlayer?.fastForward(calManager.getInteger(GIS502_TUNERHMI.FastForward_CalID).toFloat())
                else
                    mCMediaPlayer?.fastRewind(calManager.getInteger(GIS502_TUNERHMI.FastRewind_CalID).toFloat())

        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (exception: Exception) {
            Log.e(TAG, exception.message!!)
        }
    }

    /**
     * This method is used to start playing media player
     */
    private fun playMedia() {
        Log.d(TAG, "playMedia...")
        try {
            if (null == mCMediaPlayer) {
                Log.d(TAG, "playMedia...mCMediaPlayer...$mCMediaPlayer")
                return
            }
            mCMediaPlayer!!.play()
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    /**
     * This method is used to start playing media player
     */
    private fun pauseMedia() {
        Log.d(TAG, "pauseMedia...")
        try {
            if (null == mCMediaPlayer) {
                Log.d(TAG, "pauseMedia...mCMediaPlayer...$mCMediaPlayer")
                return
            }
            mCMediaPlayer!!.pause()
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            Log.e(TAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    private fun isCellularDataUnavailable(browseItem: IBrowseItem): Boolean {
        Log.d(TAG, "isCellularDataUnavailable...$browseItem")
        return try {
            (mIDevice == null && mIDevice?.isHidingItems!! && !browseItem.hasChildren
                    && !browseItem.isResidentOnDevice)
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
            false
        }
    }
}
