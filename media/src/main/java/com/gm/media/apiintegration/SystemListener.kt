package com.gm.media.apiintegration

import android.widget.Toast
import com.gm.audioapp.ui.customclasses.MediaStringMatcher
import com.gm.media.apiintegration.apiinterfaces.IAMFMManagerRes
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler.MEDIA_RES_BROWSEBUTTONSUPPORT
import com.gm.media.utils.CategoriesUtil
import com.gm.media.utils.Log
import gm.media.enums.EnumBrowseChildType
import gm.media.interfaces.IDevice
import com.gm.media.R
import com.gm.media.database.MediaData
import com.gm.media.utils.MediaUtil

/**
 * Simply updates [DataPoolDataHandler] objects
 */
object SystemListener : IAMFMManagerRes {

    private const val TAG = "SystemListener"
    /**
     *     It should contain AMFM_RES, MEDIA_RES, AUDIOMGR_RES, DAB_RES, SDARS_RES funcs "DEFINITIONS"
     *     only present under RecvData func definition from CAMFMProxy.cpp, CAudioManagerProxy.cpp,
     *     CMediaProxy.cpp, CSDARSProxy.cpp & CDABProxy.cpp
     *     It should implement ImanagerRes
     */
    var MEDIA_FAVOURITES_ARTIST_LIST = ArrayList<String>()
    var MEDIA_FAVOURITES_ALBUM_LIST = ArrayList<String>()
    var USB_BROWSE_FAV_UPDATE: Boolean = false
    var mCallback: IAMFMManagerRes? = null

    //create object for IAMFMManagerRes and use context
    fun registerApiCallback(callback: IAMFMManagerRes) {
        Log.d(TAG, " registerApiCallback...  $callback")
        this.mCallback = callback
    }

    override fun onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t?) {
        Log.d(TAG, " onAMFM_RES_AMCURRENTSTATIONINFO...  $amfmstationinfo")
        if (amfmstationinfo != null) {
            NONUIVariables.getInstance().aMFMStationInfo_t = addAMFMStation(amfmstationinfo, NONUIVariables.getInstance().favoriteList.contains(amfmstationinfo.frequency.toString()))
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.set(amfmstationinfo.frequency.toInt().toString())
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_STATIONNAME.set(amfmstationinfo.stationName)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO.set(amfmstationinfo.rdsStationInfo)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_OBJECTID.set(amfmstationinfo.objectId)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_RDSSTATUS.set(eRdsStatus.AMFM_RDS_AVAILABLE.value.toFloat())
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_PTYCATEGORY.set(amfmstationinfo.ptyCategory)
            DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS.set(amfmstationinfo.tpStationStatus.value.toFloat())
            DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set("")
        }
        mCallback!!.onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo)
    }

    override fun onAMFM_RES_FMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t?) {
        Log.d(TAG, " onAMFM_RES_FMCURRENTSTATIONINFO...  $amfmstationinfo")
        if (amfmstationinfo != null) {
            NONUIVariables.getInstance().aMFMStationInfo_t = addAMFMStation(amfmstationinfo, NONUIVariables.getInstance().favoriteList.contains(amfmstationinfo.frequency.toString()))
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.set(amfmstationinfo.frequency.toString())
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_STATIONNAME.set(amfmstationinfo.stationName)
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_OBJECTID.set(amfmstationinfo.objectId)
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_PTYCATEGORY.set(amfmstationinfo.ptyCategory)
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_TPSTATIONSTATUS.set(amfmstationinfo.tpStationStatus.value.toFloat())
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_RDSSTATUS.set(amfmstationinfo.rdsStatus.value.toFloat())
            DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_RDSSTATIONINFO.set(amfmstationinfo.rdsStationInfo)
            DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set("")
        }
        mCallback!!.onAMFM_RES_FMCURRENTSTATIONINFO(amfmstationinfo)
    }

    override fun onAMFM_RES_AMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
        Log.d(TAG, " onAMFM_RES_FMCATEGORYSTATIONLIST...  $tunercategorylist")
        NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = ArrayList()
        NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = tunercategorylist.categoryData!![tunercategorylist.stationName]!!
        DataPoolDataHandler.AMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE.set(tunercategorylist.stationName)
        DataPoolDataHandler.AMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT.set(tunercategorylist.categoryData[tunercategorylist.stationName]!!.size)
    }

    override fun onAMFM_RES_FMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
        Log.d(TAG, " onAMFM_RES_FMCATEGORYSTATIONLIST...  $tunercategorylist")
        NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = ArrayList()
        NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = tunercategorylist.categoryData!![tunercategorylist.stationName]!!
        DataPoolDataHandler.FMTUNER_CATEGORYSTATIONLIST_CATEGORYTYPE.set(tunercategorylist.stationName)
        DataPoolDataHandler.FMTUNER_CATEGORYSTATIONLIST_STATIONLISTCOUNT.set(tunercategorylist.categoryData[tunercategorylist.stationName]!!.size)
    }

    override fun onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        Log.d(TAG, " onAMFM_RES_AMSTRONGSTATIONSLIST...  $amfmstationlist")
        DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_LISTID.set(amfmstationlist.stationlistCount)
        DataPoolDataHandler.AMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS.set(0F)
        addBrowseList(amfmstationlist)
        mCallback!!.onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist)
    }

    override fun onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        Log.d(TAG, " onAMFM_RES_FMSTRONGSTATIONSLIST...  $amfmstationlist")
        DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_LISTID.set(amfmstationlist.stationlistCount)
        DataPoolDataHandler.FMTUNER_STRONGSTATIONS_LIST_UPDATE_STATUS.set(0F)
        addBrowseList(amfmstationlist)
        mCallback!!.onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist)
    }

    //common method for browseList add
    private fun addBrowseList(amFmStationList: AMFMStationList_t) {
        val browseList = ArrayList<AMFMStationInfo_t>()
        NONUIVariables.getInstance().browseList = browseList
        browseList.addAll(amFmStationList.objectList)
        NONUIVariables.getInstance().browseList = browseList
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(pData: Int) {
        Log.d(TAG, " onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS...  $pData")
        NONUIVariables.getInstance().updateProgress = pData
        if (pData >= 95 || pData < 0)
            showUpdateProgressAndAudioControlDisable(false)
        else
            NONUIVariables.getInstance().showUpdateProgress = true

    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(pData: Int) {
        Log.d(TAG, " onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS...  $pData")
    }

    override fun onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
        Log.d(TAG, " onAMFM_RES_AMTUNEBYPARTIALFREQUENCY...  $amfmtuneinfo")
        DataPoolDataHandler.AMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set(amfmtuneinfo.frequency)
        DataPoolDataHandler.AMTUNER_DIRECTTUNE_AVAILABLE_KEYS.set(amfmtuneinfo.buttonValues.toString())
        DataPoolDataHandler.manualTuneData.set(amfmtuneinfo)
    }

    override fun onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
        Log.d(TAG, " onAMFM_RES_FMTUNEBYPARTIALFREQUENCY...  $amfmtuneinfo")
        DataPoolDataHandler.FMTUNER_DIRECTTUNE_PARTIAL_FREQUENCY.set(amfmtuneinfo.frequency)
        DataPoolDataHandler.FMTUNER_DIRECTTUNE_AVAILABLE_KEYS.set(amfmtuneinfo.buttonValues.toString())
        DataPoolDataHandler.manualTuneData.set(amfmtuneinfo)
    }

    override fun onAMFM_RES_FMRDSSWITCH(pData: Int) {
        Log.d(TAG, " onAMFM_RES_FMRDSSWITCH...  $pData")
        DataPoolDataHandler.FMTUNER_RDSSWITCH.set(pData)
    }

    override fun onAMFM_RES_REGIONSETTING(pData: Int) {
        Log.d(TAG, " onAMFM_RES_REGIONSETTING...  $pData")
        DataPoolDataHandler.FMTUNER_REGIONSETTING.set(pData)
    }

    override fun onAMFM_RES_TPSTATUS(amfmtpstatus: AMFMTPStatus_t) {
        Log.d(TAG, " onAMFM_RES_TPSTATUS...  $amfmtpstatus")
        NONUIVariables.getInstance().FMTUNER_TPSTATUS = amfmtpstatus.status
        DataPoolDataHandler.FMTUNER_TPSTATUS_NOWPLAYING.set(0F)
        DataPoolDataHandler.FMTUNER_TPSTATUS_LABELINDICATOR_NOWPLAYING.set(0F)
        DataPoolDataHandler.FMTUNER_TPSTATUS_SEARCHSTATE.set(0F)
        NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME = ""
        NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT = ""

        if (amfmtpstatus.state == eTPStationState.AMFM_TP_SEARCH_INITIATED)
            autoControlDisabled(AudioService.instance!!.getString(R.string.tp_search_fm), true)
        else
            autoControlDisabled("", false)

    }

    //common method for auto control enable and disable
    private fun autoControlDisabled(tpSearch: String, isAudioControl: Boolean) {
        NONUIVariables.getInstance().audioControlDisabled = isAudioControl
        NONUIVariables.getInstance().audioControlDisabledforFM = isAudioControl
    }

    override fun onAMFM_RES_TRAFFICALERT(pData: String) {
        Log.d(TAG, " onAMFM_RES_TRAFFICALERT...  $pData")
        DataPoolDataHandler.FMTUNER_TRAFFICALERT_NAME.set(pData)
    }

    override fun onAMFM_RES_TRAFFICALERTACTIVECALL(amfmtrafficinfo: AMFMTrafficInfo_t) {
        Log.d(TAG, " onAMFM_RES_TRAFFICALERTACTIVECALL...  $amfmtrafficinfo")
        DataPoolDataHandler.FMTUNER_TRAFFICALERTACTIVECALL_NAME.set(amfmtrafficinfo.name)
        DataPoolDataHandler.FMTUNER_TRAFFICALERTACTIVECALL_PROGRAMIDENTIFIER.set(amfmtrafficinfo.programIdentifier)
    }

    override fun onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(amfmprogramtypealert: AMFMProgramTypeAlert_t) {
        Log.d(TAG, " onAMFM_RES_PROGRAMTYPERDSNOTIFICATION...  $amfmprogramtypealert")
        NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME = amfmprogramtypealert.name
        NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT = amfmprogramtypealert.content
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
        Log.d(TAG, " onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED Not Implemented...  $pData")
    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
        Log.d(TAG, " onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED Not Implemented...  $pData")
    }

    override fun onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE(pData: String) {
        Log.d(TAG, " onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE...  $pData")
        showUpdateProgressAndAudioControlDisable(false)
    }

    override fun onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE(pData: String) {
        Log.d(TAG, " onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE...  $pData")
        showUpdateProgressAndAudioControlDisable(false)
    }

    //common method for update progress and audio control disable
    private fun showUpdateProgressAndAudioControlDisable(isFlag: Boolean) {
        NONUIVariables.getInstance().showUpdateProgress = isFlag
        NONUIVariables.getInstance().audioControlDisabled = isFlag
    }

    override fun onAMFM_RES_TRAFFICALERT_END(pData: String) {
        Log.d(TAG, " onAMFM_RES_TRAFFICALERT_END Not Implemented...  $pData")
    }

    override fun onAMFM_RES_TRAFFICINFOSTART(pData: String) {
        Log.d(TAG, " onAMFM_RES_TRAFFICINFOSTART Not Implemented...  $pData")
    }

    override fun onAMFM_RES_STATIONAVAILABALITYSTATUS(fmstationavailabilityinfo: FMStationAvailabilityInfo_t) {
        Log.d(TAG, " onAMFM_RES_STATIONAVAILABALITYSTATUS...  $fmstationavailabilityinfo")
        DataPoolDataHandler.FMTUNER_STATIONAVAILABILITY_METADATA.set(fmstationavailabilityinfo.metadata)
        DataPoolDataHandler.FMTUNER_STATIONAVAILABILITY_STATUS.set(0F)
    }

    override fun onAMFM_RES_ACTIONUNAVAILABLE(pData: String) {
        Log.d(TAG, " onAMFM_RES_ACTIONUNAVAILABLE Not Implemented...  $pData")
    }

    //USB Start

    override fun onMEDIA_RES_NOWPLAYING(mediaobject: MediaObject_t) {
        Log.d(TAG, " onMEDIA_RES_NOWPLAYING...  $mediaobject")
        NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO = mediaobject
        NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING = mediaobject.isPlaying
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_ALBUMART.set(mediaobject.albumArt)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_FILENAME.set(mediaobject.fileName)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_OBJECTID.set(mediaobject.objectId)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_GENRE.set(mediaobject.metaDataGenre)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_ARTIST.set(mediaobject.metaDataArtist)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_ALBUM.set(mediaobject.metaDataAlbum)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_SONG.set(mediaobject.metaDataSong)
        NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_REMAININGTIME = mediaobject.playTime.remainingTime
        NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME = mediaobject.playTime.elapsedTime
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_CATEGORYTYPE.set(mediaobject.categoryType)
        mCallback!!.onMEDIA_RES_NOWPLAYING(mediaobject)
    }

    override fun onMEDIA_RES_PLAYTIME(mediaplaytime: MediaPlayTime_t) {
        Log.d(TAG, " onMEDIA_RES_PLAYTIME...  $mediaplaytime")
        NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_REMAININGTIME = mediaplaytime.remainingTime
        NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ELAPSEDTIME = mediaplaytime.elapsedTime

        if (!NONUIVariables.getInstance().isStartTouch) {
            if ((mediaplaytime.elapsedTime + mediaplaytime.remainingTime) > 0)
                DataPoolDataHandler.MEDIA_NOWPLAYING_PROGRESS_PERCENTAGE.set((mediaplaytime.elapsedTime * 100) / (mediaplaytime.elapsedTime + mediaplaytime.remainingTime))
            else
                DataPoolDataHandler.MEDIA_NOWPLAYING_PROGRESS_PERCENTAGE.set(0)
        }
    }


    override fun onMEDIA_RES_ACTIVEMEDIADEVICE(mediadeviceinfo: MediaDeviceInfo_t) {
        Log.d(TAG, " onMEDIA_RES_ACTIVEMEDIADEVICE...  $mediadeviceinfo")
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEID.set(mediadeviceinfo.deviceId)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICETYPE.set(mediadeviceinfo.deviceType)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICECONNECTED.set(mediadeviceinfo.deviceConnected)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEINDEXEDSTATE.set(mediadeviceinfo.deviceIndexedState)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEACTIVESOURCE.set(mediadeviceinfo.deviceActiveSource)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICENAME.set(mediadeviceinfo.deviceName)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICESERIALNUMBER.set(mediadeviceinfo.deviceSerialNumber)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_DEVICEREADABLE.set(mediadeviceinfo.deviceReadable)
        DataPoolDataHandler.MEDIA_ACTIVEMEDIADEVICE_FOLDERBROWSESUPPORTED.set(mediadeviceinfo.folderBrowseSupported)

        if (eMediaDeviceType.MEDIA_DTY_USB == mediadeviceinfo.deviceType) {
            DataPoolDataHandler.mDrawerOptions.remove(NOWPLAYING_SOURCE_TYPE.USBMSD.name)
            if (mediadeviceinfo.deviceConnected)
                DataPoolDataHandler.mDrawerOptions.add(DataPoolDataHandler.mDrawerOptions.size - 1, NOWPLAYING_SOURCE_TYPE.USBMSD.name)

            mCallback!!.onMEDIA_RES_ACTIVEMEDIADEVICE(mediadeviceinfo)
        }

    }

    override fun onMEDIA_RES_INDEXINGSTATE(activedevmediaindexingstate: ActiveDevMediaIndexingState_t) {
        Log.d(TAG, " onMEDIA_RES_INDEXINGSTATE...  $activedevmediaindexingstate")
        DataPoolDataHandler.MEDIA_INDEXINGSTATE_DEVICEID.set(activedevmediaindexingstate.deviceId)
        DataPoolDataHandler.MEDIA_INDEXINGSTATE_STATE.set(activedevmediaindexingstate.mediaDevIndexedState)
        DataPoolDataHandler.MEDIA_INDEXINGSTATE_PERCENTCOMPLETE.set(activedevmediaindexingstate.indexingPercentComplete)
        DataPoolDataHandler.MEDIA_DEV_PLAYABLECONTENT_PRESENT.set(activedevmediaindexingstate.playableContentPresent)
    }

    override fun onMEDIA_RES_PLAYBACKACTION(emediapbaction: eMediaPBAction) {
        Log.d(TAG, " onMEDIA_RES_PLAYBACKACTION...  $emediapbaction")
        if (emediapbaction == eMediaPBAction.MEDIA_PBA_PAUSE)
            NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING = false
        else if (emediapbaction == eMediaPBAction.MEDIA_PBA_PLAY)
            NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING = true
    }

    override fun onMEDIA_RES_SEARCHKEYBOARDMEDIALIST(mediaSearchKeyboardList_t: MediaSearchKeyboardList_t) {
        Log.d(TAG, " onMEDIA_RES_SEARCHKEYBOARDMEDIALIST...  $mediaSearchKeyboardList_t")
        DataPoolDataHandler.MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_SEARCHKEYBOARDLETTER.set(mediaSearchKeyboardList_t.searchKeyboardLetter)
        DataPoolDataHandler.MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_LETTERAVAILABLE.set(mediaSearchKeyboardList_t.letterAvailable)
        DataPoolDataHandler.MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_LETTERSTARTINDEX.set(mediaSearchKeyboardList_t.letterEndIndex)
        DataPoolDataHandler.MEDIA_SEARCHKEYBOARDMEDIALIST_SEARCHKEYBOARDLIST_LETTERENDINDEX.set(mediaSearchKeyboardList_t.letterEndIndex)
    }

    override fun onMEDIA_RES_MEDIAOBJECTALBUMARTINFO(mediaalbumartinfo: MediaAlbumArtInfo_t) {
        Log.d(TAG, " onMEDIA_RES_MEDIAOBJECTALBUMARTINFO...  $mediaalbumartinfo")
        DataPoolDataHandler.MEDIA_NOWPLAYING_ALBUM_ART_IMAGE.set(mediaalbumartinfo.albumArt)
        DataPoolDataHandler.MEDIA_MEDIAOBJECTALBUMARTINFO_PHOTOSIZE.set(mediaalbumartinfo.photoSize)
        DataPoolDataHandler.MEDIA_MEDIAOBJECTALBUMARTINFO_MIMETYPE.set(mediaalbumartinfo.MIMEimageSubType)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_ALBUMART.set(mediaalbumartinfo.MIMEimageSubType)
    }

    override fun onMEDIA_RES_MEDIAPLAYERINDEXEDLIST(mediaplayerlistres: MediaPlayerListRes_t) {
        Log.d(TAG, " onMEDIA_RES_MEDIAPLAYERINDEXEDLIST...  $mediaplayerlistres")
        DataPoolDataHandler.MEDIA_MEDIAPLAYERINDEXEDLIST_LISTHANDLE.set(mediaplayerlistres.currentCount)
        DataPoolDataHandler.MEDIA_MEDIAPLAYERINDEXEDLIST_TOTALCOUNT.set(mediaplayerlistres.currentCount)
        DataPoolDataHandler.MEDIA_MEDIAPLAYERINDEXEDLIST_LISTTYPE.set(mediaplayerlistres.currentCount)
        DataPoolDataHandler.MEDIA_MEDIAPLAYERINDEXEDLIST_CURRENTCOUNT.set(mediaplayerlistres.currentCount)
        DataPoolDataHandler.MEDIA_MEDIAPLAYERINDEXEDLIST_FOCUSINDEX.set(mediaplayerlistres.currentCount)
        DataPoolDataHandler.MEDIA_MEDIAPLAYERINDEXEDLIST_PLAYINDEX.set(mediaplayerlistres.currentCount)
    }

    override fun onMEDIA_RES_PLAYBACKMODE(eMediaPBMode: eMediaPBMode) {
        Log.d(TAG, " onMEDIA_RES_PLAYBACKMODE...  $eMediaPBMode")
        NONUIVariables.getInstance().MEDIA_PLAYBACKMODE = eMediaPBMode.ordinal
        DataPoolDataHandler.MEDIA_PLAYBACKMODE_EU.set(eMediaPBMode.ordinal)
    }

    override fun onMEDIA_RES_MEDIAOBJECTUNAVAILABLE() {
        Log.d(TAG, " onMEDIA_RES_MEDIAOBJECTUNAVAILABLE...")
        showQuickNotice(AudioService.instance!!.resources.getString(R.string.audio_content_is_no_longer_available))
    }

    override fun onMEDIA_RES_INDEXINGLIMITREACHED(int: Int) {
        Log.d(TAG, " onMEDIA_RES_INDEXINGLIMITREACHED...")
        NONUIVariables.getInstance().dialogDisplayEvent = AudioService.instance!!.resources.getString(R.string.audio_indexing_limit_reached)
        NONUIVariables.getInstance().dialogMainText = AudioService.instance!!.resources.getString(R.string.audio_remove_a_device_to_continue)
        NONUIVariables.getInstance().dialogButtonOneText = ""
        NONUIVariables.getInstance().dialogButtonTwoText = AudioService.instance!!.resources.getString(R.string.dialog_button_dissmiss)
    }

    override fun onMEDIA_RES_MEDIAOBJECTCONNECTIONUNAVAILABLE(int: Int) {
        Log.d(TAG, " onMEDIA_RES_MEDIAOBJECTCONNECTIONUNAVAILABLE...")
    }

    override fun onMEDIA_RES_LISTFILTERINFO(mediaplayerlistreq: MediaPlayerListReq_t) {
        Log.d(TAG, " onMEDIA_RES_LISTFILTERINFO... $mediaplayerlistreq")
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_PLAYINDEX.set(mediaplayerlistreq.deviceId)
        DataPoolDataHandler.MEDIA_NOWPLAYING_LISTFILTER.set(mediaplayerlistreq.deviceId)
    }

    override fun onMEDIA_RES_MEDIAACTIONUNAVAILABLE(int: Int) {
        Log.d(TAG, " onMEDIA_RES_MEDIAACTIONUNAVAILABLE... $int")
        showQuickNotice(AudioService.instance!!.resources.getString(R.string.action_unavailable))
    }

    override fun onMEDIA_RES_NOWPLAYING_METADATA(mediametadata: MediaMetadata_t) {
        Log.d(TAG, " onMEDIA_RES_NOWPLAYING_METADATA... $mediametadata")
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_GENRE.set(mediametadata.metaDataGenre)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_COMPOSER.set(mediametadata.metaDataComposer)
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_AUDIOBOOK.set("")
        DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_PODCAST.set("")
    }

    override fun onMEDIA_RES_DEVICE_MOUNTED_STATUS(enable: Boolean) {
        Log.d(TAG, " onMEDIA_RES_DEVICE_MOUNTED_STATUS... $enable")
        DataPoolDataHandler.MEDIA_DEVICE_MOUNTED_STATUS.set(enable)
    }

    override fun onMEDIA_RES_MEDIAACTIONUNAVAILABLE_CHECK_CONNECTION(int: Int) {
        Log.d(TAG, " onMEDIA_RES_MEDIAACTIONUNAVAILABLE_CHECK_CONNECTION... $int")
        showQuickNotice(AudioService.instance!!.resources.getString(R.string.audio_cellular_data_unavailable_check_connection_lig_21_580_29))
    }

    override fun onMEDIA_RES_SEEKTO(int: Int) {
        Log.d(TAG, " onMEDIA_RES_SEEKTO... $int")
        if (!AudioService.isSDKAvailable()) {
            val progressInTime = (int * NONUIVariables.getInstance().playTimeDuration) / 100
            if (NONUIVariables.getInstance().isStartTouch) {
                MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.REMOVECALLBACKS.name, 0))
                MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.SEEKTO.name, progressInTime))
                onMEDIA_RES_PLAYTIME(MediaPlayTime_t((NONUIVariables.getInstance().playTimeDuration - progressInTime), progressInTime))
            } else {
                MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.SEEKTO.name, progressInTime))
                MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.UPDATE_PROGRESS.name, 0))
            }
        }
        DataPoolDataHandler.MEDIA_SEEK_PENDING.set(int)
    }

    override fun onMEDIA_RES_MEDIAPLAYERSCREENTRANSITION(mediascreentransition: MediaScreenTransition_t) {
        Log.d(TAG, " onMEDIA_RES_MEDIAPLAYERSCREENTRANSITION... $mediascreentransition")
        DataPoolDataHandler.MEDIA_IS_LISTSCREEN_LOADING.set(mediascreentransition.totalCount)
    }

    override fun onMEDIA_RES_LISTBACKTRAVERSE(mediaplayerlistreq: MediaPlayerListReq_t) {
        Log.d(TAG, " onMEDIA_RES_LISTBACKTRAVERSE Not Implemented... $mediaplayerlistreq")
    }

    override fun onMEDIA_RES_BROWSEBUTTONSUPPORT(status: Boolean) {
        Log.d(TAG, " onMEDIA_RES_BROWSEBUTTONSUPPORT ... $status")
        MEDIA_RES_BROWSEBUTTONSUPPORT.set(status)
    }

    override fun onMEDIA_RES_MEDIASOURCECHANGEDTOBT() {
        Log.d(TAG, " onMEDIA_RES_MEDIASOURCECHANGEDTOBT ...")
    }

    override fun onMEDIA_RES_MEDIASOURCECHANGEDTOUSB() {
        Log.d(TAG, " onMEDIA_RES_MEDIASOURCECHANGEDTOUSB ...")
    }

    fun updateMediaIndexingStatus(device: IDevice) {
        if (device.isDeviceIndexable) {
            val lDeviceTrackIndexId = device.tracksIndexed
            val mMediaIndexedPercentage = (lDeviceTrackIndexId * MediaStringMatcher.MEDIA_INDEXING_PERCENTAGE_TOTAL).toInt()
            NONUIVariables.getInstance().MEDIA_USB_INDEXING_PROGRESS = mMediaIndexedPercentage
            // Here we set mIsMediaIndexingInProgress to true when the indexing progress less than 100%.
            // Because We need to display the 100% status. After displaying the status of 100%, it'll exit Media
            // Indexing Progress.
            NONUIVariables.getInstance().IS_MEDIA_INDEXING_VISISBLE = mMediaIndexedPercentage < 100
            val indexSate: eMediaDevIndexedState
            indexSate = if (device.isDeviceIndexable) {
                when (mMediaIndexedPercentage) {
                    in 1..99 -> eMediaDevIndexedState.MEDIA_DEV_IDS_PARTIAL
                    100 -> eMediaDevIndexedState.MEDIA_DEV_IDS_COMPLETED
                    else -> eMediaDevIndexedState.MEDIA_DEV_IDS_NOT_STARTED
                }
            } else
                eMediaDevIndexedState.MEDIA_DEV_IDS_NOT_SUPPORTED

            onMEDIA_RES_INDEXINGSTATE(ActiveDevMediaIndexingState_t(device.objectID.toInt(), indexSate, mMediaIndexedPercentage, device.deviceMightBePlayable))
        }
    }

    fun onCategoryListItemRes(category: String, localList: List<Any>) {
        NONUIVariables.getInstance().MEDIA_BROWSE_LEVEL = MEDIA_BROWSE_LEVELS.LEVEL_TWO
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
        when (category) {
            USB_BROWSE_PAGE.ARTIST.toString() -> {
                setUsbPackageNameHeaderObjectBrowseSongs(USB_BROWSE_PAGE.ARTIST, false, false, null)
                localList.forEach {
                    mediaUsbBrowseList.add(MediaObject_t(metaDataArtist = it.toString(), playTime = MediaPlayTime_t(0, 0)))
                }
            }
            USB_BROWSE_PAGE.ALBUM.toString() -> {
                setUsbPackageNameHeaderObjectBrowseSongs(USB_BROWSE_PAGE.ALBUM, false, false, null)
                val allSongs = MediaObject_t(metaDataAlbum = NONUIVariables.getInstance().allSongs_st, playTime = MediaPlayTime_t(0, 0))
                mediaUsbBrowseList.add(allSongs)
                localList.forEach {
                    if (it is MediaData)
                        mediaUsbBrowseList.add(MediaObject_t(metaDataAlbum = it.album, metaDataArtist = it.artist, metaDataGenre = it.genre, objectId = it.objectId, isFavorite = it.isFavorite, metaDataSong = it.filepath, playTime = MediaPlayTime_t(0, 0)))
                }
            }
            USB_BROWSE_PAGE.SONGS.toString() -> {
                setUsbPackageNameHeaderObjectBrowseSongs(USB_BROWSE_PAGE.SONGS, true, false, null)
                localList.forEach {
                    if (it is MediaData)
                        mediaUsbBrowseList.add(MediaObject_t(fileName = it.filename, metaDataAlbum = it.album, metaDataArtist = it.artist, metaDataSong = it.filepath, isFavorite = it.isFavorite, metaDataGenre = it.genre, objectId = it.objectId, playTime = MediaPlayTime_t(0, 0)))
                }
            }
            USB_BROWSE_PAGE.GENRES.toString() -> {
                setUsbPackageNameHeaderObjectBrowseSongs(USB_BROWSE_PAGE.GENRES, false, false, null)
                localList.forEach {
                    mediaUsbBrowseList.add(MediaObject_t(metaDataGenre = it.toString(), playTime = MediaPlayTime_t(0, 0)))
                }
            }
            USB_BROWSE_PAGE.COMPILATIONS.toString() -> {
                setUsbPackageNameHeaderObjectBrowseSongs(USB_BROWSE_PAGE.COMPILATIONS, false, true, USB_BROWSE_PAGE.ALBUM.toString())
                val allSongs = MediaObject_t(metaDataAlbum = NONUIVariables.getInstance().allSongs_st, playTime = MediaPlayTime_t(0, 0))
                mediaUsbBrowseList.add(allSongs)
                localList.forEach {
                    if (it is MediaData)
                        mediaUsbBrowseList.add(MediaObject_t(metaDataAlbum = it.album, metaDataArtist = it.artist, metaDataSong = it.filepath, metaDataGenre = it.genre, isFavorite = it.isFavorite, objectId = it.objectId, playTime = MediaPlayTime_t(0, 0)))
                }
            }
            USB_BROWSE_PAGE.COMPOSERS.toString() -> {
                setUsbPackageNameHeaderObjectBrowseSongs(USB_BROWSE_PAGE.COMPOSERS, false, false, null)
                localList.forEach {
                    mediaUsbBrowseList.add(MediaObject_t(metaDataComposers = it.toString(), fileName = it.toString(), playTime = MediaPlayTime_t(0, 0)))
                }
            }
            USB_BROWSE_PAGE.FOLDER.toString() -> {
                setUsbPackageNameHeaderObjectBrowseSongs(USB_BROWSE_PAGE.FOLDER, false, false, null)
                localList.forEach {
                    mediaUsbBrowseList.add(MediaObject_t(metaDataComposers = it.toString(), fileName = it.toString(), playTime = MediaPlayTime_t(0, 0)))
                }
            }
        }
        NONUIVariables.INSTANCE!!.MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
    }

    //common method for set USB_BROWSE_PAGE,MEDIA_HEADER_OBJECT and IS_MEDIA_BROWSE_SONGS
    private fun setUsbPackageNameHeaderObjectBrowseSongs(usb_browse_page: USB_BROWSE_PAGE, isBrowseSongs: Boolean, isTwoLineHeader: Boolean, secondLineHeaderSt: String?) {
        NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME = usb_browse_page
        if (secondLineHeaderSt != null)
            DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = isTwoLineHeader, firstLineHeaderSt = usb_browse_page.toString(), secondLineHeaderSt = secondLineHeaderSt))
        else
            DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = isTwoLineHeader, firstLineHeaderSt = usb_browse_page.toString()))
        NONUIVariables.INSTANCE!!.IS_MEDIA_BROWSE_SONGS = isBrowseSongs
    }

    //common method for set USB_BROWSE_PAGE,MEDIA_BROWSE_LEVEL and IS_MEDIA_BROWSE_SONGS and clear the MEDIA_USB_BROWSE_LIST and add again
    private fun setUsbPackageNameBrowseLevelAndSongs(usb_browse_page: USB_BROWSE_PAGE, media_browse_levels: MEDIA_BROWSE_LEVELS, isBrowseSongs: Boolean) {
        NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME = usb_browse_page
        NONUIVariables.INSTANCE!!.MEDIA_BROWSE_LEVEL = media_browse_levels
        NONUIVariables.INSTANCE!!.IS_MEDIA_BROWSE_SONGS = isBrowseSongs
        NONUIVariables.INSTANCE!!.MEDIA_USB_BROWSE_LIST = ArrayList()
    }

    fun onFolderSongItemRes(any: Any?, list: List<Any>, currentPageEnum: USB_BROWSE_PAGE) {
        try {
            setUsbPackageNameBrowseLevelAndSongs(currentPageEnum, MEDIA_BROWSE_LEVELS.LEVEL_FOUR, true)
            val mediaUsbBrowseList = ArrayList<MediaObject_t>()
            if (any is MediaObject_t) {
                list.forEach {
                    mediaUsbBrowseList.add(MediaObject_t(metaDataArtist = (it as MediaData).artist, metaDataSong = it.filepath, metaDataAlbum = it.album, fileName = it.filename, metaDataComposers = any.metaDataComposers, metaDataGenre = it.genre, objectId = it.objectId, playTime = MediaPlayTime_t(0, 0)))
                }
                NONUIVariables.INSTANCE!!.MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
                if (any.metaDataAlbum == NONUIVariables.INSTANCE!!.allSongs_st)
                    DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataComposers, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
                else
                    DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataAlbum, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun onArtistAlbumItemRes(any: Any?, list: List<Any>, currentPageEnum: USB_BROWSE_PAGE) {
        setUsbPackageNameBrowseLevelAndSongs(currentPageEnum, MEDIA_BROWSE_LEVELS.LEVEL_THREE, false)
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = (any as MediaObject_t).metaDataArtist, secondLineHeaderSt = USB_BROWSE_PAGE.ALBUM.toString()))
        val allSongs = MediaObject_t(metaDataArtist = any.metaDataArtist, metaDataAlbum = NONUIVariables.getInstance().allSongs_st, metaDataGenre = any.metaDataGenre, playTime = MediaPlayTime_t(0, 0))
        mediaUsbBrowseList.add(allSongs)
        list.forEach {
            if (it is MediaData)
                mediaUsbBrowseList.add(MediaObject_t(metaDataArtist = it.artist, metaDataAlbum = it.album, metaDataSong = it.filepath, fileName = it.filename, metaDataGenre = any.metaDataGenre, objectId = it.objectId, isFavorite = it.isFavorite, playTime = MediaPlayTime_t(0, 0)))
        }
        NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
    }

    fun onComposerAlbumItemRes(any: Any?, list: List<Any>, currentPageEnum: USB_BROWSE_PAGE) {
        setUsbPackageNameBrowseLevelAndSongs(currentPageEnum, MEDIA_BROWSE_LEVELS.LEVEL_THREE, false)
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = (any as MediaObject_t).fileName, secondLineHeaderSt = USB_BROWSE_PAGE.ALBUM.toString()))
        val allSongs = MediaObject_t(metaDataArtist = any.metaDataArtist, metaDataAlbum = NONUIVariables.getInstance().allSongs_st, metaDataComposers = any.metaDataComposers, playTime = MediaPlayTime_t(0, 0))
        mediaUsbBrowseList.add(allSongs)
        list.forEach {
            if (it is MediaData)
                mediaUsbBrowseList.add(MediaObject_t(metaDataArtist = it.artist, metaDataAlbum = it.album, metaDataSong = it.filepath, fileName = it.filename, metaDataGenre = any.fileName, metaDataComposers = any.metaDataComposers, objectId = it.objectId, isFavorite = it.isFavorite, playTime = MediaPlayTime_t(0, 0)))
        }
        NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
    }

    fun onComposerAlbumSongsItemRes(any: Any?, list: List<Any>, currentPageEnum: USB_BROWSE_PAGE) {
        setUsbPackageNameBrowseLevelAndSongs(currentPageEnum, MEDIA_BROWSE_LEVELS.LEVEL_FOUR, true)
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        if (any is MediaObject_t) {
            list.forEach {
                mediaUsbBrowseList.add(MediaObject_t(metaDataArtist = (it as MediaData).artist, metaDataSong = it.filepath, metaDataAlbum = it.album, fileName = it.filename, metaDataComposers = any.metaDataComposers, metaDataGenre = it.genre, objectId = it.objectId, playTime = MediaPlayTime_t(0, 0)))
            }
            NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
            if (any.metaDataAlbum == NONUIVariables.getInstance().allSongs_st)
                DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataComposers, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
            else
                DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataAlbum, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
        }
    }

    fun onArtistAlbumSongsItemRes(any: Any?, list: List<Any>, currentPageEnum: USB_BROWSE_PAGE) {
        setUsbPackageNameBrowseLevelAndSongs(currentPageEnum, MEDIA_BROWSE_LEVELS.LEVEL_FOUR, true)
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        list.forEach {
            mediaUsbBrowseList.add(MediaObject_t(metaDataArtist = (it as MediaData).artist, metaDataSong = it.filepath, metaDataAlbum = it.album, fileName = it.filename, metaDataGenre = (any as MediaObject_t).metaDataGenre, objectId = it.objectId, playTime = MediaPlayTime_t(0, 0)))
        }
        NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
        if (any is MediaObject_t) {
            if (any.metaDataAlbum == NONUIVariables.getInstance().allSongs_st) {
                if (currentPageEnum == USB_BROWSE_PAGE.COMPILATION_SONGS)
                    DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = USB_BROWSE_PAGE.COMPILATIONS.toString(), secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))

                if (currentPageEnum == USB_BROWSE_PAGE.GENRE_ARTIST_SONG)
                    DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataGenre, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
                else
                    DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataArtist, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
            } else
                DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataAlbum, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))

        }
    }

    fun onAlbumSongsItemRes(any: Any?, list: List<Any>, currentPageEnum: USB_BROWSE_PAGE) {
        setUsbPackageNameBrowseLevelAndSongs(currentPageEnum, MEDIA_BROWSE_LEVELS.LEVEL_THREE, true)
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        if (any is MediaObject_t) {
            if (any.metaDataAlbum == NONUIVariables.getInstance().allSongs_st) {
                if (currentPageEnum == USB_BROWSE_PAGE.COMPILATION_SONGS) {
                    DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = USB_BROWSE_PAGE.COMPILATIONS.toString(), secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
                } else
                    DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = USB_BROWSE_PAGE.ALBUM.toString(), secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
            } else {
                DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataAlbum, secondLineHeaderSt = USB_BROWSE_PAGE.SONGS.toString()))
            }
            list.forEach {
                mediaUsbBrowseList.add(MediaObject_t(fileName = (it as MediaData).filename, metaDataSong = it.filepath, metaDataAlbum = it.album, metaDataArtist = it.artist, objectId = it.objectId, isFavorite = it.isFavorite, metaDataGenre = it.genre, playTime = MediaPlayTime_t(0, 0)))
            }
            NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
        }
    }

    fun onArtistItemRes(any: Any?, list: List<Any>, currentPageEnum: USB_BROWSE_PAGE) {
        setUsbPackageNameBrowseLevelAndSongs(currentPageEnum, MEDIA_BROWSE_LEVELS.LEVEL_THREE, false)
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
        if (any is MediaObject_t) {
            DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = true, firstLineHeaderSt = any.metaDataGenre, secondLineHeaderSt = USB_BROWSE_PAGE.ARTIST.toString()))
            val allSongs = MediaObject_t(metaDataArtist = NONUIVariables.getInstance().allSongs_st, metaDataGenre = any.metaDataGenre, metaDataAlbum = NONUIVariables.getInstance().allSongs_st, playTime = MediaPlayTime_t(any.playTime.remainingTime, any.playTime.elapsedTime))
            mediaUsbBrowseList.add(allSongs)
            list.forEach {
                mediaUsbBrowseList.add(MediaObject_t(metaDataArtist = it.toString(), metaDataGenre = any.metaDataGenre, playTime = MediaPlayTime_t(0, 0)))
            }
            NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
        }
    }

    fun onUsbBrowseResp() {
        setUsbPackageNameBrowseLevelAndSongs(USB_BROWSE_PAGE.CATEGORY, MEDIA_BROWSE_LEVELS.LEVEL_ONE, false)
        DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = false, firstLineHeaderSt = AudioService.instance!!.resources.getString(R.string.browse_st)))
        val mediaUsbBrowseList = ArrayList<MediaObject_t>()
        AudioService.instance!!.resources.getStringArray(R.array.browse_items).forEach {
            mediaUsbBrowseList.add(MediaObject_t(fileName = it, playTime = MediaPlayTime_t(0, 0)))
        }
        NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
    }

    fun onPageUpdate(currentPage: USB_BROWSE_PAGE) {
        NONUIVariables.getInstance().USB_BROWSE_PAGENAME = currentPage
    }

    fun onBrowseSongItemRes() {
        NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST = ArrayList()
        NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST = NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST
    }

    fun getMediaBrowseType(): EnumBrowseChildType {
        var mCurrentBrowseType = EnumBrowseChildType.none
        if (NONUIVariables.getInstance().mActiveBrowseAddresses.size > 0) {
            val browseItem = NONUIVariables.getInstance().mActiveBrowseAddresses.peek()
            mCurrentBrowseType = browseItem.getmBrowseChildType()
        }
        return mCurrentBrowseType
    }

    fun updateUsbMediaIndexingStatus(int: Int) {
        NONUIVariables.getInstance().MEDIA_USB_INDEXING_PROGRESS = int
        // Here we set mIsMediaIndexingInProgress to true when the indexing progress less than 100%.
        // Because We need to display the 100% status. After displaying the status of 100%, it'll exit Media
        // Indexing Progress.
        NONUIVariables.getInstance().IS_MEDIA_INDEXING_VISISBLE = int < 100
    }


    //common method for add/remove MEDIA_FAVOURITES_SONGS_LIST
    private fun setUsbFavouritesListAndBrowseFav(mediaName: String) {
        if (DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST.contains(mediaName)) {
            DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST.remove(mediaName)
            USB_BROWSE_FAV_UPDATE = false
        } else {
            DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST.add(mediaName)
            USB_BROWSE_FAV_UPDATE = true
        }
    }

    /**
     * call this method to set current source type
     * @param waveBand constant fom NOWPLAYING_SOURCE_TYPE
     */
    fun onCurrentSourceChanged(waveBand: NOWPLAYING_SOURCE_TYPE) {
        NONUIVariables.getInstance().previousSourceType = waveBand
        NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE = waveBand
//        ManualTunerController.validateSourceType(waveBand.name)
    }

    //   /**
    //   * call this method when device(audio source) is connected/disconnected. It saves the device info for later use
    //   * @param iDevice connected device
    //   */
//    fun onDeviceChanged(iDevice: IDevice) {
//        val dData = DeviceData()
//        dData.type = iDevice.type.toString()
//        dData.id = iDevice.objectID.toString()
//        dData.isReadable = iDevice.deviceMightBePlayable.toString()
//        dData.isBrowsable = iDevice.canBrowse().toString()
//        if (iDevice.volumeName.isNotEmpty()) {
//            dData.name = iDevice.volumeName.toString()
//        }
//        NONUIVariables.getInstance().deviceData = dData
//    }

    /**
     * this method is called to add/remove station as favorite.
     * Station is added as favorite if not available in the favorite list otherwise it wfonFavoriteRequestill be removed from the list
     * @param  any station to be added/removed as favorite
     */
    fun updateFavorites(any: Any) {
        try {
            if (any is AMFMStationInfo_t) {
                if (NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.size <= 0) {
                    val filterItem = NONUIVariables.getInstance().browseList.filter { it -> it.frequency.toString() == any.frequency.toString() }
                    if (filterItem.isNotEmpty()) { // if open brace
                        val index = NONUIVariables.getInstance().browseList.indexOf(filterItem[0])
                        if (any.isFavorite) {
                            any.isFavorite = false
                            NONUIVariables.getInstance().favoriteList.remove(any.frequency.toString())
                        } else {
                            NONUIVariables.getInstance().favoriteList.add(any.frequency.toString())
                            any.isFavorite = true
                        }
                        updateFavoriteListWithBrowseList(any, index, filterItem[0])

                    } //if close brace

                } else {
                    val filterItem = NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.filter { it -> it.frequency.toString() == any.frequency.toString() }
                    if (filterItem.isNotEmpty()) { //if open brace
                        val index = NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST.indexOf(filterItem[0])
                        if (any.isFavorite) {
                            any.isFavorite = false
                            NONUIVariables.getInstance().favoriteList.remove(any.frequency.toString())
                        } else {
                            NONUIVariables.getInstance().favoriteList.add(any.frequency.toString())
                            any.isFavorite = true
                        }
                        updateFavoriteListWithCategoryList(any, index, filterItem[0])
                    } //if close brace
                }
            } else if (any is MediaObject_t) {
                when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) {
                    USB_BROWSE_PAGE.ARTIST -> {
                        USB_BROWSE_FAV_UPDATE = if (MEDIA_FAVOURITES_ARTIST_LIST.contains(any.metaDataArtist)) {
                            MEDIA_FAVOURITES_ARTIST_LIST.remove(any.metaDataArtist)
                            false
                        } else {
                            MEDIA_FAVOURITES_ARTIST_LIST.add(any.metaDataArtist)
                            true
                        }
                    }
                    USB_BROWSE_PAGE.SONGS, USB_BROWSE_PAGE.ALBUM_SONGS, USB_BROWSE_PAGE.ARTIST_ALBUM_SONGS ->
                        setUsbFavouritesListAndBrowseFav(any.fileName)
                    USB_BROWSE_PAGE.ALBUM, USB_BROWSE_PAGE.ARTIST_ALBUM -> {
                        USB_BROWSE_FAV_UPDATE = if (MEDIA_FAVOURITES_ALBUM_LIST.contains(any.metaDataAlbum)) {
                            MEDIA_FAVOURITES_ALBUM_LIST.remove(any.metaDataAlbum)
                            false
                        } else {
                            MEDIA_FAVOURITES_ALBUM_LIST.add(any.metaDataAlbum)
                            true
                        }
                    }
                    else ->
                        setUsbFavouritesListAndBrowseFav(any.fileName)

                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //common method for favoriteList using browseList update
    private fun updateFavoriteListWithBrowseList(any: AMFMStationInfo_t, index: Int, filterItem: AMFMStationInfo_t) {
        val browseList = NONUIVariables.getInstance().browseList
        browseList[index] = any
        NONUIVariables.getInstance().browseList = ArrayList()
        NONUIVariables.getInstance().browseList = browseList
        if (filterItem.frequency.toString() == NONUIVariables.getInstance().aMFMStationInfo_t!!.frequency.toString())
            NONUIVariables.getInstance().aMFMStationInfo_t = addAMFMStation(any, any.isFavorite)
    }

    //common method for favoriteList using browseList update
    private fun updateFavoriteListWithCategoryList(any: AMFMStationInfo_t, index: Int, filterItem: AMFMStationInfo_t) {
        val amFmTunerCategoryStationListObjectList = NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST
        amFmTunerCategoryStationListObjectList[index] = any
        NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = ArrayList()
        NONUIVariables.getInstance().AMFMTUNER_CATEGORYSTATIONLIST_OBJECTLIST = amFmTunerCategoryStationListObjectList

        if (filterItem.frequency.toString() == NONUIVariables.getInstance().aMFMStationInfo_t!!.frequency.toString())
            NONUIVariables.getInstance().aMFMStationInfo_t = addAMFMStation(any, any.isFavorite)
    }

    /**
     * Returns list of station based on category
     * @param categoryType any constant of [eRDSPtyNACategory]
     * @return ArrayList<AMFMStationInfo_t>
     */
    private fun getAMFMCategoryListByType(categoryType: eRDSPtyNACategory): MutableMap<String, ArrayList<AMFMStationInfo_t>> {
        val stationList: ArrayList<AMFMStationInfo_t> = ArrayList()
        val map: MutableMap<String, ArrayList<AMFMStationInfo_t>> = hashMapOf()
        val filterItems = NONUIVariables.getInstance().browseList.filter { it -> it.ptyCategory == categoryType.value }
        stationList.addAll(filterItems)
        map["" + categoryType] = stationList
        return map
    }

    //update category list
    fun updateCategoriesList(categoryMutableList: MutableList<AMFMStationInfo_t>?) {
        var categoryList = categoryMutableList
        if (AudioService.isSDKAvailable())
            categoryList = CategoriesUtil.getCategoriesInformationFromStations(NONUIVariables.getInstance().browseList)

        DataPoolDataHandler.amfmCategoryList.clear()
        if (categoryList != null && categoryList.isNotEmpty())
            DataPoolDataHandler.amfmCategoryList.addAll(categoryList)
        else {
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory(eRDSPtyNACategory.AMFM_RDS_PTY_NA_POP, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_POP)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory(eRDSPtyNACategory.AMFM_RDS_PTY_NA_ROCK, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_ROCK)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory(eRDSPtyNACategory.AMFM_RDS_PTY_NA_TALK, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_TALK)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory(eRDSPtyNACategory.AMFM_RDS_PTY_NA_COUNTRY, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_COUNTRY)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory(eRDSPtyNACategory.AMFM_RDS_PTY_NA_CLASSICAL, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_CLASSICAL)))
            DataPoolDataHandler.amfmCategoryList.add(addAMFMCategory(eRDSPtyNACategory.AMFM_RDS_PTY_NA_JAZZ, getAMFMCategoryListByType(eRDSPtyNACategory.AMFM_RDS_PTY_NA_JAZZ)))
        }
    }

    //add category data
    private fun addAMFMCategory(categoryType: eRDSPtyNACategory, map: MutableMap<String, ArrayList<AMFMStationInfo_t>>): AMFMStationInfo_t {
        return Builder()
                .stationName("" + categoryType)
                .categoryData(map).build()
    }

    //add FM/AM station data
    private fun addAMFMStation(stationInfo: AMFMStationInfo_t, isFavorite: Boolean): AMFMStationInfo_t {
        return Builder()
                .frequency(stationInfo.frequency)
                .stationName(stationInfo.stationName!!)
                .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                .objectId(stationInfo.objectId)
                .rdsStationInfo(stationInfo.rdsStationInfo!!)
                .ptyCategory(stationInfo.ptyCategory)
                .tpStationStatus(eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP)
                .isFavorite(isFavorite)
                .build()
    }

    /**
     * Displays Quick Notice using given message
     * @param message Description for quick notice
     */
    fun showQuickNotice(message: String) {
        Toast.makeText(AudioService.instance, message, Toast.LENGTH_SHORT).show()
    }

    fun isMediaProgressBarVisible(isVisible: Boolean) {
        DataPoolDataHandler.MEDIA_IS_PROGRESS_BAR_VISIBLE.set(isVisible)
    }


}