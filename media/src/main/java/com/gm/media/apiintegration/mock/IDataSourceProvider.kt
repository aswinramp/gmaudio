package com.gm.media.apiintegration.mock

import com.gm.media.models.*


/**
 *  Interface for fetching AM/FM channel/station data
 *
 * @author Aswin on 3/21/2018.
 */

interface IDataSourceProvider {
    /**
     * @return list of AM/FM channel/station
     */
    fun nowPlayingDataList(): ArrayList<AMFMStationInfo_t>
}