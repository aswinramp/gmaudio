package com.gm.media.apiintegration.apiinterfaces

import com.gm.media.models.eAMFMSeekType
import com.gm.media.models.eAMFMTuneType

/**
 *  It should contain AMFM_REQ funcs "DECLARATIONS" only func from CAMFMProxy.cpp.
 *  only some requests that are being used contain "AMFM_REQ" in function definition, only these should be added
 *  these funcs usually start after proxy constructor func in CAMFMProxy.cpp
 *  This should exclude all the functions under RecvData func, as the func in RecvData only corresponds to RES functions
 *
 *  These Methods are called when user interact with UI
 */
interface IAMFMManagerReq {
    /**
     * To seek next/previous available AM station
     *
     * @param any [eAMFMSeekType], defines which type of seek has to perform
     */
    fun onAMFM_REQ_AMSEEKSTATION(any: Any?)
    /**
     * To seek next/previous available FM station
     *
     * @param any [eAMFMSeekType], defines which type of seek has to perform
     */
    fun onAMFM_REQ_FMSEEKSTATION(any : Any?)
    /**
     * To tune to specific AM station
     *
     * @param any [int],  tune with specific frequency
     */
    fun onAMFM_REQ_AMTUNEBYFREQUENCY(any: Any?)

    /**
     * To tune to specific FM station
     *
     * @param any [int], tune with specific frequency
     */
    fun onAMFM_REQ_FMTUNEBYFREQUENCY(any: Any?)
    /**
     * To tune next/previous AM station
     *
     * @param any [eAMFMTuneType], defines which type of tune has to perform
     */
    fun onAMFM_REQ_AMTUNESTATION(any: Any?)
    /**
     * To tune next/previous FM station
     *
     * @param any [eAMFMTuneType], defines which type of tune has to perform
     */
    fun onAMFM_REQ_FMTUNESTATION(any: Any?)

    /**
     * To update AM station list
     */
    fun onAMFM_REQ_AMSTATIONLIST()// fun onAMFM_REQ_AMSTATIONLIST(status : Int)
    /**
     * To update FM station list
     */
    fun onAMFM_REQ_FMSTATIONLIST()//fun onAMFM_REQ_FMSTATIONLIST(status : Int)

    /**
     *To update AM station after seek process
     * @param any [eAMFMSeekType], defines which type of seek has to perform
     */
    fun onAMFM_REQ_AMHARDSEEKSTATION(any:Any?)
    /**
     *To update FM station after seek process
     * @param any [eAMFMSeekType], defines which type of seek has to perform
     */
    fun onAMFM_REQ_FMHARDSEEKSTATION(any:Any?)

    /**
     * Used to start manual update for am
     */
    fun onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST()
    /**
     *  Used to start manual update for fm
     */
    fun onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST()
    /**
     * To tune to specific AM station with ObjectId
     *
     * @param any [int],  station object id
     */
    fun onAMFM_REQ_AMTUNEBYOBJECTID(any: Any?)
    /**
     * To Tune AM station by manually entered frequencies
     * @param any [String], manually entered frequency
     */
    fun onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY(any: Any?)
    /**
     * To abort the AM station list manual updating
     */
    fun onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE()
    /**
     * To abort the FM station list manual updating
     */
    fun onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE()
    /**
     * To tune to specific FM station with ObjectId
     *
     * @param any [int],  station object id
     */
    fun onAMFM_REQ_FMTUNEBYOBJECTID(any: Any?)

    /**
     * To Tune FM station by manually entered frequencies
     * @param any [String], manually entered frequency
     */
    fun onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY(any: Any?)
    /**
     * To update category list for AM
     * @param any [TunerCategoryList_t],  category object
     */
    fun onAMFM_REQ_FMCATEGORYSTATIONLIST(any: Any?)
    /**
     * To update category list for FM
     * @param any [TunerCategoryList_t],  category object
     */
    fun onAMFM_REQ_AMCATEGORYSTATIONLIST(any: Any?)

    /**
     * To enable/disable the RDS button
     * @param rdsswitch switch status
     */
    fun onAMFM_REQ_FMRDSSWITCH(rdsswitch : Int)
    /**
     * To enable/disable TPS status
     * @param any [int], status of TPS
     */
    fun onAMFM_REQ_TPSTATUS(any: Any?)

    /**
     * To Tune to given station
     * @param programIdentifier
     */
    fun onAMFM_REQ_FMTUNETMCSTATION(programIdentifier : Int)
    /**
     * To dismiss the Traffic alert dialogs
     */
    fun onAMFM_REQ_TRAFFICALERT_DISMISS()
    /**
     * To disable TPS status when traffic alerts are
     */
    fun onAMFM_REQ_TRAFFICALERT_LISTEN()
    /**
     * To set current region of device
     * @param state device region
     */
    fun onAMFM_REQ_SETREGIONSETTING(state : Int)

}