package com.gm.media.apiintegration.mock

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.hardware.usb.UsbManager
import android.media.MediaMetadataRetriever
import android.net.Uri
import android.os.*
import android.text.TextUtils
import android.widget.Toast
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.apiintegration.apiinterfaces.IManager
import com.gm.media.apiintegration.apiinterfaces.MediaCustomInterface
import com.gm.media.apiintegration.mock.AmFmMediaPlayer.initAudioPlay
import com.gm.media.apiintegration.mock.AmFmMediaPlayer.releasePlayer
import com.gm.media.models.AMFMStationInfo_t
import com.gm.media.models.NONUIVariables
import com.gm.media.database.FavoritesContract
import com.gm.media.database.MediaData
import com.gm.media.models.*
import com.gm.media.models.EventObject
import com.gm.media.utils.AudioConstants
import com.gm.media.utils.FavoriteDataProcessor
import com.gm.media.utils.Log
import com.gm.media.utils.ManualTunerController.amLowerValue
import com.gm.media.utils.ManualTunerController.amStepValue
import com.gm.media.utils.ManualTunerController.amUpperValue
import com.gm.media.utils.ManualTunerController.fmLowerValue
import com.gm.media.utils.ManualTunerController.fmStepValue
import com.gm.media.utils.ManualTunerController.fmUpperValue
import com.gm.media.utils.ManualTunerController.getEnteredFrequency
import com.gm.media.utils.ManualTunerController.getFreqTxtForDisplay
import com.gm.media.utils.ManualTunerController.getFrequency
import com.gm.media.utils.ManualTunerController.getPossibleValues
import com.gm.media.utils.ManualTunerController.isValidEntry
import com.gm.media.utils.ManualTunerController.mCurrentModeChart
import com.gm.media.utils.ManualTunerController.prepareChart
import com.gm.media.utils.ManualTunerController.updateButtons
import com.google.gson.Gson
import java.io.File
import java.util.*
import com.gm.media.R
import com.gm.media.utils.MediaUtil

/**
 * This class provides implementation for IManager Interface using simulation data. This class is used when audio app
 * needs to work completely with static data. This class provides functionality for AM/FM tuning, direct tuning,favorite selection,browsing channels
 */
class SimulationManager : IManager {

    private val TAG = SimulationManager::class.simpleName

    private var isTrafficProgramSupported = false
    private var isTrafficAlertPlaying: Boolean = false
    private var isTPStation = false
    private var MEDIA_DEVICE_LIST = ArrayList<MediaDeviceInfo_t>()
    private var MEDIA_ACTIVE_DEVICE_LIST_INFO: MediaDeviceInfo_t? = null
    private var usbStorageMediaList = ArrayList<MediaObject_t>()
    //create object for IAMFMManagerRes and use context

    fun registerApiCallback(mediaCallBack: MediaCustomInterface) {
        Log.d(TAG, "registerTunerListener... $mediaCallBack")
        NONUIVariables.getInstance().mMediaCallBack = mediaCallBack
    }

    /**
     * Initialises Simulation Manager
     */
    init {
        Log.d(TAG, "int Block... ")
        INIT_AMFM_REQUEST()
        INIT_MEDIA_REQUEST()
    }

    companion object {
        var INSTANCE: SimulationManager? = null
        fun getInstance(): SimulationManager {
            if (INSTANCE == null)
                INSTANCE = SimulationManager()
            return INSTANCE!!
        }
    }

    override fun onAMFM_REQ_AMSTATIONLIST() {
        Log.d(TAG, "onAMFM_REQ_AMSTATIONLIST... ")
        playFMAMStationList(NOWPLAYING_SOURCE_TYPE.AM)
    }

    override fun onAMFM_REQ_FMSTATIONLIST() {
        Log.d(TAG, "onAMFM_REQ_FMSTATIONLIST... ")
        if ((NONUIVariables.getInstance().showUpdateProgress)) {
            NONUIVariables.getInstance().audioControlDisabled = false
            NONUIVariables.getInstance().showUpdateProgress = false
        }
        playFMAMStationList(NOWPLAYING_SOURCE_TYPE.FM)
    }

    override fun onAMFMTuneRequest(any: Any?) {
        Log.d(TAG, "onAMFMTuneRequest... $any")
        if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name.equals(NOWPLAYING_SOURCE_TYPE.AM.name))
            onAMFM_REQ_AMTUNEBYFREQUENCY(any)
        else
            onAMFM_REQ_FMTUNEBYFREQUENCY(any)
    }

    override fun onFavoriteRequest(any: Any?) {
        Log.d(TAG, "onFavoriteRequest... $any")
        if (NONUIVariables.getInstance().audioControlDisabled) {
            showQuickNotice(AudioService.instance!!.getString(R.string.favorite_unavailable))
            return
        }
        if (null != any) {
            FavoriteDataProcessor.addOrRemoveFavorite(any)
            SystemListener.updateFavorites(any)
        }
    }

    override fun INIT_AMFM_REQUEST() {
        Log.d(TAG, "INIT_AMFM_REQUEST...")
        SharedPreferencesController.loadPreferences()
        val stationInfo = getCurrentStation(null)
        setBrowseStationList()
        if (stationInfo.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(addAMFMStation(stationInfo, NONUIVariables.getInstance().favoriteList.contains(stationInfo.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        else if (stationInfo.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.FM.name)
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(addAMFMStation(stationInfo, NONUIVariables.getInstance().favoriteList.contains(stationInfo.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        //delay initAudioPlay so it start playing after screen is visible
        Handler().postDelayed({ initAudioPlay(getCurrentStationIndex()) }, 200)
    }

    /************************IManager Methods*******************************/

    override fun INIT_MEDIA_REQUEST() {
        Log.d(TAG, "INIT_MEDIA_REQUEST...")
    }


    override fun onSourceReset(sourceTYPE: NOWPLAYING_SOURCE_TYPE?) {
        Log.d(TAG, "onSourceReset...  $sourceTYPE")
        when (NONUIVariables.getInstance().previousSourceType) {
            NOWPLAYING_SOURCE_TYPE.AM ->
                resetTuner()
            NOWPLAYING_SOURCE_TYPE.FM ->
                resetTuner()
            NOWPLAYING_SOURCE_TYPE.USBMSD, NOWPLAYING_SOURCE_TYPE.AUX ->
                resetMedia()
        }

        if (NOWPLAYING_SOURCE_TYPE.USBMSD == sourceTYPE && NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size > 0 && !NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING) {
            SystemListener.onMEDIA_RES_DEVICE_MOUNTED_STATUS(true)
            SystemListener.onMEDIA_RES_BROWSEBUTTONSUPPORT(true)
            SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PLAY)
            MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.PLAYPAUSE.name, NONUIVariables.getInstance().playingUsbIndex))
        }

        SystemListener.onCurrentSourceChanged(sourceTYPE!!)
    }

    //play the next or previous AM stations
    override fun onAMFM_REQ_AMSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMSEEKSTATION...  $any")
        playNextPreviousSeekStation(any)
    }

    //play the next or previous FM stations
    override fun onAMFM_REQ_FMSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMSEEKSTATION...  $any")
        playNextPreviousSeekStation(any)
        try {
            if (isTrafficProgramSupported && isTPStation && NONUIVariables.getInstance().FMTUNER_TPSTATUS == 1) {
                isTrafficAlertPlaying = false
                val aMFMProgramTYPE = AMFMProgramTypeAlert_t(NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME, NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT)
                SystemListener.onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(aMFMProgramTYPE)
                onAMFM_REQ_TRAFFICALERT_LISTEN()
            }
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_AMTUNESTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNESTATION...  $any")
        nextPreviousTunerStation(any)
    }

    override fun onAMFM_REQ_FMTUNESTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNESTATION...  $any")
        nextPreviousTunerStation(any)
    }

    override fun onAMFM_REQ_AMTUNEBYOBJECTID(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNEBYOBJECTID...  $any")
        try {
            playStationByTunerObjectID(any)
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_FMTUNEBYOBJECTID(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNEBYOBJECTID...  $any")
        try {
            playStationByTunerObjectID(any)
        } catch (e: Exception) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNEBYPARTIALFREQUENCY...  $any")
        val frequencyKeys: String = getFrequency(any)!!
        mCurrentModeChart = prepareChart(amLowerValue.toLong(), amUpperValue.toLong(), amStepValue.toLong())
        val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
        val buttonValues = updateButtons(false, getPossibleValues(enteredFrequency))
        SystemListener.onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))
    }

    override fun onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNEBYPARTIALFREQUENCY...  $any")
        val frequencyKeys: String = getFrequency(any)!!
        mCurrentModeChart = prepareChart(fmLowerValue.toLong(), fmUpperValue.toLong(), fmStepValue.toLong())
        val enteredFrequency: Long = getEnteredFrequency(frequencyKeys)
        val buttonValues = updateButtons(false, getPossibleValues(enteredFrequency))
        if (isValidEntry(frequencyKeys)) {
            for (i in buttonValues.indices) {
                buttonValues[i] = false
            }
        }
        SystemListener.onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(ManualTuneData(getFreqTxtForDisplay(frequencyKeys), isValidEntry(frequencyKeys), buttonValues))
    }

    override fun onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE() {
        Log.d(TAG, "onAMFM_REQ_CANCELAMSTRONGSTATIONSLISTUPDATE... ")
        SystemListener.onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE("")
    }

    override fun onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE() {
        Log.d(TAG, "onAMFM_REQ_CANCELFMSTRONGSTATIONSLISTUPDATE... ")
        SystemListener.onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE("")
    }

    override fun onAMFM_REQ_AMHARDSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMHARDSEEKSTATION...  $any")
        playNextPreviousSeekStation(any)
        val index = getCurrentStationIndex()
        SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(addAMFMStation(NONUIVariables.getInstance().aMFMStationInfo_t!!, NONUIVariables.getInstance().favoriteList.contains(NONUIVariables.getInstance().aMFMStationInfo_t!!.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        initAudioPlay(index)
    }

    override fun onAMFM_REQ_FMHARDSEEKSTATION(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMHARDSEEKSTATION...  $any")
        playNextPreviousSeekStation(any)
        val index = getCurrentStationIndex()
        SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(addAMFMStation(NONUIVariables.getInstance().aMFMStationInfo_t!!, NONUIVariables.getInstance().favoriteList.contains(NONUIVariables.getInstance().aMFMStationInfo_t!!.frequency.toString()), eTPStationStatus.AMFM_STATION_SUPPORTS_TP))
        initAudioPlay(index)
    }

    override fun onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST() {
        Log.d(TAG, "onAMFM_REQ_UPDATEAMSTRONGSTATIONSLIST... ")
        updateAMFMStrongStationList()
    }

    override fun onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST() {
        Log.d(TAG, "onAMFM_REQ_UPDATEFMSTRONGSTATIONSLIST... ")
        updateAMFMStrongStationList()
    }

    override fun onAMFM_REQ_AMTUNEBYFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMTUNEBYFREQUENCY... ")
        if (any != null) {
            playAMFMTunerByFrequency(any)
        }
    }

    override fun onAMFM_REQ_FMTUNEBYFREQUENCY(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMTUNEBYFREQUENCY... $any")
        if (any != null) { //if open
            val it = playAMFMTunerByFrequency(any)
            isTPStation = it!!.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
            isTrafficProgramSupported = it.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
            try {
                if (isTrafficProgramSupported && isTPStation && NONUIVariables.getInstance().FMTUNER_TPSTATUS == 1) {
                    isTrafficAlertPlaying = false
                    val aMFMProgramTYPE = AMFMProgramTypeAlert_t(NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_NAME,
                            NONUIVariables.getInstance().FMTUNER_PROGRAMTYPERDSNOTIFICATION_CONTENT)
                    SystemListener.onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(aMFMProgramTYPE)
                    onAMFM_REQ_TRAFFICALERT_LISTEN()
                }
            } catch (e: Exception) {
                Log.e(TAG, e.printStackTrace().toString())
            }
        } //if close
    }

    override fun onAMFM_REQ_FMCATEGORYSTATIONLIST(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_FMCATEGORYSTATIONLIST... $any")
        SystemListener.onAMFM_RES_FMCATEGORYSTATIONLIST(any as AMFMStationInfo_t)
    }

    override fun onAMFM_REQ_FMRDSSWITCH(rdsswitch: Int) {
        Log.d(TAG, "onAMFM_REQ_FMRDSSWITCH... $rdsswitch")
    }

    override fun onAMFM_REQ_TPSTATUS(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_TPSTATUS... $any")
        // val checked: Int
        // var station = NONUIVariables.getInstance().aMFMStationInfo_t
        if (any == 0) {
            if (NONUIVariables.getInstance().aMFMStationInfo_t!!.tpStationStatus.value != eTPStationStatus.AMFM_STATION_SUPPORTS_TP.value) {
                //tpStatusChecked = 1
                // TrafficTask(simulationManager).execute()
            }
        } else {
            //tpStatusChecked = 0
            val amFmTPStatus = AMFMTPStatus_t(eTPStationState.AMFM_TP_SEARCH_NONE, 0)
            SystemListener.onAMFM_RES_TPSTATUS(amFmTPStatus)
//            try {
//                if (null != dialog) {
//                    isTrafficAlertPlaying = false
//                    dialog!!.dismiss()
//                }
//            } catch (e: TunerServiceStatusException) {
//                e.printStackTrace()
//            } catch (e: TunerException) {
//                e.printStackTrace()
//            }
        }

        onDialogNoButton()
    }

    override fun onAMFM_REQ_FMTUNETMCSTATION(programIdentifier: Int) {
        Log.d(TAG, "onAMFM_REQ_FMTUNETMCSTATION... $programIdentifier")
    }

    override fun onAMFM_REQ_TRAFFICALERT_DISMISS() {
        Log.d(TAG, "onAMFM_REQ_TRAFFICALERT_DISMISS... ")
        onDialogYesButton()
//        if (null != dialog && dialog!!.isShowing) {
//            isTrafficAlertPlaying = false
//            dialog!!.dismiss()
//        }
    }

    override fun onAMFM_REQ_TRAFFICALERT_LISTEN() {
        Log.d(TAG, "onAMFM_REQ_TRAFFICALERT_LISTEN... ")
        try {
            if (!isTrafficAlertPlaying) {
//                dialog = TrafficAlertDialog()
//                dialog!!.setCancelable(false)
//                dialog!!.setCanceledOnTouchOutside(false)
//                dialog!!.show()
                isTrafficAlertPlaying = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onAMFM_REQ_SETREGIONSETTING(state: Int) {
        Log.d(TAG, "onAMFM_REQ_SETREGIONSETTING... $state")
    }

    override fun onAMFM_REQ_GREYEDBUTTON_CLICK() {
        Log.d(TAG, "onAMFM_REQ_GREYEDBUTTON_CLICK... ")
        SystemListener.showQuickNotice(AudioService.instance!!.resources.getString(R.string.audio_action_not_supported))
    }

    /**
     * Displays Quick Notice using given message
     * @param message s s
     */
    private fun showQuickNotice(message: String) {
        Log.d(TAG, "showQuickNotice... $message")
        Toast.makeText(AudioService.instance!!, message, Toast.LENGTH_SHORT).show()
    }

    override fun onAMFM_REQ_AMCATEGORYSTATIONLIST(any: Any?) {
        Log.d(TAG, "onAMFM_REQ_AMCATEGORYSTATIONLIST... $any")
        SystemListener.onAMFM_RES_AMCATEGORYSTATIONLIST(any as AMFMStationInfo_t)
    }

    override fun onCategoryList() {
        Log.d(TAG, "onCategoryList... ")
        SystemListener.updateCategoriesList(null)
    }

    /**
     * integration of usb
     */
    override fun onDialogDisplay() {
        Log.d(TAG, "onDialogDisplay... " + NONUIVariables.getInstance())
        Log.d(TAG, "onDialogDisplay... " + NONUIVariables.getInstance().mMediaCallBack)
        NONUIVariables.getInstance().mMediaCallBack!!.showCustomAlertDialog()
    }

    override fun onDialogYesButton() {
        Log.d(TAG, "onDialogYesButton... ")
        if (NONUIVariables.getInstance().dialogDisplayEvent == AudioService.instance!!.resources.getString(R.string.dialog_display_event_reset_music_index)) {
            NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST = ArrayList()
            NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = ArrayList()
            NONUIVariables.getInstance().mMediaCallBack!!.dismissCustomAlertDialog()
            SystemListener.showQuickNotice(AudioService.instance!!.resources.getString(R.string.quick_notice_reset_music_index))
        }
    }

    override fun onDialogNoButton() {
        Log.d(TAG, "onDialogNoButton... ")
        NONUIVariables.getInstance().mMediaCallBack!!.dismissCustomAlertDialog()
    }

    override fun onMediaRequest() {
        Log.d(TAG, "onMediaRequest... ")
        onSourceReset(NOWPLAYING_SOURCE_TYPE.USBMSD)
    }

    override fun onAuxRequest() {
        Log.d(TAG, "onAuxRequest... ")
        onSourceReset(NOWPLAYING_SOURCE_TYPE.AUX)
    }

    override fun onUsbTuneRequest(any: Any?) {
        Log.d(TAG, "onUsbTuneRequest... $any")
        if (any != null) {
            val position = any as Int
            if (NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size >= position) {
                val item = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[position]
                SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, item.albumArt,
                        item.fileName, item.objectId, item.playTime, item.metaDataGenre,
                        item.metaDataArtist, item.metaDataAlbum, item.metaDataSong, item.metaDataComposers, item.categoryType))
                NONUIVariables.getInstance().playingUsbIndex = position
                MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.PLAY.name,
                        Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong))))
            }
        }
    }

    /**
     * To get list of media files
     */
    override fun onUsbRequestSource() {
        Log.d(TAG, "onUsbRequestSource... ")
        try {
            onSourceReset(NOWPLAYING_SOURCE_TYPE.USBMSD)

            Handler().postDelayed({
                SystemListener.isMediaProgressBarVisible(false)
            }, 2500)

            //This methodGet activated device information from usb
            getDeviceInformation()

            if (MEDIA_DEVICE_LIST.size > 0) {
                val station: MediaDeviceInfo_t = if (MEDIA_ACTIVE_DEVICE_LIST_INFO == null || TextUtils.isEmpty(MEDIA_ACTIVE_DEVICE_LIST_INFO!!.deviceId.toString()))
                    getUsbStorageStation()
                else
                    MEDIA_ACTIVE_DEVICE_LIST_INFO!!
                SystemListener.onMEDIA_RES_ACTIVEMEDIADEVICE(MediaDeviceInfo_t(station.deviceId, station.deviceType, station.deviceConnected, station.deviceIndexedState, station.deviceActiveSource, station.deviceName, station.deviceSerialNumber, station.deviceReadable, station.folderBrowseSupported))
            }

            if (usbStorageMediaList.size > 0) {
                SystemListener.onMEDIA_RES_DEVICE_MOUNTED_STATUS(true)
                SystemListener.onMEDIA_RES_BROWSEBUTTONSUPPORT(true)
            } else {
                val usbPath = SharedPreferencesController.prefs.getString(AudioConstants.USB_STORAGE_PATH, "")
                if (usbPath != null && usbPath != "")
                    ReadUsbData().execute()
                else {
                    if (NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST.size > 0) {
                        SystemListener.onMEDIA_RES_DEVICE_MOUNTED_STATUS(true)
                        SystemListener.onMEDIA_RES_BROWSEBUTTONSUPPORT(true)
                    } else {
                        val eventName = "loadData"
                        val intent = Intent(AudioService.instance!!, MediaPlayerService::class.java)
                        intent.putExtra("Action", eventName)
                        AudioService.instance!!.startService(intent)
                    }
                }
            }

            if (NONUIVariables.getInstance().mState == MEDIA_PLAYER_STATE.STOP.name && NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.size > 0) {
                triggerMediaPlayerAction(MEDIA_PLAYER_STATE.INIT.name,
                        Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong)), 0)
                val station: MediaObject_t = if (NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO == null || TextUtils.isEmpty(NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!.fileName))
                    getRandomUsbStation()
                else
                    NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!
                SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, station.albumArt, station.fileName, station.objectId, station.playTime, station.metaDataGenre, station.metaDataArtist, station.metaDataAlbum, station.metaDataSong, station.metaDataComposers, station.categoryType))
            }
            FavoriteDataProcessor.loadFavoritesForSimulation()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * when items in browseList is clicked this method will called, According to current page respective ddata will displaying
     * @param any [USB_BROWSE_PAGE]
     */
    override fun onUSBBrowseItemClick(any: Any?) {
        Log.d(TAG, "onUSBBrowseItemClick... $any")
        try {
            when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) {
                USB_BROWSE_PAGE.CATEGORY -> {
                    if (any is MediaObject_t)
                        RetrieveMediaBrowseData(this).execute(any.fileName)
                }
                USB_BROWSE_PAGE.GENRE_ARTIST, USB_BROWSE_PAGE.GENRES, USB_BROWSE_PAGE.COMPILATIONS,
                USB_BROWSE_PAGE.ALBUM, USB_BROWSE_PAGE.ARTIST, USB_BROWSE_PAGE.ARTIST_ALBUM,
                USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM, USB_BROWSE_PAGE.COMPILATION_ALBUMS,
                USB_BROWSE_PAGE.COMPILATION_ALBUM_SONGS, USB_BROWSE_PAGE.COMPILATION_SONGS,
                USB_BROWSE_PAGE.COMPOSERS, USB_BROWSE_PAGE.COMPOSER_ALBUM, USB_BROWSE_PAGE.FOLDER ->
                    if (any is MediaObject_t)
                        RetrieveMediaBrowseData(this).execute(any)
                else -> {
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun triggerMediaPlayerAction(action: String, songUri: Uri, progress: Int) {
        val intent = Intent(AudioService.instance!!, MediaPlayerService::class.java)
        intent.data = songUri
        intent.putExtra("Action", action)
        AudioService.instance!!.startService(intent)
    }

    private fun getRandomUsbStation(): MediaObject_t {
        Log.d(TAG, " getRandomUsbStation...")
        Log.d(TAG, "sourceType: ${NONUIVariables.getInstance().previousSourceType}")
        val list = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST
        return list[0]
    }

    private fun getUsbStorageStation(): MediaDeviceInfo_t {
        Log.d(TAG, " getUsbStorageStation...")
        Log.d(TAG, "sourceType: ${NONUIVariables.getInstance().previousSourceType}")
        return MEDIA_DEVICE_LIST[0]
    }

    private fun getDeviceInformation() {
        Log.d(TAG, " getDeviceInformation...")
        try {
            val actionString: String = AudioService.instance!!.packageName + ".USB_PERMISSION"
            val mPermissionIntent: PendingIntent = PendingIntent.getBroadcast(AudioService.instance!!.applicationContext, 0, Intent.getIntent(actionString), 0)
            val manager = AudioService.instance!!.getSystemService(Context.USB_SERVICE) as UsbManager?
            val deviceList = manager!!.deviceList
            val deviceIterator = deviceList.values.iterator()
            while (deviceIterator.hasNext()) {
                val device = deviceIterator.next()
                manager.requestPermission(device, mPermissionIntent)
//            val Model = device.getDeviceName()
                val deviceID = device.deviceId
//            val Vendor = device.getVendorId()
//            val Product = device.getProductId()
//            val Class = device.getDeviceClass()
//            val Subclass = device.getDeviceSubclass()
                val mediaDeviceInfo = MediaDeviceInfo_t(deviceId = 0, deviceType = eMediaDeviceType.MEDIA_DTY_AGGREGATION, deviceConnected = true, deviceIndexedState = eMediaDevIndexedState.MEDIA_DEV_IDS_COMPLETED, deviceActiveSource = 0, deviceName = "", deviceSerialNumber = "", deviceReadable = true, folderBrowseSupported = true)
                mediaDeviceInfo.deviceId = deviceID
                MEDIA_DEVICE_LIST.add(mediaDeviceInfo)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    /**
     * To retreive all type of data from Database for browse list
     */
    private class RetrieveMediaBrowseData internal constructor(simulationManager: SimulationManager) : AsyncTask<Any, Void, List<Any>>() {
        var mSimulationManager = simulationManager
        var itemName: Any? = null
        var tempFileList = ArrayList<File>()

        override fun doInBackground(vararg p0: Any?): List<Any>? {
            Log.d("RetrieveMediaBrowseData", "doInBackground... $p0")
            var localList: List<Any>? = ArrayList()
            try {
                itemName = if (p0[0] is MediaObject_t)
                    p0[0]!!
                else
                    p0[0]!!.toString()

                if (itemName == AudioConstants.Folder || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME.toString() == AudioConstants.Folder) {
                    var usbPath = SharedPreferencesController.prefs.getString(AudioConstants.USB_STORAGE_PATH, "")
                    if (TextUtils.isEmpty(usbPath)) {
                        usbPath = Environment.getExternalStorageDirectory().absolutePath
                    }
                    val filePath = File(usbPath)
                    val list = filePath.listFiles()
                    val tempList = ArrayList<Any>()
                    if (null != list && !list.isEmpty()) {
                        for (file in list) {
                            if (file.isDirectory) {
                                val fileList = file.listFiles { dir, name ->
                                    (name.endsWith(AudioConstants.mp3) ||
                                            (name.endsWith(AudioConstants.MP3)))
                                }
                                if (fileList != null && fileList.isNotEmpty()) {
                                    tempList.add(file.name)
                                    tempFileList.add(file)
                                }
                            }
                        }
                        if (NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME.toString() == AudioConstants.Folder
                                && itemName is MediaObject_t) {

                            val item = itemName as MediaObject_t
                            val listFiles = tempFileList.filter { file -> file.name == (item.fileName) }
                            tempList.clear()
                            if (listFiles.isNotEmpty()) {
                                val file = listFiles[0]
                                if (file.isDirectory) {
                                    val fileList = file.listFiles { dir, name ->
                                        (name.endsWith(AudioConstants.mp3)
                                                || (name.endsWith(AudioConstants.MP3)))
                                    }
                                    if (null != fileList && fileList.isNotEmpty()) {
                                        for (song in fileList) {
                                            if (song.name.endsWith(AudioConstants.mp3)
                                                    || song.name.endsWith(AudioConstants.MP3)) {
                                                val mediaData = MediaData()
                                                mediaData.albumArt = song.name
                                                mediaData.filename = song.name
                                                mediaData.genre = song.name
                                                mediaData.artist = AudioService.instance!!.resources.getString(R.string.unknown_artist)
                                                mediaData.album = AudioService.instance!!.resources.getString(R.string.unknown_album)
                                                mediaData.filepath = song.absolutePath
                                                mediaData.compilations = AudioService.instance!!.resources.getString(R.string.unknown_comilation)
                                                mediaData.composers = AudioService.instance!!.resources.getString(R.string.unknown_composer)
                                                tempList.add(mediaData)
                                            }

                                        }
                                    }
                                }
                            }
                        }
                        localList = tempList
                    }
                } else {
                    localList = mSimulationManager.getDataFromDB(itemName!!)
                }
            } catch (e: Exception) {
                Log.i("RetrieveMediaBrowseData", e.printStackTrace().toString())
            }
            return localList
        }

        override fun onPostExecute(result: List<Any>?) {
            try {
                Log.d("RetrieveMediaBrowseData", "onPostExecute... $result")
                val localList = ArrayList<Any>()
                val treeSet = TreeSet<String>()
                if (result != null) { // if open brace
                    if (NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.CATEGORY) { // sub if open brace
                        when (itemName) { // when open brace
                            USB_BROWSE_PAGE.SONGS.toString() -> {
                                result.sortedBy { (it as MediaData).filename }.forEach {
                                    if (treeSet.add((it as MediaData).filename))
                                        localList.add(it)
                                }
                            }
                            USB_BROWSE_PAGE.ALBUM.toString() -> {
                                result.sortedBy { (it as MediaData).album }.forEach {
                                    if (treeSet.add((it as MediaData).album))
                                        localList.add(it)
                                }
                            }
                            USB_BROWSE_PAGE.COMPILATIONS.toString() -> {
                                result.sortedBy { (it as MediaData).compilations }.forEach {
                                    if ((it as MediaData).compilations != AudioService.instance!!.resources.getString(R.string.unknown_comilation)) {
                                        if (treeSet.add(it.compilations))
                                            localList.add(it)
                                    }
                                }
                            }
                            else -> {
                                result.forEach {
                                    if (treeSet.add(it.toString()))
                                        localList.add(it)
                                }
                                localList.sortWith(compareBy { it.toString() })
                            }
                        } //when close brace
                        if (localList.size > 0)
                            SystemListener.onCategoryListItemRes(itemName as String, localList)
                    } // sub if close brace
                    else { // else open brace
                        when (NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME) { // when open brace
                            USB_BROWSE_PAGE.ARTIST -> {
                                result.sortedBy { (it as MediaData).album }.forEach {
                                    if (treeSet.add((it as MediaData).album))
                                        localList.add(it)
                                }
                                SystemListener.onArtistAlbumItemRes(itemName, localList, USB_BROWSE_PAGE.ARTIST_ALBUM)
                            }
                            USB_BROWSE_PAGE.ARTIST_ALBUM -> {
                                result.sortedBy { (it as MediaData).filename }.forEach {
                                    if (treeSet.add((it as MediaData).filename))
                                        localList.add(it)
                                }
                                if ((itemName as MediaObject_t).metaDataAlbum != NONUIVariables.INSTANCE!!.allSongs_st)
                                    SystemListener.onArtistAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.ARTIST_ALBUM_SONGS)
                                else
                                    SystemListener.onArtistAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.ARTIST_SONGS)

                            }
                            USB_BROWSE_PAGE.ALBUM -> {
                                result.sortedBy { (it as MediaData).filename }.forEach {
                                    if (treeSet.add((it as MediaData).filename))
                                        localList.add(it)
                                }
                                SystemListener.onAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.ALBUM_SONGS)
                            }
                            USB_BROWSE_PAGE.GENRES -> {
                                result.forEach {
                                    if (treeSet.add(it.toString()))
                                        localList.add(it.toString())
                                }
                                localList.sortWith(compareBy { it.toString() })
                                SystemListener.onArtistItemRes(itemName, localList, USB_BROWSE_PAGE.GENRE_ARTIST)
                            }
                            USB_BROWSE_PAGE.GENRE_ARTIST -> {
                                if ((itemName as MediaObject_t).metaDataAlbum != NONUIVariables.INSTANCE!!.allSongs_st) {
                                    result.sortedBy { (it as MediaData).album }.forEach {
                                        if (treeSet.add((it as MediaData).album))
                                            localList.add(it)
                                    }
                                    SystemListener.onArtistAlbumItemRes(itemName, localList, USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM)

                                } else {
                                    result.sortedBy { (it as MediaData).filename }.forEach {
                                        if (treeSet.add((it as MediaData).filename))
                                            localList.add(it)
                                    }
                                    SystemListener.onArtistAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.GENRE_ARTIST_SONG)
                                }
                            }
                            USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM -> {
                                result.sortedBy { (it as MediaData).filename }.forEach {
                                    if (treeSet.add((it as MediaData).filename))
                                        localList.add(it)
                                }
                                if (itemName is MediaObject_t) {
                                    if ((itemName as MediaObject_t).metaDataAlbum == NONUIVariables.INSTANCE!!.allSongs_st)
                                        SystemListener.onArtistAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.GENRE_ALBUM_SONG)
                                    else
                                        SystemListener.onArtistAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM_SONG)
                                }
                            }
                            USB_BROWSE_PAGE.COMPILATIONS -> {
                                result.sortedBy { (it as MediaData).filename }.forEach {
                                    if (treeSet.add((it as MediaData).filename))
                                        localList.add(it)
                                }
                                if (itemName is MediaObject_t) {
                                    if ((itemName as MediaObject_t).metaDataAlbum == NONUIVariables.INSTANCE!!.allSongs_st)
                                        SystemListener.onAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.COMPILATION_SONGS)
                                    else
                                        SystemListener.onAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.COMPILATION_ALBUM_SONGS)
                                }
                            }
                            USB_BROWSE_PAGE.COMPOSERS -> {
                                result.sortedBy { (it as MediaData).album }.forEach {
                                    if (treeSet.add((it as MediaData).album))
                                        localList.add(it)
                                }
                                SystemListener.onComposerAlbumItemRes(itemName, localList, USB_BROWSE_PAGE.COMPOSER_ALBUM)
                            }
                            USB_BROWSE_PAGE.COMPOSER_ALBUM -> {
                                result.sortedBy { (it as MediaData).filename }.forEach {
                                    if (treeSet.add((it as MediaData).filename))
                                        localList.add(it)
                                }
                                if ((itemName as MediaObject_t).metaDataAlbum != NONUIVariables.INSTANCE!!.allSongs_st)
                                    SystemListener.onComposerAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.COMPOSER_ALBUM_SONG)
                                else
                                    SystemListener.onComposerAlbumSongsItemRes(itemName, localList, USB_BROWSE_PAGE.COMPOSER_SONG)
                            }
                            USB_BROWSE_PAGE.FOLDER -> {
                                if (result.isNotEmpty() && result[0] is MediaData) {
                                    result.sortedBy { (it as MediaData).filename }.forEach {
                                        if (treeSet.add((it as MediaData).filename))
                                            localList.add(it)
                                    }
                                    SystemListener.onFolderSongItemRes(itemName, localList, USB_BROWSE_PAGE.FOLDER_SONGS)
                                } else {
                                    result.forEach {
                                        if (treeSet.add(it.toString()))
                                            localList.add(it)
                                    }
                                    localList.sortWith(compareBy { it.toString() })
                                    if (localList.size > 0)
                                        SystemListener.onCategoryListItemRes(itemName as String, localList)
                                }
                            }
                            else -> {
                            }
                        }//when close brace
                    } //else close brace
                } // if close brace
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onMEDIA_REQ_BACKTRAVERSE() {
        Log.d(TAG, "onMEDIA_REQ_BACKTRAVERSE... ")
        try {
            when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) { //when open brace
                USB_BROWSE_PAGE.CATEGORY ->
                    NONUIVariables.getInstance().MEDIA_BROWSE_LEVEL = MEDIA_BROWSE_LEVELS.LEVEL_NONE
                USB_BROWSE_PAGE.ARTIST_ALBUM -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.CATEGORY)
                    RetrieveMediaBrowseData(this).execute(USB_BROWSE_PAGE.ARTIST.toString())
                }
                USB_BROWSE_PAGE.ALBUM_SONGS -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.CATEGORY)
                    RetrieveMediaBrowseData(this).execute(USB_BROWSE_PAGE.ALBUM.toString())
                }
                USB_BROWSE_PAGE.GENRE_ARTIST -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.CATEGORY)
                    RetrieveMediaBrowseData(this).execute(USB_BROWSE_PAGE.GENRES.toString())
                }
                USB_BROWSE_PAGE.COMPILATION_SONGS, USB_BROWSE_PAGE.COMPILATION_ALBUM_SONGS -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.CATEGORY)
                    RetrieveMediaBrowseData(this).execute(USB_BROWSE_PAGE.COMPILATIONS.toString())
                }
                USB_BROWSE_PAGE.COMPOSER_ALBUM -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.CATEGORY)
                    RetrieveMediaBrowseData(this).execute(USB_BROWSE_PAGE.COMPOSERS.toString())
                }
                USB_BROWSE_PAGE.ARTIST_ALBUM_SONGS, USB_BROWSE_PAGE.ARTIST_SONGS -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.ARTIST)
                    RetrieveMediaBrowseData(this).execute(NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST[0])
                }
                USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM, USB_BROWSE_PAGE.GENRE_ARTIST_SONG -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.GENRES)
                    RetrieveMediaBrowseData(this).execute(NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST[0])
                }
                USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM_SONG, USB_BROWSE_PAGE.GENRE_ALBUM_SONG -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.GENRE_ARTIST)
                    RetrieveMediaBrowseData(this).execute(NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST[0])
                }
                USB_BROWSE_PAGE.COMPOSER_ALBUM_SONG, USB_BROWSE_PAGE.COMPOSER_SONG -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.COMPOSERS)
                    RetrieveMediaBrowseData(this).execute(NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST[0])
                }
                USB_BROWSE_PAGE.FOLDER_SONGS -> {
                    SystemListener.onPageUpdate(USB_BROWSE_PAGE.FOLDER)
                    RetrieveMediaBrowseData(this).execute(USB_BROWSE_PAGE.FOLDER.toString())
                }
                USB_BROWSE_PAGE.SONGS, USB_BROWSE_PAGE.ALBUM, USB_BROWSE_PAGE.ARTIST, USB_BROWSE_PAGE.GENRES,
                USB_BROWSE_PAGE.COMPOSERS, USB_BROWSE_PAGE.COMPILATIONS, USB_BROWSE_PAGE.FOLDER ->
                    SystemListener.onUsbBrowseResp()
                else -> {
                }
            } ////when close brace
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onUSBBrowseSongItemClick(any: Any?) {
        Log.d(TAG, " onUSBBrowseSongItemClick... $any")
        try {
            if (NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.SONGS
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.ALBUM_SONGS
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.ARTIST_ALBUM_SONGS
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.COMPILATION_SONGS
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.COMPILATION_ALBUM_SONGS
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.GENRE_ARTIST_SONG
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.ARTIST_SONGS
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.GENRE_ALBUM_SONG
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM_SONG
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.COMPOSER_ALBUM_SONG
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.COMPOSER_SONG
                    || NONUIVariables.INSTANCE!!.USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.FOLDER_SONGS) {
                if (any is Int) {
                    NONUIVariables.getInstance().playingUsbIndex = any
                    SystemListener.onBrowseSongItemRes()
                    onMEDIA_REQ_PLAYITEMFROMLISTBYTAG(NONUIVariables.getInstance().playingUsbIndex)
                    triggerMediaPlayerAction(MEDIA_PLAYER_STATE.PLAY.name, Uri.fromFile(File(NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[NONUIVariables.getInstance().playingUsbIndex].metaDataSong)), 0)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMEDIA_REQ_EXITBROWSE() {
        Log.d(TAG, " onMEDIA_REQ_EXITBROWSE... ")
        NONUIVariables.getInstance().IS_MEDIA_BROWSE_SONGS = false
    }

    /**
     * Playing next item in the List when [eMediaPBAction.MEDIA_PBA_NEXT] event occur.
     */
    override fun onPlayNext() {
        Log.d(TAG, " onPlayNext... ")
        if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE == NOWPLAYING_SOURCE_TYPE.USBMSD) {
            val eventName = "onPlayNext"
            val intent = Intent(AudioService.instance!!, MediaPlayerService::class.java)
            intent.putExtra("Action", eventName)
            AudioService.instance!!.startService(intent)
        }
    }

    /**
     * Playing next item in the List when [eMediaPBAction.MEDIA_PBA_PREV] event occur.
     */
    override fun onPlayPrevious() {
        Log.d(TAG, " onPlayPrevious... ")
        try {
            if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE == NOWPLAYING_SOURCE_TYPE.USBMSD) {
                val eventName = "onPlayPrevious"
                val intent = Intent(AudioService.instance!!, MediaPlayerService::class.java)
                intent.putExtra("Action", eventName)
                AudioService.instance!!.startService(intent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMEDIA_REQ_PLAYITEMFROMLISTBYTAG(any: Any?) {
        Log.d(TAG, " onMEDIA_REQ_PLAYITEMFROMLISTBYTAG... $any")
        val item = NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[any as Int]
        SystemListener.onMEDIA_RES_NOWPLAYING(MediaObject_t(true, item.albumArt, item.fileName, item.objectId, item.playTime, item.metaDataGenre, item.metaDataArtist, item.metaDataAlbum, item.metaDataSong, item.metaDataComposers, item.categoryType))
    }

    override fun onMEDIA_REQ_REQUESTPLAYBACKACTION(any: Any?) {
        Log.d(TAG, " onMEDIA_REQ_REQUESTPLAYBACKACTION...  $any")
        try {
            when (any) { //when open brace
                eMediaPBAction.MEDIA_PBA_PLAY -> {
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PAUSE)
                    MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.PLAYPAUSE.name, 0))
                }
                eMediaPBAction.MEDIA_PBA_PAUSE -> {
                    SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PLAY)
                    MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.PLAYPAUSE.name, 0))
                }
                eMediaPBAction.MEDIA_PBA_NEXT -> {
                    if (!NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING)
                        SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PLAY)
                    onPlayNext()
                }
                eMediaPBAction.MEDIA_PBA_PREV -> {
                    if (!NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING)
                        SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PLAY)
                    onPlayPrevious()
                }
                eMediaPBAction.MEDIA_PBA_FASTBACKWARD -> //backward seeking
                    fastForwardFastBackward(false)
                eMediaPBAction.MEDIA_PBA_FASTFORWARD -> //forward seeking
                    fastForwardFastBackward(true)
                eMediaPBAction.MEDIA_PBA_FAST_PREV_STOP ->
                    if (NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING) {
                        MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.UPDATE_PROGRESS.name, 0))
                    }
            } // when close brace
        } catch (e: IllegalStateException) {
            Log.e(TAG, e.printStackTrace().toString())
        }
    }

    override fun onMEDIA_REQ_SEEKTO(any: Any?) {
        Log.d(TAG, " onMEDIA_REQ_SEEKTO...  $any")
        SystemListener.onMEDIA_RES_SEEKTO(any as Int)
    }


    override fun onMEDIA_REQ_SETPLAYBACKMODE(any: Any?) {
        Log.d(TAG, " onMEDIA_REQ_SETPLAYBACKMODE...  $any")
        if (any as eMediaPBMode == eMediaPBMode.MEDIA_PBM_NORMAL) {
            SystemListener.onMEDIA_RES_PLAYBACKMODE(eMediaPBMode.MEDIA_PBM_RANDOM)
        } else {
            SystemListener.onMEDIA_RES_PLAYBACKMODE(eMediaPBMode.MEDIA_PBM_NORMAL)
        }
    }

    override fun onMEDIA_REQ_RESETMUSICINDEX() {
        Log.d(TAG, " onMEDIA_REQ_RESETMUSICINDEX...")
        NONUIVariables.getInstance().dialogDisplayEvent = AudioService.instance!!.resources.getString(R.string.dialog_display_event_reset_music_index)
        NONUIVariables.getInstance().dialogMainText = AudioService.instance!!.resources.getString(R.string.dialog_main_text_reset_music_index)
        NONUIVariables.getInstance().dialogButtonOneText = AudioService.instance!!.resources.getString(R.string.dialog_button_1_reset_music_index)
        NONUIVariables.getInstance().dialogButtonTwoText = AudioService.instance!!.resources.getString(R.string.dialog_button_2_reset_music_index)
        onDialogDisplay()
    }

    override fun onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST(any: Any?) {
        Log.d(TAG, " onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST Not Implemented... $any")
    }

    override fun onMEDIA_REQ_MEDIAPLAYERLISTFILTER(any: Any?) {
        Log.d(TAG, " onMEDIA_REQ_MEDIAPLAYERLISTFILTER Not Implemented... $any")
    }

    override fun onMEDIA_REQ_FOLDERBACK(indexedListReq: MediaPlayerListReq_t) {
        Log.d(TAG, " onMEDIA_REQ_FOLDERBACK Not Implemented... $indexedListReq")
    }

    override fun onMEDIA_REQ_READACK(readAck: eMediaListReadAck) {
        Log.d(TAG, " onMEDIA_REQ_READACK Not Implemented... $readAck")
    }

    override fun onMEDIA_REQ_LISTRELOAD() {
        Log.d(TAG, " onMEDIA_REQ_LISTRELOAD Not Implemented... ")
    }

    override fun onUsbBrowse() {
        Log.d(TAG, " onUsbBrowse Not Implemented... ")
    }

    override fun onUsbNowPlayingList() {
        Log.d(TAG, " onUsbNowPlayingList Not Implemented... ")
    }

    override fun onUsbNowPlayingItemClick(any: Any?) {
        Log.d(TAG, " onUsbNowPlayingItemClick Not Implemented... " + any.toString())
    }

//    override fun onKeypadLetterClick(any: Any?) {
//        Log.d(TAG, " onKeypadLetterClick Not Implemented... " + any.toString())
//    }

    override fun onShowLetterKeypad(any: Any?) {
        Log.d(TAG, " onShowLetterKeypad Not Implemented... " + any.toString())
    }

    override fun onCloseLetterKeypad() {
        Log.d(TAG, " onCloseLetterKeypad Not Implemented... ")
    }


    fun getDataFromDB(any: Any): List<Any>? {
        Log.d(TAG, "getDataFromDB... $any")
        try {
            if (NONUIVariables.getInstance().USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.CATEGORY) { // if open brace
                when (any) { //when open brace
                    USB_BROWSE_PAGE.ARTIST.toString() ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllArtists()
                    USB_BROWSE_PAGE.ALBUM.toString() ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllSongsData()
                    USB_BROWSE_PAGE.SONGS.toString() ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllSongsData()
                    USB_BROWSE_PAGE.GENRES.toString() ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllGenre()
                    USB_BROWSE_PAGE.COMPOSERS.toString() ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllComposer()
                    USB_BROWSE_PAGE.COMPILATIONS.toString() ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllCompilations()
                }//when close brace
            } //if close brace
            else { //else open brace
                when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) { //when open brace
                    USB_BROWSE_PAGE.ARTIST ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getArtistAlbumData((any as MediaObject_t).metaDataArtist)
                    USB_BROWSE_PAGE.ARTIST_ALBUM ->
                        return if ((any as MediaObject_t).metaDataAlbum == NONUIVariables.getInstance().allSongs_st)
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getArtistAllSongs(any.metaDataArtist)
                        else
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getArtistAlbumSongs(any.metaDataArtist, any.metaDataAlbum)
                    USB_BROWSE_PAGE.ALBUM ->
                        return if ((any as MediaObject_t).metaDataAlbum == NONUIVariables.getInstance().allSongs_st)
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllSongs()
                        else
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAlbumSongs((any).metaDataAlbum)
                    USB_BROWSE_PAGE.GENRES ->
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getGenreArtist((any as MediaObject_t).metaDataGenre)
                    USB_BROWSE_PAGE.GENRE_ARTIST -> {
                        return if ((any as MediaObject_t).metaDataAlbum == NONUIVariables.getInstance().allSongs_st)
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getGenreAllSongs((any).metaDataGenre)
                        else
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getGenreArtistAlbums((any).metaDataArtist, any.metaDataGenre)
                    }
                    USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM -> {
                        return if ((any as MediaObject_t).metaDataAlbum == NONUIVariables.getInstance().allSongs_st)
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getGenreArtistAlbumAllSongs(any.metaDataArtist, any.metaDataGenre)
                        else
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getGenreArtistAlbumsSongs(any.metaDataAlbum, any.metaDataArtist, any.metaDataGenre)
                    }
                    USB_BROWSE_PAGE.COMPILATIONS -> {
                        return if ((any as MediaObject_t).metaDataAlbum == NONUIVariables.getInstance().allSongs_st)
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getCompilationAllSongs()
                        else
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAlbumSongs(any.metaDataAlbum)
                    }
                    USB_BROWSE_PAGE.COMPOSERS -> {
                        return NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAlbumComposerData((any as MediaObject_t).fileName)
                    }
                    USB_BROWSE_PAGE.COMPOSER_ALBUM -> {
                        return if ((any as MediaObject_t).metaDataAlbum == NONUIVariables.getInstance().allSongs_st) {
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getAllSongsFromComposer(any.metaDataGenre)
                        } else
                            NONUIVariables.getInstance().gmDatabase?.mediaDataDao()?.getSongsFromAlbumsFromComposer((any).metaDataGenre, any.metaDataAlbum)
                    }
                    else -> {
                    }
                } // when close brace
            }//else close brace
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }


    /**
     *  call this method to play AM/FM based on type
     *  @param sourceType can be NOWPLAYING_SOURCE_TYPE.AM/FM
     */
    private fun playFMAMStationList(sourceType: NOWPLAYING_SOURCE_TYPE) {
        Log.d(TAG, "playFMAMStationList... $sourceType")
        onSourceReset(sourceType)
        setBrowseStationList()
        val station = getCurrentStation(sourceType.name)
        setFMAMCurrentStationInfo(station)
        initAudioPlay(getCurrentStationIndex())
    }

    /**
     * This method returns current station if station is already tuned
     * Otherwise it returns any random station from list.
     *
     * @param sourceType type of waveband AM/FM
     * @return AMFMStationInfo_t
     */
    private fun getCurrentStation(sourceType: String?): AMFMStationInfo_t {
        Log.d(TAG, "getCurrentStation... $sourceType")
        return if (sourceType != null && (NONUIVariables.getInstance().aMFMStationInfo_t == null ||
                        TextUtils.isEmpty(NONUIVariables.getInstance().aMFMStationInfo_t?.frequency.toString()) ||
                        NONUIVariables.getInstance().aMFMStationInfo_t?.rdsStationInfo!! != sourceType))
            getRandomStation()
        else if (NONUIVariables.getInstance().aMFMStationInfo_t == null || TextUtils.isEmpty(NONUIVariables.getInstance().aMFMStationInfo_t?.frequency.toString()))
            getRandomStation()
        else
            NONUIVariables.getInstance().aMFMStationInfo_t!!
    }

    /**
     * returns index of station that is tuned
     *
     * @return index of current station in the list
     */
    private fun getCurrentStationIndex(): Int {
        Log.d(TAG, "getCurrentStationIndex... ")
        var index = -1
        try {
            val filterItem = NONUIVariables.getInstance().browseList.filter { its -> its.frequency == NONUIVariables.getInstance().aMFMStationInfo_t!!.frequency }
            if (filterItem.isNotEmpty()) {
                index = NONUIVariables.getInstance().browseList.indexOf(filterItem[0])
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return index
    }

    /**
     * call this method to seek station. It plays up/down station
     *
     * @param any can be eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP/AMFM_SEEK_TYPE_SEEKDOWN
     */
    private fun playNextPreviousSeekStation(any: Any?) {
        Log.d(TAG, "playNextPreviousSeekStation... $any")
        try {
            var index = getCurrentStationIndex()
            if (any as eAMFMSeekType == eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP || any == eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKUP)
                if (++index < NONUIVariables.getInstance().browseList.size) else index = 0
            else if (any == eAMFMSeekType.AMFM_SEEK_TYPE_SEEKDOWN || any == eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKDOWN)
                if (--index >= 0) else index = NONUIVariables.getInstance().browseList.size - 1
            val station = NONUIVariables.getInstance().browseList[index]
            setFMAMCurrentStationInfo(station)
            isTPStation = station.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
            isTrafficProgramSupported = station.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP
            initAudioPlay(index)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * call this method to tune station. It plays up/down station
     *
     * @param any can be eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP/AMFM_SEEK_TYPE_SEEKDOWN
     */
    private fun nextPreviousTunerStation(any: Any?) {
        Log.d(TAG, "nextPreviousTunerStation... $any")
        try {
            var index = getCurrentStationIndex()
            if (any as eAMFMTuneType == eAMFMTuneType.AMFM_TUNE_TYPE_TUNEUP)
                if (++index < NONUIVariables.getInstance().browseList.size) else index = 0
            else if (any == eAMFMTuneType.AMFM_TUNE_TYPE_TUNEDOWN)
                if (--index >= 0) else index = NONUIVariables.getInstance().browseList.size - 1
            val station = NONUIVariables.getInstance().browseList[index]
            setFMAMCurrentStationInfo(station)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * called this method to directly tune  to selected station object.
     *
     * @param any object id
     */
    private fun playStationByTunerObjectID(any: Any?) {
        Log.d(TAG, "playStationByTunerObjectID... $any")
        if (NONUIVariables.getInstance().showUpdateProgress) {
            showQuickNotice(AudioService.instance!!.resources.getString(R.string.audio_action_not_supported))
            return
        }
        try {
            if (any != null) {
                val filterItem = NONUIVariables.getInstance().browseList.filter { its -> its.objectId == any }
                if (filterItem.isNotEmpty()) {
                    val index = NONUIVariables.getInstance().browseList.indexOf(filterItem[0])
                    val it = NONUIVariables.getInstance().browseList[index]
                    setFMAMCurrentStationInfo(it)
                    initAudioPlay(index)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * Sets selected station info on screen
     *
     * @param amFmStation instance of AMFMStationInfo_t
     */
    private fun setFMAMCurrentStationInfo(amFmStation: AMFMStationInfo_t) {
        Log.d(TAG, "setFMAMCurrentStationInfo... $amFmStation")
        val amFmStationInfo = addAMFMStation(amFmStation, NONUIVariables.getInstance().favoriteList.contains(amFmStation.frequency.toString()), amFmStation.tpStationStatus)
        if (amFmStation.rdsStationInfo == NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.onAMFM_RES_AMCURRENTSTATIONINFO(amFmStationInfo)
        else
            SystemListener.onAMFM_RES_FMCURRENTSTATIONINFO(amFmStationInfo)
    }

    /**
     * call this method to show progress bar for Manual Update
     */
    private fun updateAMFMStrongStationList() {
        Log.d(TAG, "updateAMFMStrongStationList... ")
        if (!(NONUIVariables.getInstance().showUpdateProgress)) {
            releasePlayer()
            NONUIVariables.getInstance().audioControlDisabled = true
            NONUIVariables.getInstance().showUpdateProgress = true
            NONUIVariables.getInstance().updateProgress = 0
            var progress: Int = NONUIVariables.getInstance().updateProgress
            val t = Thread({
                while (progress < 100 && NONUIVariables.getInstance().showUpdateProgress) {
                    try {
                        Thread.sleep(1000)
                        mHandler.sendMessage(mHandler.obtainMessage())
                        progress = NONUIVariables.getInstance().updateProgress
                    } catch (e: InterruptedException) {
                        NONUIVariables.getInstance().audioControlDisabled = false
                        break
                    }
                }
            }, "progress_bar_thread")
            t.start()
        }
    }

    /**
     * called this method to directly tune  to selected frequency.     *
     * @param any frequency
     *
     */
    private fun playAMFMTunerByFrequency(any: Any?): AMFMStationInfo_t? {
        Log.d(TAG, "playAMFMTunerByFrequency... $any")
        var it: AMFMStationInfo_t? = null
        val filterItem = NONUIVariables.getInstance().browseList.filter { its -> its.frequency.toString() == any.toString().toFloat().toString() }
        if (filterItem.isNotEmpty()) {
            val index = NONUIVariables.getInstance().browseList.indexOf(filterItem[0])
            it = NONUIVariables.getInstance().browseList[index]
            setFMAMCurrentStationInfo(it)
            initAudioPlay(index)
        }
        return it
    }

    private val mHandler: Handler = object : Handler(Looper.getMainLooper()) {
        /*
         * handleMessage() defines the operations to perform when
         * the Handler receives a new Message to process.
         */
        override fun handleMessage(inputMessage: Message) {
            Log.d(TAG, "handleMessage... $inputMessage")
            if (NONUIVariables.getInstance().showUpdateProgress) {
                NONUIVariables.getInstance().updateProgress = (NONUIVariables.getInstance().updateProgress + 5)
                if (NONUIVariables.getInstance().updateProgress == 100) {
                    NONUIVariables.getInstance().showUpdateProgress = false
                    NONUIVariables.getInstance().updateProgress = 0
                    NONUIVariables.getInstance().audioControlDisabled = false
                }
            } else {
                val filterItem = Thread.getAllStackTraces().keys.filter { t -> t.name == "progress_bar_thread" && t.isAlive }
                if (filterItem.isNotEmpty())
                    filterItem[0].interrupt()
            }
        }
    }

    //add FMAM station data
    private fun addAMFMStation(stationInfo: AMFMStationInfo_t, isFavorite: Boolean, eTPStationStatus: eTPStationStatus): AMFMStationInfo_t {
        Log.d(TAG, "addAMFMStation... $stationInfo,$isFavorite,$eTPStationStatus")
        return Builder()
                .frequency(stationInfo.frequency)
                .stationName(stationInfo.stationName!!)
                .rdsStatus(eRdsStatus.AMFM_RDS_AVAILABLE)
                .objectId(stationInfo.objectId)
                .rdsStationInfo(stationInfo.rdsStationInfo!!)
                .ptyCategory(stationInfo.ptyCategory)
                .tpStationStatus(eTPStationStatus)
                .isFavorite(isFavorite)
                .build()
    }


    /**
     * Method to play item in Fast forward mode/Reverse mode
     * @param forwardSeek checking whether seek is forward or backward
     */
    private fun fastForwardFastBackward(forwardSeek: Boolean) {
        Log.d(TAG, " fastForwardFastBackward...  $forwardSeek")

        Log.d(TAG, " fastForwardFastBackward... ")
        try {
            if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE == NOWPLAYING_SOURCE_TYPE.USBMSD) {
                val eventName = "fastForwardFastBackward"
                val intent = Intent(AudioService.instance!!, MediaPlayerService::class.java)
                intent.putExtra("Action", eventName)
                intent.putExtra("forwardSeek", forwardSeek)
                AudioService.instance!!.startService(intent)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * It updates the carousel view list
     */
    private fun setBrowseStationList() {
        Log.d(TAG, "setBrowseStationList...")
        if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.AM.name)
            SystemListener.onAMFM_RES_AMSTRONGSTATIONSLIST(getStaticStationList())
        else if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.FM.name)
            SystemListener.onAMFM_RES_FMSTRONGSTATIONSLIST(getStaticStationList())
    }

    /**
     * returns randon station from the list
     * @return AMFMStationInfo_t
     */
    private fun getRandomStation(): AMFMStationInfo_t {
        Log.d(TAG, "getRandomStation...")
        val list = DataSourceProvider.nowPlayingDataList(NONUIVariables.getInstance().previousSourceType)
        return list[0]
    }

    /**
     * It fetches data from DataSourceProvider
     * @return AMFMStationList_t
     */
    private fun getStaticStationList(): AMFMStationList_t {
        Log.d(TAG, "getStaticStationList...")
        val list = DataSourceProvider.nowPlayingDataList(NONUIVariables.getInstance().previousSourceType)
        val columns = arrayOf(FavoritesContract.Cols.FAVLABELTEXT)

        val cursor = AudioService.instance!!.contentResolver!!.query(
                FavoritesContract.Favorites.CONTENT_URI, columns, null, null, null)
        if (cursor != null) {
            while (cursor.moveToNext()) {
                NONUIVariables.getInstance().favoriteList.add(cursor.getString(cursor.getColumnIndex("favorite_desctext")))
            }
        }
        list.forEachIndexed { index, nowPlayingData ->
            kotlin.run {
                list[index].isFavorite = NONUIVariables.getInstance().favoriteList.contains(nowPlayingData.frequency.toString())
            }
        }
        return AMFMStationList_t(list.size, list)
    }

    private fun resetMedia() {
        Log.v(TAG, "resetMedia...")
        if (NONUIVariables.getInstance().MEDIA_NOWPLAYING_MEDIAOBJ_ISPLAYING) {
            SystemListener.onMEDIA_RES_DEVICE_MOUNTED_STATUS(false)
            SystemListener.onMEDIA_RES_BROWSEBUTTONSUPPORT(false)
            SystemListener.onMEDIA_RES_PLAYBACKACTION(eMediaPBAction.MEDIA_PBA_PAUSE)
            MediaUtil.processMedia(EventObject(MEDIA_PLAYER_STATE.PLAYPAUSE.name, 0))
        }
    }

    private fun resetTuner() {
        Log.v(TAG, "resetTuner...")
    }


    //async task for reading data from usb storage.
    class ReadUsbData : AsyncTask<Int, Int, String>() {
        private var counter = 0
        var fileName: String? = null
        private var metaDataAlbum: String? = null
        private var metaDataArtist: String? = null
        private var metaDataGenre: String? = null
        private var remainingTime: String? = null
        override fun doInBackground(vararg params: Int?): String? {

            val usbPath = SharedPreferencesController.prefs.getString(AudioConstants.USB_STORAGE_PATH, "")
            if (usbPath != null && usbPath != "") {
                val file = File(usbPath)
                val directoryFiles = file.listFiles()
                if (directoryFiles != null && directoryFiles.isNotEmpty()) {
                    val totalFiles: Int
                    var subDirectoryFileSize = 0
                    val directoryFilesSize = directoryFiles.size
                    for (files in directoryFiles) {
                        if (files.isDirectory) {
                            subDirectoryFileSize += files.listFiles().size
                        }
                    }
                    totalFiles = subDirectoryFileSize + directoryFilesSize
                    displayDirectory(file, totalFiles)
                }
            }
            return usbPath
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (result != null && result != "") {
                if (SimulationManager.getInstance().usbStorageMediaList.size > 0) {
                    NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST.clear()
                    NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST.addAll(SimulationManager.getInstance().usbStorageMediaList)
                    NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.clear()
                    NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.addAll(NONUIVariables.getInstance().USB_ORIGINAL_MEDIA_LIST)
                }
                //save data from usb storage in shared preference
                val gson = Gson()
                val json = gson.toJson(SimulationManager.getInstance().usbStorageMediaList)
                SharedPreferencesController.editor.putString(AudioConstants.USB_STORAGE_KEY, json)
                SharedPreferencesController.editor.commit()
                NONUIVariables.getInstance().IS_MEDIA_INDEXING_VISISBLE = false
            }
        }

        //check files directory
        private fun displayDirectory(dir: File, totalFiles: Int) {
            try {
                var mediaPlayTime: MediaPlayTime_t?
                val files = dir.listFiles()

                if (files != null && files.isNotEmpty())
                    for (file in files) { //for open brace
                        if (file.isDirectory) {
                            displayDirectory(file, totalFiles)
                        } else { //else open brace
                            if (file.name.endsWith(".mp3") || file.name.endsWith(".MP3")) { //sub if open brace
                                counter++
                                try { // main try open brace
                                    val mediaMetadataRetriever = MediaMetadataRetriever()
                                    mediaMetadataRetriever.setDataSource(file.absolutePath)
                                    val mediaObject = MediaObject_t(false, "", "", objectId = 0, playTime = MediaPlayTime_t(remainingTime = 0, elapsedTime = 0), metaDataGenre = "", metaDataAlbum = "", metaDataArtist = "", metaDataSong = "", categoryType = eMediaCategoryType.MEDIA_CTY_ALBUM)
                                    fileName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
                                    metaDataAlbum = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM)
                                    metaDataArtist = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)
                                    metaDataGenre = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE)
                                    remainingTime = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)

                                    if (fileName != null && fileName!!.isNotEmpty())
                                        mediaObject.fileName = fileName!!
                                    else
                                        mediaObject.fileName = AudioService.instance!!.resources.getString(R.string.unknown_song)

                                    if (metaDataAlbum != null && metaDataAlbum!!.isNotEmpty())
                                        mediaObject.metaDataAlbum = metaDataAlbum!!
                                    else
                                        mediaObject.metaDataAlbum = AudioService.instance!!.resources.getString(R.string.unknown_album)

                                    if (metaDataArtist != null && metaDataArtist!!.isNotEmpty())
                                        mediaObject.metaDataArtist = metaDataArtist!!
                                    else
                                        mediaObject.metaDataArtist = AudioService.instance!!.resources.getString(R.string.unknown_artist)

                                    if (metaDataGenre != null && metaDataGenre!!.isNotEmpty())
                                        mediaObject.metaDataGenre = metaDataAlbum!!
                                    else
                                        mediaObject.metaDataGenre = AudioService.instance!!.resources.getString(R.string.unknown_genre)

                                    if (remainingTime == null && remainingTime!!.length < 0)
                                        remainingTime = "0"

                                    mediaPlayTime = MediaPlayTime_t(remainingTime!!.toInt(), elapsedTime = 0)
                                    mediaObject.playTime = mediaPlayTime
                                    mediaObject.metaDataSong = file.absolutePath

                                    SimulationManager.getInstance().usbStorageMediaList.add(mediaObject)
                                    val fileLoadPercentage = (counter * 100) / totalFiles
                                    //thread sleep some time for load on progress update
                                    try {
                                        Thread.sleep(50)
                                    } catch (e: InterruptedException) {
                                        e.printStackTrace()
                                    }
                                    //this method used for update loading percentage to onProgressUpdate method
                                    publishProgress(fileLoadPercentage)
                                }// main try close brace
                                catch (e: java.lang.Exception) {
                                    System.out.print(e.printStackTrace())
                                }
                            }//sub if close brace

                        } //else close brace

                    }//for close brace
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onProgressUpdate(vararg fileLoadPercentage: Int?) {
            super.onProgressUpdate(*fileLoadPercentage)
            SystemListener.updateUsbMediaIndexingStatus(fileLoadPercentage[0] as Int)
            NONUIVariables.getInstance().IS_MEDIA_INDEXING_VISISBLE = true
        }
    }


}