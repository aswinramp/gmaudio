package com.gm.media.apiintegration

import gm.calibrations.GIS502_TUNERHMI
import gm.calibrations.GMCalibrationsManager
import gm.calibrations.LANGUAGEANDREGIONALIZATIONGLOBALA
import gm.calibrations.LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM
import gm.drivingmode.DrivingModeManager
interface ApiCallback {

    fun addCategoryStationListUpdate()

    fun updateCarouselView()

    fun updateRadioPresetsList()

}