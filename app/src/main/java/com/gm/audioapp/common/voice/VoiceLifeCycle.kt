package com.gm.audioapp.common.voice

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.arch.lifecycle.LifecycleObserver
import android.content.Intent
import android.content.pm.PackageManager
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.support.v4.app.ActivityCompat
import android.util.Log
import com.gm.audioapp.R
import com.gm.audioapp.viewmodels.EventHandler

/**
 * This class is to perform actions based on voice commands
 *
 * @constructor activity current activity
 */
class VoiceLifeCycle(var activity: Activity?) : LifecycleObserver, IVoiceControl {

    private var speechRecognizer: SpeechRecognizer? = null
    private val permission: String = Manifest.permission.RECORD_AUDIO //Record_audio permission is to access voi
    val recordPermissionRequestCode = 100

    init {
        VoiceRecognitionListener.setListener(this)
    }

    /**
     * RECORD_AUDIO Permissions dialog is shown when open the app and permissions not provided
     */
    fun requestPermission() {
        val permissionCheck = ActivityCompat.checkSelfPermission(activity!!, permission)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission)) {

                AlertDialog.Builder(activity!!, R.style.DialogTheme)
                        .setTitle("Record Audio Permission")
                        .setMessage("This is mandatory permission to enable Voice Recognizer.")
                        .setCancelable(false)
                        .setNeutralButton("Ok", { it, _ ->
                            run {
                                it.dismiss()
                                ActivityCompat.requestPermissions(activity!!, arrayOf(permission), recordPermissionRequestCode)
                            }
                        })
                        .create().show()
            } else {
                ActivityCompat.requestPermissions(activity!!, arrayOf(permission), recordPermissionRequestCode)
            }
        } else {
            startListening()
        }
    }

    /**
     * To destroy the speech recognization when permissions denied
     */
    fun stopListening() {
        if (speechRecognizer != null) {
           // speechRecognizer?.stopListening()
           // speechRecognizer?.cancel()
            speechRecognizer?.destroy()
        }
        speechRecognizer = null
    }

    /**
     * Speech recognization starts when user enable the audio permissions
     * @throws RuntimeException
     */
    fun startListening() {
        try {
            initSpeech()
            if (speechRecognizer != null) {
                val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH)
                if (!intent.hasExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE)) {
                    intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                            "com.dummy")
                }
                speechRecognizer?.startListening(intent)
            }
        } catch (ex: Exception) {
            Log.d("SpeechRecognition", "speech recognizer exception")
        }
    }

    /**
     * Declaring the Speech Recogniztion and adding listener
     */
    private fun initSpeech() {
        if (speechRecognizer == null) {
            speechRecognizer = SpeechRecognizer.createSpeechRecognizer(activity!!)
            if (!SpeechRecognizer.isRecognitionAvailable(activity!!)) {
                speechRecognizer = null
                return
            }
            speechRecognizer?.setRecognitionListener(VoiceRecognitionListener)
        }
    }

    override fun processVoiceCommands(vararg voiceControlCommands: String) {
        EventHandler.onVoiceHandler(*voiceControlCommands)
        restartListeningService()
    }

    override fun restartListeningService() {
        stopListening()// to stop voice recognization
        startListening() //to start voice recogniztion
    }
}