package com.gm.audioapp.common.voice

/**
 * This interface is for start and stop, process voice recognization commands
 */
 interface IVoiceControl {
     /**
      * This will be executed when a voice command was found
      * @param voiceControlCommands voice commands given by user
      */
    fun processVoiceCommands(vararg voiceControlCommands: String)

     /**
      * This will be executed after a voice command was processed to keep the recognition service activated
      */
     fun restartListeningService()
}
