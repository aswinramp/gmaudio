package com.gm.audioapp

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.support.multidex.MultiDex
import android.support.v7.widget.RecyclerView
import android.view.View
import com.gm.audioapp.ui.activities.BaseActivity
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.audioapp.ui.activities.fragments.*
import com.gm.audioapp.ui.customclasses.PermissionsUtils
import com.gm.audioapp.ui.navigator.ActivityNavigator
import com.gm.audioapp.viewmodels.ResponseListener
import com.gm.audioapp.viewmodels.Utility.getLanguage
import com.gm.audioapp.viewmodels.Utility.setLocale
import com.gm.media.apiintegration.AudioService
import com.gm.media.database.GMDatabase
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.NONUIVariables
import com.gm.media.models.NOWPLAYING_SOURCE_TYPE
import com.gm.media.models.eLHD_RHD_RTL
import java.util.*


/**
 * Base Class for maintaining global application state.
 * It initialises global object that lives through out the application life.
 * To Create singleton object for VoiceLifeCycle and assigning context of current activity.
 * Initialising the fragments and classes
 */

class GMAudioApp : Application(), Application.ActivityLifecycleCallbacks {

    lateinit var activityContext: BaseActivity
    lateinit var themeChange: NowPlayingActivity

    var drawerAdapter: RecyclerView.Adapter<*>? = null
    private var permissionsUtils: PermissionsUtils? = null
    val sourceNameEventName = LinkedHashMap<String, String>()

    override fun onCreate() {
        super.onCreate()
        appContext = this
        ResponseListener
        setLayoutDirection()
        registerActivityLifecycleCallbacks(this)
        DataPoolDataHandler.themeType.set(true)
        when (getLanguage(appContext)) {
            "en" ->
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.LHD)
            "uk" ->
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.RHD)
            "ar" ->
                DataPoolDataHandler.LHD_RHD_RTL.set(eLHD_RHD_RTL.RTL)
        }
        NONUIVariables.getInstance().gmDatabase = GMDatabase.getInstance(this)
        //if extra source is available . Please add here with source name and event name
        prepareDrawerItemsWithEventName()
    }

    companion object {
        lateinit var appContext: GMAudioApp

        var navigator = ActivityNavigator()
        var useCadillacTheme: Boolean? = false
        var fragmentMgr = FragmentPoolManager
        var IS_RTL = true

        init {
            Thread {
                fragmentMgr.add(NowPlayingFragment(), ManualTunerFragment(), RadioPresetsFragment(), AMFMCategoriesFragment(), AMFMCategoriesInfoFragment(), UsbNowPlayingFragment(), UsbBrowseFragment())
            }.start()
        }
    }

    /**
     * app is in background
     * @param activity current activity
     */
    override fun onActivityPaused(activity: Activity?) {
        // TODO
    }
    /**
     * activity resumed
     * @param activity current activity
     */
    override fun onActivityResumed(activity: Activity?) {
        activityContext = activity!! as BaseActivity
        getPermissionsUtils(activity)
        permissionsUtils!!.requestPermission()
    }

    override fun onActivityStarted(activity: Activity?) {
    }

    override fun onActivityDestroyed(activity: Activity?) {
    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
    }

    override fun onActivityStopped(activity: Activity?) {
    }

    override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(setLocale(base))
        MultiDex.install(this)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        setLocale(this)
    }

    private fun getPermissionsUtils(activity: Activity?): PermissionsUtils {
        if (permissionsUtils == null)
            permissionsUtils = PermissionsUtils(activity)

        return permissionsUtils!!
    }

    private fun setLayoutDirection() {
        val resources = resources
        val config = resources.configuration
        val direction = config.layoutDirection
        IS_RTL = direction == View.LAYOUT_DIRECTION_LTR
    }

    /* Add static items to drawer list*/
    private fun prepareDrawerItemsWithEventName() {

        DataPoolDataHandler.mDrawerOptions.add(NOWPLAYING_SOURCE_TYPE.AM.name)
        DataPoolDataHandler.mDrawerOptions.add(NOWPLAYING_SOURCE_TYPE.FM.name)
        if (!AudioService.isSDKAvailable())
            DataPoolDataHandler.mDrawerOptions.add(NOWPLAYING_SOURCE_TYPE.USBMSD.name)
        DataPoolDataHandler.mDrawerOptions.add(getString(R.string.configuration))

        sourceNameEventName[NOWPLAYING_SOURCE_TYPE.AM.name] = "eNowPlayingAM"
        sourceNameEventName[NOWPLAYING_SOURCE_TYPE.FM.name] = "eNowPlayingFM"
        sourceNameEventName[NOWPLAYING_SOURCE_TYPE.USBMSD.name] = "onUsbRequestSource"
        sourceNameEventName[getString(R.string.configuration)] = "eThemesScreen"

    }

}