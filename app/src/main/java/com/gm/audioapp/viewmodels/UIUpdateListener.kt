package com.gm.audioapp.viewmodels

import android.view.View

/**
 *  Listener to handle UI updates.
 */
interface UIUpdateListener {
    fun onUIUpdateListener(view: View)
}