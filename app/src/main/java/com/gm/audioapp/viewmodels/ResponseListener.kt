package com.gm.audioapp.viewmodels

import android.support.v7.widget.LinearLayoutManager
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.ui.activities.BaseActivity
import com.gm.audioapp.ui.activities.fragments.ManualTunerFragment
import com.gm.audioapp.ui.activities.fragments.RadioPresetsFragment
import com.gm.media.apiintegration.SystemListener
import com.gm.media.apiintegration.apiinterfaces.IAMFMManagerRes
import com.gm.media.models.*
import com.gm.media.models.DataPoolDataHandler.aMFMStationInfo_t
import com.gm.media.utils.Log
import com.gm.media.utils.ManualTunerController
import com.gm.audioapp.R
/**
 * A controller decides which platform api call must be invoked.
 *
 * @author GM on 3/9/2018.
 */
object ResponseListener : IAMFMManagerRes {

    private const val TAG = "ResponseListener"

    init {
        Log.d(TAG, " init...")
        SystemListener.registerApiCallback(this)
    }

    override fun onAMFM_RES_AMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t?) {
        Log.d(TAG, " onAMFM_RES_AMCURRENTSTATIONINFO... $amfmstationinfo")
        updateCarouselView(DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString())
        updateRadioPresetsList(aMFMStationInfo_t.get()!!)
    }

    override fun onAMFM_RES_FMCURRENTSTATIONINFO(amfmstationinfo: AMFMStationInfo_t?) {
        Log.d(TAG, " onAMFM_RES_FMCURRENTSTATIONINFO... $amfmstationinfo")
        updateCarouselView(DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString())
        updateRadioPresetsList(aMFMStationInfo_t.get()!!)
    }

    override fun onAMFM_RES_AMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
        Log.d(TAG, " onAMFM_RES_AMCATEGORYSTATIONLIST... $tunercategorylist")
    }

    override fun onAMFM_RES_FMCATEGORYSTATIONLIST(tunercategorylist: AMFMStationInfo_t) {
        Log.d(TAG, " onAMFM_RES_FMCATEGORYSTATIONLIST... $tunercategorylist")
    }

    override fun onAMFM_RES_AMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        Log.d(TAG, " onAMFM_RES_AMSTRONGSTATIONSLIST... $amfmstationlist")
        //in hardware device we have check multiple times this condition when we are in radiopresetfragment
        updateManualTunerController()
        addCategoryStationListUpdate()
    }

    override fun onAMFM_RES_FMSTRONGSTATIONSLIST(amfmstationlist: AMFMStationList_t) {
        Log.d(TAG, " onAMFM_RES_FMSTRONGSTATIONSLIST... $amfmstationlist")
        //in hardware device we have check multiple times this condition when we are in radiopresetfragment
        updateManualTunerController()
        addCategoryStationListUpdate()
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS(pData: Int) {
        Log.d(TAG, " onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS... $pData")
    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS(pData: Int) {
        Log.d(TAG, " onAMFM_RES_FMSTRONGSTATIONLISTPROGRESS... $pData")
    }

    override fun onAMFM_RES_AMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
        Log.d(TAG, " onAMFM_RES_AMSTRONGSTATIONLISTPROGRESS... $amfmtuneinfo")
    }

    override fun onAMFM_RES_FMTUNEBYPARTIALFREQUENCY(amfmtuneinfo: ManualTuneData) {
        Log.d(TAG, " onAMFM_RES_FMTUNEBYPARTIALFREQUENCY... $amfmtuneinfo")
    }

    override fun onAMFM_RES_FMRDSSWITCH(pData: Int) {
        Log.d(TAG, " onAMFM_RES_FMRDSSWITCH... $pData")
    }

    override fun onAMFM_RES_REGIONSETTING(pData: Int) {
        Log.d(TAG, " onAMFM_RES_REGIONSETTING... $pData")
    }

    override fun onAMFM_RES_TPSTATUS(amfmtpstatus: AMFMTPStatus_t) {
        Log.d(TAG, " onAMFM_RES_TPSTATUS... $amfmtpstatus")
    }

    override fun onAMFM_RES_TRAFFICALERT(pData: String) {
        Log.d(TAG, " onAMFM_RES_TRAFFICALERT... $pData")
    }

    override fun onAMFM_RES_TRAFFICALERTACTIVECALL(amfmtrafficinfo: AMFMTrafficInfo_t) {
        Log.d(TAG, " onAMFM_RES_TRAFFICALERTACTIVECALL... $amfmtrafficinfo")
    }

    override fun onAMFM_RES_PROGRAMTYPERDSNOTIFICATION(amfmprogramtypealert: AMFMProgramTypeAlert_t) {
        Log.d(TAG, " onAMFM_RES_PROGRAMTYPERDSNOTIFICATION... $amfmprogramtypealert")
    }

    override fun onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
        Log.d(TAG, " onAMFM_RES_AMSTRONGSTATIONLISTUPDATEFAILED... $pData")
    }

    override fun onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED(pData: String) {
        Log.d(TAG, " onAMFM_RES_FMSTRONGSTATIONLISTUPDATEFAILED... $pData")
    }

    override fun onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE(pData: String) {
        Log.d(TAG, " onAMFM_RES_CANCELAMSTRONGSTATIONLISTUPDATE... $pData")
    }

    override fun onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE(pData: String) {
        Log.d(TAG, " onAMFM_RES_CANCELFMSTRONGSTATIONLISTUPDATE... $pData")
    }

    override fun onAMFM_RES_TRAFFICALERT_END(pData: String) {
        Log.d(TAG, " onAMFM_RES_TRAFFICALERT_END... $pData")
    }

    override fun onAMFM_RES_TRAFFICINFOSTART(pData: String) {
        Log.d(TAG, " onAMFM_RES_TRAFFICINFOSTART... $pData")
    }

    override fun onAMFM_RES_STATIONAVAILABALITYSTATUS(fmstationavailabilityinfo: FMStationAvailabilityInfo_t) {
        Log.d(TAG, " onAMFM_RES_STATIONAVAILABALITYSTATUS... $fmstationavailabilityinfo")
    }

    override fun onAMFM_RES_ACTIONUNAVAILABLE(pData: String) {
        Log.d(TAG, " onAMFM_RES_ACTIONUNAVAILABLE... $pData")
    }

    override fun onMEDIA_RES_ACTIVEMEDIADEVICE(mediadeviceinfo: MediaDeviceInfo_t) {
        Log.d(TAG, " onMEDIA_RES_ACTIVEMEDIADEVICE... $mediadeviceinfo")
        if (GMAudioApp.appContext.drawerAdapter != null)
            GMAudioApp.appContext.drawerAdapter!!.notifyDataSetChanged()
    }

    override fun onMEDIA_RES_INDEXINGSTATE(activedevmediaindexingstate: ActiveDevMediaIndexingState_t) {
        Log.d(TAG, " onMEDIA_RES_INDEXINGSTATE... $activedevmediaindexingstate")
    }

    override fun onMEDIA_RES_MEDIAPLAYERINDEXEDLIST(mediaplayerlistres: MediaPlayerListRes_t) {
        Log.d(TAG, " onMEDIA_RES_MEDIAPLAYERINDEXEDLIST... $mediaplayerlistres")
    }

    override fun onMEDIA_RES_NOWPLAYING(mediaobject: MediaObject_t) {
        Log.d(TAG, " onMEDIA_RES_NOWPLAYING... $mediaobject")
        updateUsbCarouselView(DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_FILENAME.get().toString())
    }

    override fun onMEDIA_RES_PLAYBACKMODE(eMediaPBMode: eMediaPBMode) {
        Log.d(TAG, " onMEDIA_RES_PLAYBACKMODE... $eMediaPBMode")
    }

    override fun onMEDIA_RES_PLAYTIME(mediaplaytime: MediaPlayTime_t) {
        Log.d(TAG, " onMEDIA_RES_PLAYTIME... $mediaplaytime")
    }

    override fun onMEDIA_RES_PLAYBACKACTION(emediapbaction: eMediaPBAction) {
        Log.d(TAG, " onMEDIA_RES_PLAYBACKACTION... $emediapbaction")
    }

    override fun onMEDIA_RES_MEDIAOBJECTUNAVAILABLE() {
        Log.d(TAG, " onMEDIA_RES_MEDIAOBJECTUNAVAILABLE... ")
    }

    override fun onMEDIA_RES_INDEXINGLIMITREACHED(int: Int) {
        Log.d(TAG, " onMEDIA_RES_INDEXINGLIMITREACHED... $int")
    }

    override fun onMEDIA_RES_MEDIAOBJECTCONNECTIONUNAVAILABLE(int: Int) {
        Log.d(TAG, " onMEDIA_RES_MEDIAOBJECTCONNECTIONUNAVAILABLE... $int")
    }

    override fun onMEDIA_RES_LISTFILTERINFO(mediaplayerlistreq: MediaPlayerListReq_t) {
        Log.d(TAG, " onMEDIA_RES_LISTFILTERINFO... $mediaplayerlistreq")
    }

    override fun onMEDIA_RES_SEARCHKEYBOARDMEDIALIST(mediaSearchKeyboardList_t: MediaSearchKeyboardList_t) {
        Log.d(TAG, " onMEDIA_RES_SEARCHKEYBOARDMEDIALIST... $mediaSearchKeyboardList_t")
    }

    override fun onMEDIA_RES_MEDIAACTIONUNAVAILABLE(int: Int) {
        Log.d(TAG, " onMEDIA_RES_MEDIAACTIONUNAVAILABLE... $int")
    }

    override fun onMEDIA_RES_NOWPLAYING_METADATA(mediametadata: MediaMetadata_t) {
        Log.d(TAG, " onMEDIA_RES_NOWPLAYING_METADATA... $mediametadata")
    }

    override fun onMEDIA_RES_DEVICE_MOUNTED_STATUS(enable: Boolean) {
        Log.d(TAG, " onMEDIA_RES_DEVICE_MOUNTED_STATUS... $enable")
    }

    override fun onMEDIA_RES_MEDIAACTIONUNAVAILABLE_CHECK_CONNECTION(int: Int) {
        Log.d(TAG, " onMEDIA_RES_MEDIAACTIONUNAVAILABLE_CHECK_CONNECTION... $int")
    }

    override fun onMEDIA_RES_SEEKTO(int: Int) {
        Log.d(TAG, " onMEDIA_RES_LISTFILTERINFO... $int")

    }

    override fun onMEDIA_RES_MEDIAPLAYERSCREENTRANSITION(mediascreentransition: MediaScreenTransition_t) {
        Log.d(TAG, " onMEDIA_RES_MEDIAPLAYERSCREENTRANSITION... $mediascreentransition")
    }

    override fun onMEDIA_RES_MEDIAOBJECTALBUMARTINFO(mediaalbumartinfo: MediaAlbumArtInfo_t) {
        Log.d(TAG, " onMEDIA_RES_MEDIAOBJECTALBUMARTINFO... $mediaalbumartinfo")
    }

    override fun onMEDIA_RES_LISTBACKTRAVERSE(mediaplayerlistreq: MediaPlayerListReq_t) {
        Log.d(TAG, " onMEDIA_RES_LISTBACKTRAVERSE... $mediaplayerlistreq")
    }

    override fun onMEDIA_RES_BROWSEBUTTONSUPPORT(status: Boolean) {
        Log.d(TAG, " onMEDIA_RES_BROWSEBUTTONSUPPORT... $status")
    }

    override fun onMEDIA_RES_MEDIASOURCECHANGEDTOBT() {
        Log.d(TAG, " onMEDIA_RES_MEDIASOURCECHANGEDTOBT ...")
    }

    override fun onMEDIA_RES_MEDIASOURCECHANGEDTOUSB() {
        Log.d(TAG, " onMEDIA_RES_MEDIASOURCECHANGEDTOUSB ...")
    }


    private fun updateUsbCarouselView(title: String) {
        Log.d(TAG, " updateUsbCarouselView...  $title")
        if (Utility.getNowPlayingActivity().carouselRecyclerViewAdapter != null && !Utility.getNowPlayingActivity().carouselRecyclerViewAdapter!!.isScrolling)
            for (position in NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST.indices) {
                if (title == NONUIVariables.getInstance().MEDIA_PLAYING_SONG_LIST[position].fileName) {
                    (Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.recyclerView?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(position, 0)
                    Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateVisibleItem(position, true, true)
                    break
                }
            }
    }

    //add category and station list update to list
    private fun addCategoryStationListUpdate() {
        Log.d(TAG, " addCategoryStationListUpdate...")
        try {
            if (Utility.getNowPlayingActivity().browseRecyclerViewAdapter != null) {
                val browseList = NONUIVariables.getInstance().browseList
                browseList.add(0, Builder().stationName(GMAudioApp.appContext.getString(R.string.categories)).build())
                browseList.add(1, Builder().stationName(GMAudioApp.appContext.getString(R.string.station_list_update)).build())
                NONUIVariables.getInstance().browseList = ArrayList()
                NONUIVariables.getInstance().browseList = browseList
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    /**
     * It displays the given frequency as selected item in carousal view
     * @param frequency to be selected/highlighted in carousal view
     *
     */
    private fun updateCarouselView(frequency: String) {
        Log.d(TAG, " updateCarouselView...  $frequency")
        if (Utility.getNowPlayingActivity().carouselRecyclerViewAdapter != null && !Utility.getNowPlayingActivity().carouselRecyclerViewAdapter!!.isScrolling) {
            val filterItem = NONUIVariables.getInstance().browseList.filter { it -> it.frequency.toString() == frequency }
            if (filterItem.isNotEmpty()) {
                val index = NONUIVariables.getInstance().browseList.indexOf(filterItem[0])
                (Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.recyclerView?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(index, 0)
                Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateVisibleItem(index, true, true)
            }
        }
    }

    /**
     * This is called to update the metadata for given station
     * @param stationInfo this object will be added in the station list
     */
    private fun updateRadioPresetsList(stationInfo: AMFMStationInfo_t) {
        Log.d(TAG, " updateRadioPresetsList...  $stationInfo")
        try {
            if (BaseActivity.currentFragment is RadioPresetsFragment) {
                val filterItem = NONUIVariables.getInstance().browseList.filter { it -> it.frequency.toString() == stationInfo.frequency.toString() }
                if (filterItem.isNotEmpty())
                    DataPoolDataHandler.nowPlayingBrowseListPosition.set(NONUIVariables.getInstance().browseList.indexOf(filterItem[0]))
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    private fun updateManualTunerController(){
        try{
            if(BaseActivity.currentFragment is ManualTunerFragment
                    && null!=DataPoolDataHandler.browseList
                    &&!DataPoolDataHandler.browseList.isEmpty())
                ManualTunerController.validateSourceType(NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name)
        }
        catch (e:Exception){
            e.printStackTrace()
        }
    }
}

