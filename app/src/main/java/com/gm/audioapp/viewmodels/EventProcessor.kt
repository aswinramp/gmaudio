package com.gm.audioapp.viewmodels

import android.content.Intent
import android.view.View
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.GMAudioApp.Companion.appContext
import com.gm.audioapp.GMAudioApp.Companion.navigator
import com.gm.audioapp.R
import com.gm.media.apiintegration.AudioService
import com.gm.media.models.EventObject
import com.gm.media.models.NONUIVariables
import com.gm.media.utils.MediaUtil
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*
import kotlin.collections.HashMap


/**
 * This class process the event generated when user interacts  with screens
 */
object EventProcessor {

    var eventTable = HashMap<String, Any>()
    var voiceCommands = HashMap<String, Any>()
    var keyboardCommands = HashMap<String, Any>()
    var remoteCommands = HashMap<String, Any>()
    private const val resFileCommands: Int = R.raw.commands
    private const val resFileEventMap: Int = R.raw.eventmap

    init {
        voiceCommands = getCommands(resFileCommands, "voice")
        keyboardCommands = getCommands(resFileCommands, "keyboard")
        remoteCommands = getCommands(resFileCommands, "remote")
        eventTable = getEventMapTable(resFileEventMap)
    }

    /**
     * To get commands
     * @param resFileRead , used to get commands.
     * @param commandType , used to get commandtype.
     * @return hashMap.
     */
    private fun getCommands(resFileRead: Int, commandType: String): HashMap<String, Any> {
        val jsonObj = JSONObject(getJsonString(appContext.resources.openRawResource(resFileRead))).getJSONObject(commandType)
        return hashMap(jsonObj)
    }

    /**
     * To get event map table
     * @param resFileRead , used to get table.
     * @return hashMap.
     */
    private fun getEventMapTable(resFileRead: Int): HashMap<String, Any> {
        val jsonObj = JSONObject(getJsonString(appContext.resources.openRawResource(resFileRead)))
        return hashMap(jsonObj)
    }

    /**
     * @param jsonObj passing json data.
     * @return map.
     */
    private fun hashMap(jsonObj: JSONObject): HashMap<String, Any> {
        val map = HashMap<String, Any>()
        val iterator = jsonObj.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            try {
                val command = jsonObj.get(key)
                map[key] = command
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
        return map
    }

    /**
     * To get json string
     * @param stream is InputStream, used to get json string.
     * @return String.
     */
    private fun getJsonString(stream: InputStream): String {
        val strBuilder = StringBuilder()
        var jsonString: String? = null
        val bfReader = BufferedReader(InputStreamReader(stream, "UTF-8"))
        while ({ jsonString = bfReader.readLine(); jsonString }() != null) {
            strBuilder.append(jsonString)
        }
        return strBuilder.toString()
    }

    /**
     * trigger the event corresponding to given eventType,eventName and activityName.
     * @param eventType passing which type of event has to perform.
     * @param eventObject passing which name of event has to perform.
     * @param activityName passing which name of activity/fragment has to perform.
     * @param isProcess passing boolean status.
     * @param eventObject  additional data that is passed.
     */
    fun triggerEvent(view: View?, eventType: String, eventObject: EventObject?, activityName: String, isProcess: Boolean) {

        if (NONUIVariables.getInstance().mMediaCallBack == null)
            MediaCustomListener.init()

        when (eventType) {
            "navigate" -> {
                if (isProcess)
                    processEvent(eventObject)
                when {
                    eventObject!!.eventName == "evG_Back" || eventObject.eventName == "evG_Close" -> navigator.finishActivity()
                    eventObject.eventName == "evG_Recreate" -> navigator.recreateActivity()
                    GMAudioApp.appContext.activityContext.javaClass.simpleName == activityName -> replaceScreen(view, eventObject.eventName, eventObject.obj)
                    else -> navigateScreen(eventObject.eventName, eventObject.obj)
                }
            }
            "process" ->
                processEvent(eventObject)
            "media" ->
                MediaUtil.processMedia(eventObject)
        }
    }

    /**
     * process the event corresponding to given eventName
     * @param eventObject passing which name of event has to perform.
     * @param eventObject  additional data that is passed.
     */
    fun processEvent(eventObject: EventObject?) {
        ResponseListener
        val serviceIntent = Intent()
        serviceIntent.action = "com.gm.media.apiintegration.AudioService"
        serviceIntent.`package` = GMAudioApp.appContext.packageName

        val cal = Date().time
        serviceIntent.putExtra("eventName", eventObject!!.eventName)
        serviceIntent.putExtra("eventTime", cal)

        if (eventObject.obj != null)
            AudioService.anyData[cal] = eventObject.obj!!

        GMAudioApp.appContext.startService(serviceIntent)
    }

    /**
     * navigate the fragment corresponding to given eventName
     * @param eventName passing which name of event has to perform.
     * @param any  additional data that is passed.
     */
    private fun navigateScreen(eventName: String, any: Any?) {
        println("navigateScreen::navigate $eventName")
        navigator.triggerActivity(eventName, any)
    }

    /**
     * add/replaces fragment corresponding to given eventName
     * @param eventName passing which name of event has to perform.
     * @param any  additional data that is passed.
     */
    private fun replaceScreen(view: View?, eventName: String, any: Any?) {
        println("navigateScreen::navigate $eventName")
        val currentFragment = GMAudioApp.appContext.activityContext.supportFragmentManager.findFragmentById(GMAudioApp.appContext.activityContext.getContainerId())
        println("navigateScreen::currentFragment $currentFragment")
        if (view != null && currentFragment is RegisterListener)
            currentFragment.onRegisterListener(view, eventName, any)
        else
            navigator.triggerFragment(eventName, any)
    }

}