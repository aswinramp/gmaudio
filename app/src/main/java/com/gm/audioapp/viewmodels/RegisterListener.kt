package com.gm.audioapp.viewmodels

import android.view.View

/**
 * Created by Nagaraja Reddy Mallu on 7/23/2018.
 */
interface RegisterListener {
    fun onRegisterListener(view: View,eventName:String,any:Any?)
}