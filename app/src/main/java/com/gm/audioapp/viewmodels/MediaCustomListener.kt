package com.gm.audioapp.viewmodels

import com.gm.audioapp.common.customclasses.CustomAlertDialog
import com.gm.audioapp.ui.activities.BaseActivity
import com.gm.audioapp.ui.activities.fragments.UsbNowPlayingFragment
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.apiinterfaces.MediaCustomInterface
import com.gm.media.apiintegration.mock.SimulationManager
import com.gm.media.apiintegration.sdk.SDKManager
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.NONUIVariables
import com.gm.media.utils.FavoriteDataProcessor
import gm.entertainment.ActiveBrowseItem
import gm.media.CMediaPlayer
import gm.media.enums.EnumDeviceType
import gm.media.exceptions.*
import gm.media.interfaces.IBrowseItem
import gm.media.interfaces.IDevice
import com.gm.media.utils.AudioConstants
import com.gm.media.utils.Log

/**
 * A controller decides which platform api call must be invoked.
 *
 * @author GM on 3/9/2018.
 */
object MediaCustomListener : MediaCustomInterface {

    private val mTAG = MediaCustomListener::class.simpleName
    private var dialog=CustomAlertDialog()

    fun init() {
        if (AudioService.isSDKAvailable())
            SDKManager.getInstance().registerApiCallback(this)
        else
            SimulationManager.getInstance().registerApiCallback(this)
    }

    override fun checkFragmentFunction(functionType: String) {
        when (functionType) {
            AudioConstants.ShowMetaData -> {
                val fragment = BaseActivity.currentFragment
                if (null != fragment && fragment is UsbNowPlayingFragment)
                    fragment.showMediaMetaData()
            }
            AudioConstants.HideVideo -> {
                val fragment = BaseActivity.currentFragment
                if (null != fragment && fragment is UsbNowPlayingFragment)
                    fragment.hideVideo()
            }
            AudioConstants.ShowVideo -> {
                val fragment = BaseActivity.currentFragment
                if (null != fragment && fragment is UsbNowPlayingFragment)
                    fragment.showVideoBasedOnVehicleMovementState(true)
            }
            AudioConstants.EnableDisableBrowseVideo -> {
                val fragment = BaseActivity.currentFragment
                if (null != fragment && fragment is UsbNowPlayingFragment) {
                    fragment.enableDisableBrowseBasedOnWorkloadRestriction(NONUIVariables.getInstance().DRIVINGMODE!!)
                    fragment.enableDisableVideoBasedOnWorkloadRestriction(NONUIVariables.getInstance().DRIVINGMODE!!)
                }
            }
            AudioConstants.NormalScreenVideo -> {
                //val fragment = BaseActivity.currentFragment
                //if (null != fragment && fragment is UsbNowPlayingFragment) {
                //fragment.normalScreenVideo()
                //}
            }
        }
    }

    override fun closeEvent(functionType: String) {
        when (functionType) {
            AudioConstants.BROWSE_CLOSE_KEY ->
                EventHandler.onDrawerClick(AudioConstants.BROWSE_CLOSE_KEY_EVENT)
        }
    }

    override fun getCMediaPlayer(functionType: String, cmMediaPlayer: CMediaPlayer, iDevice: IDevice, any: Any) {
        when (functionType) {
            AudioConstants.Remove_Browse -> {
                try {
                    cmMediaPlayer.removeBrowseAdapter(Utility.getNowPlayingActivity().gmUsbBrowseAdapter)
                } catch (e: ExceptionBrowseFunctionNotEnabled) {
                    e.printStackTrace()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            AudioConstants.OnKeypadLetterClick -> {
                try {
                    var positionSelection = Utility.getNowPlayingActivity().gmUsbBrowseAdapter.sections?.indexOf(any)
                    if (positionSelection == null) positionSelection = 0
                    Utility.getNowPlayingActivity().gmUsbBrowseAdapter.mListView?.setSelection(Utility.getNowPlayingActivity().gmUsbBrowseAdapter.getPositionForSection(positionSelection))
                    Utility.getNowPlayingActivity().gmUsbBrowseAdapter.showBubbleOnSectionJump()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            AudioConstants.OnUsbNowPlaying -> {
                try {
                    cmMediaPlayer.playDevice(iDevice)
                    cmMediaPlayer.mediaIsActiveSource = true
                    cmMediaPlayer.addBrowseAdapter(Utility.getNowPlayingActivity().gmUsbBrowseAdapter)
                    cmMediaPlayer.selectDevice(iDevice)
                    Utility.getNowPlayingActivity().gmUsbBrowseAdapter.setActiveDevice(iDevice)
                    if (NONUIVariables.getInstance().mActiveBrowseAddresses.size > 0) {
                        val browseItem = NONUIVariables.getInstance().mActiveBrowseAddresses.peek()
                        cmMediaPlayer.setBrowseListAddress(iDevice, browseItem.getmAddress())
                    } else
                        cmMediaPlayer.setBrowseListAddress(iDevice, longArrayOf(AudioConstants.BROWSE_ROOT_SONGS))

                    Utility.getNowPlayingActivity().gmUsbBrowseAdapter.notifyDataSetChanged()
                } catch (e: ExceptionInitializationFailed) {
                    Log.e(mTAG, e.printStackTrace().toString())
                } catch (e: ExceptionPlayFunctionNotEnabled) {
                    Log.e(mTAG, e.printStackTrace().toString())
                } catch (e: ExceptionParameterOutOfBounds) {
                    Log.e(mTAG, e.printStackTrace().toString())
                } catch (e: ExceptionDeviceNotConnected) {
                    Log.e(mTAG, e.printStackTrace().toString())
                } catch (e: ExceptionBrowseFunctionNotEnabled) {
                    Log.e(mTAG, e.printStackTrace().toString())
                } catch (e: Exception) {
                    Log.e(mTAG, e.printStackTrace().toString())
                }
            }
            AudioConstants.OnUsbBrowse -> {

                if (iDevice.type == EnumDeviceType.USBMSD && iDevice.isMounted) {
                    try {
                        cmMediaPlayer.playDevice(iDevice)
                        cmMediaPlayer.mediaIsActiveSource = true
                        cmMediaPlayer.addBrowseAdapter(Utility.getNowPlayingActivity().gmUsbBrowseAdapter)
                        cmMediaPlayer.selectDevice(iDevice)
                        Utility.getNowPlayingActivity().gmUsbBrowseAdapter.setActiveDevice(iDevice)
                        val outArray = longArrayOf()//BROWSE_ROOT_ARTISTS,BROWSE_ROOT_ALBUMS
                        Utility.getNowPlayingActivity().gmUsbBrowseAdapter.activeAddress = outArray
                        cmMediaPlayer.setBrowseListAddress(iDevice, outArray)//cmMediaPlayer.getPlayContext()?.getBrowseStack()
                        // Utility.getNowPlayingActivity().gmUsbBrowseAdapter.notifyDataSetInvalidated()
                        if (NONUIVariables.getInstance().mActiveBrowseAddresses.size > 0) {
                            val browseItem = NONUIVariables.getInstance().mActiveBrowseAddresses.peek()
                            // Utility.setUsbTitle(browseItem,MediaStringMatcher.getStringBYId(R.string.browse_st))
                            NONUIVariables.getInstance().ACTIVE_MEDIA_ADDRESS = browseItem
                            updateMediaData(browseItem.getmAddress(), cmMediaPlayer, iDevice)
                        } else {
                            // Utility.setUsbTitle(null,MediaStringMatcher.getStringBYId(R.string.browse_st))
                            NONUIVariables.getInstance().ACTIVE_MEDIA_ADDRESS = null
                            updateMediaData(outArray, cmMediaPlayer, iDevice)
                        }
                    } catch (e: ExceptionInitializationFailed) {
                        Log.e(mTAG, e.printStackTrace().toString())
                    } catch (e: ExceptionPlayFunctionNotEnabled) {
                        Log.e(mTAG, e.printStackTrace().toString())
                    } catch (e: ExceptionParameterOutOfBounds) {
                        Log.e(mTAG, e.printStackTrace().toString())
                    } catch (e: ExceptionDeviceNotConnected) {
                        Log.e(mTAG, e.printStackTrace().toString())
                    } catch (e: ExceptionBrowseFunctionNotEnabled) {
                        Log.e(mTAG, e.printStackTrace().toString())
                    } catch (e: Exception) {
                        Log.e(mTAG, e.printStackTrace().toString())
                    }
                }
            }
        }
    }

    /**
     * Request mediaPlayer to update the listview based on the given address
     */
    override fun updateMediaData(functionType: String, iBrowseItem: IBrowseItem?, cmMediaPlayer: CMediaPlayer, iDevice: IDevice) {

        when (functionType) {
            AudioConstants.USBBrowseItemClick -> {
                // By default create an array of size 1.
                var newBrowseFragmentAddress = LongArray(1)
                if (NONUIVariables.getInstance().mActiveBrowseAddresses.size > 0) {
                    val currentBrowseItemAddress = NONUIVariables.getInstance().mActiveBrowseAddresses[NONUIVariables.getInstance().mActiveBrowseAddresses.size - 1].getmAddress()
                    if (currentBrowseItemAddress != null) {
                        // create new array with new size for the new fragment browse data
                        newBrowseFragmentAddress = LongArray(currentBrowseItemAddress.size + 1)
                        for (i in currentBrowseItemAddress.indices) {
                            if (i < newBrowseFragmentAddress.size)
                                newBrowseFragmentAddress[i] = currentBrowseItemAddress[i]
                        }
                    }
                }
                val mActiveBrowseItem = ActiveBrowseItem(iBrowseItem!!.displayText,
                        iBrowseItem.childType, iBrowseItem.objectID, newBrowseFragmentAddress)
                NONUIVariables.getInstance().mActiveBrowseAddresses.push(mActiveBrowseItem)
                newBrowseFragmentAddress[newBrowseFragmentAddress.size - 1] = iBrowseItem.objectID
                NONUIVariables.getInstance().ACTIVE_MEDIA_ADDRESS = mActiveBrowseItem
                updateMediaData(newBrowseFragmentAddress, cmMediaPlayer, iDevice)
            }
            AudioConstants.MEDIA_REQ_BACKTRAVERSE -> {
                NONUIVariables.getInstance().mActiveBrowseAddresses.pop()
                if (NONUIVariables.getInstance().mActiveBrowseAddresses.size > 0) {
                    val browseItem = NONUIVariables.getInstance().mActiveBrowseAddresses.peek()
                    NONUIVariables.getInstance().ACTIVE_MEDIA_ADDRESS = browseItem
                    updateMediaData(browseItem.getmAddress(), cmMediaPlayer, iDevice)
                } else {
                    NONUIVariables.getInstance().ACTIVE_MEDIA_ADDRESS = null
                    updateMediaData(longArrayOf(), cmMediaPlayer, iDevice)
                }
            }
        }

    }

    //update adapter
    override fun updateAdapter() {
        Utility.getNowPlayingActivity().gmUsbBrowseAdapter.notifyDataSetChanged()
    }

    /**
     * Request mediaPlayer to update the listview based on the given address
     */
    private fun updateMediaData(outArray: LongArray, cmMediaPlayer: CMediaPlayer, iDevice: IDevice) {
        FavoriteDataProcessor.mediaBrowseFavLoad.loadFavourite()
        DataPoolDataHandler.HIDE_AZ_BTN.set(true)
        if (outArray.isEmpty())
            DataPoolDataHandler.IS_MEDIA_BROWSE_ROOT_PAGE.set(true)
        else
            DataPoolDataHandler.IS_MEDIA_BROWSE_ROOT_PAGE.set(false)

        try {
            cmMediaPlayer.selectDevice(iDevice)
            Utility.getNowPlayingActivity().gmUsbBrowseAdapter.setActiveDevice(iDevice)
            cmMediaPlayer.setBrowseListAddress(iDevice, outArray)
            cmMediaPlayer.addBrowseAdapter(Utility.getNowPlayingActivity().gmUsbBrowseAdapter)
            // Utility.getNowPlayingActivity().gmUsbBrowseAdapter.notifyDataSetInvalidated()
        } catch (e: ExceptionInitializationFailed) {
            e.printStackTrace()
            Log.e(mTAG, e.printStackTrace().toString())
        } catch (e: ExceptionPlayFunctionNotEnabled) {
            e.printStackTrace()
            Log.e(mTAG, e.printStackTrace().toString())
        } catch (e: ExceptionParameterOutOfBounds) {
            e.printStackTrace()
            Log.e(mTAG, e.printStackTrace().toString())
        } catch (e: ExceptionDeviceNotConnected) {
            e.printStackTrace()
            Log.e(mTAG, e.printStackTrace().toString())
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e(mTAG, e.printStackTrace().toString())
        }
    }

    override fun showCustomAlertDialog() {

        try {
            dialog = CustomAlertDialog()
            dialog!!.setCancelable(false)
            dialog!!.setCanceledOnTouchOutside(false)
            dialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun dismissCustomAlertDialog() {
    if (null != dialog) {
           dialog!!.dismiss()
        }
    }
}
