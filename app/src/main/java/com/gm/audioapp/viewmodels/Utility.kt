package com.gm.audioapp.viewmodels

import android.content.Context
import android.content.res.Configuration
import android.graphics.Bitmap
import android.preference.PreferenceManager
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.ui.activities.NowPlayingActivity
import com.gm.audioapp.ui.customclasses.MediaStringMatcher
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.HeaderObject
import com.gm.media.utils.Log
import gm.entertainment.ActiveBrowseItem
import gm.media.enums.EnumBrowseChildType
import gm.media.utilities.CConstants
import java.util.*
import com.gm.audioapp.R

object Utility {

    private const val TAG = "Utility"

    fun getStringById(id: Int): String {
        return GMAudioApp.appContext.resources.getString(id)
    }

    fun setLocale(c: Context): Context {
        return updateResources(c, getLanguage(c))
    }

    fun setNewLocale(c: Context, language: String): Context {
        persistLanguage(c, language)
        return updateResources(c, language)
    }

    fun getLanguage(c: Context): String {
        val prefs = PreferenceManager.getDefaultSharedPreferences(c)
        return prefs.getString(c.getString(R.string.shared_pref_language_key), c.getString(R.string.language_type))
    }

    private fun updateResources(context: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val res = context.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        return context.createConfigurationContext(config)
    }

    private fun persistLanguage(c: Context, language: String) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(c)
        prefs.edit().putString(getStringById(R.string.shared_pref_language_key), language).apply()
    }

    //get Now Playing Activity
    fun getNowPlayingActivity(): NowPlayingActivity {
        return if (GMAudioApp.appContext.activityContext is NowPlayingActivity)
            GMAudioApp.appContext.activityContext as NowPlayingActivity
        else
            GMAudioApp.appContext.themeChange
    }

    //set empty bitmap
    fun setEmptyBitmap() {
        //for empty bitmap
        val conf = Bitmap.Config.ARGB_8888
        val bitmap = Bitmap.createBitmap(100, 100, conf)
        DataPoolDataHandler.MEDIA_NOWPLAYING_ALBUM_ART_IMAGE.set(bitmap)
        DataPoolDataHandler.MEDIA_NOWPLAYING_EMPTY_BITMAP_IMAGE.set(bitmap)
    }

    fun setUsbTitle(browseItem: ActiveBrowseItem?, text: String) {
        if (browseItem == null) {//for root screen
            Log.v(TAG, " setUsbTitle ... $browseItem")
            setUsbBrowseTypeAndHeaderObject(text, false, text, null)
            return
        }
        val displayTextConstant = browseItem.getmDisplayText()
        val displayText = MediaStringMatcher.getMediaNameByConstant(displayTextConstant)
        val enumBrowseChildType = browseItem.getmBrowseChildType()
        val subTitle = MediaStringMatcher.getMediaNameByType(enumBrowseChildType)
        // "all songs" no need to display in header
        if (displayTextConstant.compareTo(CConstants.Browse_All_Songs_Token) == 0)
            setUsbBrowseTypeAndHeaderObject(subTitle, true, DataPoolDataHandler.MEDIA_HEADER_OBJECT.get()?.firstLineHeaderSt!!, subTitle)
        else if (enumBrowseChildType == EnumBrowseChildType.Chapters
                || enumBrowseChildType == EnumBrowseChildType.Episodes
                || enumBrowseChildType == EnumBrowseChildType.Albums
                || enumBrowseChildType == EnumBrowseChildType.Artists
                || enumBrowseChildType == EnumBrowseChildType.Songs) {
            if (displayText.compareTo(subTitle) != 0)
                setUsbBrowseTypeAndHeaderObject(subTitle, true, displayText, subTitle)
            else
            //only single title for artist,songs and album --
                setUsbBrowseTypeAndHeaderObject(displayText, false, displayText, null)

        } else
            setUsbBrowseTypeAndHeaderObject(displayText, false, displayText, null)

    }

    //common method for set mediaUsbBrowseType,isTwoLineHeader , firstLineHeaderSt and secondLineHeaderSt
    private fun setUsbBrowseTypeAndHeaderObject(displayText: String, isTwoLineHeader: Boolean, firstLineHeaderSt: String, secondLineHeaderSt: String?) {
        Utility.getNowPlayingActivity().mediaUsbBrowseType = displayText
        if (secondLineHeaderSt != null)
            DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = isTwoLineHeader, firstLineHeaderSt = firstLineHeaderSt, secondLineHeaderSt = secondLineHeaderSt))
        else
            DataPoolDataHandler.MEDIA_HEADER_OBJECT.set(HeaderObject(isTwoLineHeader = isTwoLineHeader, firstLineHeaderSt = firstLineHeaderSt))
    }

}