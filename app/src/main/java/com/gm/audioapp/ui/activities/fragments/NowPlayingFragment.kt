package com.gm.audioapp.ui.activities.fragments

import android.animation.ObjectAnimator
import android.content.res.Configuration
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.databinding.NowPlayingFragmentBinding
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.Utility
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.NONUIVariables
import com.gm.media.utils.AudioConstants
import kotlinx.android.synthetic.main.ics_audio_nowplaying_layout.view.*
import com.gm.audioapp.R
/**
 *  This screen is used to tune to AM/FM station.
 *  It shows the current playing station and other media metadata
 */
class NowPlayingFragment : BaseFragment() {

    private var mRootView: View? = null
    private var mMainDisplay: View? = null
    /* private var fm_nowplaying_include: View? = null
     private var am_nowplaying_include: View? = null*/
    private var cardView: View? = null
    private val sInterpolator = FastOutSlowInInterpolator()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = DataBindingUtil.inflate<NowPlayingFragmentBinding>(inflater, R.layout.ics_audio_nowplaying_layout, container, false)

        mRootView = binding.root
        binding.let {
            it?.clickHandler = EventHandler
            it?.dataPoolHandler = DataPoolDataHandler
        }

        //initialize views
        initializeViews()

        return mRootView
    }

    //initialize the views
    private fun initializeViews() {
        mMainDisplay = mRootView!!.recycler_view
        cardView = mRootView!!.controlButtonsLayout
    }

    private fun fadeOutContent() {
        fadeOut(mMainDisplay!!)
        //fadeOut(am_nowplaying_include!!)
        //fadeOut(fm_nowplaying_include!!)
        fadeOut(cardView!!)
    }

    /**
     * Fades out the given view     *
     * @param view view to be fade out
     */
    private fun fadeOut(view: View) {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 1f, 0f)
        containerAlphaAnimator.interpolator = sInterpolator
        containerAlphaAnimator.startDelay = FADE_OUT_START_DELAY_MS.toLong()
        containerAlphaAnimator.duration = FADE_ANIM_TIME_MS.toLong()
        containerAlphaAnimator.start()
    }

    private fun fadeInContent() {
        fadeIn(mMainDisplay!!)
        //fadeIn(am_nowplaying_include!!)
        //fadeIn(fm_nowplaying_include!!)
        fadeIn(cardView!!)
    }

    /**
     * Fades in the given view     *
     * @param view view to be fade in
     */
    private fun fadeIn(view: View) {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f)
        containerAlphaAnimator.interpolator = sInterpolator
        containerAlphaAnimator.duration = FADE_ANIM_TIME_MS.toLong()
        containerAlphaAnimator.start()
    }


    override fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction? {
        val fragmentTransaction = GMAudioApp.appContext.activityContext.supportFragmentManager.beginTransaction()
                ?.replace(containerId, fragment, AudioConstants.CONTENT_FRAGMENT_TAG)
        fragmentTransaction?.commit()
        return fragmentTransaction
    }

    override fun entryAnimation() {
        fadeInContent()
    }

    override fun exitAnimation() {
        fadeOutContent()
    }

    /**
     * This object holds the instance of NowPlayingFragment and FastOutSlowInInterpolator
     */
    companion object {
        /**
         * this interpolator is used for fade in/out
         */
        //private val sInterpolator = FastOutSlowInInterpolator()
        private val FADE_OUT_START_DELAY_MS = 150
        private val FADE_ANIM_TIME_MS = 100
        // private val fragment = NowPlayingFragment()

        /**
         * @return returns instance of NowPlayingFragment
         */
        // fun getInstance(): NowPlayingFragment = fragment
    }

    // onConfigurationChanged get called While rotating tab from landscape to portrait, inorder to get Focus on first element
    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)

        if (NONUIVariables.getInstance().browseList.size > 0) {
            val layoutManager = LinearLayoutManager(Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.recyclerView?.context)
            Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.recyclerView?.layoutManager = layoutManager
            layoutManager.scrollToPositionWithOffset(0, 0)
            Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateVisibleItem(0, false, true)
        }
    }
}
