package com.gm.audioapp.ui.adapters

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Configuration
import android.databinding.BindingAdapter
import android.databinding.ObservableArrayList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.media.MediaMetadataRetriever
import android.os.CountDownTimer
import android.os.Handler
import android.os.Message
import android.support.constraint.ConstraintLayout
import android.support.constraint.Guideline
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.text.format.DateUtils
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.WindowManager
import android.widget.*
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.common.customclasses.StartSnapHelper
import com.gm.audioapp.ui.activities.BaseActivity.Companion.currentFragment
import com.gm.audioapp.ui.activities.fragments.RadioPresetsFragment
import com.gm.audioapp.ui.customclasses.MediaStringMatcher
import com.gm.audioapp.viewmodels.EventProcessor
import com.gm.audioapp.viewmodels.UIUpdateListener
import com.gm.audioapp.viewmodels.Utility
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.SystemListener
import com.gm.media.models.*
import com.gm.media.utils.Log
import gm.media.enums.EnumBrowseChildType
import gm.media.interfaces.IBrowseItem
import gm.media.utilities.CConstants
import java.text.DecimalFormat
import com.gm.audioapp.R
/**
 *
 * Created by Aswin on 3/16/2018.
 */

private val TAG = "BindingAdapter"

@BindingAdapter("bind:items", "bind:childLayout")
        /**
         * It display a list of radio station.It is used to binding the list to the corresponding adapter.
         * @param view recyclerView to display the views of radio station list.
         * @param items the List of  this class.
         * @param childLayout the childLayout of this class.
         *
         */
fun bindList(view: RecyclerView, items: ObservableArrayList<*>, childLayout: Int) {
    Log.d(TAG, "bindList...$items,$childLayout")
    val layoutManager = LinearLayoutManager(view.context, OrientationHelper.VERTICAL, false)
    view.layoutManager = layoutManager
    when (childLayout) {
        R.layout.uil_audio_row_list_am_carousel_list_view, R.layout.uil_audio_row_list_usb_carousel_list_view -> {
            isPaddingAlreadySet = false

            Utility.getNowPlayingActivity().carouselRecyclerViewAdapter = RecyclerViewAdapter(view, items, childLayout)
            view.adapter = Utility.getNowPlayingActivity().carouselRecyclerViewAdapter

            val startSnapHelper = StartSnapHelper()
            view.onFlingListener = null
            startSnapHelper.attachToRecyclerView(view)
            if (childLayout == R.layout.uil_audio_row_list_am_carousel_list_view) { //if open for am/fm main list
                var frequency: String? = ""
                when (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE) {
                    NOWPLAYING_SOURCE_TYPE.AM ->
                        if (DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get() != null)
                            frequency = DataPoolDataHandler.AMTUNER_CURRENTSTATIONINFO_FREQUENCY.get()!!.toFloat().toString()
                    else ->
                        if (DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get() != null)
                            frequency = DataPoolDataHandler.FMTUNER_CURRENTSTATIONINFO_FREQUENCY.get().toString()
                }

                val filterItem = items.filter { it -> (it as AMFMStationInfo_t).frequency.toString() == frequency }
                if (filterItem.isNotEmpty()) {
                    val index = items.indexOf(filterItem[0])
                    layoutManager.scrollToPositionWithOffset(index, 0)
                    Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateVisibleItem(index, true, true)
                }
            } //if close for am/fm main list

            if (childLayout == R.layout.uil_audio_row_list_usb_carousel_list_view && items.size > 0) { //if open for usb main list
                val title: String = DataPoolDataHandler.MEDIA_NOWPLAYING_MEDIAOBJ_FILENAME.get().toString()
                val filterItem = items.filter { it -> (it as MediaObject_t).fileName == title }
                val index = items.indexOf(filterItem[0])
                layoutManager.scrollToPositionWithOffset(index, 0)
                Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateVisibleItem(index, true, true)
            } //if close for usb main list

            view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                var isScrollStareDragging = false
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (!isPaddingAlreadySet)
                        setRecyclerViewPadding(layoutManager, recyclerView)
                    if (isScrollStareDragging)
                        recyclerView.post { updateCarouselView(layoutManager, false) }

                    val firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition()
                    if (firstVisiblePosition == 0) {
                        isScrollStareDragging = false
                        Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)
                    }
                    Log.d("Carousal View", "onScrolled::")
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)

                    if (newState == RecyclerView.SCROLL_STATE_IDLE || newState == RecyclerView.SCROLL_STATE_SETTLING) {
                        isScrollStareDragging = false
                        recyclerView.post { updateCarouselView(layoutManager, true) }
                    }

                    if (newState == RecyclerView.SCROLL_STATE_DRAGGING)
                        isScrollStareDragging = true

                    Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateIsScrolling(isScrollStareDragging)
                }
            })
        }
        else -> {
            val adapter = RecyclerViewAdapter(view, items, childLayout)
            view.adapter = adapter
            GMAudioApp.appContext.drawerAdapter = adapter
        }
    }
}

@BindingAdapter("bind:items", "bind:childLayout", "bind:progress")
fun bindList(view: RecyclerView, items: ObservableArrayList<*>, childLayout: Int, progress: Int) {
    Log.d(TAG, "bindList...$items,$childLayout,$progress")
    val layoutManager = LinearLayoutManager(view.context)
    if (null != currentFragment && (currentFragment !is RadioPresetsFragment))
        view.layoutManager = layoutManager

    if (Utility.getNowPlayingActivity().browseRecyclerViewAdapter != null && Utility.getNowPlayingActivity().browseRecyclerViewAdapter!!.itemCount > 0)
        Utility.getNowPlayingActivity().browseRecyclerViewAdapter!!.updateData(items)
    else {
        view.layoutManager = layoutManager
        Utility.getNowPlayingActivity().browseRecyclerViewAdapter = RecyclerViewAdapter(view, items, childLayout)
        Utility.getNowPlayingActivity().browseRecyclerViewAdapter?.setPresetListView(true)
    }

    if (view.adapter == null || view.adapter!!.itemCount != Utility.getNowPlayingActivity().browseRecyclerViewAdapter!!.itemCount)
        view.adapter = Utility.getNowPlayingActivity().browseRecyclerViewAdapter!!

}

/**
 * It update a list of radio station.
 * @param layoutManager passing layoutManager.
 */
fun updateCarouselView(layoutManager: LinearLayoutManager, isScrollStopped: Boolean) {
    Log.d(TAG, "updateCarouselView...$isScrollStopped")
    val position = layoutManager.findFirstCompletelyVisibleItemPosition()
    if (position != -1)
        Utility.getNowPlayingActivity().carouselRecyclerViewAdapter?.updateVisibleItem(position, false, isScrollStopped)

}

var isPaddingAlreadySet = false

/**
 * To set padding to the recyclerview
 * @param layoutManager passing layoutmanager.
 * @param recyclerView set the list and display the list
 */
fun setRecyclerViewPadding(layoutManager: LinearLayoutManager, recyclerView: RecyclerView) {
    Log.d(TAG, " setRecyclerViewPadding... $recyclerView")
    val firstVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition()
    if (firstVisiblePosition != -1) {
        val rvRect = Rect()
        recyclerView.getGlobalVisibleRect(rvRect)
        val view = layoutManager.findViewByPosition(firstVisiblePosition)
        if (null == view) {
            Log.d(TAG, "setRecyclerViewPadding...$view")
            return
        }
        val height = view.measuredHeight
        val totalHeight: Int
        isPaddingAlreadySet = true
        val dis = DisplayMetrics()
        totalHeight = when {
            dis.heightPixels > 1000 -> rvRect.bottom - GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.ics_audio_am_station_list_CardView12_height) - height
            else -> rvRect.bottom - GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.ics_audio_am_station_list_CardView12_height1) - height / 2
        }
        recyclerView.setPadding(0, 0, 0, totalHeight)
    }
}

@BindingAdapter("bind:setTPStatusSrc")
fun setTPStatusSrc(imageButton: ImageButton, tpsStatus: Int) {
    Log.d(TAG, " setTPStatusSrc... $tpsStatus")
    if (null == NONUIVariables.getInstance().aMFMStationInfo_t?.tpStationStatus) {
        Log.d(TAG, "setTPStatusSrc..." + NONUIVariables.getInstance().aMFMStationInfo_t?.tpStationStatus)
        return
    }

    if (NONUIVariables.getInstance().aMFMStationInfo_t?.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP && tpsStatus == 1) {
        imageButton.setImageResource(R.drawable.ic_audio_tp_on)
    } else if (NONUIVariables.getInstance().aMFMStationInfo_t?.tpStationStatus == eTPStationStatus.AMFM_STATION_SUPPORTS_TP && tpsStatus == 0) {
        imageButton.setImageResource(R.drawable.ic_audio_tp_off)
    } else if (NONUIVariables.getInstance().aMFMStationInfo_t?.tpStationStatus == eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP && tpsStatus == 1) {
        imageButton.setImageResource(R.drawable.ic_audio_non_tp_on)
    } else if (NONUIVariables.getInstance().aMFMStationInfo_t?.tpStationStatus == eTPStationStatus.AMFM_STATION_DOES_NOT_SUPPORT_TP && tpsStatus == 0) {
        imageButton.setImageResource(R.drawable.ic_audio_non_tp_off)
    }

}

@BindingAdapter("bind:Visibility")
fun setVisibility(view: View, boolean: Boolean) {
    Log.d(TAG, " setVisibility... $boolean")
    when (boolean) {
        true -> view.visibility = View.VISIBLE
        else -> view.visibility = View.INVISIBLE
    }
}

@BindingAdapter("bind:showUpdateProgress")
fun setVisibilityPosition(view: View, boolean: Boolean) {
    Log.d(TAG, " setVisibilityPosition... $boolean")
    if (boolean && DataPoolDataHandler.AUDIOMANAGER_CHANGE_AUDIOSOURCE.get()?.name == NOWPLAYING_SOURCE_TYPE.AM.name)
        view.visibility = View.VISIBLE
    else
        view.visibility = View.GONE
}

@BindingAdapter("bind:color")
fun textColor(textView: TextView, color: Int) {
    Log.d(TAG, " textColor... $color")
    textView.setTextColor(color)
}

var previousBackgroundColor = 0

/**
 * To set the text color
 * @param view the TextView.
 * @param color
 */
@BindingAdapter("bind:color")
fun setBackgroundColor(view: View, color: Int) {
    Log.d(TAG, " setBackgroundColor... $color")
    val mBackgroundColorUpdater = { animator: ValueAnimator ->
        val backgroundColor = animator.animatedValue as Int
        view.setBackgroundColor(backgroundColor)
    }
    val backgroundChangeAnimTimeMs = 450
    val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(),
            previousBackgroundColor, color)
    colorAnimation.duration = backgroundChangeAnimTimeMs.toLong()
    colorAnimation.addUpdateListener(mBackgroundColorUpdater)
    colorAnimation.start()
    previousBackgroundColor = color
}

/**
 * To set the text.
 * @param textView the TextView.
 * @param aMFMStationInfo_t the AMFMStationInfo_t.
 */
@BindingAdapter("bind:text")
fun setText(textView: TextView, aMFMStationInfo_t: AMFMStationInfo_t) {
    Log.d(TAG, " setText... $aMFMStationInfo_t")
    if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.AM.name)
        textView.text = aMFMStationInfo_t.frequency.toString().split(".")[0]
    else
        textView.text = aMFMStationInfo_t.frequency.toString()
}

var previousFrequency = "0.0f"
/**
 * To set the text.
 * @param textView the TextView.
 * @param frequency the String.
 */
@BindingAdapter("bind:text")
fun setText(textView: TextView, frequency: String) {
    Log.d(TAG, " setText... $frequency")
    if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.AM.name) {
        val mAmAnimatorListener = ValueAnimator.AnimatorUpdateListener { animation ->
            textView.text = AM_FORMATTER.format(animation.animatedValue)
        }
        animateRadioChannelChange(previousFrequency.toFloat(), frequency.toFloat(), mAmAnimatorListener)
    } else {
        val mFmAnimatorListener = ValueAnimator.AnimatorUpdateListener { animation ->
            textView.text = FM_FORMATTER.format(animation.animatedValue)
        }
        animateRadioChannelChange(previousFrequency.toFloat(), frequency.toFloat(), mFmAnimatorListener)
    }
    previousFrequency = frequency
}

const val FM_CHANNEL_FORMAT = "###.#"
const val AM_CHANNEL_FORMAT = "####"
/**
 * The formatter for AM radio stations.
 */
val FM_FORMATTER = DecimalFormat(FM_CHANNEL_FORMAT)
/**
 * The formatter for FM radio stations.
 */
val AM_FORMATTER = DecimalFormat(AM_CHANNEL_FORMAT)

/**
 * To set the animateRadioChannelChange.
 * @param startValue the Float.
 * @param endValue the Float.
 * @param listener the AnimatorUpdateListener.
 */
private fun animateRadioChannelChange(startValue: Float, endValue: Float,
                                      listener: ValueAnimator.AnimatorUpdateListener) {
    Log.d(TAG, " animateRadioChannelChange... $startValue,$endValue,$listener")
    val channelChangeDurationMs = 200
    val animator = ValueAnimator()
    animator.setObjectValues(startValue, endValue)
    animator.duration = channelChangeDurationMs.toLong()
    animator.addUpdateListener(listener)
    animator.start()
}

@BindingAdapter("bind:width")
fun setWidth(view: View, dummy: Int) {
    Log.d(TAG, " setWidth... $dummy")
    if (DataPoolDataHandler.isSkewEnabled.get()!!) {
        val params = view.layoutParams
        val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val displayMetrics = DisplayMetrics()
        display.getMetrics(displayMetrics)

        val vto = view.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {

            override fun onGlobalLayout() {

                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val height = view.measuredHeight
                val mWidth = view.measuredWidth
                val width = (height * 0.275).toInt()
                params.width = mWidth - width
                if (view.id == R.id.controlButtonsLayout)
                    Utility.getNowPlayingActivity().skewNowPlayingWidth = params.width
                view.layoutParams = params

            }
        })
    }
}


@BindingAdapter("bind:textWidth")
fun setWidth(view: TextView, dummy: Int) {
    Log.d(TAG, " setWidth... $dummy")
    if (DataPoolDataHandler.isSkewEnabled.get()!!) {
        val params = view.layoutParams
        view.measure(0, 0)
        params.width = view.measuredWidth + 40
    } else {
        val params = view.layoutParams
        view.measure(0, 0)
        params.width = view.measuredWidth
    }
}


@BindingAdapter("bind:marginStart")
fun setMarginStart(view: TextView, dummy: Int) {
    Log.d(TAG, " setMarginStart... $dummy")
    var leftMargin: Int = GMAudioApp.appContext.resources.getDimension(R.dimen.bcd).toInt()
    val params = view.layoutParams as ConstraintLayout.LayoutParams
    if (DataPoolDataHandler.isSkewEnabled.get()!!)
        leftMargin = GMAudioApp.appContext.resources.getDimension(R.dimen.abc).toInt()

    params.setMargins(leftMargin, 0, 0, 0)
    view.layoutParams = params
}


var myCountDownTimer: MyCountDownTimer? = MyCountDownTimer(1000, 1000)
var isOnFinishCalled: Boolean = false
var UP_DOWN: String = ""
/**
 * To set the setTouchListener.
 * @param self the ImageButton.
 * @param textAbove the String.
 */
@SuppressLint("ClickableViewAccessibility")
@BindingAdapter("app:onClickORonLongPress")
fun setTouchListener(self: ImageButton, textAbove: String) {
    Log.d(TAG, " setTouchListener... $textAbove")
    self.setOnTouchListener(View.OnTouchListener { v, motionEvent ->
        if (NONUIVariables.getInstance().audioControlDisabled) {
            showQuickNotice(GMAudioApp.appContext.resources.getString(R.string.audio_action_not_supported))
            return@OnTouchListener true
        }
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                self.isPressed = true
                when (v.id) {
                    R.id.img_audio_timeshift_leftseek ->
                        UP_DOWN = "DOWN"
                    R.id.img_audio_timeshift_rightseek ->
                        UP_DOWN = "UP"
                }
                UP_DOWN = textAbove
                myCountDownTimer!!.start()
            }
            MotionEvent.ACTION_UP -> {
                self.isPressed = false
                myCountDownTimer!!.cancel()
                if (isOnFinishCalled)
                    updateHardSeekStation()
                else {
                    val eNumType: eAMFMSeekType = when (UP_DOWN) {
                        "UP" -> eAMFMSeekType.AMFM_SEEK_TYPE_SEEKUP
                        else -> eAMFMSeekType.AMFM_SEEK_TYPE_SEEKDOWN
                    }
                    if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.AM.name)
                        EventProcessor.triggerEvent(null, "process", EventObject("onAMFM_REQ_AMSEEKSTATION", eNumType), "NowPlayingActivity", true)
                    else if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.FM.name)
                        EventProcessor.triggerEvent(null, "process", EventObject("onAMFM_REQ_FMSEEKSTATION", eNumType), "NowPlayingActivity", true)
                } //else close
            }
        }
        true
    })
}

/**
 * This class have MyCountDownTimer information.
 * @param millisInFuture the Long.
 * @param countDownInterval the Long.
 */
//our own countdown timer class
class MyCountDownTimer(millisInFuture: Long, countDownInterval: Long) : CountDownTimer(millisInFuture, countDownInterval) {

    override fun onTick(millisUntilFinished: Long) {
    }

    override fun onFinish() {
        updateHardSeekStation()
        isOnFinishCalled = true
        myCountDownTimer!!.start()
    }
}

/**
 * To update eHardSeekStation.
 */
//common method for update hard seek station
fun updateHardSeekStation() {
    Log.d(TAG, " updateHardSeekStation...")
    val eNumType: eAMFMSeekType = when (UP_DOWN) {
        "UP" -> eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKUP
        else -> eAMFMSeekType.AMFM_SEEK_TYPE_FASTSEEKDOWN
    }
    if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.AM.name)
        EventProcessor.triggerEvent(null, "process", EventObject("onAMFM_REQ_AMHARDSEEKSTATION", eNumType), "NowPlayingActivity", true)
    else if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE.name == NOWPLAYING_SOURCE_TYPE.FM.name)
        EventProcessor.triggerEvent(null, "process", EventObject("onAMFM_REQ_FMHARDSEEKSTATION", eNumType), "NowPlayingActivity", true)
}

/**
 * To show QuickNotice.
 * @param message passing message.
 */
fun showQuickNotice(message: String) {
    Toast.makeText(GMAudioApp.appContext, message, Toast.LENGTH_SHORT).show()
}

@BindingAdapter("bind:marginStart", "bind:marginEnd", "bind:marginTop")
fun setInfoCurvedMargins(view: View, marginStart: Int, marginEnd: Int, marginTop: Int) {
    Log.d(TAG, " setInfoCurvedMargins...", marginStart.toString() + "----" + marginEnd)
    val layoutParams = view.layoutParams as ViewGroup.MarginLayoutParams
    layoutParams.marginStart = marginStart
    layoutParams.marginEnd = marginEnd
    layoutParams.topMargin = marginTop
    view.layoutParams = layoutParams
}

/**
 * To set the setTouchListener.
 * @param touchView the ImageView.
 * @param value the Boolean.
 */
@BindingAdapter("touchListener")
fun setTouchListener(touchView: View, value: Boolean) {
    Log.d(TAG, "setTouchListener ----$touchView,$value")
    // SetTouchListener for StationList Progress Cancel.
    // Onclick was getting called inappropriately, hence handling the cancel functionality through touch
    touchView.setOnTouchListener { view, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                val currentFragment = GMAudioApp.appContext.activityContext.supportFragmentManager.findFragmentById(GMAudioApp.appContext.activityContext.getContainerId())
                if (currentFragment is UIUpdateListener)
                    currentFragment.onUIUpdateListener(view)
            }
            MotionEvent.ACTION_UP -> {
                // Do nothing here. Just consume the event
            }
        }
        true
    }
}

//To set Manual tune height percentage based on orientation.
@BindingAdapter("bind:guideline_percentage")
fun setGuideLinePercentage(view: View, dummy: Int) {
    Log.d(TAG, " setGuideLinePercentage...bind:guideline_percentage...$dummy")
    val params = (view as Guideline).layoutParams as ConstraintLayout.LayoutParams
    val orientation = GMAudioApp.appContext.activityContext.resources.configuration.orientation
    if (orientation == Configuration.ORIENTATION_PORTRAIT)
        params.guidePercent = 0.5f
    else
        params.guidePercent = 0.2f
    view.layoutParams = params
}

@BindingAdapter("bind:slopedAngle")
fun setSloppedAngle(view: View, angle: Float) {
    Log.d(TAG, " setSloppedAngle...bind:slopedAngle...$angle")
    if (DataPoolDataHandler.isSkewEnabled.get()!!) {
        when (view) {
            is com.gm.audioapp.common.customclasses.SkewLinearLayout -> view.setAngle(angle)
            is com.gm.audioapp.common.customclasses.SkewRelativeLayout -> view.setAngle(angle)
            is com.gm.audioapp.common.customclasses.SkewConstraintLayout -> view.setAngle(angle)
            is com.gm.audioapp.common.customclasses.SkewTextView -> view.setAngle(angle)
            is com.gm.audioapp.common.customclasses.SkewMaterialButton -> view.setAngle(angle)
        }
    }
}

// USB browse - END

@BindingAdapter("bind:text")
fun setText(textView: TextView, any: MediaObject_t) {
    Log.d(TAG, " setText...bind:text...$any")
    textView.text = any.fileName
}

@BindingAdapter("bind:itemTimestamp")
fun setELapsedRemainingTime(textView: TextView, any: Any) {

    Log.d(TAG, " setELapsedRemainingTime...bind:itemTimestamp...$any")
    if (textView.id == R.id.duration)
        textView.text = "-" + getTimeString(any as Int)
    else
        textView.text = getTimeString(any as Int)
}

fun getTimeString(millis: Int): String {
    val strBuilder = StringBuilder()
    val songPlayTime = StringBuffer(DateUtils.formatElapsedTime(strBuilder, millis.toLong()))
    if (songPlayTime.isNotEmpty()) {
        if (songPlayTime[0] == '0') {
            songPlayTime.deleteCharAt(0)
        }
    }
    return converToNumberFormate(songPlayTime.toString())
}

fun converToNumberFormate(input: String): String {
    var value = ""
    try{
        for (character in input.toCharArray()) {
            var str = ""
            val ascii = character.toInt()
            str = if (ascii in 1632..1641) {
                //arabic number
                val valueOld = ascii - 1584
                val valueChar = valueOld.toChar()
                valueChar.toString()
            } else {
                //default
                character.toString()
            }
            value += str
        }
    }
    catch (e:Exception){
        e.printStackTrace()
    }
    if(TextUtils.isEmpty(value))
        value = input

    return value
}

const val FAST_FORWARD_REWIND_START_TIME = 1000
const val FAST_FORWARD = 10
const val FAST_REWIND = 11
var isMediaFastSpeedEnabled = false
var isLongPressed = false

@SuppressLint("ClickableViewAccessibility")
@BindingAdapter("app:longPress")
fun setMediaTouchListener(nextPrevious: ImageButton, textAbove: eMediaPBAction) {
    Log.d(TAG, " setMediaTouchListener...bind:longPress...$textAbove")
    nextPrevious.setOnTouchListener(View.OnTouchListener { v, motionEvent ->
        when (motionEvent.action) {
            MotionEvent.ACTION_DOWN -> {
                if (!DataPoolDataHandler.MEDIA_DEVICE_MOUNTED_STATUS.get()!!) {
                    showQuickNotice(GMAudioApp.appContext.resources.getString(R.string.action_unavailable))
                    return@OnTouchListener true
                }
                isLongPressed = true
                v.isPressed = true
                val id = v.id
                when (id) {
                    R.id.img_usb_audio_timeshift_rightseek ->
                        MediaForwardRewindHandler.sendEmptyMessageDelayed(FAST_FORWARD, FAST_FORWARD_REWIND_START_TIME.toLong())

                    R.id.img_usb_audio_timeshift_leftseek ->
                        MediaForwardRewindHandler.sendEmptyMessageDelayed(FAST_REWIND, FAST_FORWARD_REWIND_START_TIME.toLong())

                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                MediaForwardRewindHandler.removeMessages(FAST_FORWARD)
                MediaForwardRewindHandler.removeMessages(FAST_REWIND)
                isLongPressed = false
                v.isPressed = false
                when {
                    isMediaFastSpeedEnabled -> {
                        NONUIVariables.getInstance().isFastBackEnabled = false
                        EventProcessor.triggerEvent(null, "process", EventObject("onMEDIA_REQ_REQUESTPLAYBACKACTION", eMediaPBAction.MEDIA_PBA_FAST_PREV_STOP), "NowPlayingActivity", true)
                        isMediaFastSpeedEnabled = false
                    }
                    textAbove == eMediaPBAction.MEDIA_PBA_NEXT -> EventProcessor.triggerEvent(null, "process", EventObject("onMEDIA_REQ_REQUESTPLAYBACKACTION", eMediaPBAction.MEDIA_PBA_NEXT), "NowPlayingActivity", true)
                    textAbove == eMediaPBAction.MEDIA_PBA_PREV -> EventProcessor.triggerEvent(null, "process", EventObject("onMEDIA_REQ_REQUESTPLAYBACKACTION", eMediaPBAction.MEDIA_PBA_PREV), "NowPlayingActivity", true)
                }
            }
        }
        true
    })
}

/**
 * Handler for handling the change of station on press and hold
 **/
object MediaForwardRewindHandler : Handler() {
    override fun handleMessage(msg: Message) {
        if (!AudioService.isSDKAvailable())
            isMediaFastSpeedEnabled = false

        performHandlerOperations(msg)
    }
}

private fun performHandlerOperations(msg: Message) {
    Log.d(TAG, " performHandlerOperations...$msg")
    if (!isLongPressed || isMediaFastSpeedEnabled) {
        Log.d(TAG, " performHandlerOperations...$isLongPressed,$isMediaFastSpeedEnabled")
        return
    }
    if (msg.what == FAST_FORWARD)
        performFastForwardRewind(false, eMediaPBAction.MEDIA_PBA_FASTFORWARD, FAST_FORWARD)
    else if (msg.what == FAST_REWIND)
        performFastForwardRewind(true, eMediaPBAction.MEDIA_PBA_FASTBACKWARD, FAST_REWIND)

}

//common method for FAST_FORWARD,FAST_REWIND
private fun performFastForwardRewind(isFastBackEnabled: Boolean, eMediaPBAction: eMediaPBAction, what: Int) {
    isMediaFastSpeedEnabled = true
    NONUIVariables.getInstance().isFastBackEnabled = isFastBackEnabled
    EventProcessor.triggerEvent(null, "process", EventObject("onMEDIA_REQ_REQUESTPLAYBACKACTION", eMediaPBAction), "NowPlayingActivity", true)
    if (!AudioService.isSDKAvailable())
        MediaForwardRewindHandler.sendEmptyMessageDelayed(what, FAST_FORWARD_REWIND_START_TIME.toLong())
}


@BindingAdapter("bind:browseItem")
fun setAlbumArt(imageView: ImageView, browseItem: IBrowseItem?) {
    Log.d(TAG, " seekBarTouch...bind:browseItem...$browseItem")
    try{
        if (browseItem == null) {
            Log.d(TAG, " seekBarTouch...browseItem...$browseItem")
            return
        }
        if (Utility.getNowPlayingActivity().mediaUsbBrowseType.compareTo(MediaStringMatcher.getStringBYId(R.string.audio_albums_small)) == 0
                || Utility.getNowPlayingActivity().mediaUsbBrowseType.compareTo(MediaStringMatcher.getStringBYId(R.string.audio_folder_small)) == 0) {
            val bmp = browseItem.albumArtImage
            if (bmp != null) {
                imageView.setImageBitmap(bmp)
                imageView.visibility = View.VISIBLE
            }
        }
        else{
            imageView.visibility = View.INVISIBLE
        }
    }
    catch (e:Exception){
        e.printStackTrace()
    }
}


@BindingAdapter("android:seekBarUpdate")
fun seekBarTouch(seekBar: SeekBar, elapsedTime: Int) {
    Log.d(TAG, " seekBarTouch...android:seekBarUpdate...$elapsedTime")
    seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
        override fun onStopTrackingTouch(p0: SeekBar?) {
            NONUIVariables.getInstance().isStartTouch = false
            EventProcessor.triggerEvent(null, "process", EventObject("onMEDIA_REQ_SEEKTO", p0!!.progress), "NowPlayingActivity", true)
        }

        override fun onStartTrackingTouch(p0: SeekBar?) {
            NONUIVariables.getInstance().isStartTouch = true
        }

        override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
            if (NONUIVariables.getInstance().isStartTouch)
                EventProcessor.triggerEvent(null, "process", EventObject("onMEDIA_REQ_SEEKTO", p1), "NowPlayingActivity", true)
        }
    })
}

@BindingAdapter("bind:displayText")
fun setMediaDisplayText(textView: TextView, displayText: String?) {
    Log.d(TAG, " setMediaDisplayText...bind:displayText...$displayText")
    if (displayText == null) {
        Log.d(TAG, " setMediaDisplayText...displayText...$displayText")
        return
    }
    textView.text = MediaStringMatcher.getMediaNameByConstant(displayText)
}


@BindingAdapter("bind:subTitle")
fun setMediaSubTitle(textView: TextView, browseItem: IBrowseItem?) {
    Log.d(TAG, " setMediaSubTitle...bind:subTitle...$browseItem")
    if (browseItem == null) {
        Log.d(TAG, " setMediaSubTitle...browseItem...$browseItem")
        return
    }
    textView.visibility = View.GONE
    if ((DataPoolDataHandler.MEDIA_HEADER_OBJECT.get()?.firstLineHeaderSt?.trim().equals(MediaStringMatcher.getStringBYId(R.string.audio_albums_small)) || DataPoolDataHandler.MEDIA_HEADER_OBJECT.get()?.secondLineHeaderSt.equals(MediaStringMatcher.getStringBYId(R.string.audio_albums_small))) && browseItem.displayText != CConstants.Browse_All_Songs_Token) {
        textView.text = browseItem.itemArtist
        textView.visibility = View.VISIBLE
    } else if (DataPoolDataHandler.MEDIA_HEADER_OBJECT.get()?.firstLineHeaderSt?.trim().equals(MediaStringMatcher.getStringBYId(R.string.audio_songs_small))) {
        textView.text = browseItem.albumName + "/" + browseItem.itemArtist
        textView.visibility = View.VISIBLE
    }
}

@BindingAdapter("bind:rowTag")
fun setRowTag(linearLayout: View, browseItem: IBrowseItem?) {
    Log.d(TAG, " setRowTag...bind:rowTag...$browseItem")
    if (browseItem == null) {
        Log.d(TAG, " setRowTag...browseItem...$browseItem")
        return
    }
    if (browseItem.playable)
        linearLayout.tag = "eUsbSongsBrowseProcess"
    else
        linearLayout.tag = "eUsbChildBrowseProcess"

}

@BindingAdapter("bind:mediaFavorite")
fun setMediaFav(imageView: ImageView, browseItem: IBrowseItem?) {
    Log.d(TAG, " setMediaFav...bind:mediaFavorite...$browseItem")
    if (browseItem == null) {
        Log.d(TAG, " setMediaFav...browseItem... $browseItem")
        return
    }
    imageView.setImageResource(R.drawable.ic_star_inactive)
    val mCurrentBrowseType = SystemListener.getMediaBrowseType()
    val displayedText = browseItem.displayText
    if (DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST.isNotEmpty())
        if (DataPoolDataHandler.MEDIA_FAVOURITES_SONGS_LIST.contains(browseItem.objectID.toString()))
            imageView.setImageResource(R.drawable.ic_favourite_star_active)

    imageView.visibility = View.INVISIBLE
    if (NONUIVariables.getInstance().mActiveBrowseAddresses.size > 0) {
        val enabled = browseItem.hasChildren || browseItem.playable
        if (enabled && CConstants.Browse_All_Songs_Token != displayedText)
            if (mCurrentBrowseType != EnumBrowseChildType.FolderInfo)
                imageView.visibility = View.VISIBLE
    }
}

@BindingAdapter("bind:src")
fun setAlbumArt(imageView: ImageView, any: Any) {
    Log.d(TAG, " setAlbumArt...bind:src...$any")
    when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) {
        USB_BROWSE_PAGE.ARTIST_ALBUM, USB_BROWSE_PAGE.ALBUM, USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM -> {
            if (any is MediaObject_t && !TextUtils.isEmpty(any.metaDataSong)) {
                imageView.visibility = View.VISIBLE
                val metaRetriever = MediaMetadataRetriever()
                metaRetriever.setDataSource(any.metaDataSong)
                try {
                    val art = metaRetriever.embeddedPicture
                    if(null!=art){
                        val bitmap = BitmapFactory.decodeByteArray(art, 0, art.size)
                        imageView.setImageBitmap(bitmap)
                    }
                    else{
                        imageView.setImageResource(R.drawable.ic_place_holder)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                metaRetriever.release()
            } else if (any is Bitmap && !any.sameAs(DataPoolDataHandler.MEDIA_NOWPLAYING_EMPTY_BITMAP_IMAGE.get())) {
                imageView.setImageBitmap(any)
                imageView.visibility = View.VISIBLE
            } else {
                imageView.setImageResource(R.drawable.ic_place_holder)
            }
        }
        else ->
            imageView.visibility = View.INVISIBLE
    }
}

// USB browse - START

@BindingAdapter("bind:browseText")
fun setBrowseText(textView: TextView, any: MediaObject_t) {
    Log.d(TAG, " setBrowseText...bind:browseText...$any")
    val title: String

    when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) {
        USB_BROWSE_PAGE.CATEGORY ->
            title = any.fileName
        USB_BROWSE_PAGE.ARTIST ->
            title = any.metaDataArtist
        USB_BROWSE_PAGE.ARTIST_ALBUM, USB_BROWSE_PAGE.COMPILATIONS ->
            title = any.metaDataAlbum
        USB_BROWSE_PAGE.ALBUM, USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM, USB_BROWSE_PAGE.COMPOSER_ALBUM ->
            title = any.metaDataAlbum
        USB_BROWSE_PAGE.GENRES ->
            title = any.metaDataGenre
        USB_BROWSE_PAGE.GENRE_ARTIST ->
            title = any.metaDataArtist
        USB_BROWSE_PAGE.COMPOSERS ->
            title = any.fileName
        USB_BROWSE_PAGE.FOLDER ->
            title = any.fileName
        USB_BROWSE_PAGE.SONGS, USB_BROWSE_PAGE.ALBUM_SONGS, USB_BROWSE_PAGE.ARTIST_ALBUM_SONGS, USB_BROWSE_PAGE.COMPOSER_ALBUM_SONG,
        USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM_SONG, USB_BROWSE_PAGE.GENRE_ARTIST_SONG,
        USB_BROWSE_PAGE.GENRE_ALBUM_SONG, USB_BROWSE_PAGE.ARTIST_SONGS, USB_BROWSE_PAGE.COMPOSER_SONG,
        USB_BROWSE_PAGE.COMPILATION_ALBUM_SONGS, USB_BROWSE_PAGE.COMPILATION_SONGS, USB_BROWSE_PAGE.FOLDER_SONGS ->
            title = any.fileName
        else ->
            title = ""
    }
    textView.text = title
}


@BindingAdapter("bind:browseSubTitle")
fun setBrowseSubTitle(textView: TextView, any: MediaObject_t) {
    Log.d(TAG, " setBrowseSubTitle...bind:browseSubTitle...$any")
    var subTitle = ""
    if (any.metaDataAlbum == NONUIVariables.getInstance().allSongs_st)
        textView.visibility = View.GONE
    else {
        when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) {
            USB_BROWSE_PAGE.CATEGORY, USB_BROWSE_PAGE.SONGS, USB_BROWSE_PAGE.ALBUM_SONGS, USB_BROWSE_PAGE.ARTIST_ALBUM_SONGS -> {
                textView.visibility = View.VISIBLE
                if (!TextUtils.isEmpty(any.metaDataAlbum))
                    subTitle = any.metaDataAlbum + "/" + any.metaDataArtist
                textView.text = subTitle
            }
            USB_BROWSE_PAGE.ALBUM, USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM, USB_BROWSE_PAGE.COMPILATIONS, USB_BROWSE_PAGE.COMPOSER_ALBUM -> {
                textView.visibility = View.VISIBLE
                textView.text = any.metaDataArtist
            }
            else -> {
            }
        }
    }
}

@BindingAdapter("bind:npIconVisibility")
fun setBrowseNPIcon(imageView: ImageView, any: Any) {
    Log.d(TAG, " setBrowseNPIcon...bind:npIconVisibility...$any")
    var isNPVisible = false

    if (any is MediaObject_t)
        when (NONUIVariables.getInstance().USB_BROWSE_PAGENAME) {
            USB_BROWSE_PAGE.GENRES ->
                if (NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!.metaDataGenre == any.metaDataGenre) isNPVisible = true
            USB_BROWSE_PAGE.ARTIST, USB_BROWSE_PAGE.GENRE_ARTIST ->
                if (NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!.metaDataArtist == any.metaDataArtist) isNPVisible = true
            USB_BROWSE_PAGE.ARTIST_ALBUM, USB_BROWSE_PAGE.ALBUM, USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM, USB_BROWSE_PAGE.COMPOSER_ALBUM, USB_BROWSE_PAGE.COMPILATIONS ->
                if (NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!.metaDataAlbum == any.metaDataAlbum) isNPVisible = true
            USB_BROWSE_PAGE.SONGS, USB_BROWSE_PAGE.ALBUM_SONGS, USB_BROWSE_PAGE.ARTIST_ALBUM_SONGS, USB_BROWSE_PAGE.COMPOSER_SONG, USB_BROWSE_PAGE.COMPOSER_ALBUM_SONG, USB_BROWSE_PAGE.GENRE_ALBUM_SONG,
            USB_BROWSE_PAGE.GENRE_ARTIST_SONG, USB_BROWSE_PAGE.COMPILATION_SONGS, USB_BROWSE_PAGE.COMPILATION_ALBUM_SONGS, USB_BROWSE_PAGE.ARTIST_SONGS, USB_BROWSE_PAGE.GENRE_ARTIST_ALBUM_SONG ->
                if (NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO!!.fileName == any.fileName) isNPVisible = true
            else ->
                isNPVisible = false
        }
    imageView.visibility = if (isNPVisible) View.VISIBLE else View.GONE
}
