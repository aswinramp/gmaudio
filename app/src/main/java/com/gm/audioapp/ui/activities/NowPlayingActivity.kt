package com.gm.audioapp.ui.activities

import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.ui.activities.fragments.NowPlayingFragment
import com.gm.audioapp.ui.activities.fragments.UsbNowPlayingFragment
import com.gm.audioapp.ui.adapters.*
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.DataPoolDataHandler.isInfoCurvedView
import com.gm.media.models.NONUIVariables
import com.gm.media.models.NOWPLAYING_SOURCE_TYPE
import com.gm.media.utils.AudioConstants

/**
 *  A platform for all sub screens (Fragments).
 * @author YZPNMQ on 3/7/2018.
 */
class NowPlayingActivity : BaseActivity() {

    private val evgRadioInit = "INIT_AMFM_REQUEST"
    /**
     *  It holds the instance of RecyclerViewAdapter
     */
    var browseRecyclerViewAdapter: RecyclerViewAdapter? = null
    /**
     *  It holds the instance of CarouselRecyclerViewAdapter
     */
    var carouselRecyclerViewAdapter: RecyclerViewAdapter? = null
    /**
     *  It holds the instance of GmUsbBrowseAdapter
     */
    lateinit var gmUsbBrowseAdapter: GmUsbBrowseAdapter

    /**
     *  It holds the string object
     */
    var mediaUsbBrowseType: String = ""

    /**
     *  It holds the width of the skew
     */
    var skewNowPlayingWidth: Int = 0

    /**
     * Whether or not it is safe to make transactions on the
     * [FragmentManager]. This variable prevents a possible exception
     * when calling commit() on the FragmentManager.
     *
     *
     * The default value is `true` because it is only after
     * [.onSaveInstanceState] has been called that fragment commits are not allowed.
     */
    private var mAllowFragmentCommits = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        GMAudioApp.appContext.themeChange = this

        setLargeScreenView()

        if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE == NOWPLAYING_SOURCE_TYPE.USBMSD)
            setContentFragment(UsbNowPlayingFragment(), true)
        else
            setContentFragment(NowPlayingFragment(), false)
    }

    // Add views to parent to set navigation drawer as slide bar
    private fun setLargeScreenView() {

        isLargeScreen()

        val child = this.findViewById<View>(R.id.ll_side_bar)
        val parent = this.findViewById<View>(this.contentContainerId) as ViewGroup
        if (DataPoolDataHandler.isLargeScreenLayout.get()!!) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setHomeButtonEnabled(false)
            mToolbar.navigationIcon = null

            when (DataPoolDataHandler.isSkewEnabled.get()!!) {
                true ->
                    // Add's layout to the parent
                    setMainContent(R.layout.uil_audio_nowplaying_layout_content_frame_large_with_skew)
                else ->
                    // Add's layout to the parent
                    setMainContent(R.layout.uil_audio_nowplaying_layout_content_frame_large_without_skew)
            }
            val recyclerView = this.findViewById<RecyclerView>(R.id.recycler_view)
            val childView = this.findViewById<View>(R.id.ll_side_bar)
            bindList(recyclerView, DataPoolDataHandler.mDrawerOptions, R.layout.uil_audio_nowplaying_layout_content_frame_large_list_item)
            setWidth(recyclerView, 0)
            if (isInfoCurvedView.get()!!)
                setInfoCurvedMargins(childView, resources.getInteger(R.integer.info35c_curvedscreen_marginstart)
                        , resources.getInteger(R.integer.info35c_curvedscreen_marginend),
                        resources.getInteger(R.integer.info35c_curvedscreen_margintop))
        } else {
            if (child != null) parent.removeView(child)
        }
    }


    /**
     * replace a NowPlayingFragment to this class.
     */
    private fun setContentFragment(fragment: Fragment, isAddBackStack: Boolean) {
        if (!mAllowFragmentCommits)
            return


        for (oldFragment in supportFragmentManager.fragments) {
            if (oldFragment != null)
                supportFragmentManager.beginTransaction().remove(oldFragment).commit()
        }

        if (isAddBackStack)
            supportFragmentManager.beginTransaction()
                    .replace(getContainerId(), fragment, AudioConstants.CONTENT_FRAGMENT_TAG)
                    .addToBackStack(null)
                    .commit()
        else
            supportFragmentManager.beginTransaction()
                    .replace(getContainerId(), fragment, AudioConstants.CONTENT_FRAGMENT_TAG)
                    .commit()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0)
        // A station can only be selected if the manual tuner fragment has been shown; so, remove
        // that here.
            supportFragmentManager.popBackStack()
        else
            super.onBackPressed()

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        mAllowFragmentCommits = false
    }

    override fun getTheme(): Resources.Theme {
        val theme = super.getTheme()
        GMAudioApp.useCadillacTheme = DataPoolDataHandler.themeType.get()
        if (GMAudioApp.useCadillacTheme!!)
            theme.applyStyle(R.style.CadillacTheme, true)
        else
            theme.applyStyle(R.style.NonCadillacTheme, true)

        return theme
    }

    override fun onResume() {
        super.onResume()
        setBackgroundColor()
        if (DataPoolDataHandler.isLargeScreenLayout.get()!!)
        // Disabling navigation drawer when XLarge screen
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        else
        // Disabling navigation drawer when XLarge screen
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)

    }

    override fun onStart() {
        try{
            super.onStart()
            // Fragment commits are not allowed once the Activity's state has been saved. Once
            // onStart() has been called, the FragmentManager should now allow commits.
            mAllowFragmentCommits = true
            if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE != NOWPLAYING_SOURCE_TYPE.USBMSD)
                EventHandler.initEvent(evgRadioInit)
        }
        catch (e:Exception){
            e.printStackTrace()
        }
    }

    override fun enableNavigationDrawer(): Boolean = true

    override fun navigationDrawerTitle(): String = getString(R.string.ui_audio_small)
}
