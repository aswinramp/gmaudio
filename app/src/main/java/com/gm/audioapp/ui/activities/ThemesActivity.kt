package com.gm.audioapp.ui.activities

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.R
import com.gm.audioapp.databinding.ActivityThemeBinding
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.NONUIVariables
import com.gm.media.models.NOWPLAYING_SOURCE_TYPE

/**
 * This screen allows user to select cadillac and non cadillac themes
 * @author Aswin on 3/12/2018.
 */


class ThemesActivity : BaseActivity() {

    override fun enableNavigationDrawer(): Boolean = false

    //override fun navigationDrawerItemsWithKeyMap(): HashMap<String, String> = emptyHashMap()

    override fun navigationDrawerTitle(): String = emptyString()

    var binding: ActivityThemeBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityThemeBinding = DataBindingUtil.setContentView(this, R.layout.ics_themes)
        binding.let {

            it.clickHandler = EventHandler
            it.dataPoolHandler = DataPoolDataHandler
        }
    }

    override fun onStop() {
        super.onStop()

        //USB data display when navigate from themes activity to now playing activity
        if(GMAudioApp.appContext.activityContext is NowPlayingActivity)
        {
           // Handler().postDelayed({
                if (NONUIVariables.getInstance().AUDIOMANAGER_CHANGE_AUDIOSOURCE == NOWPLAYING_SOURCE_TYPE.USBMSD) {
                    val screenKey = GMAudioApp.appContext.sourceNameEventName[NOWPLAYING_SOURCE_TYPE.USBMSD.name]
                    EventHandler.onDrawerClick(screenKey)
                }
           // }, 400)
        }

    }

}
