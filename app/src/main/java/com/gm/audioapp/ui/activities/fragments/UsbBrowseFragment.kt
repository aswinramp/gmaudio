package com.gm.audioapp.ui.activities.fragments


import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.FrameLayout
import android.widget.ListView
import com.gm.audioapp.BR
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.common.animations.AnimationManager
import com.gm.audioapp.ui.adapters.GmUsbBrowseAdapter
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.RegisterListener
import com.gm.media.apiintegration.AudioService
import com.gm.media.models.*
import com.gm.media.utils.AudioConstants
import com.gm.audioapp.R
import com.gm.audioapp.viewmodels.Utility



/**
 * A fragment that allows to browse media item by artist,album song etc
 */
open class UsbBrowseFragment : BaseFragment(), AnimationManager.OnExitCompleteListener, RegisterListener {

    private val animDurationMs = 200
    //    private val evGBrowseInit = "onUsbBrowse"
    private val evGBrowseInit = "onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST"

    private var mRootView: View? = null

    private lateinit var mCurrentRadioCard: View
    private lateinit var mPresetsList: View
    private lateinit var mControlButtonsLayout: View
    private lateinit var mBrowseContainer: View

    private var mAnimManager: AnimationManager? = null
    private var binding: ViewDataBinding? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = if (AudioService.isSDKAvailable())
            DataBindingUtil.inflate(
                    inflater, R.layout.ics_audio_usb_browse_list_sdk, container, false)
        else
            DataBindingUtil.inflate(
                    inflater, R.layout.ics_audio_usb_browse_child_list, container, false)

        mRootView = binding!!.root

        binding!!.setVariable(BR.dataPoolHandler, DataPoolDataHandler)
        binding!!.setVariable(BR.clickHandler, EventHandler)
        binding!!.setVariable(BR.usbupdateProgress, DataPoolDataHandler.MEDIA_USB_INDEXING_PROGRESS)

        //initialize the views
        initializeViews()

        //initialize the Adapters
        initializeMediaPlayer()

        return mRootView
    }

    //initialize Views
    private fun initializeViews() {
        mPresetsList = mRootView!!.findViewById(R.id.stations_list)
        mCurrentRadioCard = mRootView!!.findViewById(R.id.current_radio_station_card)
        mBrowseContainer = mRootView!!.findViewById(R.id.usb_browser_container)
        mControlButtonsLayout = mRootView!!.findViewById(R.id.controlButtonsLayout)

        if (DataPoolDataHandler.isSkewEnabled.get()!!) {
            val params = mControlButtonsLayout.layoutParams
            params.width = Utility.getNowPlayingActivity().skewNowPlayingWidth
            mControlButtonsLayout.layoutParams = params
        }


        setPresetsListMarginTop(mBrowseContainer)
    }

    //check sdk manager and initialize adapters
    private fun initializeMediaPlayer() {

        if (AudioService.isSDKAvailable()) {

            NONUIVariables.getInstance().ISFROMUSBNOWPLAYING = false

            Utility.getNowPlayingActivity().gmUsbBrowseAdapter = GmUsbBrowseAdapter(false)
            Utility.getNowPlayingActivity().gmUsbBrowseAdapter.mListView = mPresetsList as ListView
            Utility.getNowPlayingActivity().gmUsbBrowseAdapter.mBubble = mRootView?.findViewById(R.id.mBubble)
            Utility.getNowPlayingActivity().gmUsbBrowseAdapter.setScrollListener(mPresetsList as ListView)
            (mPresetsList as ListView).adapter = Utility.getNowPlayingActivity().gmUsbBrowseAdapter
            EventHandler.initEvent(evGBrowseInit, "")

        } else {
            Utility.getNowPlayingActivity().browseRecyclerViewAdapter = null
            if (NONUIVariables.getInstance().USB_BROWSE_PAGENAME == USB_BROWSE_PAGE.CATEGORY) {
                val mediaUsbBrowseList = ArrayList<MediaObject_t>()
                NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
                resources.getStringArray(R.array.browse_items).forEach {
                    mediaUsbBrowseList.add(MediaObject_t(fileName = it, playTime = MediaPlayTime_t(0, 0)))
                }
                NONUIVariables.getInstance().MEDIA_USB_BROWSE_LIST = mediaUsbBrowseList
            }
        }

        mAnimManager = AnimationManager(context!!, mRootView!!, true)
        mAnimManager?.playEnterAnimation()
    }

    /**
     * sets margin to given PageListView     *
     * @param layout PagedListView whose margin is to be set
     */
    private fun setPresetsListMarginTop(layout: View?) {
        val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val dis = DisplayMetrics()
        display.getMetrics(dis)
        val marginTop = if (DataPoolDataHandler.isInfoCurvedView.get()!!) dis.densityDpi else dis.densityDpi / 2

        val params = FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
        )
        val mPresetFinalHeight = GMAudioApp.appContext.resources.getDimensionPixelSize(R.dimen.car_preset_item_height)
        params.setMargins(0, marginTop + mPresetFinalHeight + 20, 0, 0)
        layout?.layoutParams = params
    }

    private fun fadeOutContent() {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(mCurrentRadioCard, View.ALPHA, 0f)
        containerAlphaAnimator.duration = animDurationMs.toLong()
        containerAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                mCurrentRadioCard.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val presetListAlphaAnimator = ObjectAnimator.ofFloat(mBrowseContainer, View.ALPHA, 0f)
        presetListAlphaAnimator.duration = animDurationMs.toLong()
        presetListAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                mBrowseContainer.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(containerAlphaAnimator, presetListAlphaAnimator)
        animatorSet.start()
    }

    private fun fadeInContent() {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(mCurrentRadioCard, View.ALPHA, 1f)
        containerAlphaAnimator.duration = animDurationMs.toLong()
        containerAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                mCurrentRadioCard.visibility = View.VISIBLE
            }

            override fun onAnimationEnd(animation: Animator) {}
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val presetListAlphaAnimator = ObjectAnimator.ofFloat(mBrowseContainer, View.ALPHA, 1f)
        presetListAlphaAnimator.duration = animDurationMs.toLong()
        presetListAlphaAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                mBrowseContainer.visibility = View.GONE
            }

            override fun onAnimationEnd(animation: Animator) {}
            override fun onAnimationCancel(animation: Animator) {}
            override fun onAnimationRepeat(animation: Animator) {}
        })

        val animatorSet = AnimatorSet()
        animatorSet.playTogether(containerAlphaAnimator, presetListAlphaAnimator)
        animatorSet.start()
    }


    override fun onExitAnimationComplete() {
        if (AudioService.isSDKAvailable())
            NONUIVariables.getInstance().IS_MEDIA_BROWSE_SONGS = false
        Utility.getNowPlayingActivity().browseRecyclerViewAdapter = null
        GMAudioApp.appContext.activityContext.supportFragmentManager.popBackStack()
    }


    override fun onRegisterListener(view: View, eventName: String, any: Any?) {
        when (view.tag.toString()) {
            "onUsbRequestSource", "eUsbSongsBrowseProcess", "eUsbBrowseClose" -> closeFragment()
        }
    }

    //close the fragment
    private fun closeFragment() {
        mAnimManager!!.playExitAnimation(this)
    }

    override fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction? {
        val fragmentTransaction = GMAudioApp.appContext.activityContext.supportFragmentManager.beginTransaction()
                ?.replace(containerId, fragment, AudioConstants.CONTENT_FRAGMENT_TAG)?.addToBackStack(null)
        fragmentTransaction?.commit()
        return fragmentTransaction
    }

    override fun entryAnimation() {
        fadeInContent()
    }

    override fun exitAnimation() {
        fadeOutContent()
    }

}
