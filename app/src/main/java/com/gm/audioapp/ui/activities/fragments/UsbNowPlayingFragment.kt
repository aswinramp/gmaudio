package com.gm.audioapp.ui.activities.fragments

import android.animation.ObjectAnimator
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.view.LayoutInflater
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.databinding.UsbNowPlayingFragmentBinding
import com.gm.audioapp.ui.adapters.GmUsbBrowseAdapter
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.audioapp.viewmodels.EventProcessor
import com.gm.audioapp.viewmodels.Utility
import com.gm.media.apiintegration.AudioService
import com.gm.media.apiintegration.mock.CinemoVideoSurfaceListener
import com.gm.media.apiintegration.mock.MediaPlayerListener
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.EventObject
import com.gm.media.models.NONUIVariables
import com.gm.media.utils.AudioConstants
import gm.calibrations.GIS502_TUNERHMI
import gm.calibrations.GMCalibrationsManager
import gm.calibrations.LANGUAGEANDREGIONALIZATIONGLOBALA
import gm.calibrations.LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM
import gm.drivingmode.DrivingModeManager
import kotlinx.android.synthetic.main.ics_audio_usb_nowplaying.view.*
import kotlinx.android.synthetic.main.uil_audio_usb_carousal_list.view.*
import com.gm.audioapp.R

class UsbNowPlayingFragment : BaseFragment(), MediaPlayerListener, CinemoVideoSurfaceListener.IGestureListener {

    private val msgMakeVideoFullScreen: Int = 4
    private val sInterpolator = FastOutSlowInInterpolator()
    private val fadeOutStartDelayMs = 150
    private val fadeAnimTimeMs = 100
    //    private val evgBrowseInit = "onUsbNowPlayingList"
    private val evgBrowseInit = "onMEDIA_REQ_CREATEMEDIAPLAYERINDEXEDLIST"
    private val evgMediaInit = "onMediaRequest"
    private val fullScreenDelay = 3000L

    private var mRootView: View? = null
    private var mMainDisplay: View? = null
    //private var mUsbNowPlayingInclude: View? = null
    private var mSdkSongList: ListView? = null

    private var mCardView: View? = null
    private var mVideoView: SurfaceView? = null
    private var mVideoSurfaceListener: CinemoVideoSurfaceListener? = null

    private var mUIHandler: UIHandler? = null
    private var mIsVideoPlayingInFullScreenMode: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding: UsbNowPlayingFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.ics_audio_usb_nowplaying, container, false)
        mRootView = binding.root

        binding.let {
            it.dataPoolHandler = DataPoolDataHandler
            it.clickHandler = EventHandler
        }

        //initialize the views
        initializeViews()

        //set empty bitmap
        Utility.setEmptyBitmap()

        //initializing Media player for SDK
        initializeMediaPlayer()

        return mRootView
    }


    //initialize Views
    private fun initializeViews() {
        mMainDisplay = mRootView!!.usb_recycler_view
        mSdkSongList = mRootView!!.usb_songs_list
        mCardView = mRootView!!.controlButtonsLayout
    }

    override fun onResume() {
        super.onResume()
        mUIHandler?.removeMessages(msgMakeVideoFullScreen)
        if (AudioService.isSDKAvailable())
            enableDisableBrowseBasedOnWorkloadRestriction(NONUIVariables.getInstance().DRIVINGMODE!!)
        if (NONUIVariables.getInstance().ISVIDEOMODE && NONUIVariables.getInstance().ISVEHICLEPARKED)
            normalScreenVideo()
        else
            showMediaMetaData()

    }

    override fun onPause() {
        super.onPause()
        if (mUIHandler != null) {
            mUIHandler?.removeCallbacksAndMessages(null)
            mUIHandler = null
        }
        GMAudioApp.appContext.activityContext.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }

    override fun getFragmentTransaction(containerId: Int, fragment: Fragment): FragmentTransaction? {
        val fragmentTransaction = GMAudioApp.appContext.activityContext.supportFragmentManager.beginTransaction()
                ?.replace(containerId, fragment, AudioConstants.CONTENT_FRAGMENT_TAG)
                ?.addToBackStack(null)
        fragmentTransaction?.commit()
        return fragmentTransaction
    }

    override fun entryAnimation() {
        fadeInContent()
    }

    override fun exitAnimation() {
        fadeOutContent()
    }

    //check sdk manager and initialize media player
    private fun initializeMediaPlayer() {
        if (AudioService.isSDKAvailable()) {
            mVideoView = mRootView!!.videoView
            NONUIVariables.getInstance().DRIVINGMODE = DrivingModeManager.getCurrentDrivingMode()
            val regionId = GMCalibrationsManager(GMAudioApp.appContext).getEnumeration(LANGUAGEANDREGIONALIZATIONGLOBALA.REGIONS_CalID)
            if (regionId != LANGUAGEANDREGIONALIZATIONGLOBALA_ENUM.REGIONS.GMNA.ordinal) {
                mVideoSurfaceListener = CinemoVideoSurfaceListener(mVideoView)
                mVideoSurfaceListener?.setGestureListener(this)
            }
            mUIHandler = UIHandler(this)
            NONUIVariables.getInstance().ISFROMUSBNOWPLAYING = true
            Utility.getNowPlayingActivity().gmUsbBrowseAdapter = GmUsbBrowseAdapter(true)
            mSdkSongList?.adapter = Utility.getNowPlayingActivity().gmUsbBrowseAdapter
            EventHandler.initEvent(evgBrowseInit, "")
            EventHandler.initEvent(evgMediaInit)
        }
    }

    private fun fadeOutContent() {
        fadeOut(mMainDisplay!!)
        //fadeOut(mUsbNowPlayingInclude!!)
        fadeOut(mCardView!!)
    }

    /**
     * Fades out the given view
     *
     * @param view view to be fade out
     */
    private fun fadeOut(view: View) {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 1f, 0f)
        containerAlphaAnimator.interpolator = sInterpolator
        containerAlphaAnimator.startDelay = fadeOutStartDelayMs.toLong()
        containerAlphaAnimator.duration = fadeAnimTimeMs.toLong()
        containerAlphaAnimator.start()
    }

    private fun fadeInContent() {
        fadeIn(mMainDisplay!!)
        // fadeIn(mUsbNowPlayingInclude!!)
        fadeIn(mCardView!!)
    }

    /**
     * Fades in the given view
     *
     * @param view view to be fade in
     */
    private fun fadeIn(view: View) {
        val containerAlphaAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, 0f, 1f)
        containerAlphaAnimator.interpolator = sInterpolator
        containerAlphaAnimator.duration = fadeAnimTimeMs.toLong()
        containerAlphaAnimator.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mVideoSurfaceListener != null)
            mVideoSurfaceListener?.onDestroy()
    }

    override fun onTapDetected() {
        if (NONUIVariables.getInstance().ISVEHICLEPARKED)
            if (mIsVideoPlayingInFullScreenMode)
                normalScreenVideo()
            else
                showFullScreenVideo()
        else
            showMediaMetaData()
    }

    override fun onTriggerAction(action: String, any: Any) {
        EventProcessor.triggerEvent(null, "media", EventObject(action, any), "NowPlayingActivity", false)
    }

    fun showFullScreenVideo() {
        mVideoView?.visibility = View.VISIBLE
        NONUIVariables.getInstance().HIDEMEDIAMETADATA = true
        mIsVideoPlayingInFullScreenMode = true
        mUIHandler?.removeMessages(msgMakeVideoFullScreen)
        GMAudioApp.appContext.activityContext.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE
    }

    private fun normalScreenVideo() {
        mUIHandler?.removeMessages(msgMakeVideoFullScreen)
        mVideoView?.visibility = View.VISIBLE
        NONUIVariables.getInstance().HIDEMEDIAMETADATA = false
        mIsVideoPlayingInFullScreenMode = false
        mUIHandler?.sendEmptyMessageDelayed(msgMakeVideoFullScreen, fullScreenDelay)
        GMAudioApp.appContext.activityContext.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }

    fun showVideoBasedOnVehicleMovementState(isParkedMode: Boolean) {
        if (isParkedMode)
            normalScreenVideo()
        else
            showMediaMetaData()
    }

    fun showMediaMetaData() {
        mUIHandler?.removeMessages(msgMakeVideoFullScreen)
        mIsVideoPlayingInFullScreenMode = false
        NONUIVariables.getInstance().HIDEMEDIAMETADATA = false
        mVideoView?.visibility = View.GONE
        GMAudioApp.appContext.activityContext.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }

    fun hideVideo() {
        mUIHandler?.removeMessages(msgMakeVideoFullScreen)
        mIsVideoPlayingInFullScreenMode = false
        NONUIVariables.getInstance().HIDEMEDIAMETADATA = false
        mVideoView?.visibility = View.GONE
        GMAudioApp.appContext.activityContext.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
    }

    fun enableDisableBrowseBasedOnWorkloadRestriction(mCurrentDrivingMode: Int) {
        val calManager = GMCalibrationsManager(GMAudioApp.appContext)
        val lockOut = calManager.getInteger(GIS502_TUNERHMI.MediaBrowseLockOut_CalID)
    }

    fun enableDisableVideoBasedOnWorkloadRestriction(mCurrentDrivingMode: Int) {
        val calManager = GMCalibrationsManager(GMAudioApp.appContext)
        val lockOut = calManager.getInteger(GIS502_TUNERHMI.VideoLockOut_CalID)
        if (lockOut == mCurrentDrivingMode)
            showMediaMetaData()
    }

    class UIHandler constructor(val fragment: UsbNowPlayingFragment) : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            when (msg!!.what) {
                fragment.msgMakeVideoFullScreen -> fragment.showFullScreenVideo()
            }
        }
    }
}