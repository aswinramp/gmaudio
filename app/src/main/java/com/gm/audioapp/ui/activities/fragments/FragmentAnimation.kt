package com.gm.audioapp.ui.activities.fragments

/**
 * Implement this interface to define entry and exit animations for fragment.
 */
interface FragmentAnimation{

    /**
     * Animate content of the fragment while displaying
     */
    fun entryAnimation()

    /**
     * Animate contents of the fragment while disappearing
     */
    fun exitAnimation()

}