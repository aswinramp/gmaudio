package com.gm.audioapp.ui.activities.fragments

object FragmentPoolManager {

    private val fragmentPool = LinkedHashMap<String, BaseFragment>()

    fun add(vararg fragments: BaseFragment) {
        fragments.forEach {
            fragmentPool[it::class.java.simpleName] = it
        }
    }

    fun getFragmentByTag(fragmentClassName: String?) : BaseFragment {
        return fragmentPool[fragmentClassName]!!
    }

}