package com.gm.audioapp.ui.adapters

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.graphics.Bitmap
import android.graphics.Point
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.AbsListView
import android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE
import android.widget.AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL
import android.widget.ListView
import android.widget.SectionIndexer
import android.widget.TextView
import com.gm.audioapp.BR
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.databinding.UsbListRowBinding
import com.gm.audioapp.databinding.UsbNowPlayingDataBinding
import com.gm.audioapp.ui.customclasses.MediaStringMatcher
import com.gm.audioapp.viewmodels.EventHandler
import com.gm.media.models.DataPoolDataHandler
import com.gm.media.models.NONUIVariables
import com.gm.media.utils.Log
import gm.media.interfaces.IAlphaJumpItem
import gm.media.interfaces.IBrowseItem
import gm.media.interfaces.IDevice
import gm.media.utilities.CHybridAdapter
import gm.media.utilities.MediaRunnable
import java.util.concurrent.CopyOnWriteArrayList
import com.gm.audioapp.R
import com.gm.audioapp.viewmodels.Utility
import com.gm.media.apiintegration.SystemListener
import com.gm.media.models.MediaAlbumArtInfo_t

class GmUsbBrowseAdapter(private val isFromUsbNowPlaying: Boolean) : CHybridAdapter(), SectionIndexer {

    private val mTAG = GmUsbBrowseAdapter::class.simpleName

    private val mBubbleDuration = 2000L
    private val itemClickHandler = EventHandler

    private var mAlphaJumpItems: Array<out IAlphaJumpItem>? = null
    private var mAlphaJumpSections: Array<Any?>? = null
    var mBubble: TextView? = null
    var mListView: ListView? = null
    private var sectionSize: Int = 0
    private var mScreenWidth: Int = 0

    override fun onAlphaJumpChange(Address: LongArray?, JumpKeys: Array<out IAlphaJumpItem>): Boolean {
        if (CompareAddress(Address)) {
            Log.d(mTAG!!, "onAlphaJumpChange: updating with " + JumpKeys.size + " keys")
            mAlphaJumpItems = JumpKeys
            sectionSize = mAlphaJumpItems!!.size
            mAlphaJumpSections = arrayOfNulls(mAlphaJumpItems!!.size)
            for (iLoop in 0 until (mAlphaJumpItems!!.size)) {
                mAlphaJumpSections!![iLoop] = mAlphaJumpItems!![iLoop].key
            }
            return true
        } else {
            //Log.d(TAG, "onAlphaJumpChange: set to null")
            mAlphaJumpItems = null
            mAlphaJumpSections = null
            sectionSize = 0
            return false
        }
    }


    override fun getSections(): Array<Any?>? {
        return mAlphaJumpSections
    }

    override fun getSectionForPosition(position: Int): Int {
        if (mAlphaJumpItems != null) {
            for (iLoop in 0 until mAlphaJumpItems!!.size) {
                if (mAlphaJumpItems!![iLoop].index > position) {
                    return iLoop - 1
                } else if (mAlphaJumpItems!![iLoop].index.toInt() == position) {
                    return iLoop
                }
            }
            return mAlphaJumpItems!!.size - 1
        } else {
            return 0
        }
    }

    override fun getPositionForSection(sectionIndex: Int): Int {
        if ((mAlphaJumpItems != null) && (sectionIndex > 0)
                && (sectionIndex < mAlphaJumpItems!!.size)) {
            return (mAlphaJumpItems!![sectionIndex].index).toInt()
        }
        return 0
    }


    override fun onErrorUnexpected(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorUnexpected")
    }

    override fun onErrorConnectionRefused(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorConnectionRefused")
    }

    override fun onActiveTrackIndexChange(p0: Int) {
        Log.e(mTAG, "onActiveTrackIndexChange$p0")
    }

    override fun onErrorNotConnected(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorNotConnected")
    }

    override fun onErrorImpossible(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorImpossible")
        NONUIVariables.getInstance().IS_MEDIA_BROWSE_LISTVIEW_VISIBLE = false
        DataPoolDataHandler.IS_MEDIA_BROWSE_NO_CONTENT_VISIBLE.set(true)
        DataPoolDataHandler.HIDE_AZ_BTN.set(true)
    }

    override fun onErrorOperationProhibited(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorOperationProhibited")
    }

    override fun onErrorTimeout(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorTimeout")
    }

    override fun onErrorOperationFailed(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorOperationFailed")
    }

    override fun onErrorConnectionFailed(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorConnectionFailed")
    }

    override fun onErrorFailed(p0: MediaRunnable?) {
        Log.e(mTAG, "onErrorFailed")
    }

    override fun onListDataValidChange() {
        Log.e(mTAG, "onListDataValidChange")
        NONUIVariables.getInstance().IS_MEDIA_BROWSE_LISTVIEW_VISIBLE = true
        DataPoolDataHandler.IS_MEDIA_BROWSE_NO_CONTENT_VISIBLE.set(false)
        if (count > 0) DataPoolDataHandler.HIDE_AZ_BTN.set(false)
        Utility.setUsbTitle(NONUIVariables.getInstance().ACTIVE_MEDIA_ADDRESS, MediaStringMatcher.getStringBYId(R.string.browse_st))
    }


    private var mInflater: LayoutInflater? = null

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        if (mInflater == null)
            mInflater = p2?.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val binding: ViewDataBinding?
        if (!isFromUsbNowPlaying) {
            binding = if (p1 == null)
                DataBindingUtil.inflate(mInflater!!, R.layout.uil_audio_row_list_usb_browse_list_view_sdk, p2, false)
            else
                DataBindingUtil.getBinding<UsbListRowBinding>(p1)//getBinding<ListRowBinding>(p1)

            setPresetCardBackground(p0, binding!!.root.findViewById(R.id.preset_card))
        } else {
            binding = if (p1 == null)
                DataBindingUtil.inflate(mInflater!!, R.layout.uil_usb_sdk_list_item, p2, false)
            else
                DataBindingUtil.getBinding<UsbNowPlayingDataBinding>(p1)

        }

        if (null != getItem(p0)) {
            var isVisible = false
            val item = getItem(p0) as IBrowseItem
            if (isFromUsbNowPlaying) {
                val iMetadata = NONUIVariables.getInstance().USB_MEDIA_OBJECT_INFO
                if (iMetadata != null && iMetadata.metaDataSong == item.displayText && iMetadata.metaDataArtist == item.itemArtist) {
                    isVisible = true
                    if (null != item.albumArtImage) {
                        SystemListener.onMEDIA_RES_MEDIAOBJECTALBUMARTINFO(MediaAlbumArtInfo_t("",item.albumArtImage,byteSizeOf(item.albumArtImage)))
                    }
                }
            }

            binding!!.setVariable(BR.obj, item)
            binding.setVariable(BR.position, p0)
            binding.setVariable(BR.isVisible, isVisible)
            binding.setVariable(BR.clickHandler, itemClickHandler)
            binding.setVariable(BR.dataPoolHandler, DataPoolDataHandler)

        }
        return binding!!.root
    }

    public override fun setActiveDevice(_activeDevice: IDevice?) {
        super.setActiveDevice(_activeDevice)
    }

    public override fun getArrayListForAddress(p0: LongArray?): CopyOnWriteArrayList<IBrowseItem> {
        return super.getArrayListForAddress(p0)
    }

    public override fun setActiveAddress(p0: LongArray?) {
        super.setActiveAddress(p0)
    }

    override fun notifyDataSetChanged() {
        super.notifyDataSetChanged()
        Log.e(mTAG, "notifyDataSetChanged-size  $count")
    }

    /**
     * Sets the appropriate background on the card containing the media information. The cards
     * need to have rounded corners depending on its position in the list and the number of items
     * in the list.
     * @param position item position
     */
    private fun setPresetCardBackground(position: Int, mPresetsCard: View) {
        val itemCount = count

        if (mScreenWidth <= 0) {
            val windowManager = GMAudioApp.appContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val display = windowManager.defaultDisplay!!
            val size = Point()
            display.getRealSize(size)
            mScreenWidth = size.x
            mScreenWidth = if (DataPoolDataHandler.isInfoCurvedView.get()!!) mScreenWidth / 6 else mScreenWidth / 2
        }
        mPresetsCard.layoutParams.width = mScreenWidth

        when {
            itemCount == 1 -> // One card - all corners are rounded
                mPresetsCard.setBackgroundResource(R.drawable.preset_item_card_rounded_bg)
            position == 0 -> // First card gets rounded top
                mPresetsCard.setBackgroundResource(R.drawable.preset_item_card_rounded_top_bg)
            position == itemCount - 1 -> // Last one has a rounded bottom
                mPresetsCard.setBackgroundResource(R.drawable.preset_item_card_rounded_bottom_bg)
            else -> // Middle have no rounded corners
            {
                val a = GMAudioApp.appContext.obtainStyledAttributes(intArrayOf(R.attr.gm_car_card_color))
                val cardColor: Int
                try {
                    cardColor = if (GMAudioApp.useCadillacTheme!!)
                        GMAudioApp.appContext.getColor(R.color.car_card_light)
                    else
                        GMAudioApp.appContext.getColor(R.color.car_card_dark)

                } finally {
                    a.recycle()
                }
                mPresetsCard.setBackgroundColor(cardColor)
            }
        }
    }

    fun setScrollListener(mListView: ListView?) {
        mListView?.setOnScrollListener(object : AbsListView.OnScrollListener {
            override fun onScroll(p0: AbsListView?, p1: Int, p2: Int, p3: Int) {
                val sectionPosition = getSectionForPosition(p1)
                Log.e(mTAG, "onScroll   $p1  ---  sectionSize  $sectionSize   ----sectionPosition---   $sectionPosition")
                if (sectionSize > 0 && sectionPosition > -1) {
                    val msg = mAlphaJumpSections!![sectionPosition]
                    if (msg != null)
                        mBubble?.text = msg as String
                }
            }

            override fun onScrollStateChanged(p0: AbsListView?, p1: Int) {
                Log.e(mTAG, "onScrollStateChanged    $p1")
                if (sectionSize > 0) {
                    if (p1 == SCROLL_STATE_TOUCH_SCROLL) {
                        Log.e(mTAG, "clearAnimation")
                        AnimationListener.isCancel = true
                        mBubble?.clearAnimation()
                        mBubble?.visibility = View.VISIBLE
                    }
                    if (p1 == SCROLL_STATE_IDLE)
                        fadeOutAnimation(mBubble, mBubbleDuration)
                }
            }
        })
    }

    fun showBubbleOnSectionJump() {
        mBubble?.visibility = View.VISIBLE
        fadeOutAnimation(mBubble, mBubbleDuration)
    }

    fun fadeOutAnimation(view: View?, duration: Long) {
        val alphaAnimation = AlphaAnimation(1f, 0f)
        alphaAnimation.duration = duration
        view?.startAnimation(alphaAnimation)
        AnimationListener.view = view
        AnimationListener.isCancel = false
        alphaAnimation.setAnimationListener(AnimationListener)
    }

    @SuppressLint("StaticFieldLeak")
    object AnimationListener : Animation.AnimationListener {
        private val mTAG = AnimationListener::class.simpleName
        var isCancel: Boolean = false
        var view: View? = null
        override fun onAnimationEnd(p0: Animation?) {
            Log.e(mTAG, "onAnimationEnd")
            if (!isCancel) view?.visibility = View.GONE
            isCancel = false
        }

        override fun onAnimationStart(p0: Animation?) {
        }

        override fun onAnimationRepeat(p0: Animation?) {
        }
    }

fun byteSizeOf(bitmap: Bitmap): Int{
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        return bitmap.getAllocationByteCount();
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
        return bitmap.getByteCount();
    } else {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }
}





}