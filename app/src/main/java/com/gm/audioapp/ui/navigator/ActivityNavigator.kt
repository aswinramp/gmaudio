package com.gm.audioapp.ui.navigator

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import com.gm.audioapp.GMAudioApp
import com.gm.audioapp.GMAudioApp.Companion.fragmentMgr
import com.gm.audioapp.ui.activities.BaseActivity
import com.gm.audioapp.ui.activities.fragments.BaseFragment

/**
 * Class used for navigating to other Activity or fragment
 */
open class ActivityNavigator : Navigator {

    /**
     * starts activity corresponding to given eventName
     *
     * @param eventName it identifies the activity in 'eventTable' table
     * @param any  additional data that is passed to the activity
     */
    fun triggerActivity(eventName: String, any: Any?) {
        startActivity(ScreenMapper.eventTable[eventName]!!, Bundle())
    }

    /**
     * add/replaces fragment corresponding to given eventName
     *
     * @param eventName it identifies the fragment in 'eventFragmentTable' table
     * @param any  additional data that is passed to the fragment
     */
    fun triggerFragment(eventName: String, any: Any?){
        val fragmentName = ScreenMapper.eventFragmentTable[eventName]
        val fragment = fragmentMgr.getFragmentByTag(fragmentName)
        /*when (eventName) {
            "onAMFM_REQ_AMSTATIONLIST", "onAMFM_REQ_FMSTATIONLIST" -> {

                NowPlayingFragment.getInstance()
            }
            else -> Fragment.instantiate(activityContext, fragmentName)
        }*/
        replaceFragment(GMAudioApp.appContext.activityContext.getContainerId(), fragment, Bundle())
    }

    //clear all the history of fragments
    fun clearBackStack() {
        for (i in 0 until GMAudioApp.appContext.activityContext.supportFragmentManager.backStackEntryCount) {
            GMAudioApp.appContext.activityContext.supportFragmentManager.popBackStack()
        }
    }

    override fun finishActivity() {
        GMAudioApp.appContext.themeChange.recreate()
        GMAudioApp.appContext.activityContext.finish()
    }

    override fun recreateActivity() {
        GMAudioApp.appContext.themeChange.recreate()
    }

    override fun startActivity(intent: Intent) {
        GMAudioApp.appContext.activityContext.startActivity(intent)
    }

    override fun startActivity(action: String) {
        val intent = Intent(action)
        startActivity(intent)
    }

    override fun startActivity(action: String, uri: Uri) {}

    override fun startActivity(action: String, args: Bundle) {
        val intent = Intent(action)
        intent.putExtras(args)
        startActivity(intent)
    }

    override fun startActivity(activityClass: Class<out BaseActivity>) {
        startActivity(Intent(GMAudioApp.appContext.activityContext, activityClass))
    }

    override fun startActivity(activityClass: Class<out BaseActivity>, args: Bundle) {}

    override fun startActivity(activityClass: Class<out BaseActivity>, args: Parcelable) {}

    override fun startActivity(activityClass: Class<out BaseActivity>, arg: String) {}

    override fun startActivity(activityClass: Class<out BaseActivity>, arg: Int) {}

    override fun startActivityForResult(activityClass: Class<out BaseActivity>, requestCode: Int) {}


    override fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: Parcelable, requestCode: Int) {}

    override fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: String, requestCode: Int) {}

    override fun startActivityForResult(activityClass: Class<out BaseActivity>, arg: Int, requestCode: Int) {}

    override fun replaceFragment(@IdRes containerId: Int, fragment: BaseFragment, args: Bundle) {
        fragment.getFragmentTransaction(containerId, fragment)
    }

    override fun replaceFragment(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle) {}

    override fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, args: Bundle, backstackTag: String) {}

    override fun replaceFragmentAndAddToBackStack(@IdRes containerId: Int, fragment: Fragment, fragmentTag: String, args: Bundle, backstackTag: String) {}

}
